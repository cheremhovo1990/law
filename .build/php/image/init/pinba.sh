#!/usr/bin/env sh

if [ "$PINBA_ENABLED" = "yes" ]; then
    echo "\033[0;33m🛠 Enable Pinba\033[0m"
else
    echo "\033[0;33m🛠 Disabling Pinba\033[0m"
    rm -f /usr/local/etc/php/conf.d/pinba.ini
fi
