RED=\e[31m
YELLOW=\e[33m
GREEN=\e[32m
END=\e[0m

SHELL:=/bin/bash
DOCKER_COMPOSE=docker-compose
PHP=$(DOCKER_COMPOSE) exec -T php
PHP_CONSOLE=$(PHP) bin/console
APP_ENV?=dev
APP_DEBUG?=on
YARN=$(DOCKER_COMPOSE) exec -T node yarn
COMPOSER=$(PHP)
ifeq (on, $(KUBE_ENABLE))
	APP_ENV=prod
	PHP=microk8s kubectl exec -it pod/law-pod -c php-fpm --
	PHP_CONSOLE=$(PHP) bin/console
	YARN=docker run --env APP_ENV=$(APP_ENV) --tty --user 1000:1000 --rm --volume $(PWD):/app --workdir /app node:18-bullseye yarn
	COMPOSER=docker run --env APP_ENV=$(APP_ENV) --tty --user 1000:1000 --rm --volume $(PWD):/app --workdir /app cheremhovo/php:law
endif
PHP_DEBUG=$(PHP)-debug

ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

echo-test:
	@echo -e "$(RED)==> Run $@ $(END)"
	@true

up: cp-override .env.local docker-up vendor migrate fixture yarn-install-$(APP_ENV)
	@echo -e "$(RED)==> Run $@ $(END)"
	@true

prod-up: .env.local vendor prod migrate yarn-install-$(APP_ENV) rm-cache
	@echo -e "$(RED)==> Run $@ $(END)"
	$(PHP_CONSOLE) app:create:sitemap

docker-up:
	@echo -e "$(RED)==> Run $@ $(END)"
	USER_ID=$(shell id -u) \
	GROUP_ID=$(shell id -g) \
	$(DOCKER_COMPOSE) \
		up \
		-d \
		--force-recreate \
		--quiet-pull \
		$(DOCKER_COMPOSE_UP_OPTS)

down:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(DOCKER_COMPOSE) down --remove-orphans

.PHONY: vendor
vendor:
	@echo -e "$(red)==> Run $@ $(end)"
	$(COMPOSER) composer \
		install \
		--no-interaction \
		--optimize-autoloader \
		--prefer-dist \
		--no-progress \
		$(COMPOSER_INSTALL_OPTS)

.PHONY: .env.local
.env.local:
	@echo "$(red)==> Run $@ $(end)"
	cp .env.local.dist .env.local
	echo "APP_ENV=$(APP_ENV)" >> .env.local
	echo "APP_DEBUG=$(APP_DEBUG)" >> .env.local

fixture:
	@echo -e "$(red)==> Run $@ $(end)"
	$(PHP_CONSOLE) doctrine:fixture:load --no-interaction

migrate:
	@echo -e "$(red)==> Run $@ $(end)"
	$(PHP_CONSOLE) doctrine:database:create --if-not-exists
	$(PHP_CONSOLE) doctrine:migrations:migrate --no-interaction

db-fill-remote:
	ssh ten-laws@ten-laws.ru "cd /var/www/ten-laws && docker-compose exec -T postgres pg_dump --clean --create --if-exists --username=ten-laws ten-laws > /tmp/ten-laws.sql"
	rsync --verbose --checksum --archive --compress --rsh="ssh" ten-laws@ten-laws.ru:/tmp/ten-laws.sql ./var
	cat ./var/ten-laws.sql | docker-compose exec -T postgres psql --username=ten-laws

schema-validate:
	@echo -e "$(red)==> Run $@ $(end)"
	$(PHP_CONSOLE) doctrine:schema:validate

fixer: php-fixer js-fixer
	@echo -e "$(RED)==> Run $@ $(END)"
	@true

js-fixer:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(YARN) run eslint --fix --ext .js,.vue assets

php-fixer:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(PHP) vendor/bin/php-cs-fixer fix

phpunit:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(PHP_CONSOLE) doctrine:database:create --if-not-exists  --env=test
	$(PHP_CONSOLE) doctrine:migrations:migrate  --no-interaction --env=test
	$(PHP_CONSOLE) doctrine:fixtures:load  --no-interaction --env=test
	$(PHP) ./vendor/bin/phpunit $(PHPUNIT_OPTS)

phpunit-coverage:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(DOCKER_COMPOSE) exec \
		-e XDEBUG_MODE=coverage \
		-T \
		php-debug ./vendor/bin/phpunit \
			--coverage-html \
			./var/coverage

rsync-coverage:
	@echo -e "$(RED)==> Run $@ $(END)"
	rsync --compress --delete --delete --progress --recursive $(PWD)/var/coverage cheremhovo1990@ten-laws.ru:/home/cheremhovo1990/www/law/var

phpstan:
	@echo -e "$(RED)==> Run $@ $(END)"
	@if [ $(APP_ENV) = 'test' ]; then \
  		sed -i 's#var/cache/dev/App_KernelDevDebugContainer.xml#var/cache/test/App_KernelTestDebugContainer.xml#g' ./phpstan.neon; \
  	fi
	$(PHP) vendor/bin/phpstan analyse --ansi

php-cs-check:
	@echo -e "$(red)==> Run $@ $(end)"
	$(PHP) vendor/bin/php-cs-fixer fix --dry-run --using-cache=no -v

cp-override:
	@echo -e "$(RED)==> Run $@ $(END)"
	cp docker-compose.override.$(APP_ENV).yml docker-compose.override.yml

rm-cache:
	@echo -e "$(RED)==> Run $@ $(END)"
	rm -r var/cache

yarn-install-prod:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(YARN) install
	$(YARN) encore production

yarn-install-dev:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(YARN) install
	$(YARN) encore dev

yarn-watch:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(YARN) encore dev --watch

js-routing:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(PHP_CONSOLE) fos:js-routing:dump --target=./assets/js/router.json --format=json

xdebug: ## активировать xdebug для web запросов
	@echo -e "$(RED)==> Run $@ $(END)"
	@([ -f $(ROOT_DIR)/var/.web-xdebug ] && echo -e "$(YELLOW)xdebug for web requests already enabled$(END)") || \
		(touch $(ROOT_DIR)/var/.web-xdebug && echo -e "xdebug for web requests $(GREEN)enabled$(END)")

xdebug-stop: ## де-активировать xdebug для web запросов
	@echo -e "$(RED)==> Run $@ $(END)"
	@([ ! -f $(ROOT_DIR)/var/.web-xdebug ] && echo -e "$(YELLOW)xdebug for web requests is not enabled$(END)") || \
		(rm -f $(ROOT_DIR)/var/.web-xdebug && echo -e "xdebug for web requests $(GREEN)disabled$(END)")

db-dump:
	@echo -e "$(RED)==> Run $@ $(END)"
	mkdir --parents $(ROOT_DIR)/var/backup && \
	export PGPASSWORD=ten-laws && \
	pg_dump \
		--host=192.168.221.5 \
		--port=5432 \
		--username=ten-laws \
		--if-exists \
		--clean \
		--schema=public \
		--dbname=ten-laws \
		| gzip > $(ROOT_DIR)/var/backup/ten-laws_$(shell date "+%Y-%m-%d").sql.gz

db-fill:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(PHP_CONSOLE) doctrine:database:drop --force
	$(PHP_CONSOLE) doctrine:database:create --if-not-exists
	export PGPASSWORD=ten-laws && \
	zcat $(ROOT_DIR)/var/backup/ten-laws_$(shell date "+%Y-%m-%d").sql.gz \
	| psql \
		--host=192.168.221.5 \
		--port=5432 \
		--username=ten-laws \
		--dbname=ten-laws

dev:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(PHP) composer dump-env dev
	$(PHP_CONSOLE) cache:clear

prod:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(PHP) composer dump-env prod
	$(PHP_CONSOLE) cache:clear

test:
	@echo -e "$(RED)==> Run $@ $(END)"
	$(PHP) composer dump-env test
	$(PHP_CONSOLE) cache:clear

git-checkout:
	@echo -e "$(RED)==> Run $@ $(END)"
	git fetch && git checkout -f -B master origin/master

apply-kube: vendor yarn-install-prod
	@echo -e "$(RED)==> Run $@ $(END)"
	microk8s kubectl apply -f kube-pod.yaml
	microk8s kubectl apply -f kube-service.yaml
	microk8s kubectl apply -f kube-xdebug-pod.yaml
	microk8s kubectl apply -f kube-xdebug-service.yaml
	microk8s kubectl apply -f kube-storybook-pod.yaml
	microk8s kubectl apply -f kube-storybook-service.yaml
	microk8s kubectl apply -f kube-swagger-pod.yaml
	microk8s kubectl apply -f kube-swagger-service.yaml

delete-kube:
	@echo -e "$(RED)==> Run $@ $(END)"
	microk8s kubectl delete -f kube-service.yaml
	microk8s kubectl delete -f kube-pod.yaml
	microk8s kubectl delete -f kube-xdebug-service.yaml
	microk8s kubectl delete -f kube-xdebug-pod.yaml
	microk8s kubectl delete -f kube-storybook-service.yaml
	microk8s kubectl delete -f kube-storybook-pod.yaml
	microk8s kubectl delete -f kube-swagger-service.yaml
	microk8s kubectl delete -f kube-swagger-pod.yaml

docker-build:
	@echo -e "$(RED)==> Run $@ $(END)"
	docker build --no-cache --tag cheremhovo/php:law  --build-arg USER_ID=1000 --build-arg GROUP_ID=1000 .build/php

docker-push:
	@echo -e "$(RED)==> Run $@ $(END)"
	docker push cheremhovo/php:law