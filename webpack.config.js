const Encore = require('@symfony/webpack-encore');
const path = require('path');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')
    .enableVueLoader(() => {}, { runtimeCompilerBuild: false })
    .enableVersioning()
    /*
     * ENTRY CONFIG
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('app', './assets/app.js')
    .addEntry('main.pdf', './assets/main.pdf.js')
    .addEntry('cp', './assets/cp.js')
    .addEntry('vue.main', './assets/vue/main.js')
    .addEntry('vue.descriptionSin', './assets/vue/descriptionSin.js')
    .addEntry('vue.books', './assets/vue/books')
    .addEntry('vue.book', './assets/vue/book')
    .addEntry('vue.cp.precepts', './assets/vue/cp.precepts')
    .addEntry('vue.cp.books', './assets/vue/cp.books')
    .addEntry('vue.cp.descriptionSins', './assets/vue/cp.descriptionSins')
    .addEntry('vue.cp.dictionaries', './assets/vue/cp.dictionaries')
    .addEntry('vue.cp.meta', './assets/vue/cp.meta')
    .addEntry('vue.cp.video', './assets/vue/cp.video')
    .addEntry('vue.cp.music', './assets/vue/cp.music')
    .addEntry('vue.cp.sins', './assets/vue/cp.sins')
    .addEntry('vue.cp.pages', './assets/vue/cp.pages')
    .addEntry('vue.cp.schedule.month', './assets/vue/cp.schedule.month')
    .addEntry('vue.cp.schedule.week', './assets/vue/cp.schedule.week')
    .addEntry('vue.cp.form.sin', './assets/vue/cp.form.sin')
    .addEntry('vue.cp.form.precept', './assets/vue/cp.form.precept')
    .addEntry('vue.cp.form.meta', './assets/vue/cp.form.meta')
    .addEntry('vue.cp.form.video', './assets/vue/cp.form.video')
    .addEntry('vue.cp.form.music', './assets/vue/cp.form.music')
    .addEntry('vue.cp.form.description.sin', './assets/vue/cp.form.description.sin')
    .addEntry('vue.cp.form.book', './assets/vue/cp.form.book')
    .addEntry('vue.cp.form.dictionary', './assets/vue/cp.form.dictionary')

    // enables the Symfony UX Stimulus bridge (used in assets/bootstrap.js)
    .enableStimulusBridge('./assets/controllers.json')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    }).configureDefinePlugin(function (options) {
        let env = require('dotenv').config().parsed;
        let appEnv = process.env['APP_ENV'];
        let addEnv = require('dotenv').config({path: '.env.dev'}).parsed;
        if (appEnv === 'prod') {
            addEnv = require('dotenv').config({path: '.env.prod'}).parsed;
        }

        env = {...env, ...addEnv};
        for (let key in env) {
            options['process.env.' + key] = JSON.stringify(env[key]);
        }
        options['__VUE_OPTIONS_API__'] = true;
        options['__VUE_PROD_DEVTOOLS__'] = true;
        options['process.env.STORYBOOK_ENABLE'] = JSON.stringify('off');
    })
    // enables Sass/SCSS support
    .enableSassLoader()

    .addAliases({
        '@Js': path.resolve(__dirname, 'assets/js'),
        '@Vue': path.resolve(__dirname, 'assets/vue'),
        '@Assets': path.resolve(__dirname, 'assets'),
    })
    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you use React
    //.enableReactPreset()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    .enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    // .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
