/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
// js
import '@Js/main'
import '@Js/form'
import ready  from '@Js/ready';
document.addEventListener('DOMContentLoaded', function () {
    ready();
})
import 'bootstrap/dist/js/bootstrap';
// css
import '@Assets/styles/font.css'
import 'bootstrap/scss/bootstrap.scss'
import 'toastr/toastr.scss';
import '@Assets/styles/ckeditor-content-styles.scss';
import '@Assets/styles/main.scss';
import '@Assets/styles/app.scss';

