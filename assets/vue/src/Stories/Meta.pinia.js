import {watch, ref} from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore('meta', {
    state: () => {
        const title = ref();
        watch(title, function (value) {
            let title = document.querySelector('title');
            title.innerText = value;
        })
        const description = ref();
        watch(description, function (value) {
            let description = document.querySelector('meta[name="description"]');
            description.setAttribute('content', value);
        })
        return {
            title,
            description
        }
    },
    actions: {
        setTitle(title) {
            this.title = title;
        },
        setDescription(description) {
            this.description = description;
        }
    }
});
