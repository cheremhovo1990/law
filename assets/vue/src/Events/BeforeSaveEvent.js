export default class BeforeSaveEvent extends Event {
    static #nameEvent = 'on-before-save';
    constructor(vue, EventInit) {
        let type = BeforeSaveEvent.getNameEvent(vue);
        super(type, EventInit);
        this.vue = vue;
    }
    static getNameEvent(vue) {
        return this.#nameEvent + vue.getUuid();
    }
}