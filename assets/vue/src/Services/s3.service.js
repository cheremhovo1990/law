export default class S3Service {
    bucket = process.env.AWS_BUCKET;
    prefix = 'books';
    getUrl (path) {
        return 'https://storage.yandexcloud.net/' + this.bucket + '/' + this.prefix + '/' + path;
    }
}