import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export default class SettingRepositoryService {
    static instance = undefined;
    settings = undefined;
    constructor(settings) {
        this.settings = settings;
    }
    static async create() {
        if (SettingRepositoryService.instance === undefined) {
            let requestSettings = FactoryRequest.create(new RouteCpService().generateSettings());
            let settings = await fetch(requestSettings).then(function (res) {
                return res.json();
            })
            console.debug({'method': 'SettingRepositoryService::create', message: 'load setting', settings: settings})
            SettingRepositoryService.instance = new SettingRepositoryService(settings);
        }
        return SettingRepositoryService.instance;
    }

    static createOptions(options, placeholder = false) {
        let result = [];
        if (placeholder !== false) {
            if (placeholder === true) {
                result.push({value: '', text: ''})
            }
        }

        for (let key in options) {
            result.push({value: key, text: options[key]})
        }

        return result;
    }
    getMetaKeyOptions() {
        return this.settings.meta.key.options
    }

    getBookLanguageOfThePublicationOptions() {
        return this.settings.book.languageOfThePublication.options
    }

    getBookLinksOptions() {
        return this.settings.book.links.options
    }
}
