export class RequestService {
    static instance = undefined;
    constructor() {

    }
    static create() {
        if (RequestService.instance === undefined) {
            RequestService.instance = new RequestService();
        }
        return RequestService.instance;
    }

    requestUri() {
        return window.location.pathname;
    }

    contains(pattern) {
        let result = this.requestUri().search(pattern)
        return result !== -1;
    }
}