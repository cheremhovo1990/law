export default class FactoryRequest {
    static create(url) {
        let header = new Headers();
        header.append('accept', 'application/json');
        return new Request(url, {
            headers: header
        });
    }
}