export class PaginatorService {
    paginator = undefined;
    constructor(paginator) {
        this.paginator = paginator;
    }

    getPage() {
        return this.paginator.page;
    }

    getMaxPage() {
        return this.paginator.maxPage;
    }

    getPrevPage(number) {
        return this.paginator.page - number;
    }

    getNextPage(number) {
        return this.paginator.page + number;
    }

    haveNextPage(number) {
        return (this.paginator.page + number) <= this.paginator.maxPage;
    }

    havePrevPage(number) {
        return (this.paginator.page - number) < this.paginator.page && (this.paginator.page - number) >= 1;
    }

    getLimit() {
        console.log(this.paginator.limit);
        return this.paginator.limit;
    }
}