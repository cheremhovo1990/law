
export function getUrl (uri) {
	if (process.env.STORYBOOK_ENABLE === 'on') {
		return process.env.VUE_APP_HOST + uri;
	} else {
		return window.location.protocol + '//' + window.location.host + uri;
	}
}