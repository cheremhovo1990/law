import {createPinia} from 'pinia';

export default class Pinia {
    static staticProperty
    static isUseOne = false;
    static create() {
        if (Pinia.staticProperty === undefined) {
            console.debug('create pinia')
            Pinia.staticProperty = createPinia();
        }
        return Pinia.staticProperty;
    }
    static useOne(app) {
        if (!Pinia.isUseOne) {
            app.use(Pinia.create())
            Pinia.isUseOne = true;
        }
    }
}