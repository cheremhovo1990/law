import RouteFrontCpService from '@Js/services/RouteFrontCpService';
import RouteCpService from '@Js/services/RouteCpService';
import lodash from 'lodash';

export default class Page {
    constructor(data) {
        this.id = data.id;
        this.title = data.title;
        this.content = data.content;
        this.publishedAt = data.publishedAt;
        if (lodash.isArray(data.children) && data.children.length) {
            this.children = data.children.map(function (item) {
                return new Page(item);
            })
        }
    }

    editCpUrl() {
        return new RouteFrontCpService().generatePageEdit(this.id);
    }
    delete() {
        return new RouteCpService().generatePageDelete(this.id);
    }
}
