import lodash from 'lodash';
import Precept from '@Vue/src/Entities/Precept';
import RouteFrontCpService from '@Js/services/RouteFrontCpService';
import RouteCpService from '@Js/services/RouteCpService';

export default class DescriptionSin {
	constructor(data) {
		this.id = data.id;
		this.name = data.name;
		this.text = data.text;
		this.meta = data.meta;
		if (!lodash.isNil(data.precept)) {
			this.precept = new Precept(data.precept);
		}
	}

	editCpUrl() {
		return new RouteFrontCpService().generateDescriptionSinEdit(this.id);
	}

    delete() {
        return new RouteCpService().generateDescriptionSinDelete(this.id)
    }
}
