import S3Service from '@Vue/src/Services/s3.service';
import Image from '@Vue/src/Entities/Image';
import moment from 'moment';
import lodash from 'lodash';
import RouteFrontCpService from '@Js/services/RouteFrontCpService';
import RouteFrontService from '@Js/services/RouteFrontService';
import RouteCpService from '@Js/services/RouteCpService';

export default class Book {
	constructor(data) {
		this.name = data.name;
		this.coverImage = new Image(data.coverImage);
		if (!lodash.isNil(data.dateOfWriting)) {
			this.dateOfWriting = moment(data.dateOfWriting);
		}
		if (!lodash.isNil(data.dateOfPublished)) {
			this.dateOfPublished = moment(data.dateOfPublished);
		}
		this.pageSize = data.pageSize;
		this.description = data.description;
		this.id = data.id;
		this.isbnCollection = data.isbnCollection;
		this.meta = data.meta;
		this.links = data.links;
		this.firstLinkBuy = data.firstLinkBuy;
		this.publish = data.publish;
		this.languageOfThePublication = data.languageOfThePublication
		this.blessedByPatriarchKirill = data.blessedByPatriarchKirill;
		this.recommendedByPublishingCouncilRoc = data.recommendedByPublishingCouncilRoc;
		this.recommendedByPublishingCouncilBoc = data.recommendedByPublishingCouncilBoc;
		this.codeOfPublishingCouncil = data.codeOfPublishingCouncil;
	}
	image() {
		return new S3Service().getUrl(this.coverImage.name);
	}
	url() {
		return new RouteFrontService().generateBook(this.id);
	}
	editCpUrl() {
		return new RouteFrontCpService().generateBookEdit(this.id);
	}
    delete() {
        return new RouteCpService().generateBookDelete(this.id);
    }
}
