'use strict';

import DescriptionSin from '@Vue/src/Entities/DescriptionSin';
import Precept from '@Vue/src/Entities/Precept';
import lodash from 'lodash';
import RouteFrontCpService from '@Js/services/RouteFrontCpService';
import RouteFrontService from '@Js/services/RouteFrontService';
import RouteCpService from '@Js/services/RouteCpService';

export default class Sin {
	descriptionSin = undefined;
	precept = undefined;
	constructor(data, precept) {
		this.name = data.name;
		this.id = data.id;
		this.popoverContent = data.popoverContent;
		this.position = data.position;
		if (!lodash.isNil(data.descriptionSin)) {
			this.descriptionSin = new DescriptionSin(data.descriptionSin);
		}
		if (!lodash.isNil(data.precept)) {
			this.precept = new Precept(data.precept);
		}
		if (precept instanceof Precept) {
			this.precept = precept;
		}

	}

	url () {
		return new RouteFrontService().generateDescriptionSin(this.precept.id, this.descriptionSin.id)
	}

	cpUrlEdit() {
		return new RouteFrontCpService().generateSinEdit(this.precept.id, this.id);
	}

    delete() {
        return new RouteCpService().generateSinDelete(this.id);
    }
}
