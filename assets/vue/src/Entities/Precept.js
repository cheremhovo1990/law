'use strict';
import lodash from 'lodash';

import Sin from '@Vue/src/Entities/Sin'

export default class Precept {
	constructor(data) {
		this.id = data.id;
		this.link = data.link;
		this.name = data.name;
		this.nameSlavonic = data.nameSlavonic;
		this.verse = data.verse;
		if (!lodash.isNil(data.sins)) {
			this.sins = data.sins.map(item => new Sin(item, this));
		}
	}
}