import RouteFrontCpService from '@Js/services/RouteFrontCpService';
import RouteCpService from '@Js/services/RouteCpService';

export default class Dictionary {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.description = data.description;
        this.titlo = data.titlo;
        this.normalizeName = data.normalizeName;
    }

    titloHumanRead() {
        return this.titlo ? 'Yes': 'No';
    }

    editCpUrl() {
        return new RouteFrontCpService().generateDictionaryEdit(this.id);
    }
    delete() {
        return new RouteCpService().generateDictionaryDelete(this.id);
    }
}
