import RouteFrontCpService from '@Js/services/RouteFrontCpService';
import RouteCpService from '@Js/services/RouteCpService';

export default class Music {
    constructor(data) {
        this.id = data.id;
        this.link = data.link;
        this.title = data.title;
        this.description = data.description;
        this.publishedAt = data.publishedAt;
    }

    editCpUrl() {
        return new RouteFrontCpService().generateMusicEdit(this.id);
    }
    delete() {
        return new RouteCpService().generateMusicDelete(this.id);
    }
}
