import RouteFrontCpService from '@Js/services/RouteFrontCpService';

export default class Meta {
    constructor(data) {
        this.key = data.key;
        this.title = data.title;
    }

    editCpUrl() {
        return new RouteFrontCpService().generateMetaEdit(this.key);
    }

}
