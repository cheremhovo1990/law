export default class Feedback {
    constructor(data) {
        this.id = data.id;
        this.email = data.email;
        this.firstName = data.firstName;
        this.message = data.message;
        this.phone = data.phone;
        this.theme = data.theme;
    }
}