import {defineStore} from 'pinia';
import {actions} from '@Vue/component/Form/action.pinia';
import {Variable} from '@Vue/component/Form/Variable.pinia';
import CSRFService from '@Js/services/CSRFService';

export const useStore = defineStore('form.feedback', {
	state: () => {
		return {
			email:  new Variable(''),
			phone:  new Variable(''),
			firstName:  new Variable(''),
			theme:  new Variable(''),
			message:  new Variable(''),
			'g-recaptcha-response': undefined,
		}
	},
	actions: {
		...actions,
		...{
			getBody() {
                let csrfService = new CSRFService();
				return  {
					'email': this.email.value,
					'phone': this.phone.value,
					'firstName': this.firstName.value,
					'theme': this.theme.value,
					'message': this.message.value,
					'g-recaptcha-response': this['g-recaptcha-response'],
					'_token': csrfService.getToken(),
				};
			}
		},
	},
})
