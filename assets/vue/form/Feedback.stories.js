import FormFeedback from '@Vue/form/Feedback';

let story = {
	title: 'Form/Feedback',
	component: FormFeedback
}

export default story;

const Template = (args) => ({
	// Components used in your story `template` are defined in the `components` object
	components: { FormFeedback },
	// The story's `args` need to be mapped into the template through the `setup()` method
	setup() {
		return { args };
	},
	// And then the `args` are bound to your component with `v-bind="args"`
	template: '<FormFeedback v-bind="args"></FormFeedback>',
});

export const Feedback = Template.bind({});

Feedback.args = {

}
