import {defineStore} from 'pinia';
import {actions} from '@Vue/component/Form/action.pinia';
import {Variable, Checkbox} from '@Vue/component/Form/Variable.pinia';
import lodash from 'lodash';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.form.video', {
    state: () => {
        return {
            title: new Variable(),
            link: new Variable(),
            description: new Variable(),
            publish: new Checkbox(),
            metaTitle: new Variable(),
            metaDescription: new Variable(),
        }
    },
    actions: {
        ...actions,
        async init(videoId = undefined) {
            if (!lodash.isNil(videoId)) {
                let request = FactoryRequest.create(new RouteCpService().generateVideoOne(videoId));
                let video = await fetch(request).then(function (res) {
                    return res.json();
                })
                console.debug({method: 'init', message: 'load video', video: video})
                this.title.value = video.title;
                this.link.value = video.link;
                this.description.value = video.description;
                if (!lodash.isNil(video.publishedAt)) {
                    this.publish.value = true;
                }
                this.metaTitle.value = video.meta.title;
                this.metaDescription.value = video.meta.description;
            }
        },
        getBody() {
            return  {
                title: String(this.title),
                link: String(this.link),
                description: String(this.description),
                publish: String(this.publish),
                meta: {
                    title: String(this.metaTitle),
                    description: String(this.metaDescription),
                },
            };
        },
    }
})
