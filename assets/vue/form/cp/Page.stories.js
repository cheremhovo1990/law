import FormPage from '@Vue/form/cp/Page';

let story = {
    title: 'Form/Cp/Page',
    component: FormPage
}

export default story;

const Template = (args) => ({
    // Components used in your story `template` are defined in the `components` object
    components: { FormPage },
    // The story's `args` need to be mapped into the template through the `setup()` method
    setup() {
        return { args };
    },
    // And then the `args` are bound to your component with `v-bind="args"`
    template: '<FormPage v-bind="args"></FormPage>',
});

export const Page = Template.bind({});

Page.args = {

}
