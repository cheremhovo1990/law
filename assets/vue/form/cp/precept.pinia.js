import {defineStore} from 'pinia';
import {actions} from '@Vue/component/Form/action.pinia';
import {Variable} from '@Vue/component/Form/Variable.pinia';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.form.precept', {
    state: () => {
        return {
            link: new Variable(''),
            verse: new Variable(''),
        }
    },
    actions: {
        ...actions,
        async init(preceptId) {
            let request = FactoryRequest.create(new RouteCpService().generatePrecept(preceptId));
            let precept = await fetch(request).then(function (res) {
                return res.json();
            });
            this.link.value = precept.link;
            this.verse.value = precept.verse;
        },
        getBody() {
            return  {
                link: String(this.link),
                verse: String(this.verse),
            };
        }
    }
})
