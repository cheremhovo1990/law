import {defineStore} from 'pinia';
import {actions} from '@Vue/component/Form/action.pinia';
import {Variable, Select} from '@Vue/component/Form/Variable.pinia';
import lodash from 'lodash';
import FactoryRequest from '@Vue/src/Services/Request';
import SettingRepositoryService from '@Vue/src/Services/SettingRepositoryService';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.form.meta', {
    state: () => {
        return {
            key: new Select(),
            title: new Variable(),
            description: new Variable(),
        }
    },
    actions: {
        ...actions,
        async init(key = undefined) {
            let settings = await SettingRepositoryService.create();
            this.key.options = SettingRepositoryService.createOptions(settings.getMetaKeyOptions(), true);
            if (!lodash.isNil(key)) {
                this.key.value = key;
                let requestMeta = FactoryRequest.create(new RouteCpService().generateMetaOne(key))
                let meta = await fetch(requestMeta).then(function (res) {
                    return res.json();
                })

                this.title.value = meta.title;
                this.description.value = meta.description;
            }
        },
        getBody() {
            return {
                key: String(this.key),
                title: String(this.title),
                description: String(this.description),
            };
        }
    }
})
