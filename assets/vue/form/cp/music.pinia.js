import {defineStore} from 'pinia';
import {actions} from '@Vue/component/Form/action.pinia';
import {Variable, Checkbox} from '@Vue/component/Form/Variable.pinia';
import lodash from 'lodash';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.form.music', {
    state: () => {
        return {
            title: new Variable(),
            link: new Variable(),
            description: new Variable(),
            publish: new Checkbox(),
            metaTitle: new Variable(),
            metaDescription: new Variable(),
        }
    },
    actions: {
        ...actions,
        async init(id = undefined) {
            if (!lodash.isNil(id)) {
                let request = FactoryRequest.create(new RouteCpService().generateMusicOne(id));
                let entity = await fetch(request).then(function (res) {
                    return res.json();
                })
                console.debug({method: 'init', message: 'load music', music: entity})
                this.title.value = entity.title;
                this.link.value = entity.link;
                this.description.value = entity.description;
                if (!lodash.isNil(entity.publishedAt)) {
                    this.publish.value = true;
                }
                this.metaTitle.value = entity.meta.title;
                this.metaDescription.value = entity.meta.description;
            }
        },
        getBody() {
            return  {
                title: String(this.title),
                link: String(this.link),
                description: String(this.description),
                publish: String(this.publish),
                meta: {
                    title: String(this.metaTitle),
                    description: String(this.metaDescription),
                },
            };
        },
    }
})
