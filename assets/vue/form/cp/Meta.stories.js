import FormMeta from '@Vue/form/cp/Meta';

let story = {
    title: 'Form/Cp/Meta',
    component: FormMeta
}

export default story;

const Template = (args) => ({
    // Components used in your story `template` are defined in the `components` object
    components: { FormMeta },
    // The story's `args` need to be mapped into the template through the `setup()` method
    setup() {
        return { args };
    },
    // And then the `args` are bound to your component with `v-bind="args"`
    template: '<FormMeta v-bind="args"></FormMeta>',
});

export const Meta = Template.bind({});

Meta.args = {

}
