import FormDictionary from '@Vue/form/cp/Dictionary';

let story = {
    title: 'Form/Cp/Dictionary',
    component: FormDictionary
}

export default story;

const Template = (args) => ({
    // Components used in your story `template` are defined in the `components` object
    components: { FormDictionary },
    // The story's `args` need to be mapped into the template through the `setup()` method
    setup() {
        return { args };
    },
    // And then the `args` are bound to your component with `v-bind="args"`
    template: '<FormDictionary v-bind="args"></FormDictionary>',
});

export const Dictionary = Template.bind({});

Dictionary.args = {

}
