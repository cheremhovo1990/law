import {defineStore} from 'pinia';
import {actions} from '@Vue/component/Form/action.pinia';
import {Variable, Checkbox, Select} from '@Vue/component/Form/Variable.pinia';
import lodash from 'lodash';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.form.sin', {
    state: () => {
        return {
            name: new Variable(),
            nameSlavonic: new Variable(),
            popoverContent: new Variable(),
            position: new Variable(),
            precept: new Select(),
            isPublish: new Checkbox(),
        }
    },
    actions: {
        ...actions,
        async init(preceptId = undefined, sinId = undefined) {
            let request = FactoryRequest.create(new RouteCpService().generatePrecepts());
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.precept.options = json.map(function (item) {
                return {value: item.id, text: item.name};
            });
            this.precept.options.unshift({value: '', text: ''});
            console.debug({method: 'init', message: 'store.init'}, this.precept.options);
            if (!lodash.isNil(preceptId)) {
                this.precept.value = preceptId;
            }
            if (!lodash.isNil(sinId)) {
                let request = FactoryRequest.create(new RouteCpService().generateSin(sinId));
                let sin = await fetch(request).then(function (res) {
                    return res.json();
                });
                this.name.value = sin.name;
                if (!lodash.isNil(sin.nameSlavonic)) {
                    this.nameSlavonic.value = sin.nameSlavonic;
                }
                this.popoverContent.value = sin.popoverContent;
                this.position.value = sin.position;
                if (!lodash.isNil(sin.publishedAt)) {
                    this.isPublish.value = true;
                }
            }
        },
        getBody() {
            return  {
                name: String(this.name),
                nameSlavonic: String(this.nameSlavonic),
                popoverContent: String(this.popoverContent),
                position: String(this.position),
                precept: String(this.precept),
                isPublish: String(this.isPublish),
            };
        },
    }
})
