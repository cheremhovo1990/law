import FormPrecept from '@Vue/form/cp/Precept';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';

export default function () {
    const app = createApp(FormPrecept);
    Pinia.useOne(app);
    app.mount('#vue-cp-form-precept');
}
