import {defineStore} from 'pinia';
import {actions} from '@Vue/component/Form/action.pinia';
import {Variable, Checkbox, Image, Multiple, Select} from '@Vue/component/Form/Variable.pinia';
import lodash from 'lodash';
import moment from 'moment';
import FactoryRequest from '@Vue/src/Services/Request';
import ImageEntity from '@Vue/src/Entities/Image'
import SettingRepositoryService from '@Vue/src/Services/SettingRepositoryService';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.form.book', {
    state: () => {
        return {
            name:  new Variable(''),
            links: new Multiple(),
            description: new Variable(''),
            isbnCollection: new Multiple(),
            codeOfPublishingCouncil: new Variable(''),
            dateOfWriting: new Variable(''),
            dateOfPublished: new Variable(''),
            pageSize:  new Variable(''),
            languageOfThePublication:  new Select(),
            metaTitle:   new Variable(''),
            metaDescription: new Variable(''),
            publish:  new Checkbox(false),
            blessedByPatriarchKirill:  new Checkbox(false),
            recommendedByPublishingCouncilRoc:  new Checkbox(false),
            recommendedByPublishingCouncilBoc:  new Checkbox(false),
            coverImage: new Image(),
            linkOptions: [],
        }
    },
    actions: {
        ...actions,
        async init(bookId = undefined) {
            let settings = await SettingRepositoryService.create();
            this.languageOfThePublication.options = SettingRepositoryService.createOptions(settings.getBookLanguageOfThePublicationOptions());
            this.languageOfThePublication.value = lodash.first(this.languageOfThePublication.options).value;
            this.linkOptions = SettingRepositoryService.createOptions(settings.getBookLinksOptions());
            console.log(this.linkOptions);
            if (!lodash.isNil(bookId)) {
                let request = FactoryRequest.create(new RouteCpService().generateBook(bookId))
                let book = await fetch(request).then(function (res) {
                    return res.json();
                });
                console.debug({method: 'init', message: 'load book', book: book})
                this.coverImage.value = new ImageEntity(book.coverImage);
                this.name.value = book.name;
                this.links.items = book.links;
                this.description.value = book.description;
                this.isbnCollection.items = book.isbnCollection;
                this.blessedByPatriarchKirill.value = book.blessedByPatriarchKirill
                this.recommendedByPublishingCouncilRoc.value = book.recommendedByPublishingCouncilRoc
                this.recommendedByPublishingCouncilBoc.value = book.recommendedByPublishingCouncilBoc
                this.codeOfPublishingCouncil.value = book.codeOfPublishingCouncil
                this.languageOfThePublication.value = book.languageOfThePublication
                if (!lodash.isNil(book.dateOfWriting)) {
                    this.dateOfWriting.value = moment(book.dateOfWriting).format('YYYY-MM-DD');
                }
                if (!lodash.isNil(book.dateOfPublished)) {
                    this.dateOfPublished.value = moment(book.dateOfPublished).format('YYYY-MM-DD');
                }
                if (!lodash.isNil(book.publishedAt)) {
                    this.publish.value = true;
                }
                this.pageSize.value = book.pageSize;
                this.metaTitle.value = book.meta.title;
                this.metaDescription.value = book.meta.description;
            }
        },
        getBody() {
            return  {
                name: String(this.name),
                links: this.links.toRaw().filter((value) => value.link !== ''),
                isbnCollection: this.isbnCollection.toRaw().filter((value) => value.text !== ''),
                description: String(this.description),
                blessedByPatriarchKirill: String(this.blessedByPatriarchKirill),
                recommendedByPublishingCouncilRoc: String(this.recommendedByPublishingCouncilRoc),
                recommendedByPublishingCouncilBoc: String(this.recommendedByPublishingCouncilBoc),
                languageOfThePublication: String(this.languageOfThePublication),
                dateOfWriting: String(this.dateOfWriting),
                dateOfPublished: String(this.dateOfPublished),
                pageSize: String(this.pageSize),
                publish: String(this.publish),
                codeOfPublishingCouncil: String(this.codeOfPublishingCouncil),
                coverImage: this.coverImage.value,
                meta: {
                    title: String(this.metaTitle),
                    description: String(this.metaDescription),
                },
            };
        },
    }
})
