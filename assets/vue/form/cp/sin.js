import FormSin from '@Vue/form/cp/Sin';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';

export default function () {
    const app = createApp(FormSin);
    app.use(Pinia.create());
    app.mount('#vue-cp-form-sin');
}
