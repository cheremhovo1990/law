import {defineStore} from 'pinia';

import {Checkbox, Variable} from '@Vue/component/Form/Variable.pinia';
import {actions} from '@Vue/component/Form/action.pinia';
import lodash from 'lodash';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.form.dictionary', {
    state: () => {
        return {
            name: new Variable(''),
            normalizeName: new Variable(''),
            description: new Variable(''),
            titlo: new Checkbox(false),
        }
    },
    actions: {
        ...actions,
        async init(dictionaryId = undefined) {
            if (!lodash.isNil(dictionaryId)) {
                let request = FactoryRequest.create(new RouteCpService().generateDictionary(dictionaryId))
                let dictionary = await fetch(request).then(function (res) {
                    return res.json();
                });
                console.debug({method: 'init', message: 'load dictionary', book: dictionary})
                this.name.value = dictionary.name;
                if (!lodash.isNull(dictionary.normalizeName)) {
                    this.normalizeName.value = dictionary.normalizeName;
                }
                this.description.value = dictionary.description;
                this.titlo.value = dictionary.titlo;
            }
        },
        getBody() {
            return  {
                name: String(this.name),
                normalizeName: String(this.normalizeName),
                description: String(this.description),
                titlo: String(this.titlo),
            };
        },
    }
});
