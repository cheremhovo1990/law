import FormDictionary from '@Vue/form/cp/Dictionary';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';

export default function () {
    const app = createApp(FormDictionary);
    app.use(Pinia.create());
    app.mount('#vue-cp-form-dictionary');
}
