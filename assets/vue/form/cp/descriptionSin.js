import FormDescriptionSin from '@Vue/form/cp/DescriptionSin';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';

export default function () {
    const app = createApp(FormDescriptionSin);
    app.use(Pinia.create());
    app.mount('#vue-cp-form-description-sin');
}
