import FormMusic from '@Vue/form/cp/Music';

let story = {
    title: 'Form/Cp/Music',
    component: FormMusic
}

export default story;

const Template = (args) => ({
    // Components used in your story `template` are defined in the `components` object
    components: { FormMusic },
    // The story's `args` need to be mapped into the template through the `setup()` method
    setup() {
        return { args };
    },
    // And then the `args` are bound to your component with `v-bind="args"`
    template: '<FormMusic v-bind="args"></FormMusic>',
});

export const Create = Template.bind({});
Create.args = {

}

export const Edit = Template.bind({});
Edit.args = {
    id: '018172ab-a7ac-aff1-4cbf-ba6c6df9372f',
}
