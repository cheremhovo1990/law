import FormVideo from '@Vue/form/cp/Video';

let story = {
    title: 'Form/Cp/Video',
    component: FormVideo
}

export default story;

const Template = (args) => ({
    // Components used in your story `template` are defined in the `components` object
    components: { FormVideo },
    // The story's `args` need to be mapped into the template through the `setup()` method
    setup() {
        return { args };
    },
    // And then the `args` are bound to your component with `v-bind="args"`
    template: '<FormVideo v-bind="args"></FormVideo>',
});

export const Video = Template.bind({});

Video.args = {

}
