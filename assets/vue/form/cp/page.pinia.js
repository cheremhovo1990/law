import {defineStore} from 'pinia';
import {Checkbox, Variable} from '@Vue/component/Form/Variable.pinia';
import {actions} from '@Vue/component/Form/action.pinia';
import lodash from 'lodash';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.form.page', {
    state: () => {
        return {
            url: undefined,
            title: new Variable(),
            content: new Variable(),
            parent: new Variable(),
            publish: new Checkbox(),
            metaTitle: new Variable(),
            metaDescription: new Variable(),
        }
    },
    actions: {
        ...actions,
        async init(id) {
            if (!lodash.isNil(id)) {
                let request = FactoryRequest.create(new RouteCpService().generatePage(id));
                let entity = await fetch(request).then(function (res) {
                    return res.json();
                })
                console.debug({method: 'init', message: 'load page', page: entity})
                this.title.value = entity.title;
                this.content.value = entity.content;
                this.publish.value = !lodash.isNil(entity.publishedAt);
                if (!lodash.isNil(entity.parent)) {
                    this.parent.value = entity.parent.id;
                } else {
                    this.parent.value = '';
                }
                this.metaTitle.value = entity.meta.title;
                this.metaDescription.value = entity.meta.description;
            }
        },
        getBody() {
            return  {
                title: String(this.title),
                content: String(this.content),
                publish: String(this.publish),
                parent:  String(this.parent),
                meta: {
                    title: String(this.metaTitle),
                    description: String(this.metaDescription),
                },
            };
        },
    }
});
