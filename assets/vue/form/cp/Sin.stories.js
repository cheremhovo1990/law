import FormSin from '@Vue/form/cp/Sin';

let story = {
    title: 'Form/Cp/Sin',
    component: FormSin
}

export default story;

const Template = (args) => ({
    // Components used in your story `template` are defined in the `components` object
    components: { FormSin },
    // The story's `args` need to be mapped into the template through the `setup()` method
    setup() {
        return { args };
    },
    // And then the `args` are bound to your component with `v-bind="args"`
    template: '<FormSin v-bind="args"></FormSin>',
});

export const Sin = Template.bind({});

Sin.args = {

}
