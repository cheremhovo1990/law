import {defineStore} from 'pinia';
import {actions} from '@Vue/component/Form/action.pinia';
import {Variable, Select} from '@Vue/component/Form/Variable.pinia';
import lodash from 'lodash';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.form.descriptionSin', {
    state: () => {
        return {
            name: new Variable(''),
            text: new Variable(''),
            sins: new Select([]),
            precept: new Select(''),
            metaTitle: new Variable(''),
            metaDescription: new Variable(''),
        }
    },
    actions: {
        ...actions,
        async init(descriptionSinId = undefined) {
            let requestPrecepts = FactoryRequest.create(new RouteCpService().generatePrecepts())
            let precepts = await fetch(requestPrecepts)
                .then(function (res) {
                    return res.json();
                })
            this.precept.options = precepts.map(function (item) {
                return {value: item.id, text: item.name};
            });
            this.precept.options.unshift({value: '', text: ''});
            console.debug({method: 'init', message: 'load precepts for options'}, this.precept.options);

            let requestSins = FactoryRequest.create(new RouteCpService().generateSins())
            let sins = await fetch(requestSins)
                .then(function (res) {
                    return res.json();
                });
            this.sins.options = sins.data.map(function (item) {
                return {value: item.id, text: item.name};
            });
            this.sins.options.unshift({value: '', text: ''});
            console.debug({method: 'init', message: 'load sins for options'}, this.sins.options);
            if (!lodash.isNil(descriptionSinId)) {
                let request = FactoryRequest.create(new RouteCpService().generateDescriptionSin(descriptionSinId))
                let descriptionSin = await fetch(request).then(function (res) {
                    return res.json();
                });
                console.debug({method: 'init', message: 'load description sin'}, descriptionSin);
                this.name.value = descriptionSin.name;
                this.text.value = descriptionSin.text;
                this.metaTitle.value = descriptionSin.meta.title;
                this.metaDescription.value = descriptionSin.meta.description;
                this.precept.value = descriptionSin.precept.id;
                this.sins.value = descriptionSin.sins.map(function (item) {
                    return item.id;
                })
            }
        },
        getBody() {
            return {
                name: String(this.name),
                text: String(this.text),
                precept: String(this.precept.value),
                sins: this.sins.value,
                meta: {
                    title: String(this.metaTitle),
                    description: String(this.metaDescription),
                },
            };
        },
    }
})
