import FormPage from '@Vue/form/cp/Page';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';

export default function () {
    const app = createApp(FormPage);
    app.use(Pinia.create());
    app.mount('#vue-cp-form-page');
}
