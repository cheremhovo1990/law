import FormBook from '@Vue/form/cp/Book';

let story = {
    title: 'Form/Cp/Book',
    component: FormBook
}

export default story;

const Template = (args) => ({
    // Components used in your story `template` are defined in the `components` object
    components: { FormBook },
    // The story's `args` need to be mapped into the template through the `setup()` method
    setup() {
        return { args };
    },
    // And then the `args` are bound to your component with `v-bind="args"`
    template: '<FormBook v-bind="args"></FormBook>',
});

export const Book = Template.bind({});

Book.args = {
    location: '/cp/book/01G60YZXMAA9C2PABNM4X8B3GF/edit',
}
