import FormDescriptionSin from '@Vue/form/cp/DescriptionSin';

let story = {
    title: 'Form/Cp/Description Sin',
    component: FormDescriptionSin
}

export default story;

const Template = (args) => ({
    // Components used in your story `template` are defined in the `components` object
    components: { FormDescriptionSin },
    // The story's `args` need to be mapped into the template through the `setup()` method
    setup() {
        return { args };
    },
    // And then the `args` are bound to your component with `v-bind="args"`
    template: '<FormDescriptionSin v-bind="args"></FormDescriptionSin>',
});

export const DescriptionSin = Template.bind({});

DescriptionSin.args = {

}
