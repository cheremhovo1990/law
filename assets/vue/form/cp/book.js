import FormBook from '@Vue/form/cp/Book';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';
import {mixin as mixinPicture} from '@Js/services/PictureService'

export default function () {
    const app = createApp(FormBook);
    app.mixin(mixinPicture)
    app.use(Pinia.create());
    app.mount('#vue-cp-form-book');
}
