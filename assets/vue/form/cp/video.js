import FormVideo from '@Vue/form/cp/Video';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';

export default function () {
    const app = createApp(FormVideo);
    app.use(Pinia.create());
    app.mount('#vue-cp-form-video');
}
