import FormMusic from '@Vue/form/cp/Music';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';

export default function () {
    const app = createApp( FormMusic);
    app.use(Pinia.create());
    app.mount('#vue-cp-form-music');
}
