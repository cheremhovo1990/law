import FormPrecept from '@Vue/form/cp/Precept';

let story = {
    title: 'Form/Cp/Precept',
    component: FormPrecept
}

export default story;

const Template = (args) => ({
    // Components used in your story `template` are defined in the `components` object
    components: { FormPrecept },
    // The story's `args` need to be mapped into the template through the `setup()` method
    setup() {
        return { args };
    },
    // And then the `args` are bound to your component with `v-bind="args"`
    template: '<FormPrecept v-bind="args"></FormPrecept>',
});

export const Precept = Template.bind({});

Precept.args = {
    location: '/cp/precept/01G6MP6RCDJ694AMVYZHZ5NC5Z/edit',
}
