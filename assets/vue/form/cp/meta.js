import FormMeta from '@Vue/form/cp/Meta';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';

export default function () {
    const app = createApp(FormMeta);
    app.use(Pinia.create());
    app.mount('#vue-cp-form-meta');
}
