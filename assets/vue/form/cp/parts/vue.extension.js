import {Multiple} from '@Vue/component/Form/Variable.pinia';

export let methods = {
    remove(position) {
        this.data.items.splice(position, 1)
    },
}

export let props = {
    input: {
        type: Object,
        require: true,
        default: new Multiple(),
    },
}
