import FormFeedback from '@Vue/form/Feedback';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';

export default function () {
    const app = createApp(FormFeedback);
    app.use(Pinia.create());
    app.mount('#vue-form-feedback');
}
