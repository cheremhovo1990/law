import Book from '@Vue/page/Book';

let story = {
	title: 'Page/Book',
	component: Book
}

export default story;

const Template = (args) => ({
	components: { Book },
	setup() {
		return { args };
	},
	template: '<section class="container front-font"><Book v-bind="args"></Book></section>',
});

export const Page = Template.bind({});

Page.args = {
	location: '/book/01G60YZXMAA9C2PABNM4X8B3GF'
}
