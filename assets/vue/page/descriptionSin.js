import Component from '@Vue/page/DescriptionSin';
import { createApp } from 'vue'
import { createPinia } from 'pinia'

export default function () {
    const pinia = createPinia();
    const app = createApp(Component);
    app.use(pinia);
    app.mount('#vue-descriptionSin')
}