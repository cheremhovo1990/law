import DescriptionSin from '@Vue/page/DescriptionSin';

let story = {
	title: 'Page/DescriptionSin',
	component: DescriptionSin
}

export default story;

const Template = (args) => ({
	components: { DescriptionSin },
	setup() {
		return { args };
	},
	template: '<section class="container front-font"><DescriptionSin v-bind="args"></DescriptionSin></section>',
});

export const Page = Template.bind({});

Page.args = {
	location: '/precept/01G6MP6RCDJ694AMVYZHZ5NC5Z/description/sin/01G6B8H4WRVF4AWCAG1XDF73VV',
}
