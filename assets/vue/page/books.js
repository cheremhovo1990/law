import {createPinia} from 'pinia';
import Component from '@Vue/page/Books';
import {mixin as mixinPicture} from '@Js/services/PictureService'
import { createApp } from 'vue'


export default function () {
    const pinia = createPinia();
    const app = createApp(Component);
    app.mixin(mixinPicture)
    app.use(pinia);

    app.mount('#vue-books')
}
