import Component from '@Vue/page/Main';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';

export default function () {
    const app = createApp(Component);
    app.use(Pinia.create());
    app.mount('#vue-main')
}