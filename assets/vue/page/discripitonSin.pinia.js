import { defineStore } from 'pinia'
import DescriptionSin from '@Vue/src/Entities/DescriptionSin';
import FactoryRequest from '@Vue/src/Services/Request';
import { useStore as useStoreMeta } from '@Vue/src/Stories/Meta.pinia';
import lodash from 'lodash';
import RouteService from '@Js/services/RouteService';

export const useStore = defineStore('descriptionSin', {
	state: () => {
		return {
			descriptionSin: undefined
		}
	},
	actions: {
		async loadDescription(preceptId, id) {
			let storeMeta = useStoreMeta();
			let request = FactoryRequest.create(new RouteService().generateDescriptionSin(preceptId, id));
			let json = await fetch(request)
				.then(function (res) {
					return res.json();
				})
			this.descriptionSin = new DescriptionSin(json);
			if (!lodash.isEmpty(this.descriptionSin.meta.title)) {
				storeMeta.setTitle(this.descriptionSin.meta.title);
			}
			if (!lodash.isEmpty(this.descriptionSin.meta.description)) {
				storeMeta.setDescription(this.descriptionSin.meta.description);
			}
		}
	},
})
