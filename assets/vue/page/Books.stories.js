import Books from '@Vue/page/Books';

let story = {
	title: 'Page/Books',
	component: Books
}

export default story;

const Template = (args) => ({
	components: { Books },
	setup() {
		return { args };
	},
	template: '<section class="container front-font"><Books v-bind="args"></Books></section>',
});

export const Page = Template.bind({});

Page.args = {}
