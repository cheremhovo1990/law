import Main from '@Vue/page/Main';

let story = {
	title: 'Page/Main',
	component: Main
}

export default story;

const Template = (args) => ({
	components: { Main },
	setup() {
		return { args };
	},
	template: '<section class="container front-font"><Main v-bind="args"></Main></section>',
});

export const Page = Template.bind({});

Page.args = {}
