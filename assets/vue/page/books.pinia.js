import { defineStore } from 'pinia'
import Book from '@Vue/src/Entities/Book';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteService from '@Js/services/RouteService';

export const useStore = defineStore('books', {
    state: () => {
        return {
            books: []
        }
    },
    actions: {
        async loadBooks() {
            let request = FactoryRequest.create(new RouteService().generateBooks());
            let json = await fetch(request)
                .then(function (res) {
                return res.json();
            })
            this.books = json.map(function (item) {
                return new Book(item);
            })
        }
    },
})
