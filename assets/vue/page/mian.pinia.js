import { defineStore } from 'pinia'
import Precept from '@Vue/src/Entities/Precept';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteService from '@Js/services/RouteService';

export const useStore = defineStore('main', {
	state: () => {
		return {
			precepts: []
		}
	},
	actions: {
		async loadPrecepts() {
			let request = FactoryRequest.create(new RouteService().generatePrecepts());
			let json = await fetch(request).then(function (res) {
				return res.json();
			})
			this.precepts = json.map(function (item) {
				return new Precept(item);
			})
			return new Promise(function(resolve) {
				resolve();
			});
		}
	},
})
