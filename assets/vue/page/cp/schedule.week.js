import Component from '@Vue/page/cp/Schedule.week';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';
import {mixin} from '@Js/services/mixin';

export default function () {
    const app = createApp(Component);
    app.use(Pinia.create());
    app.mixin(mixin);
    app.mount('#vue-cp-schedule-week')
}
