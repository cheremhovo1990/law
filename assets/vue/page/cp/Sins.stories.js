import Sins from '@Vue/page/cp/Sins';

let story = {
    title: 'Page/Cp/Sins',
    component: Sins
}

export default story;

const Template = (args) => ({
    components: { Sins },
    setup() {
        return { args };
    },
    template: '<section class="container"><Sins v-bind="args"></Sins></section>',
});

export const Page = Template.bind({});

Page.args = {

}