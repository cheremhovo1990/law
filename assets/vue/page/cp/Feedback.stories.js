import Feedbacks from '@Vue/page/cp/Feedbacks';

let story = {
    title: 'Page/Cp/Feedback',
    component: Feedbacks
}

export default story;

const Template = (args) => ({
    components: { Feedbacks },
    setup() {
        return { args };
    },
    template: '<section class="container"><Feedbacks v-bind="args"></Feedbacks></section>',
});

export const Page = Template.bind({});

Page.args = {

}