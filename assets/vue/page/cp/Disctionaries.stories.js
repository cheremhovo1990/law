import Dictionaries from '@Vue/page/cp/Dictionaries';

let story = {
    title: 'Page/Cp/Dictionaries',
    component: Dictionaries
}

export default story;

const Template = (args) => ({
    components: { Dictionaries },
    setup() {
        return { args };
    },
    template: '<section class="container"><Dictionaries v-bind="args"></Dictionaries></section>',
});

export const Page = Template.bind({});

Page.args = {

}