import DescriptionSins from '@Vue/page/cp/DescriptionSins';

let story = {
    title: 'Page/Cp/DescriptionSins',
    component: DescriptionSins
}

export default story;

const Template = (args) => ({
    components: { DescriptionSins },
    setup() {
        return { args };
    },
    template: '<section class="container"><DescriptionSins v-bind="args"></DescriptionSins></section>',
});

export const Page = Template.bind({});

Page.args = {

}