import Component from '@Vue/page/cp/SinsByPrecept';
import { createApp } from 'vue'
import Pinia from '@Vue/src/Services/pinia';

export default function () {
    const app = createApp(Component);
    Pinia.useOne(app);
    app.mount('#vue-cp-sins-by-precept')
}