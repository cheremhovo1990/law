import { defineStore } from 'pinia'
import Video from '@Vue/src/Entities/Video';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.video', {
    state: () => {
        return {
            video: []
        }
    },
    actions: {
        async loadVideo() {
            let request = FactoryRequest.create(new RouteCpService().generateVideo())
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.video = json.data.map(function (item) {
                return new Video(item);
            });
        }
    },
})
