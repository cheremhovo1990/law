import Precepts from '@Vue/page/cp/Precepts';

let story = {
	title: 'Page/Cp/Precepts',
	component: Precepts
}

export default story;

const Template = (args) => ({
	components: { Precepts },
	setup() {
		return { args };
	},
	template: '<section class="container"><Precepts v-bind="args"></Precepts></section>',
});

export const Page = Template.bind({});

Page.args = {

}