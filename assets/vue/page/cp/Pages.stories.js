import Pages from '@Vue/page/cp/Pages';

let story = {
    title: 'Page/Cp/Pages',
    component: Pages
}

export default story;

const Template = (args) => ({
    components: { Pages },
    setup() {
        return { args };
    },
    template: '<section class="container"><Pages v-bind="args"></Pages></section>',
});

export const Page = Template.bind({});

Page.args = {

}
