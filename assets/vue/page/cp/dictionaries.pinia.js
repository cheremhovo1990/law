import { defineStore } from 'pinia'
import Dictionary from '@Vue/src/Entities/Dictionary';
import FactoryRequest from '@Vue/src/Services/Request';
import {PaginatorService} from '@Vue/src/Services/PaginatorService';
import httpBuildQuery from 'http-build-query';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.dictionaries', {
    state: () => {
        return {
            groupNames: [],
            params: {
                search: {
                    have_titlo: 0,
                    description: '',
                    group: '',
                },
                page: 1,
                limit: 100,
            },
            dictionaries: [],
            paginator: undefined,
        }
    },
    actions: {
        async loadGroupName() {
            let url = new URL(new RouteCpService().generateSymbolMapGroupName());
            let request = FactoryRequest.create(url.toString())
            this.groupNames = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
        },
        async loadDictionaries() {
            let url = new URL(new RouteCpService().generateDictionaries());
            url = new URL(url.toString() + '?'  + httpBuildQuery(this.params));
            let request = FactoryRequest.create(url.toString())
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.dictionaries = json.data.map(function (item) {
                return new Dictionary(item);
            });
            this.paginator = new PaginatorService(json.paginator);
        }
    },
})
