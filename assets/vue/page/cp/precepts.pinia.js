import { defineStore } from 'pinia'
import Precept from '@Vue/src/Entities/Precept';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.precepts', {
    state: () => {
        return {
            precepts: []
        }
    },
    actions: {
        async loadPrecepts() {
            let request = FactoryRequest.create(new RouteCpService().generatePrecepts())
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.precepts = json.map(function (item) {
                return new Precept(item);
            });
        }
    },
})
