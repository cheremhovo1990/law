import { defineStore } from 'pinia'
import httpBuildQuery from 'http-build-query';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';
import moment from 'moment';

export const useStore = defineStore('cp.schedule.month', {
    state: () => {
        moment.locale('ru');
        return {
            currentDate: moment(),
            currentDateAsString: moment().format('MMMM YYYY'),
            weeksInMonth: 0,
            rows: [],
        }
    },
    actions: {
        async load() {
            let start = this.currentDate.clone().startOf('month').startOf('isoWeek');
            let end = this.currentDate.clone().endOf('month').endOf('isoWeek');
            let params = {
                startDate: start.format('YYYY-MM-DD'),
                endDate: end.format('YYYY-MM-DD'),
            };
            let url = new URL(new RouteCpService().generateScheduleMonth() + '?'  + httpBuildQuery(params));
            let request = FactoryRequest.create(url.toString())
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.weeksInMonth = json.weeksInMonth;
            this.rows = json.data;
        }
    },
})
