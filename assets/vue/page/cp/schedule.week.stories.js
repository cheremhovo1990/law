import ScheduleWeek from '@Vue/page/cp/Schedule.week';

let story = {
	title: 'Page/Cp/Schedule/Week',
	component: ScheduleWeek
}

export default story;

const Template = (args) => ({
	components: { ScheduleWeek },
	setup() {
		return { args };
	},
	template: '<section class="container front-font"><ScheduleWeek v-bind="args"></ScheduleWeek></section>',
});

export const Page = Template.bind({});

Page.args = {}
