import { defineStore } from 'pinia'
import Feedback from '@Vue/src/Entities/Feedback';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.feedbacks', {
    state: () => {
        return {
            feedbacks: []
        }
    },
    actions: {
        async loadFeedbacks() {
            let request = FactoryRequest.create(new RouteCpService().generateFeedbacks())
            let json = await fetch(request )
                .then(function (res) {
                    return res.json();
                })
            this.feedbacks = json.data.map(function (item) {
                return new Feedback(item);
            });
        }
    },
})
