import ScheduleMonth from '@Vue/page/cp/Schedule.month';

let story = {
	title: 'Page/Cp/Schedule/Month',
	component: ScheduleMonth
}

export default story;

const Template = (args) => ({
	components: { ScheduleMonth },
	setup() {
		return { args };
	},
	template: '<section class="container front-font"><ScheduleMonth v-bind="args"></ScheduleMonth></section>',
});

export const Page = Template.bind({});

Page.args = {}
