import { defineStore } from 'pinia'
import Page from '@Vue/src/Entities/Page';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.pages', {
    state: () => {
        return {
            pages: []
        }
    },
    actions: {
        async load() {
            let request = FactoryRequest.create(new RouteCpService().generatePagesRootTree())
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.pages = json.map(function (item) {
                return new Page(item);
            });
        }
    },
})
