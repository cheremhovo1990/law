import Video from '@Vue/page/cp/Video';

let story = {
    title: 'Page/Cp/Video',
    component: Video
}

export default story;

const Template = (args) => ({
    components: { Video },
    setup() {
        return { args };
    },
    template: '<section class="container"><Video v-bind="args"></Video></section>',
});

export const Page = Template.bind({});

Page.args = {

}