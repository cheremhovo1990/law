import Music from '@Vue/page/cp/Music';

let story = {
    title: 'Page/Cp/Music',
    component: Music
}

export default story;

const Template = (args) => ({
    components: { Music },
    setup() {
        return { args };
    },
    template: '<section class="container"><Music v-bind="args"></Music></section>',
});

export const Page = Template.bind({});

Page.args = {

}
