import { defineStore } from 'pinia'
import DescriptionSin from '@Vue/src/Entities/DescriptionSin';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.descriptionSins', {
    state: () => {
        return {
            descriptionSins: []
        }
    },
    actions: {
        async loadDescriptionSins() {
            let request = FactoryRequest.create(new RouteCpService().generateDescriptionSins())
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.descriptionSins = json.data.map(function (item) {
                return new DescriptionSin(item);
            });
        }
    },
})
