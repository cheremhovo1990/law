import Books from '@Vue/page/cp/Books';

let story = {
    title: 'Page/Cp/Books',
    component: Books
}

export default story;

const Template = (args) => ({
    components: { Books },
    setup() {
        return { args };
    },
    template: '<section class="container"><Books v-bind="args"></Books></section>',
});

export const Page = Template.bind({});

Page.args = {

}