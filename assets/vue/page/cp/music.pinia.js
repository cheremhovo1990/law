import { defineStore } from 'pinia'
import Music from '@Vue/src/Entities/Music';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.music', {
    state: () => {
        return {
            music: []
        }
    },
    actions: {
        async loadMusic() {
            let request = FactoryRequest.create(new RouteCpService().generateMusic())
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.music = json.data.map(function (item) {
                return new Music(item);
            });
        }
    },
})
