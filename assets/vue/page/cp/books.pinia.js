import { defineStore } from 'pinia'
import Book from '@Vue/src/Entities/Book';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.books', {
    state: () => {
        return {
            books: []
        }
    },
    actions: {
        async loadBooks() {
            let request = FactoryRequest.create(new RouteCpService().generateBooks())
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.books = json.data.map(function (item) {
                return new Book(item);
            });
        }
    },
})
