import { defineStore } from 'pinia'
import Sin from '@Vue/src/Entities/Sin';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.sins', {
    state: () => {
        return {
            sins: []
        }
    },
    actions: {
        async loadSins() {
            let request = FactoryRequest.create(new RouteCpService().generateSins())
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.sins = json.data.map(function (item) {
                return new Sin(item);
            });
        },
        async loadSinsByPrecept(preceptId) {
            let request = FactoryRequest.create(new RouteCpService().generateSins(preceptId))
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.sins = json.data.map(function (item) {
                return new Sin(item);
            });
        }
    },
})
