import { defineStore } from 'pinia'
import Meta from '@Vue/src/Entities/Meta';
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';

export const useStore = defineStore('cp.meta', {
    state: () => {
        return {
            meta: []
        }
    },
    actions: {
        async loadMeta() {
            let request = FactoryRequest.create(new RouteCpService().generateMeta())
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.meta = json.data.map(function (item) {
                return new Meta(item);
            });
        }
    },
})
