import { defineStore } from 'pinia'
import FactoryRequest from '@Vue/src/Services/Request';
import RouteCpService from '@Js/services/RouteCpService';
import moment from 'moment';
import httpBuildQuery from 'http-build-query';

export const useStore = defineStore('cp.schedule.week', {
    state: () => {
        moment.locale('ru');
        return {
            currentDate: moment(),
            currentDateAsString: moment().format('MMMM YYYY'),
            week: undefined,
            days: []
        }
    },
    actions: {
        async load() {
            let start = this.currentDate.clone().startOf('isoWeek');
            let end = this.currentDate.clone().endOf('isoWeek');
            let params = {
                startDate: start.format('YYYY-MM-DD'),
                endDate: end.format('YYYY-MM-DD'),
            };
            let url = new URL(new RouteCpService().generateScheduleWeek() + '?'  + httpBuildQuery(params));
            let request = FactoryRequest.create(url.toString())
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.week = json.week;
            this.days = json.days
        }
    },
})
