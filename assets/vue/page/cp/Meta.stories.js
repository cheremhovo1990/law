import Meta from '@Vue/page/cp/Meta';

let story = {
    title: 'Page/Cp/Meta',
    component: Meta
}

export default story;

const Template = (args) => ({
    components: { Meta },
    setup() {
        return { args };
    },
    template: '<section class="container"><Meta v-bind="args"></Meta></section>',
});

export const Page = Template.bind({});

Page.args = {

}