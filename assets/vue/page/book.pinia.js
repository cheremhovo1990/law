import { defineStore } from 'pinia'
import Book from '@Vue/src/Entities/Book';
import FactoryRequest from '@Vue/src/Services/Request';
import { useStore as useStoreMeta } from '@Vue/src/Stories/Meta.pinia';
import lodash from 'lodash';
import RouteService from '@Js/services/RouteService';

export const useStore = defineStore('book', {
    state: () => {
        return {
            book: undefined
        }
    },
    actions: {
        async loadBook(id) {
            let storeMeta = useStoreMeta();
            let request = FactoryRequest.create(new RouteService().generateBook(id));
            let json = await fetch(request)
                .then(function (res) {
                    return res.json();
                })
            this.book = new Book(json);
            if (!lodash.isEmpty(this.book.meta.title)) {
                storeMeta.setTitle(this.book.meta.title);
            }
            if (!lodash.isEmpty(this.book.meta.description)) {
                storeMeta.setDescription(this.book.meta.description);
            }
        }
    },
})
