import FormInputGroup from '@Vue/component/Form/InputGroup';
import {inputGroup as argTypes} from '@Vue/component/Form/argTypes'
import {Variable} from '@Vue/component/Form/Variable.pinia';

let story = {
	title: 'Form/Component/InputGroup',
	component: FormInputGroup,
	argTypes: {}
}
Object.assign(story.argTypes, argTypes);
export default story;
const Template = (args) => ({
	components: { FormInputGroup },
	setup() {
		return { args };
	},
	template: '<FormInputGroup v-bind="args"></FormInputGroup>',
})

export const Email = Template.bind({});

Email.args = {
	type: 'email',
	help: 'We\'ll never share your email with anyone else.',
	input: new Variable('', 'Please provide a valid email.'),
	validFeedback: 'Looks good!',
	size: 'lg',
	placeholder: 'Username',
	left: 'https://example.com/users/',
	right: '@example.com'
}
