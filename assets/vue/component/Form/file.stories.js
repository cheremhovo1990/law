import FormFile from '@Vue/component/Form/File';
import {file as argTypes} from '@Vue/component/Form/argTypes';

let story = {
    title: 'Form/Component/File',
    component: FormFile,
    argTypes: {}
}
Object.assign(story.argTypes, argTypes);
export default story;

const Template = (args) => ({
    components: { FormFile },
    setup() {
        return { args };
    },
    template: '<FormFile v-bind="args"></FormFile>',
})

export const File = Template.bind({});

File.args = {
    action: process.env.VUE_APP_HOST + '/api/cp/image/upload/mutation',
    label: 'Default file input example',
    help: 'We\'ll never share your email with anyone else.',
    validFeedback: 'Looks good!',
}
