import { toRaw } from 'vue';
export class Base {
    isInvalid = false;
    invalidFeedback = undefined;
    value = '';
    setInvalid(value) {
        this.isInvalid = value;
    }
    setInvalidFeedback(value) {
        this.invalidFeedback = value;
    }
    toString() {
        if (this.value == null) {
            return '';
        }
        return this.value;
    }
}
export class Variable extends Base {
    constructor(value = '', invalidFeedback = undefined) {
        super();
        this.value = value;
        this.invalidFeedback = invalidFeedback;
    }
}
export class Select extends Base {
    options = {};
    constructor(value = '', invalidFeedback = undefined) {
        super();
        this.value = value;
        this.invalidFeedback = invalidFeedback;
    }
}
export class Checkbox {
    value = false;
    constructor(value = false) {
        this.value = value;
    }
    toString() {
        if (this.value) {
            return '1';
        } else {
            return '';
        }
    }
}

export class Image extends Base {
    value = undefined;
    constructor(value = undefined, invalidFeedback = undefined) {
        super();
        this.value = value;
        this.invalidFeedback = invalidFeedback;
    }
}

export class Multiple extends Base {
    items = [];
    toRaw() {
        return toRaw(this.items);
    }
}