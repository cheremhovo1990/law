import FormCheckbox from '@Vue/component/Form/Checkbox';
import {checkbox as argTypes} from '@Vue/component/Form/argTypes'

let story = {
	title: 'Form/Component/Checkbox',
	component: FormCheckbox,
	argTypes: {}
}
Object.assign(story.argTypes, argTypes);
export default story;

const Template = (args) => ({
	components: { FormCheckbox },
	setup() {
		return { args };
	},
	template: '<FormCheckbox  v-bind="args"></FormCheckbox>',
})


export const Checkbox = Template.bind({});

Checkbox.args = {
	label: 'Default checkbox'
}
