import FormButton from '@Vue/component/Form/Button';

let story = {
	title: 'Form/Component/Button',
	component: FormButton,
	argTypes: {}
}

export default story;
const Template = (args) => ({
	components: { FormButton },
	setup() {
		return { args };
	},
	template: '<FormButton v-bind="args"></FormButton>',
})

export const Button = Template.bind({});

Button.args = {
	text: 'Save',
	buttonClasses: 'btn-outline-secondary'
}
