import {Base} from '@Vue/component/Form/Variable.pinia';
import lodash from 'lodash';
import CSRFService from '@Js/services/CSRFService';

export let actions = {
    getBody() {
        return {};
    },
    async save(action) {
        let body = this.getBody()
        if (body['_token'] === undefined) {
            let csrfService = new CSRFService();
            if (!lodash.isNil(csrfService.getToken())) {
                body['_token'] = csrfService.getToken();
            }
        }
        console.debug({
            data: body,
            function: 'submit',
            action: action,
        });

        let request = new Request(action, {
            method: 'post',
            body: JSON.stringify(body),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        let res = await fetch(request);
        let json = await res.json();
        if (res.status === 200) {
            this.removeInvalid();
            if (json.success) {
                console.debug({message: 'send success'})
                if (!lodash.isNil(json.redirect)) {
                    console.debug('redirect', json.redirect);
                    window.location.assign(json.redirect);
                }
            } else {
                console.debug({message: 'send fail'})
                this.handleError(json.form.children)
            }
        }
        return json;
    },
    removeInvalid() {
        for (let item in this) {
            if (this[item] instanceof Base) {
                this[item].setInvalid(false);
                this[item].setInvalidFeedback(undefined);
            }
        }
    },
    handleError(errors) {
        for (let field in errors) {
            let item = errors[field];
            if (item.errors.length) {
                if (this[field] instanceof Base) {
                    this[field].setInvalid(true);
                    for (let i = 0; i < item.errors.length; i++) {
                        this[field].setInvalidFeedback(item.errors[i].message);
                    }
                }
            }
        }
    },
}
