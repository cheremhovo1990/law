import FormInput from '@Vue/component/Form/Input';
import {input as argTypes} from '@Vue/component/Form/argTypes';
import {Variable} from '@Vue/component/Form/Variable.pinia';

let story = {
    title: 'Form/Component/Input',
    component: FormInput,
    argTypes: {}
}
Object.assign(story.argTypes, argTypes);
export default story;

const Template = (args) => ({
    components: { FormInput },
    setup() {
        return { args };
    },
    template: '<FormInput v-bind="args"></FormInput>',
})

export const Email = Template.bind({});

Email.args = {
    label: 'Email address',
    type: 'email',
    input: new Variable('', 'Please provide a valid email.'),
    help: 'We\'ll never share your email with anyone else.',
    validFeedback: 'Looks good!',
    size: 'lg',
}

export const Title = Template.bind({});

Title.args = {
    dataList: ['San Francisco', 'New York'],
}

export const Password = Template.bind({});

Password.args = {
    label: 'Password',
    type: 'password',
    placeholder: 'Password',
}
