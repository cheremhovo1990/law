import FormCkeditor from '@Vue/component/Form/Ckeditor';
import {ckeditor as argTypes} from '@Vue/component/Form/argTypes';
import {Variable} from '@Vue/component/Form/Variable.pinia';

let story = {
    title: 'Form/Component/Ckeditor',
    component: FormCkeditor,
    argTypes: {}
}
Object.assign(story.argTypes, argTypes);
export default story;

const Template = (args) => ({
    components: { FormCkeditor },
    setup() {
        return { args };
    },
    template: '<FormCkeditor v-bind="args"></FormCkeditor>',
})

export const Text = Template.bind({});

Text.args = {
    label: 'Text',
    input: new Variable('', 'Please provide a valid email.'),
    help: 'We\'ll never share your email with anyone else.',
    validFeedback: 'Looks good!',
}
