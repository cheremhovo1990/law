import FormSelect from '@Vue/component/Form/Select'
import {select as argTypes} from '@Vue/component/Form/argTypes'

let story = {
	title: 'Form/Component/Select',
	component: FormSelect,
	argTypes: {}
}
Object.assign(story.argTypes, argTypes);
export default story;

const Template = (args) => ({
	components: { FormSelect },
	setup() {
		return { args };
	},
	template: '<FormSelect v-bind="args"></FormSelect>',
})

export const Select = Template.bind({});

Select.args = {
	options: [
		{value: '', text: 'Open this select menu'},
		{value: 1, text: 'One'},
		{value: 2, text: 'Two'},
		{value: 3, text: 'Three'},
	],
}
