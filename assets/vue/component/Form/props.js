import {Variable, Checkbox} from '@Vue/component/Form/Variable.pinia';
let inputText = {
	input: {
		type: Object,
		require: true,
		default: new Variable(''),
	}
}
let label = {
	label: {
		type: String,
		default: '',
	},
}
let modelValue = {
	modelValue: {
		type: String,
		require: true,
	},
}
let disabled = {
	disabled: {
		type: Boolean,
		default: false
	},
}
let type = {
	type: {
		type: String,
		default: 'text',
		validator: function (value) {
			return ['text', 'email', 'password' , 'number', 'date'].indexOf(value) !== -1;
		},
	}
}
let placeholder = {
	placeholder: {
		type: String,
		default: '',
	},
}
let size = {
	size: {
		type: String,
		default: '',
		validator: function (value) {
			return ['lg', 'sm', ''].indexOf(value) !== -1;
		},
	},
}
export let base = {
	help: {
		type: String,
		default: '',
	},
	validFeedback: {
		type: String,
		default: '',
	},
	haveIsInvalid: {
		type: Boolean,
		default: false
	},
	haveIsValid: {
		type: Boolean,
		default: false
	},
	isRequired: {
		type: Boolean,
		default: false
	},
}
export let input = {
	dataList: {
		type: Array,
		default: () => []
	},
};
input = {...input, ...base, ...inputText, ...modelValue, ...type, ...label, ...placeholder, ...disabled, ...size}

export let inputGroup = {
	left: {
		type: String,
		default: ''
	},
	right: {
		type: String,
		default: ''
	},
	leftWidthClass: {
		type: String,
		default: undefined
	}
}
inputGroup = {...inputGroup, ...base, ...inputText, ...modelValue, ...type, ...placeholder, ...disabled, ...size}

export let textarea = {
	rows: {
		type: Number,
		default: 3
	},
}
textarea = {...textarea, ...base, ...modelValue,  ...label, ...disabled, ...size};

export let checkbox = {
	input: {
		type: Object,
		require: true,
		default: new Checkbox(false),
	}
}
checkbox = {...base, ...checkbox, ...disabled,  ...label, ...size};

export let radio = {
	radios: {
		type: Array,
		default: () => []
	},
	checked: {
		default: '',
	}
}

radio = {...radio, ...disabled}

export let range = {
	min: {
		type: Number,
		default: undefined
	},
	max: {
		type: Number,
		default: undefined
	},
	step: {
		type: Number,
		default: undefined
	}
}

range = {...range, ...disabled, ...label}

export let select = {
	multiple: {
		type: Boolean,
		default: false
	},
	options: {
		type: Array,
		require: true,
	}
}
select = {...select, ...base, ...inputText, ...label, ...disabled, ...size};

export let file = {
	input: {
		type: Object,
		require: true,
		default: new Variable(''),
	},
	action: {
		type: String,
		require: true,
	}
}

file = {...file, ...base, ...disabled, ...label, ...size}

export let ckeditor = {

}

ckeditor = {...ckeditor, ...base, ...label, ...inputText}

