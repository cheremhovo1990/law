import {v4 as uuidv4} from 'uuid';

export let classesComputed = {
	classes() {
		return {
			'form-control': true,
			'is-invalid': this.input.isInvalid,
			'is-valid': this.haveIsValid,
		}
	},
}
export let classesWithSizeComputed = {
	classes() {
		let classes = {
			'form-control': true,
			'is-invalid': this.input.isInvalid,
			'is-valid': this.haveIsValid,
		}
		if (this.size !== '') {
			classes['form-control-' + this.size] = true
		}
		return classes;
	},
}
export let baseComputed = {
	ids() {
		return [
			uuidv4(),
		]
	},
	labelClasses() {
		return {
			'form-label': true,
			'required': this.isRequired
		}
	}
};
export let inputComputed = {
	idsDataList() {
		return [
			uuidv4(),
		]
	}
}
inputComputed = {...inputComputed, ...classesWithSizeComputed, ...baseComputed}

export let inputGroupComputed = {
	leftClasses() {
		let classes = {
			'input-group-text': true,
			'required': this.isRequired
		}
		if (this.leftWidthClass) {
			classes['col-' + this.leftWidthClass] = true
		}
		return classes;
	}
}

inputGroupComputed = {...inputGroupComputed, ...inputComputed};

export let textareaComputed = {
}
textareaComputed = {...textareaComputed, ...baseComputed, ...classesComputed}

export let selectComputed = {}

selectComputed = {...selectComputed, ...baseComputed, ...classesComputed}

export let radioComputed = {}
radioComputed = {...radioComputed, ...baseComputed};

export let fileComputed = {}
fileComputed = {...fileComputed, ...baseComputed,  ...classesWithSizeComputed}

export let ckeditorComputed = {}

ckeditorComputed = {...ckeditorComputed, ...baseComputed, ...classesComputed};
