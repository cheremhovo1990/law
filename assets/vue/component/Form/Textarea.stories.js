import FormTextarea from '@Vue/component/Form/Textarea';
import {textarea as argTypes} from '@Vue/component/Form/argTypes';

let story = {
	title: 'Form/Component/Textarea',
	component: FormTextarea,
	argTypes: {}
}
Object.assign(story.argTypes, argTypes);
export default story;

const Template = (args) => ({
	components: { FormTextarea },
	setup() {
		return { args };
	},
	template: '<FormTextarea v-bind="args"></FormTextarea>',
})

export const Textarea = Template.bind({});

Textarea.args = {
	label: 'Example textarea',
}
