import {Variable, Image, Checkbox} from '@Vue/component/Form/Variable.pinia';

let inputType = {
	input: {
		type: Object,
		defaultValue: new Variable(''),
	}
}
let disabled = {
	disabled: {
		control: 'boolean',
		defaultValue: false,
	}
}
let size = {
	size: {
		control: { type: 'select' },
		options: ['lg', 'sm', ''],
	},
}
export let base = {
	haveIsValid: {
		control: 'boolean',
		defaultValue: false,
	},
}

export let input = {
	dataList: {
		control: 'object',
		defaultValue: [],
	},
	type: {
		control: { type: 'select' },
		options: ['email', 'text', 'password'],
	},
}
input = {...base, ...input, ...size, ...inputType, ...disabled};

export let inputGroup = {
};
inputGroup = {...inputGroup, ...base, ...input,  ...size, ...inputType,  ...disabled}

export let textarea = {};

textarea = {...textarea, ...inputType};

export let checkbox = {
	input: {
		type: Object,
		defaultValue: new Checkbox(false),
	}
};

checkbox = {...checkbox, ...disabled}

export let radio = {
	radios: {
		control: 'object',
		defaultValue: [],
	},
};
radio = {...radio, ...disabled}

export let range = {}

range = {...range, ...disabled}

export let select = {
	options: {
		control: 'object',
		defaultValue: [],
	},
}

select = {...select, ...base, ...inputType, ...disabled}

export let file = {
	isRequired: {
		control: 'boolean',
		defaultValue: false,
	},
	input: {
		type: Object,
		defaultValue: new Image(''),
	}
}

file = {...file, ...base, ...size, ...disabled}

export let ckeditor = {}
ckeditor = {...ckeditor, ...base, ...inputType}
