import Keyboard from '@Vue/component/Keyboard';

let story = {
    title: 'Components/Keyboard',
    component: Keyboard
}

export default story;

const Template = (args) => ({
    // Components used in your story `template` are defined in the `components` object
    components: { Keyboard },
    // The story's `args` need to be mapped into the template through the `setup()` method
    setup() {
        return { args };
    },
    // And then the `args` are bound to your component with `v-bind="args"`
    template: '<Keyboard v-bind="args"></Keyboard>',
});

export const Component = Template.bind({});

Component.args = {

}
