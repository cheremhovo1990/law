// js
import ready  from '@Js/ready';
document.addEventListener('DOMContentLoaded', function () {
    ready();
})
import 'bootstrap/dist/js/bootstrap';
// css
import 'bootstrap/scss/bootstrap.scss'
import '@Assets/styles/font.css'
import 'toastr/toastr.scss';
import '@Assets/styles/cp/app.scss';
