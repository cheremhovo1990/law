import toastr from 'toastr';
import {CentrifugeService} from '@Js/services/CentrifugeService';
import {StoreService} from '@Js/services/StoreService';
import Cookie from 'js-cookie';
import lodash from 'lodash';
import {MomentService} from '@Js/services/MomentService';

toastr.options = {
    'closeButton': true,
    'debug': false,
    'newestOnTop': true,
    'progressBar': true,
    'positionClass': 'toast-bottom-right',
    'preventDuplicates': false,
    'onclick': null,
    'showDuration': '300',
    'hideDuration': '1000',
    'timeOut': 0,
    'extendedTimeOut': '1000',
    'showEasing': 'swing',
    'hideEasing': 'linear',
    'showMethod': 'fadeIn',
    'hideMethod': 'fadeOut'
}

window.toastr = toastr;

let centrifugeService = new CentrifugeService();
centrifugeService.open();
let identity = Cookie.get('centrifugo-identity');

let storeService = new StoreService();
showAll();

centrifugeService.addSubscribe('alerts#' + identity, function (message) {
    storeService.addStore(message.data);
    show(message.data);
})
centrifugeService.connect();

function showAll() {
    let items = storeService.getStore();

    for (let key in items) {
        let data = items[key];
        show(data)
    }
}
function show(data) {

    let id = data.id;
    let config = {
        onHidden: function () {
            storeService.removeStore(id);
        },
    };
    if (!lodash.isNil(data.duration)) {
        config.timeOut = data.duration * 1000;
    }
    if (!lodash.isNil(data.delay)) {
        let now = new Date;
        let date = MomentService.createMoment(data.created);
        let timeShow = data.delay * 1000 - (now.getTime() - date.valueOf());
        setTimeout(function () {
            toastr[data.alert](data.title, data.message, config);
        }, timeShow);
    } else {
        toastr[data.alert](data.title, data.message, config);
    }
}
