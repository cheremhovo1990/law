const routes = require('@Js/router.json');
import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import {getUrl} from '@Vue/src/Services/host'

export default class RouteFrontService {
    route = undefined;

    constructor() {
        this.route = Routing;
        this.route.setRoutingData(routes);
    }

    generateBook(id) {
        return getUrl(this.route.generate('book', {id: id}));
    }
    generateDescriptionSin(preceptId, id) {
        return getUrl(this.route.generate('description.sin', {preceptId: preceptId, id: id}));
    }
}
