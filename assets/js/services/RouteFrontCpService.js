const routes = require('@Js/router.json');
import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import {getUrl} from '@Vue/src/Services/host'

export default class RouteFrontCpService {
    route = undefined;

    constructor() {
        this.route = Routing;
        this.route.setRoutingData(routes);
    }

    generatePreceptEdit(id) {
        return getUrl(this.route.generate('cp.precept.edit', {id: id}));
    }

    generateBookEdit(id) {
        return getUrl(this.route.generate('cp.book.edit', {id: id}));
    }
    generateMusicEdit(id) {
        return getUrl(this.route.generate('cp.music.edit', {id: id}));
    }
    generateVideoEdit(id) {
        return getUrl(this.route.generate('cp.video.edit', {id: id}));
    }
    generatePageEdit(id) {
        return getUrl(this.route.generate('cp.page.edit', {id: id}));
    }
    generateMetaEdit(key) {
        return getUrl(this.route.generate('cp.meta.edit', {key: key}));
    }
    generateDescriptionSinEdit(id) {
        return getUrl(this.route.generate('cp.description.sin.edit', {id: id}));
    }
    generateDictionaryEdit(id) {
        return getUrl(this.route.generate('cp.dictionary.edit', {id: id}));
    }
    generateSinEdit(preceptId, id) {
        return getUrl(this.route.generate('cp.sin.edit', {preceptId: preceptId, id: id}));
    }
    generateScheduleMonth() {
        return getUrl(this.route.generate('cp.schedule.month'));
    }
    generateScheduleWeek() {
        return getUrl(this.route.generate('cp.schedule.week'));
    }
}
