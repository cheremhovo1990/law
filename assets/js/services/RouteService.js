const routes = require('@Js/router.json');
import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import {getUrl} from '@Vue/src/Services/host'

export default class RouteService{
    route = undefined;
    constructor() {
        this.route = Routing;
        this.route.setRoutingData(routes);
    }

    generateFeedbackMutation() {
        return getUrl(this.route.generate('api.feedback-mutation'));
    }

    generateBooks() {
        return getUrl(this.route.generate('api.books'));
    }
    generateBook(id) {
        return getUrl(this.route.generate('api.book', {id: id}));
    }
    generatePrecepts() {
        return getUrl(this.route.generate('api.precepts'));
    }
    generateDescriptionSin(preceptId, id) {
        return getUrl(this.route.generate('api.description.sin', {preceptId: preceptId, id: id}));
    }
}
