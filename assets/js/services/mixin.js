import PictureService from '@Js/services/PictureService';
import RouteFrontCpService from '@Js/services/RouteFrontCpService';

export let mixin = {
    methods: {
        routeFrontCp() {
            return new RouteFrontCpService();
        },
        generatePicture(url, options) {
            return PictureService.generate(url, options);
        },
    }
}
