import moment from 'moment/moment';

export class MomentService {
    static createMoment(dateTime) {
        return moment(dateTime, moment.ISO_8601);
    }
}
