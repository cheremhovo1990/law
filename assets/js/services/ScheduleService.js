import moment from 'moment';

export default class ScheduleService {
    constructor() {
    }
    subDay(date) {
        let prev = moment().subtract(1, 'months');
        if (date.format('E') == 1 && (date.format('D') == 1 || date.format('M') == prev.format('M'))) {
            return date;
        } else {
            date.subtract(1, 'days');
            return this.subDay(date);
        }
    }

    begin() {
        let date = moment();
        return this.subDay(date)
    }

    getWeeks(year, month) {
        let l = new Date(year, month + 1, 0);
        return Math.ceil((l.getDate() - (l.getDay() ? l.getDay() : 7)) / 7) + 1;
    }
}
