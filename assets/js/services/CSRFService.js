import Cookie from 'js-cookie';

export default class CSRFService {
    getToken(key) {
        if (key !== undefined) {
            return Cookie.get(key)
        }
        return Cookie.get('csrf-token')
    }
}
