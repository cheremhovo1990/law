export class StoreService {
    static KEY = 'alert';
    getStore() {
        if (localStorage.getItem(StoreService.KEY)) {
            return JSON.parse(localStorage.getItem(StoreService.KEY));
        }
        return {};
    }
    addStore(data) {
        let items = this.getStore();
        items[data.id] = data;
        localStorage.setItem(StoreService.KEY, JSON.stringify(items));
    }
    removeStore(id) {
        let items = this.getStore();
        delete items[id];
        localStorage.setItem(StoreService.KEY, JSON.stringify(items));
    }
}