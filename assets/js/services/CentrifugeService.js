import Centrifuge from 'centrifuge';
import Cookie from 'js-cookie';

export class CentrifugeService {
    centrifuge = undefined;
    open() {
        let token = Cookie.get('centrifugo-token')
        this.centrifuge = new Centrifuge(process.env.CENTRIFUGO_SOCKET_URL);
        this.centrifuge.setToken(token);
        this.centrifuge.on('connect', function(ctx) {
            console.log('connected', ctx);
        });

        this.centrifuge.on('disconnect', function(ctx) {
            console.log('disconnected', ctx);
        });
    }

    addSubscribe(name, callable) {
        this.centrifuge.subscribe(name, callable);
    }

    connect() {
        this.centrifuge.connect();
    }
}