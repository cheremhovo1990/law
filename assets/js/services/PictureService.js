export let mixin = {
    methods: {
        generatePicture(url, options) {
            return PictureService.generate(url, options);
        }
    }
}

export default class PictureService {
    static generate(url, options) {
        options = options || {};
        let result = '';
        if (options.width !== undefined) {
            result += '/width/'+ options.width;
        }
        if (options.height !== undefined) {
            result += '/height/'+ options.height;
        }
        if (options.thumbnail !== undefined) {
            result += '/thumbnail/' + options.thumbnail;
        }
        return process.env.VUE_PICTURE_HOST + result + '/' + url;
    }
}
