const routes = require('@Js/router.json');
import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import {getUrl} from '@Vue/src/Services/host'

export default class RouteCpService{
    route = undefined;
    constructor() {
        this.route = Routing;
        this.route.setRoutingData(routes);
    }
    generateFeedbacks() {
        return getUrl(this.route.generate('api.cp.feedbacks'));
    }
    // START Sin
    generateSin(id) {
        return getUrl(this.route.generate('api.cp.sin', {id: id}));
    }
    generateSins(preceptId = undefined) {
        if (preceptId !== undefined) {
            return getUrl(this.route.generate('api.cp.sins.by.precept', {preceptId: preceptId}));
        }
        return getUrl(this.route.generate('api.cp.sins'));
    }
    generateSinCreate() {
        return getUrl(this.route.generate('api.cp.sin.create-mutation'));
    }
    generateSinEdit(id) {
        return getUrl(this.route.generate('api.cp.sin.edit-mutation', {id: id}));
    }
    generateSinDelete(id) {
        return getUrl(this.route.generate('api.cp.sin.delete-mutation', {id: id}));
    }
    // END Sin
    // START description Sin
    generateDescriptionSin(id) {
        return getUrl(this.route.generate('api.cp.description.sin', {id: id}));
    }
    generateDescriptionSins() {
        return getUrl(this.route.generate('api.cp.description.sins'));
    }
    generateDescriptionSinCreate() {
        return getUrl(this.route.generate('api.cp.description.sin.create-mutation'));
    }
    generateDescriptionSinEdit(id) {
        return getUrl(this.route.generate('api.cp.description.sin.edit-mutation', {id: id}));
    }
    generateDescriptionSinDelete(id) {
        return getUrl(this.route.generate('api.cp.description.sin.delete-mutation', {id: id}));
    }
    // END description Sin
    // START Precept
    generatePrecept(id) {
        return getUrl(this.route.generate('api.cp.precept', {id: id}));
    }
    generatePrecepts() {
        return getUrl(this.route.generate('api.cp.precepts'));
    }
    generatePreceptEdit(id) {
        return getUrl(this.route.generate('api.cp.precept.edit-mutation', {id: id}));
    }
    // END Precept
    // START Book
    generateBook(id) {
        return getUrl(this.route.generate('api.cp.book', {id: id}));
    }
    generateBooks() {
        return getUrl(this.route.generate('api.cp.books'));
    }
    generateBookCreate() {
        return getUrl(this.route.generate('api.cp.book.create-mutation'));
    }
    generateBookEdit(id) {
        return getUrl(this.route.generate('api.cp.book.edit-mutation', {id: id}));
    }
    generateBookDelete(id) {
        return getUrl(this.route.generate('api.cp.book.delete-mutation', {id: id}));
    }
    // END Book
    // START Meta
    generateMetaOne(key) {
        return getUrl(this.route.generate('api.cp.meta.one', {key: key}));
    }
    generateMeta() {
        return getUrl(this.route.generate('api.cp.meta'));
    }
    generateMetaCreate() {
        return getUrl(this.route.generate('api.cp.meta.create-mutation'));
    }
    generateMetaEdit(key) {
        return getUrl(this.route.generate('api.cp.meta.edit-mutation', {key: key}));
    }
    // END Meta
    // START Video
    generateVideoOne(id) {
        return getUrl(this.route.generate('api.cp.video.one', {id: id}));
    }
    generateVideo() {
        return getUrl(this.route.generate('api.cp.video'));
    }
    generateVideoCreate() {
        return getUrl(this.route.generate('api.cp.video.create-mutation'));
    }
    generateVideoEdit(id) {
        return getUrl(this.route.generate('api.cp.video.edit-mutation', {id: id}));
    }
    generateVideoDelete(id) {
        return getUrl(this.route.generate('api.cp.video.delete-mutation', {id: id}));
    }
    // END Video
    // START Music
    generateMusicOne(id) {
        return getUrl(this.route.generate('api.cp.music.one', {id: id}));
    }
    generateMusic() {
        return getUrl(this.route.generate('api.cp.music'));
    }
    generateMusicCreate() {
        return getUrl(this.route.generate('api.cp.music.create-mutation'));
    }
    generateMusicEdit(id) {
        return getUrl(this.route.generate('api.cp.music.edit-mutation', {id: id}));
    }
    generateMusicDelete(id) {
        return getUrl(this.route.generate('api.cp.music.delete-mutation', {id: id}));
    }
    // END Music
    // START Page
    generatePages() {
        return getUrl(this.route.generate('api.cp.pages'));
    }
    generatePagesRootTree() {
        return getUrl(this.route.generate('api.cp.pages.root.tree'));
    }
    generatePageCreate() {
        return getUrl(this.route.generate('api.cp.page.create-mutation'));
    }
    generatePageEdit(id) {
        return getUrl(this.route.generate('api.cp.page.edit-mutation', {id: id}));
    }
    generatePage(id) {
        return getUrl(this.route.generate('api.cp.page', {id: id}));
    }
    generatePageDelete(id) {
        return getUrl(this.route.generate('api.cp.page.delete-mutation', {id: id}));
    }
    // END Page
    // START Schedule
    generateScheduleMonth() {
        return getUrl(this.route.generate('api.cp.schedule.month'));
    }
    generateScheduleWeek() {
        return getUrl(this.route.generate('api.cp.schedule.week'));
    }
    // END Schedule
    // START Schedule
    generateSettings() {
        return getUrl(this.route.generate('api.cp.settings'));
    }
    // END Schedule
    // START Image
    generateImageUpload() {
        return getUrl(this.route.generate('api.cp.image.upload-mutation'));
    }
    // END Image
    // START Dictionaries
    generateDictionaries() {
        return getUrl(this.route.generate('api.cp.dictionaries'));
    }
    generateDictionaryDelete(id) {
        return getUrl(this.route.generate('api.cp.dictionary.delete-mutation', {id: id}));
    }
    generateDictionary(id) {
        return getUrl(this.route.generate('api.cp.dictionary', {id: id}));
    }
    generateDictionaryCreate() {
        return getUrl(this.route.generate('api.cp.dictionary.create-mutation'));
    }
    generateDictionaryEdit(id) {
        return getUrl(this.route.generate('api.cp.dictionary.edit-mutation', {id: id}));
    }
    // END Dictionaries
    // START Symbol
    generateSymbols() {
        return getUrl(this.route.generate('api.cp.symbols'));
    }
    generateSymbolMapGroupName() {
        return getUrl(this.route.generate('api.cp.symbol.map.group.name'));
    }
    // END Symbol
}
