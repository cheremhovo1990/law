import { app } from '@storybook/vue3';
import Pinia from "../assets/vue/src/Services/pinia";
import {mixin} from "@Js/services/mixin";

app.mixin({
    methods: mixin.methods
});
app.use(Pinia.create());

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
