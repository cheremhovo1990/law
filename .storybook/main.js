const path = require('path');

module.exports = {
  "stories": [
    "../assets/vue/**/*.stories.js",
    "../assets/vue/*.stories.js"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    '@storybook/preset-scss'
  ],
  "framework": "@storybook/vue3",
  "core": {
    "builder": "@storybook/builder-webpack5"
  },
  env: function (config) {
    let addEnv = {};
    if (config['APP_ENV'] == 'prod') {
      addEnv = require('dotenv').config({path: '.env.prod'}).parsed;
    }
    return {
      ...config,
      ...addEnv,
      STORYBOOK_ENABLE: 'on',
    }
  },
  webpackFinal: async (config, { configType }) => {
    config.resolve.alias = {
      ...config.resolve.alias,
      '@Js': path.resolve(__dirname, '../assets/js'),
      '@Vue': path.resolve(__dirname, '../assets/vue'),
      '@Assets': path.resolve(__dirname, '../assets'),
    };
    config.resolve.extensions.push(".vue", ".js");

    return config;
  }
}
