<?php

declare(strict_types=1);

namespace App\Exceptions;

class FailedVerificationRuntimeException extends \RuntimeException
{
    public static function verifyNull(): self
    {
        return new self('recaptcha key is null');
    }

    public static function notVerify(): self
    {
        return new self('recaptcha is not verify');
    }
}
