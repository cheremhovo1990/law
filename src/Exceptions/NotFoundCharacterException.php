<?php

declare(strict_types=1);

namespace App\Exceptions;

class NotFoundCharacterException extends \DomainException
{
    public static function create(string $character): self
    {
        return new self(sprintf('not found character %s: %s', $character, mb_ord($character)));
    }
}
