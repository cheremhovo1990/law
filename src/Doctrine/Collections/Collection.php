<?php

declare(strict_types=1);

namespace App\Doctrine\Collections;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\ClosureExpressionVisitor;

/**
 * @implements \Doctrine\Common\Collections\Collection<integer|string, mixed>
 */
class Collection implements \Doctrine\Common\Collections\Collection, CollectionInterface
{
    /**
     * @var array<integer|string, mixed>
     */
    protected array $elements;

    /**
     * @param array<integer|string, mixed> $elements
     */
    public function __construct(array $elements = [])
    {
        $this->elements = $elements;
    }

    /**
     * @return array<integer|string, mixed>
     */
    public function toArray(): array
    {
        return $this->elements;
    }

    public function first(): mixed
    {
        return reset($this->elements);
    }

    /**
     * @param array<integer|string, mixed> $elements
     */
    protected function createFrom(array $elements): self
    {
        return new static($elements);
    }

    public function last(): mixed
    {
        return end($this->elements);
    }

    public function key(): int|string|null
    {
        return key($this->elements);
    }

    public function next(): mixed
    {
        return next($this->elements);
    }

    public function current(): mixed
    {
        return current($this->elements);
    }

    public function remove($key): mixed
    {
        if (!isset($this->elements[$key]) && !array_key_exists($key, $this->elements)) {
            return null;
        }

        $removed = $this->elements[$key];
        unset($this->elements[$key]);

        return $removed;
    }

    public function removeElement($element): bool
    {
        $key = array_search($element, $this->elements, true);

        if (false === $key) {
            return false;
        }

        unset($this->elements[$key]);

        return true;
    }

    public function offsetExists($offset): bool
    {
        return $this->containsKey($offset);
    }

    public function offsetGet(mixed $offset): mixed
    {
        return $this->get($offset);
    }

    public function offsetSet($offset, $value): void
    {
        if (!isset($offset)) {
            $this->add($value);

            return;
        }

        $this->set($offset, $value);
    }

    public function offsetUnset($offset): void
    {
        $this->remove($offset);
    }

    public function containsKey($key): bool
    {
        return isset($this->elements[$key]) || array_key_exists($key, $this->elements);
    }

    /**
     * @return bool
     */
    public function contains(mixed $element)
    {
        return in_array($element, $this->elements, true);
    }

    public function exists(\Closure $p): bool
    {
        foreach ($this->elements as $key => $element) {
            if ($p($key, $element)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param mixed $element
     *
     * @return false|int|string
     */
    public function indexOf($element): int|string|bool
    {
        return array_search($element, $this->elements, true);
    }

    public function get($key): mixed
    {
        return $this->elements[$key] ?? null;
    }

    public function getKeys(): array
    {
        return array_keys($this->elements);
    }

    public function getValues(): array
    {
        return array_values($this->elements);
    }

    public function count(): int
    {
        return count($this->elements);
    }

    public function set($key, $value): void
    {
        $this->elements[$key] = $value;
    }

    public function add($element): bool
    {
        $this->elements[] = $element;

        return true;
    }

    public function isEmpty(): bool
    {
        return empty($this->elements);
    }

    /**
     * @return \ArrayIterator<integer|string, mixed>
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->elements);
    }

    public function map(\Closure $func): self
    {
        return $this->createFrom(array_map($func, $this->elements));
    }

    public function filter(\Closure $p): self
    {
        return $this->createFrom(array_filter($this->elements, $p, ARRAY_FILTER_USE_BOTH));
    }

    public function forAll(\Closure $p): bool
    {
        foreach ($this->elements as $key => $element) {
            if (!$p($key, $element)) {
                return false;
            }
        }

        return true;
    }

    public function partition(\Closure $p): array
    {
        $matches = $noMatches = [];

        foreach ($this->elements as $key => $element) {
            if ($p($key, $element)) {
                $matches[$key] = $element;
            } else {
                $noMatches[$key] = $element;
            }
        }

        return [$this->createFrom($matches), $this->createFrom($noMatches)];
    }

    public function __toString()
    {
        return self::class . '@' . spl_object_hash($this);
    }

    public function clear(): void
    {
        $this->elements = [];
    }

    /**
     * @return array<integer, mixed>|mixed[]
     */
    public function slice(mixed $offset, mixed $length = null): array
    {
        return array_slice($this->elements, $offset, $length, true);
    }

    public function matching(Criteria $criteria): self
    {
        $expr = $criteria->getWhereExpression();
        $filtered = $this->elements;

        if ($expr) {
            $visitor = new ClosureExpressionVisitor();
            $filter = $visitor->dispatch($expr);
            $filtered = array_filter($filtered, $filter);
        }

        $orderings = $criteria->getOrderings();

        if ($orderings) {
            $next = null;
            foreach (array_reverse($orderings) as $field => $ordering) {
                $next = ClosureExpressionVisitor::sortByField($field, Criteria::DESC === $ordering ? -1 : 1, $next);
            }

            uasort($filtered, $next);
        }

        $offset = $criteria->getFirstResult();
        $length = $criteria->getMaxResults();

        if ($offset || $length) {
            $filtered = array_slice($filtered, (int) $offset, $length);
        }

        return $this->createFrom($filtered);
    }
}
