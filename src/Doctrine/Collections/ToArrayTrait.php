<?php

declare(strict_types=1);

namespace App\Doctrine\Collections;

trait ToArrayTrait
{
    public function toArray(): array
    {
        $result = [];
        foreach ($this->elements as $element) {
            $result[] = $element->toArray();
        }

        return $result;
    }
}
