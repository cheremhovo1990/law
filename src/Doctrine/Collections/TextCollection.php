<?php

declare(strict_types=1);

namespace App\Doctrine\Collections;

use App\Doctrine\Models\Text;
use App\Services\ToArrayInterface;

class TextCollection extends Collection implements ToArrayInterface
{
    use CollectionTrait;
    use ToArrayTrait;

    public function __construct(array $elements = [])
    {
        $elements = array_map(function (array $element) {
            return new Text(
                text: $element['text'],
            );
        }, $elements);
        parent::__construct($elements);
    }
}
