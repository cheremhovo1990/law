<?php

declare(strict_types=1);

namespace App\Doctrine\Collections;

use App\Services\ToArrayInterface;

trait CollectionTrait
{
    /**
     * @param array<integer, mixed> $value
     */
    public function createNewOrReturnThis(array $value): static
    {
        $collection = null;
        if (count($value) !== $this->count()) {
            $collection = new static($value);
        } else {
            foreach ($value as $item) {
                if (!$this->exists(function ($key, ToArrayInterface $link) use ($item) {
                    return $link->toArray() == $item;
                })) {
                    $collection = new static($value);
                    break;
                }
            }
        }
        if (null === $collection) {
            return $this;
        }

        return $collection;
    }
}
