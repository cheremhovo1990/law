<?php

declare(strict_types=1);

namespace App\Doctrine\Collections;

interface CollectionInterface
{
    /**
     * @param array<integer, array<string, mixed>> $elements
     */
    public function __construct(array $elements = []);
}
