<?php

declare(strict_types=1);

namespace App\Doctrine\Collections;

use App\Doctrine\Models\BookLink;
use App\Services\ToArrayInterface;

class BookLinkCollection extends Collection implements ToArrayInterface
{
    use CollectionTrait;
    use ToArrayTrait;

    /**
     * @param array<int, mixed> $elements
     */
    public function __construct(array $elements = [])
    {
        $elements = array_map(function (BookLink|array $element): BookLink {
            if ($element instanceof BookLink) {
                return $element;
            }

            return new BookLink(
                link: $element['link'],
                type: $element['type'],
            );
        }, $elements);
        parent::__construct($elements);
    }
}
