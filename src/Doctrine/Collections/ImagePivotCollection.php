<?php

declare(strict_types=1);

namespace App\Doctrine\Collections;

use App\Doctrine\Exceptions\CannotConvertToTypeRuntimeException;
use App\Doctrine\Models\ImagePivot;
use App\Services\IdInterface;
use App\Services\ToArrayInterface;

/**
 * @implements \IteratorAggregate<integer, ImagePivot>
 */
class ImagePivotCollection implements ToArrayInterface, \Countable, \IteratorAggregate
{
    /** @var array|ImagePivot[] */
    private array $elements = [];

    /**
     * @param array<integer, mixed> $elements
     */
    public function __construct(array $elements = [])
    {
        $this->elements = array_map(function (array $element): ImagePivot {
            return new ImagePivot(
                id: $element['id'],
                type: $element['type'],
            );
        }, $elements);
    }

    /**
     * @return \ArrayIterator<integer, ImagePivot>|ImagePivot[]
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->elements);
    }

    public function contains(IdInterface $element): bool
    {
        foreach ($this as $pivot) {
            if ($pivot->getId() == (string) $element->getId() && $pivot->getType() === self::getType($element)) {
                return true;
            }
        }

        return false;
    }

    public function push(IdInterface $object): self
    {
        $this->elements[] = new ImagePivot(
            id: (string) $object->getId(),
            type: self::getType($object),
        );

        return $this;
    }

    public function toArray(): array
    {
        $result = [];
        foreach ($this->elements as $element) {
            $result[] = $element->toArray();
        }

        return $result;
    }

    public function count(): int
    {
        return count($this->elements);
    }

    public static function getType(object|string $class): string
    {
        if (is_object($class)) {
            $class = get_class($class);
        }
        if (!class_exists($class)) {
            throw new CannotConvertToTypeRuntimeException();
        }

        return basename(str_replace('\\', '/', $class));
    }
}
