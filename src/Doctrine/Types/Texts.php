<?php

declare(strict_types=1);

namespace App\Doctrine\Types;

use App\Doctrine\Collections\TextCollection;
use App\Helpers\JsonHelper;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

class Texts extends JsonType
{
    public const NAME = 'json_texts';

    public function convertToPHPValue($value, AbstractPlatform $platform): TextCollection
    {
        if (null == $value) {
            return new TextCollection();
        }

        $val = JsonHelper::decode($value);

        return new TextCollection($val);
    }

    public function convertToDatabaseValue(mixed $value, AbstractPlatform $platform): null|string
    {
        if (!$value instanceof TextCollection || !count($value)) {
            return null;
        }
        $data = $value->toArray();

        return JsonHelper::encode($data);
    }

    /**
     * @codeCoverageIgnore
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
