<?php

declare(strict_types=1);

namespace App\Doctrine\Types;

use App\Doctrine\Models\Image as ImageModel;
use App\Helpers\JsonHelper;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

class Image extends JsonType
{
    public const NAME = 'json_image';

    public function convertToPHPValue($value, AbstractPlatform $platform): null|ImageModel
    {
        if (null == $value) {
            return null;
        }

        $val = JsonHelper::decode($value);

        return new ImageModel(
            $val['id'],
            $val['name'],
        );
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): null|string
    {
        if (!$value instanceof ImageModel) {
            return null;
        }
        $data = $value->toArray();

        return JsonHelper::encode($data);
    }

    /**
     * @codeCoverageIgnore
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @codeCoverageIgnore
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
