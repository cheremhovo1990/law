<?php

declare(strict_types=1);

namespace App\Doctrine\Types;

use App\Doctrine\Exceptions\CannotConvertToDatabaseValueException;
use App\Doctrine\Models\Meta as MetaModel;
use App\Helpers\JsonHelper;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

class Meta extends JsonType
{
    public const NAME = 'json_meta';

    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        $val = JsonHelper::decode($value);

        return new MetaModel(
            $val['title'] ?? '',
            $val['description'] ?? '',
        );
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        if (!$value instanceof MetaModel) {
            throw new CannotConvertToDatabaseValueException();
        }
        $data = $value->toArray();

        return JsonHelper::encode($data);
    }

    /**
     * @codeCoverageIgnore
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @codeCoverageIgnore
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
