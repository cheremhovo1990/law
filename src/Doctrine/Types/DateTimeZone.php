<?php

declare(strict_types=1);

namespace App\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class DateTimeZone extends StringType
{
    public const NAME = 'date_time_zone';

    public function convertToPHPValue($value, AbstractPlatform $platform): null|\DateTimeZone
    {
        if (null == $value) {
            return null;
        }

        return new \DateTimeZone($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): null|string
    {
        if (!$value instanceof \DateTimeZone) {
            return null;
        }

        return $value->getName();
    }

    /**
     * @codeCoverageIgnore
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @codeCoverageIgnore
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
