<?php

declare(strict_types=1);

namespace App\Doctrine\Types;

use App\Doctrine\Collections\BookLinkCollection;
use App\Helpers\JsonHelper;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

class BookLinks extends JsonType
{
    public const NAME = 'json_book_links';

    public function convertToPHPValue($value, AbstractPlatform $platform): BookLinkCollection
    {
        if (null == $value) {
            return new BookLinkCollection();
        }

        $val = JsonHelper::decode($value);

        return new BookLinkCollection($val);
    }

    public function convertToDatabaseValue(mixed $value, AbstractPlatform $platform): null|string
    {
        if (!$value instanceof BookLinkCollection || !count($value)) {
            return null;
        }
        $data = $value->toArray();

        return JsonHelper::encode($data);
    }

    /**
     * @codeCoverageIgnore
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
