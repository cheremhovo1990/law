<?php

declare(strict_types=1);

namespace App\Doctrine\Types;

use App\Doctrine\Collections\ImagePivotCollection;
use App\Helpers\JsonHelper;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

class ImagePivots extends JsonType
{
    public const NAME = 'json_image_pivots';

    public function convertToPHPValue($value, AbstractPlatform $platform): ImagePivotCollection
    {
        if (null == $value) {
            return new ImagePivotCollection();
        }

        $val = JsonHelper::decode($value);

        return new ImagePivotCollection($val);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): null|string
    {
        if (!$value instanceof ImagePivotCollection || !count($value)) {
            return null;
        }
        $data = $value->toArray();

        return JsonHelper::encode($data);
    }

    /**
     * @codeCoverageIgnore
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
