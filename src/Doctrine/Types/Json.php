<?php

declare(strict_types=1);

namespace App\Doctrine\Types;

use App\Helpers\JsonHelper;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

class Json extends JsonType
{
    public function convertToDatabaseValue($value, AbstractPlatform $platform): null|string
    {
        if (null === $value) {
            return null;
        }

        return JsonHelper::encode($value);
    }
}
