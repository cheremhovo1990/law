<?php

declare(strict_types=1);

namespace App\Doctrine\Models;

use App\Controller\Api\Book\BookQueryController;
use App\Controller\Api\Book\BooksQueryController;
use App\Controller\Api\Cp\BookQueryController as BookQueryControllerCp;
use App\Controller\Api\Cp\BooksQueryController as BooksQueryControllerCp;
use App\Services\ToArrayInterface;
use Symfony\Component\Serializer\Annotation\Groups;

class Text implements ToArrayInterface
{
    public function __construct(
        protected string $text,
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return [
            'text' => $this->text,
        ];
    }

    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    public function getText(): string
    {
        return $this->text;
    }
}
