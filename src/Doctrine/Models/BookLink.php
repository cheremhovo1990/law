<?php

declare(strict_types=1);

namespace App\Doctrine\Models;

use App\Controller\Api\Book\BookQueryController;
use App\Controller\Api\Book\BooksQueryController;
use App\Controller\Api\Cp\BookQueryController as BookQueryControllerCp;
use App\Controller\Api\Cp\BooksQueryController as BooksQueryControllerCp;
use App\Services\ToArrayInterface;
use Symfony\Component\Serializer\Annotation\Groups;

class BookLink implements ToArrayInterface
{
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected BookLinkTypeEnum $type;

    public function __construct(
        protected string $link,
        string $type,
    ) {
        $this->type = BookLinkTypeEnum::from($type);
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return [
            'link' => $this->link,
            'type' => $this->type->value,
        ];
    }

    public function isTypeBuy(): bool
    {
        return $this->type->value === BookLinkTypeEnum::BUY->value;
    }

    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    public function getLink(): string
    {
        return $this->link;
    }

    public function getType(): string
    {
        return $this->type->value;
    }
}
