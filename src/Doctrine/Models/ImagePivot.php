<?php

declare(strict_types=1);

namespace App\Doctrine\Models;

use App\Services\ToArrayInterface;

class ImagePivot implements ToArrayInterface
{
    public function __construct(
        protected string|int $id,
        protected string $type,
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
        ];
    }

    public function getId(): string|int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
