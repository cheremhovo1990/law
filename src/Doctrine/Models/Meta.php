<?php

declare(strict_types=1);

namespace App\Doctrine\Models;

use App\Controller\Api\Book\BookQueryController;
use App\Controller\Api\Cp\BookQueryController as BookQueryControllerCp;
use App\Controller\Api\Cp\DescriptionSinQueryController as DescriptionSinQueryControllerCp;
use App\Controller\Api\Cp\MusicOneQueryController;
use App\Controller\Api\Cp\PageQueryController;
use App\Controller\Api\Cp\PagesQueryController;
use App\Controller\Api\Cp\VideoOneQueryController;
use App\Controller\Api\DescriptionSinQueryController;
use App\Controller\Api\PreceptsQueryController;
use Symfony\Component\Serializer\Annotation\Groups;

class Meta
{
    public function __construct(
        protected string $title = '',
        protected string $description = '',
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
        ];
    }

    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        VideoOneQueryController::class,
        DescriptionSinQueryControllerCp::class,
        BookQueryControllerCp::class,
        BookQueryController::class,
        MusicOneQueryController::class,
        PageQueryController::class,
        PagesQueryController::class,
    ])]
    public function getTitle(): string
    {
        return $this->title;
    }

    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        VideoOneQueryController::class,
        DescriptionSinQueryControllerCp::class,
        BookQueryControllerCp::class,
        BookQueryController::class,
        MusicOneQueryController::class,
        PageQueryController::class,
        PagesQueryController::class,
    ])]
    public function getDescription(): string
    {
        return $this->description;
    }
}
