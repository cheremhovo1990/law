<?php

declare(strict_types=1);

namespace App\Doctrine\Models;

use App\Controller\Api\Book\BookQueryController;
use App\Controller\Api\Book\BooksQueryController;
use App\Controller\Api\Cp\BookQueryController as BookQueryControllerCp;
use App\Controller\Api\Cp\BooksQueryController as BooksQueryControllerCp;
use Symfony\Component\Serializer\Annotation\Groups;

class Image
{
    public function __construct(
        protected string $id,
        protected string $name,
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }

    #[Groups([
        BookQueryController::class,
        BooksQueryController::class,
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
    ])]
    public function getId(): string
    {
        return $this->id;
    }

    #[Groups([
        BookQueryController::class,
        BooksQueryController::class,
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
    ])]
    public function getName(): string
    {
        return $this->name;
    }
}
