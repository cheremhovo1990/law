<?php

declare(strict_types=1);

namespace App\Doctrine\Models;

enum BookLinkTypeEnum: string
{
    case UNKNOWN = 'unknown';
    case BUY = 'buy';

    /**
     * @return array<string, string>
     */
    public static function getChoices(): array
    {
        return [
            self::UNKNOWN->value => self::UNKNOWN->value,
            self::BUY->value => self::BUY->value,
        ];
    }
}
