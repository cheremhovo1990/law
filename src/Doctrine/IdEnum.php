<?php

declare(strict_types=1);

namespace App\Doctrine;

enum IdEnum: string
{
    case ULID = 'ulid';
}
