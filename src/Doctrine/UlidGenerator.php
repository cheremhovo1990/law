<?php

declare(strict_types=1);

namespace App\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Symfony\Component\Uid\Factory\UlidFactory;

class UlidGenerator extends AbstractIdGenerator
{
    private UlidFactory $factory;

    public function __construct()
    {
        $this->factory = new UlidFactory();
    }

    public function generateId(EntityManagerInterface $em, $entity): mixed
    {
        try {
            if (method_exists($entity, 'getId')) {
                return $entity->getId();
            }
        } catch (\Error) {
            return $this->factory->create();
        }

        return $this->factory->create();
    }
}
