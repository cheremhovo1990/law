<?php

declare(strict_types=1);

namespace App\Doctrine\Exceptions;

class CannotConvertToTypeRuntimeException extends \RuntimeException
{
}
