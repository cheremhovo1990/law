<?php

declare(strict_types=1);

namespace App\Doctrine\Exceptions;

class CannotConvertToDatabaseValueException extends \RuntimeException
{
}
