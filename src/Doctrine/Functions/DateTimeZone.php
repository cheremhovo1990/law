<?php

declare(strict_types=1);

namespace App\Doctrine\Functions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class DateTimeZone extends FunctionNode
{
    public mixed $dateTime;
    public mixed $timeZone;

    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->dateTime = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->timeZone = $parser->StringPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(SqlWalker $sqlWalker): string
    {
        return sprintf(
            '%s::timestamptz AT TIME ZONE %s',
            $this->dateTime->dispatch($sqlWalker),
            $this->timeZone->dispatch($sqlWalker),
        );
    }
}
