<?php

declare(strict_types=1);

namespace App\Doctrine;

use App\Controller\Api\Book\BookQueryController;
use App\Controller\Api\Book\BooksQueryController;
use App\Controller\Api\Cp\BookQueryController as BookQueryControllerCp;
use App\Controller\Api\Cp\BooksQueryController as BooksQueryControllerCp;
use App\Controller\Api\Cp\DescriptionSinQueryController as CpDescriptionSinQueryController;
use App\Controller\Api\Cp\DescriptionSinsQueryController;
use App\Controller\Api\Cp\DictionariesQueryController;
use App\Controller\Api\Cp\DictionaryQueryController;
use App\Controller\Api\Cp\FeedbacksQueryController;
use App\Controller\Api\Cp\MusicOneQueryController;
use App\Controller\Api\Cp\MusicQueryController;
use App\Controller\Api\Cp\PageQueryController;
use App\Controller\Api\Cp\PagesQueryController;
use App\Controller\Api\Cp\PagesRootTreeController;
use App\Controller\Api\Cp\PreceptQueryController;
use App\Controller\Api\Cp\PreceptsQueryController as PreceptsQueryControllerCp;
use App\Controller\Api\Cp\SinQueryController;
use App\Controller\Api\Cp\SinsQueryController;
use App\Controller\Api\Cp\SymbolsQueryController;
use App\Controller\Api\Cp\VideoOneQueryController;
use App\Controller\Api\Cp\VideoQueryController;
use App\Controller\Api\DescriptionSinQueryController;
use App\Controller\Api\PreceptsQueryController;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\AbstractUid;

trait UlidTrait
{
    #[ORM\Id]
    #[ORM\Column(type: 'ulid', unique: true, nullable: false)]
    #[ORM\GeneratedValue('CUSTOM')]
    #[ORM\CustomIdGenerator(class: UlidGenerator::class)]
    #[Groups([
        MusicQueryController::class,
        MusicOneQueryController::class,
        VideoQueryController::class,
        VideoOneQueryController::class,
        FeedbacksQueryController::class,
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        PreceptsQueryControllerCp::class,
        SinsQueryController::class,
        SinQueryController::class,
        PreceptQueryController::class,
        CpDescriptionSinQueryController::class,
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        DescriptionSinsQueryController::class,
        CpDescriptionSinQueryController::class,
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        SinsQueryController::class,
        SinQueryController::class,
        DescriptionSinQueryController::class,
        CpDescriptionSinQueryController::class,
        DictionariesQueryController::class,
        PageQueryController::class,
        PagesQueryController::class,
        PagesRootTreeController::class,
        SymbolsQueryController::class,
        DictionaryQueryController::class,
    ])]
    protected AbstractUid $id;

    public function setId(AbstractUid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): AbstractUid
    {
        return $this->id;
    }
}
