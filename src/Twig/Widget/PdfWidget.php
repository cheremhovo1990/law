<?php

declare(strict_types=1);

namespace App\Twig\Widget;

use App\Controller\MainPdfController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class PdfWidget extends AbstractExtension
{
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(
        protected string $url,
        UrlGeneratorInterface $urlGenerator,
    ) {
        $this->urlGenerator = $urlGenerator;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('pdf_widget', [$this, 'render'], ['is_safe' => ['html']]),
        ];
    }

    public function render(): string
    {
        return sprintf('%s?url=%s', $this->url, $this->urlGenerator->generate(MainPdfController::NAME, referenceType: UrlGeneratorInterface::ABSOLUTE_URL));
    }
}
