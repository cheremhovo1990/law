<?php

declare(strict_types=1);

namespace App\Twig\Widget;

use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MenuLinkActiveWidget extends AbstractExtension
{
    private RequestStack $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_active_widget', [$this, 'getActive'], ['is_safe' => ['html']]),
        ];
    }

    public function getActive(string $route): string
    {
        $routeCurrent = $this->requestStack->getCurrentRequest()->attributes->get('_route');
        if ($routeCurrent === $route) {
            return 'active';
        }

        return '';
    }
}
