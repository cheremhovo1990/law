<?php

declare(strict_types=1);

namespace App\Twig\Widget;

use App\Form\Types\FeedbackType;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FeedbackWidget extends AbstractExtension
{
    public const NAME_CAPTCHA_SITE_TOKEN_KEY = 'captcha-site-key';
    private Environment $twig;
    private FormFactoryInterface $formFactory;
    private ParameterBagInterface $parameterBag;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        Environment $twig,
        FormFactoryInterface $formFactory,
        EventDispatcherInterface $eventDispatcher,
        ParameterBagInterface $parameterBag,
    ) {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->parameterBag = $parameterBag;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('feedback_modal_widget', [$this, 'renderModal'], ['is_safe' => ['html']]),
            new TwigFunction('feedback_trigger_widget', [$this, 'trigger'], ['is_safe' => ['html']]),
        ];
    }

    public function renderModal(): string
    {
        $form = $this->formFactory->create(FeedbackType::class);

        $this->eventDispatcher->addListener(KernelEvents::RESPONSE, function (ResponseEvent $event): void {
            $event->getResponse()->headers->setCookie(new Cookie(
                self::NAME_CAPTCHA_SITE_TOKEN_KEY,
                $this->parameterBag->get('captchaSiteKey'),
                httpOnly: false,
            ));
        });

        return $this->twig->render('widgets/feedback.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function trigger(): string
    {
        return 'data-bs-toggle="modal" data-bs-target="#feedback-modal"';
    }
}
