<?php

declare(strict_types=1);

namespace App\Twig\Widget;

use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FlashWidget extends AbstractExtension
{
    private Environment $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function getFunctions(): array
    {
        return [new TwigFunction('flash_widget', [$this, 'render'], ['is_safe' => ['html']])];
    }

    public function render(): string
    {
        return $this->twig->render('widgets/flashes.html.twig');
    }
}
