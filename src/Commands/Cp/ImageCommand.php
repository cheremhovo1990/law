<?php

declare(strict_types=1);

namespace App\Commands\Cp;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageCommand
{
    protected UploadedFile $image;

    public function getImage(): UploadedFile
    {
        return $this->image;
    }

    public function setImage(UploadedFile $image): void
    {
        $this->image = $image;
    }
}
