<?php

declare(strict_types=1);

namespace App\Commands\Cp;

use App\Doctrine\Models\Meta;
use App\Entity\Page;
use App\Services\Meta\MetaCommandInterface;
use App\Services\Meta\MetaCommandTrait;
use App\Services\Publication\PublicationCommandTrait;
use App\Services\Publication\PublishCommandInterface;

class PageCommand implements PublishCommandInterface, MetaCommandInterface
{
    use MetaCommandTrait;
    use PublicationCommandTrait;

    public function __construct(
        protected string $content = '',
        protected string $title = '',
        protected null|Page $parent = null,
        bool $publish = false,
        Meta $meta = null,
    ) {
        $this->publish = $publish;
        if (null !== $meta) {
            $this->meta = $meta->toArray();
        }
    }

    public static function create(Page $page): self
    {
        return new self(
            content: $page->getContent(),
            title: $page->getTitle(),
            parent: $page->getParent(),
            publish: $page->isPublish(),
            meta: $page->getMeta(),
        );
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getParent(): ?Page
    {
        return $this->parent;
    }

    public function setParent(?Page $parent): void
    {
        $this->parent = $parent;
    }
}
