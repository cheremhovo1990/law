<?php

declare(strict_types=1);

namespace App\Commands\Cp;

use App\Doctrine\Models\Meta;
use App\Entity\DescriptionSin;
use App\Entity\Precept;
use App\Services\Meta\MetaCommandInterface;
use App\Services\Meta\MetaCommandTrait;

class DescriptionSinCommand implements MetaCommandInterface
{
    use MetaCommandTrait;

    /**
     * @param array<integer, string> $sins
     */
    public function __construct(
        protected string $name = '',
        protected string $text = '',
        protected ?Precept $precept = null,
        null|Meta $meta = null,
        protected array $sins = [],
    ) {
        if (null !== $meta) {
            $this->meta = $meta->toArray();
        }
    }

    public static function create(
        DescriptionSin $descriptionSin,
    ): self {
        $ids = [];
        foreach ($descriptionSin->getSins() as $sin) {
            $ids[] = (string) $sin->getId();
        }

        return new self(
            $descriptionSin->getName(),
            $descriptionSin->getText(),
            $descriptionSin->getPrecept(),
            $descriptionSin->getMeta(),
            $ids,
        );
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getPrecept(): ?Precept
    {
        return $this->precept;
    }

    public function setPrecept(?Precept $precept): void
    {
        $this->precept = $precept;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array<integer, string>
     */
    public function getSins(): array
    {
        return $this->sins;
    }

    /**
     * @param array<integer, string> $sins
     */
    public function setSins(array $sins): void
    {
        $this->sins = $sins;
    }
}
