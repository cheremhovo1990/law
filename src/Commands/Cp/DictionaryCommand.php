<?php

declare(strict_types=1);

namespace App\Commands\Cp;

use App\Entity\Dictionary;

class DictionaryCommand
{
    public function __construct(
        protected string $name = '',
        protected string $description = '',
        protected bool $titlo = false,
        protected null|string $normalizeName = null,
    ) {
    }

    public static function create(Dictionary $dictionary): self
    {
        return new self(
            $dictionary->getName(),
            $dictionary->getDescription(),
            $dictionary->isTitlo(),
            $dictionary->getNormalizeName(),
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function isTitlo(): bool
    {
        return $this->titlo;
    }

    public function setTitlo(bool $titlo): void
    {
        $this->titlo = $titlo;
    }

    public function getNormalizeName(): ?string
    {
        return $this->normalizeName;
    }

    public function setNormalizeName(?string $normalizeName): void
    {
        $this->normalizeName = $normalizeName;
    }
}
