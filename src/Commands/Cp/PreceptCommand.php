<?php

declare(strict_types=1);

namespace App\Commands\Cp;

use App\Entity\Precept;

class PreceptCommand
{
    public function __construct(
        protected string $link = '',
        protected string $verse = '',
    ) {
    }

    public static function create(Precept $precept): self
    {
        return new self(
            $precept->getLink(),
            $precept->getVerse(),
        );
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function getVerse(): string
    {
        return $this->verse;
    }

    public function setVerse(string $verse): void
    {
        $this->verse = $verse;
    }
}
