<?php

declare(strict_types=1);

namespace App\Commands\Cp;

use App\Doctrine\Collections\BookLinkCollection;
use App\Doctrine\Collections\TextCollection;
use App\Doctrine\Models\Image;
use App\Doctrine\Models\Meta;
use App\Entity\Book;
use App\Services\Meta\MetaCommandInterface;
use App\Services\Meta\MetaCommandTrait;
use App\Services\Publication\PublishCommandInterface;

class BookCommand implements PublishCommandInterface, MetaCommandInterface
{
    use MetaCommandTrait;

    /**
     * @var array<string, string>
     */
    protected array $coverImage = [];
    /**
     * @var array<integer, array<string, string>>
     */
    protected array $links;
    /**
     * @var array<integer, array<string, string>>
     */
    protected array $isbnCollection;

    public function __construct(
        protected string $name = '',
        BookLinkCollection $links = null,
        protected string $description = '',
        TextCollection $isbnCollection = null,
        protected bool $blessedByPatriarchKirill = false,
        protected bool $recommendedByPublishingCouncilRoc = false,
        protected bool $recommendedByPublishingCouncilBoc = false,
        protected null|string $codeOfPublishingCouncil = null,
        protected null|\DateTimeInterface $dateOfWriting = null,
        protected null|\DateTimeInterface $dateOfPublished = null,
        protected null|int $pageSize = null,
        Image $coverImage = null,
        protected bool $publish = false,
        Meta $meta = null,
        protected string $languageOfThePublication = '',
    ) {
        if (null === $links) {
            $links = new BookLinkCollection();
        }
        if (null === $isbnCollection) {
            $isbnCollection = new TextCollection();
        }
        $this->links = $links->toArray();
        $this->isbnCollection = $isbnCollection->toArray();
        if (null !== $coverImage) {
            $this->coverImage = $coverImage->toArray();
        }
        if (null !== $meta) {
            $this->meta = $meta->toArray();
        }
    }

    public static function create(Book $book): self
    {
        return new self(
            name: $book->getName(),
            links: $book->getLinks(),
            description: $book->getDescription(),
            isbnCollection: $book->getIsbnCollection(),
            blessedByPatriarchKirill: $book->isBlessedByPatriarchKirill(),
            recommendedByPublishingCouncilRoc: $book->isRecommendedByPublishingCouncilRoc(),
            recommendedByPublishingCouncilBoc: $book->isRecommendedByPublishingCouncilBoc(),
            codeOfPublishingCouncil: $book->getCodeOfPublishingCouncil(),
            dateOfWriting: $book->getDateOfWriting(),
            dateOfPublished: $book->getDateOfPublished(),
            pageSize: $book->getPageSize(),
            coverImage: $book->getCoverImage(),
            publish: $book->isPublish(),
            meta: $book->getMeta(),
            languageOfThePublication: $book->getLanguageOfThePublication(),
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array<integer, mixed>
     */
    public function getLinks(): array
    {
        return $this->links;
    }

    /**
     * @param array<integer, array<string, string>> $links
     */
    public function setLinks(array $links): void
    {
        $this->links = $links;
    }

    /**
     * @return array<integer, array<string, string>>
     */
    public function getIsbnCollection(): array
    {
        return $this->isbnCollection;
    }

    /**
     * @param array<integer, array<string, string>> $isbnCollection
     */
    public function setIsbnCollection(array $isbnCollection): void
    {
        $this->isbnCollection = $isbnCollection;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDateOfWriting(): ?\DateTimeInterface
    {
        return $this->dateOfWriting;
    }

    public function setDateOfWriting(?\DateTimeInterface $dateOfWriting): void
    {
        $this->dateOfWriting = $dateOfWriting;
    }

    public function getPageSize(): ?int
    {
        return $this->pageSize;
    }

    public function setPageSize(?int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return array<string, string>
     */
    public function getCoverImage(): array
    {
        return $this->coverImage;
    }

    /**
     * @param array<string, string> $coverImage
     */
    public function setCoverImage(array $coverImage): void
    {
        $this->coverImage = $coverImage;
    }

    public function isPublish(): bool
    {
        return $this->publish;
    }

    public function setPublish(bool $publish): void
    {
        $this->publish = $publish;
    }

    public function isBlessedByPatriarchKirill(): bool
    {
        return $this->blessedByPatriarchKirill;
    }

    public function isRecommendedByPublishingCouncilRoc(): bool
    {
        return $this->recommendedByPublishingCouncilRoc;
    }

    public function getCodeOfPublishingCouncil(): null|string
    {
        return $this->codeOfPublishingCouncil;
    }

    public function setBlessedByPatriarchKirill(bool $blessedByPatriarchKirill): self
    {
        $this->blessedByPatriarchKirill = $blessedByPatriarchKirill;

        return $this;
    }

    public function setRecommendedByPublishingCouncilRoc(bool $recommendedByPublishingCouncilRoc): self
    {
        $this->recommendedByPublishingCouncilRoc = $recommendedByPublishingCouncilRoc;

        return $this;
    }

    public function setCodeOfPublishingCouncil(null|string $codeOfPublishingCouncil): self
    {
        $this->codeOfPublishingCouncil = $codeOfPublishingCouncil;

        return $this;
    }

    public function getDateOfPublished(): ?\DateTimeInterface
    {
        return $this->dateOfPublished;
    }

    public function setDateOfPublished(?\DateTimeInterface $dateOfPublished): void
    {
        $this->dateOfPublished = $dateOfPublished;
    }

    public function isRecommendedByPublishingCouncilBoc(): bool
    {
        return $this->recommendedByPublishingCouncilBoc;
    }

    public function setRecommendedByPublishingCouncilBoc(bool $recommendedByPublishingCouncilBoc): void
    {
        $this->recommendedByPublishingCouncilBoc = $recommendedByPublishingCouncilBoc;
    }

    public function getLanguageOfThePublication(): string
    {
        return $this->languageOfThePublication;
    }

    public function setLanguageOfThePublication(string $languageOfThePublication): void
    {
        $this->languageOfThePublication = $languageOfThePublication;
    }
}
