<?php

declare(strict_types=1);

namespace App\Commands\Cp;

use App\Doctrine\Models\Meta;
use App\Entity\Music;
use App\Services\Meta\MetaCommandInterface;
use App\Services\Meta\MetaCommandTrait;
use App\Services\Publication\PublicationCommandTrait;
use App\Services\Publication\PublishCommandInterface;

class MusicCommand implements PublishCommandInterface, MetaCommandInterface
{
    use MetaCommandTrait;
    use PublicationCommandTrait;

    public function __construct(
        protected string $title = '',
        protected string $link = '',
        protected string $description = '',
        bool $publish = false,
        Meta $meta = null,
    ) {
        $this->publish = $publish;
        if (null !== $meta) {
            $this->meta = $meta->toArray();
        }
    }

    public static function create(Music $entity): self
    {
        return new self(
            $entity->getTitle(),
            $entity->getLink(),
            $entity->getDescription(),
            $entity->isPublish(),
            $entity->getMeta(),
        );
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}
