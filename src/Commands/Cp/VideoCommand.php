<?php

declare(strict_types=1);

namespace App\Commands\Cp;

use App\Doctrine\Models\Meta;
use App\Entity\Video;
use App\Services\Meta\MetaCommandInterface;
use App\Services\Meta\MetaCommandTrait;
use App\Services\Publication\PublicationCommandTrait;
use App\Services\Publication\PublishCommandInterface;

class VideoCommand implements PublishCommandInterface, MetaCommandInterface
{
    use MetaCommandTrait;
    use PublicationCommandTrait;

    public function __construct(
        protected string $title = '',
        protected string $link = '',
        protected string $description = '',
        bool $publish = false,
        Meta $meta = null,
    ) {
        $this->publish = $publish;
        if (null !== $meta) {
            $this->meta = $meta->toArray();
        }
    }

    public static function create(Video $video): self
    {
        return new self(
            $video->getTitle(),
            $video->getLink(),
            $video->getDescription(),
            $video->isPublish(),
            $video->getMeta(),
        );
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function isPublish(): bool
    {
        return $this->publish;
    }

    public function setPublish(bool $publish): void
    {
        $this->publish = $publish;
    }
}
