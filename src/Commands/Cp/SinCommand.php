<?php

declare(strict_types=1);

namespace App\Commands\Cp;

use App\Entity\Precept;
use App\Entity\Sin;
use App\Services\Publication\PublishCommandInterface;

class SinCommand implements PublishCommandInterface
{
    public function __construct(
        protected string $name = '',
        protected null|string $nameSlavonic = null,
        protected ?Precept $precept = null,
        protected ?int $position = null,
        protected string $popoverContent = '',
        protected bool $isPublishedAt = false,
    ) {
    }

    public static function create(Sin $sin): self
    {
        return new self(
            $sin->getName(),
            $sin->getNameSlavonic(),
            $sin->getPrecept(),
            $sin->getPosition(),
            $sin->getPopoverContent(),
            $sin->getPublishedAt() instanceof \DateTimeInterface ? true : false,
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): void
    {
        $this->position = $position;
    }

    public function getPrecept(): ?Precept
    {
        return $this->precept;
    }

    public function setPrecept(?Precept $precept): self
    {
        $this->precept = $precept;

        return $this;
    }

    public function getPopoverContent(): string
    {
        return $this->popoverContent;
    }

    public function setPopoverContent(string $popoverContent): void
    {
        $this->popoverContent = $popoverContent;
    }

    public function isPublish(): bool
    {
        return $this->isPublishedAt;
    }

    public function setIsPublish(bool $isPublishedAt): void
    {
        $this->isPublishedAt = $isPublishedAt;
    }

    public function getNameSlavonic(): null|string
    {
        return $this->nameSlavonic;
    }

    public function setNameSlavonic(null|string $nameSlavonic = null): void
    {
        $this->nameSlavonic = $nameSlavonic;
    }
}
