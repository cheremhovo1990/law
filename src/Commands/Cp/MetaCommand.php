<?php

declare(strict_types=1);

namespace App\Commands\Cp;

use App\Entity\Meta;

class MetaCommand
{
    public function __construct(
        protected string $key = '',
        protected string $title = '',
        protected string $description = '',
    ) {
    }

    public static function create(Meta $meta): self
    {
        return new self(
            $meta->getKey(),
            $meta->getTitle(),
            $meta->getDescription(),
        );
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): void
    {
        $this->key = $key;
    }
}
