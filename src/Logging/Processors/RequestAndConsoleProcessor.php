<?php

declare(strict_types=1);

namespace App\Logging\Processors;

use Doctrine\Common\Collections\ArrayCollection;
use Monolog\LogRecord;

class RequestAndConsoleProcessor
{
    protected string $sapi;
    /**
     * @var array<string|mixed>
     */
    protected array $server;

    /**
     * @param array<string|mixed>|null $server
     */
    public function __construct(
        string $sapi = null,
        array $server = null,
    ) {
        $this->sapi = $sapi ?? PHP_SAPI;
        $this->server = $server ?? $_SERVER;
    }

    public function __invoke(LogRecord $record): LogRecord
    {
        if ('cli' === $this->sapi) {
            $record = $this->console($record);
        } else {
            $record = $this->http($record);
        }

        return $record;
    }

    protected function http(LogRecord $record): LogRecord
    {
        $extra['app'] = 'http';
        $extra['REQUEST_URI'] = $this->server['REQUEST_URI'];
        $extra['REQUEST_METHOD'] = $this->server['REQUEST_METHOD'];
        $extra['HTTP_HOST'] = $this->server['HTTP_HOST'];
        $extra['GET'] = $_GET;
        $extra['APP_ENV'] = $this->server['APP_ENV'];
        $extra['extra']['HTTP_X_FORWARDED_FOR'] = $this->server['HTTP_X_FORWARDED_FOR'] ?? null;
        if ('POST' == $extra['REQUEST_METHOD']) {
            $extra['POST'] = $_POST;
        }
        if (null !== ($this->server['HTTP_REFERER'] ?? null)) {
            $extra['HTTP_REFERER'] = $this->server['HTTP_REFERER'];
        }
        $record['extra'] = $extra;

        return $record;
    }

    protected function console(LogRecord $record): LogRecord
    {
        $args = new ArrayCollection($this->server['argv']);
        $extra['app'] = 'console';
        $extra['command'] = $args->first();
        $extra['argv'] = $this->server['argv'];
        $record['extra'] = $extra;

        return $record;
    }
}
