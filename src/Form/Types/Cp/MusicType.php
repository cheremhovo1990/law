<?php

declare(strict_types=1);

namespace App\Form\Types\Cp;

use App\Commands\Cp\MusicCommand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class MusicType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'title',
            TextType::class,
            [
                'constraints' => [
                    new NotBlank(),
                    new Type('string'),
                ],
                'empty_data' => '',
            ],
        );

        $builder->add(
            'link',
            TextType::class,
            [
                'constraints' => [
                    new NotBlank(),
                    new Type('string'),
                ],
                'empty_data' => '',
            ],
        );

        $builder->add(
            'description',
            TextareaType::class,
            [
                'constraints' => [
                    new Type('string'),
                ],
                'empty_data' => '',
            ],
        );

        $builder->add('publish', CheckboxType::class, [
            'constraints' => [
                new Type('boolean'),
            ],
            'required' => false,
        ]);

        $builder
            ->add('meta', \App\Form\Types\Model\MetaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MusicCommand::class,
        ]);
    }
}
