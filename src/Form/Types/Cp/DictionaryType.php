<?php

declare(strict_types=1);

namespace App\Form\Types\Cp;

use App\Commands\Cp\DictionaryCommand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class DictionaryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('name', TextType::class, [
            'constraints' => [
                new NotBlank(),
                new Type('string'),
            ],
            'attr' => [
                'class' => 'keyboard__input',
            ],
            'empty_data' => '',
        ]);

        $builder->add('normalizeName', TextType::class, [
            'required' => false,
            'constraints' => [
                new Type('string'),
            ],
            'attr' => [
                'class' => 'keyboard__text',
            ],
        ]);

        $builder->add(
            'description',
            TextareaType::class,
            [
                'constraints' => [
                    new NotBlank(),
                    new Type('string'),
                ],
                'empty_data' => '',
            ],
        );

        $builder->add(
            'titlo',
            CheckboxType::class,
            [
                'required' => false,
                'constraints' => [
                    new Type('boolean'),
                ],
            ],
        );

        $builder->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DictionaryCommand::class,
        ]);
    }
}
