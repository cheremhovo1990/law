<?php

declare(strict_types=1);

namespace App\Form\Types\Cp;

use App\Commands\Cp\MetaCommand;
use App\Entity\MetaKeyEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class MetaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('key', ChoiceType::class, [
            'constraints' => [
                new NotBlank(),
            ],
            'choices' => ['' => ''] + MetaKeyEnum::getChoices(),
        ]);

        $builder->add('title', TextType::class, [
            'required' => false,
            'constraints' => [
                new Type('string'),
            ],
            'empty_data' => '',
        ]);

        $builder->add('description', TextareaType::class, [
            'constraints' => [
                new Type('string'),
                new NotBlank(),
            ],
            'empty_data' => '',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MetaCommand::class,
        ]);
    }
}
