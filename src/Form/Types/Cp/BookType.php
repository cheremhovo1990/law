<?php

declare(strict_types=1);

namespace App\Form\Types\Cp;

use App\Commands\Cp\BookCommand;
use App\Entity\LanguageOfThePublicationEnum;
use App\Form\Listeners\BookLinkListener;
use App\Form\Types\Model\BookLinkType;
use App\Form\Types\Model\ImageType;
use App\Form\Types\Model\IsbnType;
use App\Form\Validator\BookLinkConstrain;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Type;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('coverImage', ImageType::class, [
            'constraints' => [
                new Type('array'),
                new NotBlank(),
            ],
        ]);

        $builder->add('name', TextType::class, [
            'constraints' => [
                new Type('string'),
                new NotBlank(),
            ],
            'empty_data' => '',
        ]);

        $builder
            ->add('links', CollectionType::class, [
            'entry_type' => BookLinkType::class,
            'allow_add' => true,
            'error_bubbling' => false,
            'allow_delete' => true,
            'constraints' => [
                    new BookLinkConstrain(),
                ],
            ])
        ;
        $builder->addEventSubscriber(new BookLinkListener());

        $builder->add('isbnCollection', CollectionType::class, [
            'entry_type' => IsbnType::class,
            'allow_add' => true,
            'error_bubbling' => false,
            'allow_delete' => true,
            'constraints' => [
                new NotBlank(
                    allowNull: false,
                ),
            ],
        ]);

        $builder->add('description', TextareaType::class, [
            'required' => false,
            'constraints' => [
                new Type('string'),
                new NotBlank(),
            ],
            'empty_data' => '',
        ]);

        $builder->add('codeOfPublishingCouncil', TextType::class, [
            'required' => false,
            'constraints' => [
                new Type('string'),
            ],
        ]);

        $builder->add('dateOfWriting', DateType::class, [
            'widget' => 'single_text',
        ]);

        $builder->add('dateOfPublished', DateType::class, [
            'widget' => 'single_text',
        ]);

        $builder->add('pageSize', IntegerType::class, [
            'constraints' => [
                new NotBlank(),
                new Positive(),
            ],
        ]);

        $builder->add('languageOfThePublication', ChoiceType::class, [
            'choices' => ['' => ''] + LanguageOfThePublicationEnum::getChoices(),
            'empty_data' => '',
            'constraints' => [
                new NotBlank(),
                new Type('string'),
            ],
        ]);

        $builder->add('publish', CheckboxType::class, [
            'constraints' => [
                new Type('boolean'),
            ],
            'required' => false,
        ]);

        $builder->add('blessedByPatriarchKirill', CheckboxType::class, [
            'constraints' => [
                new Type('boolean'),
            ],
            'required' => false,
        ]);

        $builder->add('recommendedByPublishingCouncilRoc', CheckboxType::class, [
            'constraints' => [
                new Type('boolean'),
            ],
            'required' => false,
        ]);

        $builder->add('recommendedByPublishingCouncilBoc', CheckboxType::class, [
            'constraints' => [
                new Type('boolean'),
            ],
            'required' => false,
        ]);

        $builder->add('codeOfPublishingCouncil', TextType::class, [
            'constraints' => [
                new Type('string'),
            ],
            'required' => false,
        ]);

        $builder
            ->add('meta', \App\Form\Types\Model\MetaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BookCommand::class,
            'error_bubbling' => false,
        ]);
    }
}
