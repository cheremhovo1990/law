<?php

declare(strict_types=1);

namespace App\Form\Types\Cp;

use App\Commands\Cp\DescriptionSinCommand;
use App\Entity\Precept;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class DescriptionSinType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('name', TextType::class, [
            'constraints' => [
                new NotBlank(),
                new Type('string'),
            ],
            'empty_data' => '',
        ]);

        $builder->add(
            'text',
            TextareaType::class,
            [
                'constraints' => [
                    new NotBlank(),
                    new Type('string'),
                ],
                'purify_html' => true,
                'empty_data' => '',
            ],
        );

        $builder->add('sins', CollectionType::class, [
            'allow_add' => true,
            'allow_delete' => true,
            'delete_empty' => true,
            'required' => false,
        ]);

        $builder->add('precept', EntityType::class, [
            'class' => Precept::class,
            'choice_label' => 'name',
            'constraints' => [
                new NotBlank(),
            ],
        ]);

        $builder
            ->add('meta', \App\Form\Types\Model\MetaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DescriptionSinCommand::class,
        ]);
    }
}
