<?php

declare(strict_types=1);

namespace App\Form\Types\Cp;

use App\Commands\Cp\PreceptCommand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PreceptType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('link', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ],
            'empty_data' => '',
        ]);

        $builder->add('verse', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ],
            'empty_data' => '',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PreceptCommand::class,
        ]);
    }
}
