<?php

declare(strict_types=1);

namespace App\Form\Types\Cp;

use App\Commands\Cp\SinCommand;
use App\Entity\Precept;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Type;

class SinType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('name', TextType::class, [
            'constraints' => [
                new NotBlank(),
                new Type('string'),
            ],
            'empty_data' => '',
        ]);
        $builder->add('nameSlavonic', TextType::class, [
            'constraints' => [
                new Type('string'),
            ],
        ]);
        $builder->add('popoverContent', TextareaType::class, [
            'required' => false,
            'constraints' => [
                new Type('string'),
                new NotNull(),
            ],
            'empty_data' => '',
        ]);

        $builder->add('position', IntegerType::class, [
            'constraints' => [
                new Positive(),
            ],
            'required' => false,
        ]);
        $builder->add('precept', EntityType::class, [
            'class' => Precept::class,
            'choice_label' => 'name',
            'constraints' => [
                new NotBlank(),
            ],
        ]);

        $builder->add('isPublish', CheckboxType::class, [
            'constraints' => [
                new Type('boolean'),
            ],
            'required' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SinCommand::class,
        ]);
    }
}
