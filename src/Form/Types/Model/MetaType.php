<?php

declare(strict_types=1);

namespace App\Form\Types\Model;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;

class MetaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new Type('string'),
                ],
                'empty_data' => '',
                'required' => false,
            ])
        ;

        $builder->add('description', TextType::class, [
            'constraints' => [
                new NotNull(),
                new Type('string'),
            ],
            'required' => false,
            'empty_data' => '',
        ]);
    }
}
