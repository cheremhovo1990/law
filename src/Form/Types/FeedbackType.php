<?php

declare(strict_types=1);

namespace App\Form\Types;

use App\Commands\FeedbackCommand;
use App\Form\PhoneExtension;
use App\Form\RecaptchaExtension;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class FeedbackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('email', EmailType::class, [
            'constraints' => [
                new NotBlank(),
                new Length(min: 2, max: 100),
                new Email(),
            ],
            'empty_data' => '',
        ]);

        $builder->add('phone', TextType::class, [
            'required' => false,
            'label' => 'Телефон',
            PhoneExtension::OPTION_NAME => true,
        ]);

        $builder->add('firstName', TextType::class, [
            'constraints' => [
                new NotBlank(),
                new Length(min: 2, max: 40),
            ],
            'empty_data' => '',
        ]);

        $builder->add('theme', TextType::class, [
            'constraints' => [
                new NotBlank(),
                new Length(min: 2, max: 255),
            ],
            'empty_data' => '',
        ]);

        $builder->add('message', TextareaType::class, [
            'constraints' => [
                new NotBlank(),
            ],
            'purify_html' => true,
            'purify_html_profile' => 'front',
            'empty_data' => '',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => FeedbackCommand::class,
            RecaptchaExtension::OPTIONS_NAME => true,
        ]);
    }
}
