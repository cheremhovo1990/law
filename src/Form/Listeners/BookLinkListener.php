<?php

declare(strict_types=1);

namespace App\Form\Listeners;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class BookLinkListener implements EventSubscriberInterface
{
    public function preSubmit(FormEvent $event): void
    {
        $data = $event->getData();

        $data['links'] = $this->filter($data['links'] ?? []);

        $event->setData($data);
    }

    /**
     * @param array<string|integer, mixed> $data
     *
     * @return array<string|integer, mixed>
     */
    protected function filter(array $data): array
    {
        $result = [];
        foreach ($data as $item) {
            if (!empty($item['link']) && !in_array($item['link'], array_column($result, 'link'))) {
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * @return array<string, string>
     */
    public static function getSubscribedEvents(): array
    {
        return [FormEvents::PRE_SUBMIT => 'preSubmit'];
    }
}
