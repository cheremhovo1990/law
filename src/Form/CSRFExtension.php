<?php

declare(strict_types=1);

namespace App\Form;

use App\Services\CommonService;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CSRFExtension extends AbstractTypeExtension
{
    public const COOKIE_KEY = 'csrf-token';

    private CommonService $commonService;

    public function __construct(
        CommonService $commonService,
    ) {
        $this->commonService = $commonService;
    }

    public static function getExtendedTypes(): iterable
    {
        return [FormType::class];
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault('csrf_token_id', 'form')
            ->setDefault('csrf_protection', !$this->commonService->isTestEnvironment())
        ;
    }
}
