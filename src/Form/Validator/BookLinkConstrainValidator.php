<?php

declare(strict_types=1);

namespace App\Form\Validator;

use App\Doctrine\Collections\BookLinkCollection;
use App\Doctrine\Models\BookLink;
use App\Doctrine\Models\BookLinkTypeEnum;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class BookLinkConstrainValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!is_array($value)) {
            $this->context->buildViolation('This value should be of type {{ type }}.')
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setParameter('{{ type }}', BookLinkCollection::class)
                ->setCode(BookLinkConstrain::ERROR)
                ->addViolation()
            ;

            return;
        }
        if (0 === count($value)) {
            $this->context->buildViolation('Add at least one link.')
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(BookLinkConstrain::ERROR)
                ->addViolation()
            ;

            return;
        }
        $haveTypeBuy = false;
        /** @var BookLink $item */
        foreach ($value as $item) {
            if ($item['type'] === BookLinkTypeEnum::BUY->value) {
                $haveTypeBuy = true;
            }
        }
        if (false === $haveTypeBuy) {
            $this->context->buildViolation('At least one of the links must be of type {{ type }}')
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setParameter('{{ type }}', 'buy')
                ->setCode(BookLinkConstrain::ERROR)
                ->addViolation()
            ;
        }
    }
}
