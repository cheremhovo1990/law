<?php

declare(strict_types=1);

namespace App\Form\Validator;

use Symfony\Component\Validator\Constraint;

class BookLinkConstrain extends Constraint
{
    public const ERROR = '7024a008-ba59-4a56-ae5b-469c7a1cd173';
}
