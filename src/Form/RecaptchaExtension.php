<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecaptchaExtension extends AbstractTypeExtension
{
    public const OPTIONS_NAME = 'have_recaptcha';

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault(self::OPTIONS_NAME, false)
            ->setAllowedTypes(self::OPTIONS_NAME, 'bool')
        ;
    }

    public static function getExtendedTypes(): iterable
    {
        return [
            FormType::class,
        ];
    }
}
