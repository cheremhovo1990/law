<?php

declare(strict_types=1);

namespace App\Form\Services;

use App\Form\RecaptchaExtension;
use App\Helpers\JsonHelper;
use App\Services\CommonService;
use App\Services\Google\RecapthcaService;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\RequestHandlerInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class JsonRequestHandler implements RequestHandlerInterface
{
    private RecapthcaService $recapthcaService;
    private CommonService $commonService;

    public function __construct(
        RecapthcaService $recapthcaService,
        CommonService $commonService,
    ) {
        $this->recapthcaService = $recapthcaService;
        $this->commonService = $commonService;
    }

    public function handleRequest(FormInterface $form, mixed $request = null): void
    {
        if (!$request instanceof Request) {
            throw new UnexpectedTypeException($request, 'Symfony\Component\HttpFoundation\Request');
        }

        $method = $form->getConfig()->getMethod();

        if ($method !== $request->getMethod()) {
            return;
        }
        $content = JsonHelper::decode($request->getContent());
        $input = [];
        foreach ($content as $key => $value) {
            if (empty($value)) {
                $input[$key] = null;
            } else {
                $input[$key] = $value;
            }
        }
        $haveRecaptcha = $form->getConfig()->getOption(RecaptchaExtension::OPTIONS_NAME, false);
        if (true === $haveRecaptcha && !$this->commonService->isTestEnvironment()) {
            $this->recapthcaService->verify($input['g-recaptcha-response'] ?? null);
            unset($input['g-recaptcha-response']);
        }
        $form->submit($input);
    }

    public function isFileUpload(mixed $data): bool
    {
        return $data instanceof File;
    }

    public function getUploadFileError(mixed $data): ?int
    {
        if (!$data instanceof UploadedFile || $data->isValid()) {
            return null;
        }

        return $data->getError();
    }
}
