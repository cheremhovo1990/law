<?php

declare(strict_types=1);

namespace App\Form;

use App\Form\Services\JsonRequestHandler;
use App\Helpers\MimeTypeRequestEnumHelper;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class JsonExtension extends AbstractTypeExtension
{
    private RequestStack $requestStack;
    private JsonRequestHandler $jsonRequestHandler;

    public function __construct(
        RequestStack $requestStack,
        JsonRequestHandler $jsonRequestHandler,
    ) {
        $this->requestStack = $requestStack;
        $this->jsonRequestHandler = $jsonRequestHandler;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $request = $this->requestStack->getMainRequest();
        if ($request instanceof Request && MimeTypeRequestEnumHelper::CONTENT_TYPE->value === $request->headers->get(MimeTypeRequestEnumHelper::CONTENT_TYPE->name)) {
            $builder->setRequestHandler($this->jsonRequestHandler);
        }
    }

    public static function getExtendedTypes(): iterable
    {
        return [FormType::class];
    }
}
