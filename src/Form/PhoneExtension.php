<?php

declare(strict_types=1);

namespace App\Form;

use App\Services\PhoneFilterService;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PhoneExtension extends AbstractTypeExtension
{
    public const OPTION_NAME = 'filter_phone';
    private PhoneFilterService $phoneFilterService;

    public function __construct(
        PhoneFilterService $phoneFilterService,
    ) {
        $this->phoneFilterService = $phoneFilterService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (true === $options[self::OPTION_NAME]) {
            $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event): void {
                if (!is_scalar($data = $event->getData())) {
                    // Hope there is a view transformer, otherwise an error might happen
                    return; // because we don't want to handle it here
                }

                if ('' === ($submittedData = trim((string) $data))) {
                    if ($submittedData !== $data) {
                        $event->setData($submittedData);
                    }

                    return;
                }

                $event->setData($this->phoneFilterService->normalize($submittedData));
            });
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault(self::OPTION_NAME, false)
            ->setAllowedTypes(self::OPTION_NAME, 'bool')
        ;
    }

    public static function getExtendedTypes(): iterable
    {
        return [
            TextType::class,
        ];
    }
}
