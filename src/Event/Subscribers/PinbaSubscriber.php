<?php

declare(strict_types=1);

namespace App\Event\Subscribers;

use App\Services\Pinba\PinbaService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ControllerArgumentsEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class PinbaSubscriber implements EventSubscriberInterface
{
    private PinbaService $pinbaService;
    protected mixed $timer;
    private RequestStack $requestStack;

    public function __construct(
        PinbaService $pinbaService,
        RequestStack $requestStack,
    ) {
        $this->pinbaService = $pinbaService;
        $this->requestStack = $requestStack;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER_ARGUMENTS => [
                'timerStart',
            ],
            KernelEvents::RESPONSE => [
                'timerEnd',
            ],
        ];
    }

    public function timerStart(ControllerArgumentsEvent $event): void
    {
        if ($this->cannot()) {
            return;
        }
        $this->timer = $this->pinbaService->timerStart(['group' => 'controller']);
    }

    public function timerEnd(ResponseEvent $event): void
    {
        if ($this->cannot()) {
            return;
        }
        if (!$this->pinbaService->isTimerStarted($this->timer)) {
            return;
        }
        $this->pinbaService->timerStop($this->timer);
    }

    protected function cannot(): bool
    {
        return !$this->pinbaService->extensionLoaded() || $this->isProfile();
    }

    protected function isProfile(): bool
    {
        $controller = $this->requestStack->getCurrentRequest()->attributes->get('_controller');
        if (null === $controller) {
            return false;
        }
        if (0 === strncmp($controller, 'web_profiler.controller.profiler', strlen('web_profiler.controller.profiler'))) {
            return true;
        }

        return false;
    }
}
