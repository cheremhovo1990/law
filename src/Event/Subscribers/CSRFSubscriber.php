<?php

declare(strict_types=1);

namespace App\Event\Subscribers;

use App\Form\CSRFExtension;
use App\Helpers\MimeTypeResponseEnumHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class CSRFSubscriber implements EventSubscriberInterface
{
    private CsrfTokenManagerInterface $csrfTokenManager;

    public function __construct(
        CsrfTokenManagerInterface $csrfTokenManager,
    ) {
        $this->csrfTokenManager = $csrfTokenManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ResponseEvent::class => [['onResponseEvent', -100]],
        ];
    }

    public function onResponseEvent(ResponseEvent $event): void
    {
        if ($event->getResponse()->headers->get(MimeTypeResponseEnumHelper::CONTENT_TYPE->name) === MimeTypeResponseEnumHelper::CONTENT_TYPE->value) {
            $event->getResponse()->headers->setCookie(new Cookie(
                CSRFExtension::COOKIE_KEY,
                $this->csrfTokenManager->getToken('form')->getValue(),
                httpOnly: false,
            ));
        }
    }
}
