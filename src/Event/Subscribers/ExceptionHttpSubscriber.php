<?php

declare(strict_types=1);

namespace App\Event\Subscribers;

use App\Helpers\MimeTypeResponseEnumHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExceptionHttpSubscriber implements EventSubscriberInterface
{
    private TranslatorInterface $translator;

    public function __construct(
        TranslatorInterface $translator,
    ) {
        $this->translator = $translator;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [['onException', 10]],
        ];
    }

    public function onException(ExceptionEvent $event): void
    {
        if (MimeTypeResponseEnumHelper::ACCEPT->value !== $event->getRequest()->headers->get(MimeTypeResponseEnumHelper::ACCEPT->name)) {
            return;
        }
        if ($event->getThrowable() instanceof HttpExceptionInterface) {
            return;
        }
        if ($event->getThrowable() instanceof \DomainException) {
            $event->allowCustomResponseCode();
            $event->setResponse(new JsonResponse(['success' => false, 'message' => $this->translator->trans($event->getThrowable()->getMessage())]));
            $event->stopPropagation();
        }
        if ($event->getThrowable() instanceof \RuntimeException) {
            $event->allowCustomResponseCode();
            $event->setResponse(new JsonResponse(['success' => false, 'message' => $this->translator->trans('app.error')]));
            $event->stopPropagation();
        }
    }
}
