<?php

declare(strict_types=1);

namespace App\Event\Subscribers;

use App\Helpers\MimeTypeResponseEnumHelper;
use App\Services\PaginatorInterface;
use App\Services\Serialize\SerializeRuntimeException;
use App\Services\Serialize\SerializeToJson;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class SerializerSubscriber implements EventSubscriberInterface
{
    public const KEY_SERIALIZE = '_serialize';
    private SerializerInterface $serializer;

    public function __construct(
        SerializerInterface $serializer,
    ) {
        $this->serializer = $serializer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
            KernelEvents::VIEW => 'onKernelView',
        ];
    }

    public function onKernelController(ControllerEvent $event): void
    {
        $controller = $event->getController();
        if (!\is_array($controller) && method_exists($controller, '__invoke')) {
            $controller = [$controller, '__invoke'];
        }
        $method = new \ReflectionMethod($controller[0], $controller[1]);
        $attributes = $method->getAttributes(SerializeToJson::class);
        if (!empty($attributes)) {
            $serialize = $attributes[0]->newInstance();
            $event->getRequest()->attributes->set(self::KEY_SERIALIZE, $serialize);
        }
    }

    public function onKernelView(ViewEvent $event): void
    {
        if (($serialize = $this->getSerializeToJson($event)) instanceof SerializeToJson) {
            if (MimeTypeResponseEnumHelper::ACCEPT->value !== $event->getRequest()->headers->get(MimeTypeResponseEnumHelper::ACCEPT->name)) {
                throw new SerializeRuntimeException('headers accept must application/json');
            }
            if ($event->getControllerResult() instanceof Form) {
                $content = $this->serializer->serialize(
                    [
                        'success' => false,
                        'message' => 'Validation Failed',
                        'form' => $event->getControllerResult(),
                    ],
                    'json',
                );
            } else {
                $context = [];
                if (is_array($serialize->getGroups())) {
                    $context = [AbstractNormalizer::GROUPS => $serialize->getGroups()];
                }
                if ($event->getControllerResult() instanceof PaginatorInterface) {
                    $result = $this->buildContent($event->getControllerResult());
                } else {
                    $result = $event->getControllerResult();
                }
                $content = $this->serializer->serialize(
                    $result,
                    'json',
                    $context,
                );
            }
            $event->setResponse(JsonResponse::fromJsonString($content));
        }
    }

    /**
     * @return array<string, mixed>
     */
    protected function buildContent(PaginatorInterface $paginator): array
    {
        $result['data'] = $paginator;
        $result['paginator']['total'] = $paginator->getTotal();
        $result['paginator']['page'] = $paginator->getPage();
        $result['paginator']['maxPage'] = $paginator->getMaxPage();
        $result['paginator']['limit'] = $paginator->getLimit();

        return $result;
    }

    protected function getSerializeToJson(ViewEvent $event): null|SerializeToJson
    {
        return $event->getRequest()->attributes->get(self::KEY_SERIALIZE);
    }
}
