<?php

declare(strict_types=1);

namespace App\Event\Subscribers;

use App\Services\MonitoringSstatistics\MonitoringStatisticsService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Stopwatch\Stopwatch;

class MonitoringStatisticSubscriber implements EventSubscriberInterface
{
    private Stopwatch $stopwatch;

    public function __construct(Stopwatch $stopwatch)
    {
        $this->stopwatch = $stopwatch;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [['onRequest', 9999]],
        ];
    }

    public function onRequest(): void
    {
        MonitoringStatisticsService::setStopwatch($this->stopwatch);
    }
}
