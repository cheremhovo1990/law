<?php

declare(strict_types=1);

namespace App\Event\Subscribers;

use App\Services\Centrifugo\JWTTokenService;
use App\Services\Identity\IdentityService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @todo TEST
 */
class CentrifugoSubscriber implements EventSubscriberInterface
{
    private IdentityService $identityService;

    public const IDENTITY_KEY = 'centrifugo-identity';
    public const TOKEN_KEY = 'centrifugo-token';
    private JWTTokenService $JWTTokenService;

    public function __construct(
        IdentityService $identityService,
        JWTTokenService $JWTTokenService,
    ) {
        $this->identityService = $identityService;
        $this->JWTTokenService = $JWTTokenService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => 'onResponse',
            KernelEvents::REQUEST => 'onRequest',
        ];
    }

    public function onRequest(RequestEvent $event): void
    {
        if ($event->getRequest()->cookies->has(self::IDENTITY_KEY)) {
            $this->identityService->setIdentity($event->getRequest()->cookies->get(self::IDENTITY_KEY));
        }
    }

    public function onResponse(ResponseEvent $event): void
    {
        if (!$event->getRequest()->cookies->has(self::IDENTITY_KEY)) {
            $datetime = new \DateTime();
            $datetime->add(new \DateInterval('P1Y'));
            $cookie = new Cookie(
                self::IDENTITY_KEY,
                $this->identityService->getIdentity(),
                $datetime,
                httpOnly: false,
            );
            $event->getResponse()->headers->setCookie($cookie);
        }
        if (!$event->getRequest()->cookies->has(self::TOKEN_KEY)) {
            $datetime = new \DateTime();
            $datetime->add(new \DateInterval('P1Y'));
            $cookie = new Cookie(
                self::TOKEN_KEY,
                $this->JWTTokenService->generateConnectionToken($this->identityService->getIdentity()),
                $datetime,
                httpOnly: false,
            );
            $event->getResponse()->headers->setCookie($cookie);
        }
    }
}
