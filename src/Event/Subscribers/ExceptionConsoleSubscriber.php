<?php

declare(strict_types=1);

namespace App\Event\Subscribers;

use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExceptionConsoleSubscriber implements EventSubscriberInterface
{
    private TranslatorInterface $translator;

    public function __construct(
        TranslatorInterface $translator,
    ) {
        $this->translator = $translator;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ConsoleEvents::ERROR => [['onConsoleEvent', 100]],
        ];
    }

    public function onConsoleEvent(ConsoleErrorEvent $event): void
    {
        if (!$event->getError() instanceof \DomainException && !$event->getError() instanceof \RuntimeException) {
            return;
        }
        $event->setExitCode(0);
        if ($event->getError() instanceof \RuntimeException) {
            $event->setExitCode(1);
        }
        $event->stopPropagation();

        $io = new SymfonyStyle($event->getInput(), $event->getOutput());
        $io->error($this->translator->trans($event->getError()->getMessage()));
    }
}
