<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Font;
use App\Entity\Symbol;
use App\Entity\SymbolMap;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Ulid;

class SymbolMapFixture extends Fixture implements DependentFixtureInterface
{
    protected ObjectManager $manager;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        foreach ($this->getData() as $datum) {
            $font = $this->findFont($datum['font']);
            $symbol = $this->findSymbol($datum['symbol']);
            $entity = new SymbolMap(
                unicode: $datum['unicode'],
                font: $font,
                symbol: $symbol,
                groupName: $datum['groupName'] ?? null,
                groupNamePosition: $datum['groupNamePosition'] ?? null,
            );
            $entity->setId(new Ulid($datum['id'] ?? null));
            $manager->persist($entity);
        }
        $manager->flush();
    }

    public function findFont(string $code): Font
    {
        return $this->manager->getRepository(Font::class)->findOneBy(['code' => $code]);
    }

    public function findSymbol(string $code): Symbol
    {
        return $this->manager->getRepository(Symbol::class)->findOneBy(['code' => $code]);
    }

    /**
     * @return iterable<integer, array<string, mixed>>
     */
    public function getData(): iterable
    {
        require_once __DIR__ . '/symbol/ponomar_unicode.php';
        yield from ponomarUnicode();

        require_once __DIR__ . '/symbol/hirmos_ie_ucs.php';
        yield from hirmosIeUcs();
    }

    /**
     * @return array<integer, string>
     */
    public function getDependencies(): array
    {
        return [
            FontFixture::class,
            SymbolFixture::class,
        ];
    }
}
