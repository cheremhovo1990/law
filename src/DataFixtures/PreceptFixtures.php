<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Doctrine\Models\Meta;
use App\Entity\DescriptionSin;
use App\Entity\Precept;
use App\Entity\Sin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Ulid;

class PreceptFixtures extends Fixture
{
    public const ID_SIX = '01G6MP6RCDJ694AMVYZHZ5NC5Z';
    public const SIN_ID_FIRST = '01G68PGW6W8YANXGGWVN9V3QAK';
    public const SIN_NAME_FIRST = 'маловерия и неверие в Промысел Божий';
    public const SIN_ID_SECOND = '01G7M76FBW4F4RY5JB4QEJWVGB';
    public const SIN_NAME_SECOND = 'гнев';
    protected ObjectManager $manager;
    protected int $positionSin = 0;
    public const DESCRIPTION_SIN_ID_FIRST = '01G6B8H4WRVF4AWCAG1XDF73VV';
    protected const DESCRIPTION_SIN_INDEX = 0;
    protected const DESCRIPTION_SIN_INDEX_FIRST = 1;
    public const DESCRIPTION_SIN_NAME_FIRST = 'Гнев';

    /** @var array|DescriptionSin[] */
    protected array $descriptionSins = [];

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        foreach ($this->getDescriptionSins() as $index => $data) {
            $descriptionSin = new DescriptionSin($data['name'], $data['text']);
            $descriptionSin->setId(new Ulid($data['id'] ?? null));
            $descriptionSin->setMeta(new Meta());
            $this->manager->persist($descriptionSin);
            $this->descriptionSins[$index] = $descriptionSin;
        }
        foreach ($this->getData() as $datum) {
            $precept = new Precept(
                name: $datum['name'],
                nameSlavonic: $datum['name_slavonic'],
                link: $datum['link'],
                verse: $datum['verse'],
                position: $datum['position'],
            );
            if (isset($datum['description_sin'])) {
                $descriptionSin = $this->descriptionSins[$datum['description_sin']];
                $descriptionSin->setPrecept($precept);
            }
            foreach ($datum['sins'] as $item) {
                $this->createSin($precept, $item);
            }
            $precept->setId(new Ulid($datum['id'] ?? null));
            $this->manager->persist($precept);
        }
        $this->manager->flush();
    }

    /**
     * @param array<string, mixed> $item
     */
    protected function createSin(Precept $precept, array $item): void
    {
        $sin = new Sin(
            name: $item['name'],
            precept: $precept,
            position: ++$this->positionSin,
            popoverContent: $item['popoverContent'] ?? '',
            nameSlavonic: $item['name_slavonic'] ?? null,
        );
        $sin->setId(new Ulid($item['id'] ?? null));
        if (isset($item['description_sin'])) {
            $descriptionSin = $this->descriptionSins[$item['description_sin']];
            $sin->setDescriptionSin($descriptionSin);
        }
        if ($item['isPublishedAt'] ?? true) {
            $sin->setPublishedAt(new \DateTimeImmutable());
        }
        $this->manager->persist($sin);
    }

    /**
     * @return array<integer, array<string, mixed>>
     */
    protected function getData(): iterable
    {
        yield [
            'name' => 'Я Господь, Бог твой, Который вывел тебя из земли Египетской, из дома рабства; да не будет у тебя других богов пред лицом моим.',
            'name_slavonic' => 'А҆́зъ є҆́смь Гдⷭ҇ь Бг҃ъ тво́й, и҆зведы́й тѧ̀ ѿ землѝ Е҆гѵ́петскїѧ, ѿ до́мꙋ рабо́ты. Да не бꙋ́дꙋть тебѣ́ бо́зи и҆ні́и ра́звѣ менѐ.',
            'link' => 'https://azbyka.ru/biblia/?Ex.20:2-3',
            'verse' => 'Исх.20:2-3',
            'position' => 1,
            'sins' => [
                [
                    'id' => self::SIN_ID_SECOND,
                    'name' => self::SIN_NAME_FIRST,
                ],
                ['name' => 'безбожие'],
                ['name' => 'лжеучения'],
                ['name' => 'магия'],
                ['name' => 'хождение к бабкам и целителям'],
                ['name' => 'астрология (в том числе и чтение гороскопово)'],
                ['name' => 'участие в сектах'],
                ['name' => 'гордыня'],
                ['name' => 'хвастовство'],
                ['name' => 'карьеризм'],
                ['name' => 'самооправдания'],
                ['name' => 'любоначалия'],
                ['name' => 'самонадеянность'],
                ['name' => 'нет культуры самоосуждения'],
                [
                    'name' => 'человеконадеяния',
                    'name_slavonic' => 'человѣконадѣ́ѧнїе',
                    'popoverContent' => 'когда кто надеяться на способности и силы свой, или других людей, а не на милость и помощь божию',
                ],
                ['name' => 'самолюбие', 'name_slavonic' => 'самолю́бїе'],
            ],
        ];

        yield [
            'name' => 'Не делай себе кумира и никакого изображения того, что на небе вверху, и что на земле внизу, и что воде ниже земли; не поклоняйся им и не служи им, ибо Я Господь, Бог твой ревнитель, наказывающий детей за вину отцов до тертьего и четвертого рода, ненавидящих Меня, и творящий милость до тысячи родов любящим Меня и соблюдающим заповеди Мои.',
            'name_slavonic' => 'Не сотворѝ себѣ̀ кꙋмі́ра, и всѧ́кагѡ подо́бїѧ, є҆ли̑ка на небесѝ горѣ̀, и҆ є҆ли̑ка на землѝ ни́зꙋ, и҆ є҆ли̑ка въ вода́хъ под земле́ю: Да не поклони́шисѧ и҆̀мъ, ни послꙋ́жиши и҆̀мъ: а҆́зъ бо є҆́смь Гдⷭ҇ь Бг҃ъ тво́й, Бг҃ъ ревни́тель, ѿдаѧ́й грѣхѝ о҆тє́цъ на ча̑да до тре́тїѧгѡ и҆ четве́ртагѡ ро́да ненави́дѧщымъ менѐ: И҆ творѧ́й млⷭ҇ть въ ты́сѧщахъ лю́бѧщымъ мѧ̀, и҆ хранѧ́щымъ повелѣ̑нїѧ моѧ̀.',
            'link' => 'https://azbyka.ru/biblia/?Ex.20:4-6',
            'verse' => 'Исх.20:4-6',
            'position' => 2,
            'sins' => [
                 ['name' => 'идолопоклонство'],
                 ['name' => 'вызывание духов'],
                 ['name' => 'кормление домовых'],
                 ['name' => 'гадание'],
                 [
                     'name' => 'человекоугодие',
                     'name_slavonic' => 'человѣкоꙋго́дїе',
                     'popoverContent' => 'когда угождают людям так, что для того нерадеть угождения богу',
                 ],
                 ['name' => 'сребролюбие'],
                 [
                     'name' => 'лихоимство (корысть)',
                     'name_slavonic' => 'лихои́мство',
                     'popoverContent' => 'чрезмерная алчность к приобретению материальных благ любыми способами',
                 ], // https://azbyka.ru/biblia/?Ps.118:36
                 [
                     'name' => 'мздоимство (мзда)',
                     'isPublishedAt' => false,
                     'name_slavonic' => 'мшелои́мство',
                 ], // https://azbyka.ru/biblia/?Ps.25:10&r~utfcs
                 ['name' => 'надеяться на серебро и золото'], // https://azbyka.ru/biblia/?Ps.113:12-16&r~utfcs
            ],
        ];

        yield [
            'name' => 'Не произноси имени Господа, Бога твоего напрасно, ибо Господь не оставит без наказания того, кого произносит имя Его напрасно.',
            'name_slavonic' => 'Не во́змеши и҆́мене Гдⷭ҇а Бг҃а твоегѡ̀  всꙋ́е: не ѡ҆чи́стить бо Гдⷭ҇ь прїе́млющаго и҆́мѧ є҆гѡ̀ всꙋ́е.',
            'link' => 'https://azbyka.ru/biblia/?Ex.20:7',
            'verse' => 'Исх.20:7',
            'position' => 3,
            'sins' => [
                ['name' => 'кощунство'],
                ['name' => 'издевательство над святыней'],
                ['name' => 'мат'],
                ['name' => 'божба'],
                ['name' => 'нарушение обещания, данного Богу'],
                ['name' => 'сквернословие'],
                ['name' => 'не чтение Евангелия'],
            ],
        ];

        yield [
            'name' => 'Помни день субботний, чтобы святить его; шесть дней работай, и делай всякие дела твои, а день седьмой - суббота Господу, Богу твоему: не делай в оный никаго дела ни ты, ни сын твой, ни дочь твоя, ни раб твой, ни рабыня твоя, ни вол твой, ни осел твой, ни всякий скот твой, ни пришлец, который в жилищах твоих; ибо в шесть дней создал Господь небо и землю, море и все, что в них, а в день седьмой почил; посему благословил Господь день субботний и освятил его.',
            'name_slavonic' => 'По́мни де́нь сꙋббѡ́тный, є҆́же свѧти́ти є҆го̀: Ше́сть дне́й дѣ́лай, и҆ сотвори́ши (въ ни́хъ) всѧ̀ дѣла̀ твоѧ̀: Въ де́нь же седмы́й, сꙋббѡ́та Гдⷭ҇ꙋ Бг҃ꙋ твоемꙋ̀: да не сотвори́ши всѧ́кагѡ дѣ́ла въ о҆́нь, ты̀, и҆ сы́нъ тво́й, и҆ дще́рь твоѧ̀, и҆ ра́бъ тво́й, и҆ раба̀ твоѧ̀, и҆ во́лъ тво́й, и҆ о҆слѧ̀ твоѐ, и҆ всѧ́кїй ско́тъ тво́й, н҆ пришле́цъ ѡ҆бита́ѧй ѹ҆ тебѐ. Занѐ въ шестѝ дне́хъ сотворѝ Гдⷭ҇ь нб҃о и҆ зе́млю, мо́ре, и҆ всѧ̑ ꙗ҆̀же въ ни́хъ, и҆ почѝ въ де́нь седмы́й: сегѡ̀ ра́ди бл҃гословѝ Гдⷭ҇ь де́нь седмы́й, и҆ ѡ҆ст҃ѝ є҆го.',
            'link' => 'https://azbyka.ru/biblia/?Ex.20:8-11',
            'verse' => 'Исх.20:8-11',
            'position' => 4,
            'sins' => [
                ['name' => 'пропуск воскресного богослужения'],
                ['name' => 'работа в праздники'],
                ['name' => 'праздность'],
                ['name' => 'лень', 'name_slavonic' => 'лѣ́ность'],
                ['name' => 'нарушение поста'],
            ],
        ];

        yield [
            'name' => 'Почитай отца твоего и мать твою, чтобы продлились дни твои на земле, которую Господь, Бог твой, дает тебе.',
            'name_slavonic' => 'Чтѝ о҆ц҃а̀ твоего̀ и҆ ма́терь твою̀, да бл҃го тѝ бꙋ́деть, и҆ да долголѣ́тенъ бꙋ́деши на землѝ бла́зѣ, ю҆́же Гдⷭ҇ь Бг҃ъ тво́й дае́ть тебѣ̀.',
            'link' => 'https://azbyka.ru/biblia/?Ex.20:12',
            'verse' => 'Исх.20:12',
            'position' => 5,
            'sins' => [
                ['name' => 'оскорбление родителей, непочитание их'],
                ['name' => 'злословие ближних'],
                ['name' => 'непочтение к старшим и учителям'],
                ['name' => 'беспечность в отношении воспитания детей'],
            ],
        ];

        yield [
            'id' => self::ID_SIX,
            'name' => 'Не убивай.',
            'name_slavonic' => 'Не ѹ҆бі́й.',
            'link' => 'https://azbyka.ru/biblia/?Ex.20:13',
            'verse' => 'Исх.20:13',
            'position' => 6,
            'description_sin' => self::DESCRIPTION_SIN_INDEX,
            'sins' => [
                ['name' => 'убийство'],
                ['name' => 'аборты'],
                [
                    'id' => self::SIN_ID_FIRST,
                    'name' => self::SIN_NAME_SECOND,
                    'popoverContent' => 'Гнев – это желание зла огорчившему.',
                    'description_sin' => self::DESCRIPTION_SIN_INDEX,
                    'name_slavonic' => 'гнѣ́в',
                ],
                [
                    'name' => 'злопамятство(памятозлобия)',
                    'popoverContent' => 'Злопамятство - это воспоминание сокровенной ненависти.',
                    'description_sin' => self::DESCRIPTION_SIN_INDEX,
                    'name_slavonic' => 'памѧтоѕло́бїе',
                ],
                ['name' => 'злорадство'],
                ['name' => 'ругань'],
                ['name' => 'драки'],
                [
                    'name' => 'ненависть',
                    'name_slavonic' => 'не́нависть',
                ],
                ['name' => 'обиды'],

                ['name' => 'раздражительность'],
                ['name' => 'сварливость'],
                ['name' => 'уныние и отчаяние', 'name_slavonic' => 'ѹны́нїе'],
                ['name' => 'помыслы о самоубийстве'],
                [
                     'name' => 'любопрение',
                     'popoverContent' => 'любовь состязаться в спорах, дискуссиях',
                ],
            ],
        ];

        yield [
            'name' => 'Не прелюбодействуй.',
            'name_slavonic' => 'Не прелюбы̀ сотворѝ.',
            'link' => 'https://azbyka.ru/biblia/?Ex.20:14',
            'verse' => 'Исх.20:14',
            'position' => 7,
            'sins' => [
                ['name' => 'супрожеская измена'],
                ['name' => 'блуд (в т.ч. сожительство без регистрации в ЗАГСе)'],
                ['name' => 'гомосексуализм'],
                ['name' => 'онанизм (рукоблудие)'],
                ['name' => 'смотрение порнографии'],
            ],
        ];

        yield [
            'name' => 'Не кради.',
            'name_slavonic' => 'Не ѹкра́ди.',
            'link' => 'https://azbyka.ru/biblia/?Ex.20:15',
            'verse' => 'Исх.20:15',
            'position' => 8,
            'sins' => [
                ['name' => 'вороство'],
                ['name' => 'грабеж'],
                ['name' => 'тунеядство'],
                ['name' => 'мошеничество'],
                ['name' => 'ростовщичество'],
                ['name' => 'скупость'],
                ['name' => 'неуплата долгов'],
            ],
        ];

        yield [
            'name' => 'Не произноси ложного свидетельства на ближнего твоего.',
            'name_slavonic' => 'Не послꙋ́шествꙋй на дрꙋ́га своего̀ свидѣ́телства ло́жна.',
            'link' => 'https://azbyka.ru/biblia/?Ex.20:16',
            'verse' => 'Исх.20:16',
            'position' => 9,
            'sins' => [
                ['name' => 'лжесвидетельство'],
                ['name' => 'ложь'],
                ['name' => 'клевета'],
                ['name' => 'сплетни'],
                ['name' => 'предательство'],
                ['name' => 'осуждение', 'name_slavonic' => 'ѡ҆сужде́нїе'],
                [
                    'name' => 'лесть',
                    'popoverContent' => 'Обман, обольщение, притворство, коварство, хитрость',
                    'name_slavonic' => 'лес́сть',
                ],
            ],
        ];

        yield [
            'name' => 'Не желай дома ближнего твоего; не желай жены ближнего твоего, ни поля его, ни раба его, ни рабыни его, ни вола его, ни осла его, ни всякого скота его, ничего, что у ближнего твоего.',
            'name_slavonic' => 'Не пожела́й жены̀ и҆́скреннагѡ твоегѡ̀: не пожела́й до́мꙋ бли́жнѧгѡ твоегѡ̀, ни села̀ є҆гѡ̀, ни раба̀ є҆гѡ̀, ни рабы́ни є҆гѡ̀, ни вола̀ є҆гѡ̀, ни о҆сла̀ є҆гѡ̀, ни всѧ́кагѡ скота̀ є҆гѡ̀, ни всегѡ̀ є҆ли̑ка сꙋ́ть бли́жнѧгѡ твоегѡ́.',
            'link' => 'https://azbyka.ru/biblia/?Ex.20:17',
            'verse' => 'Исх.20:17',
            'position' => 10,
            'description_sin' => self::DESCRIPTION_SIN_INDEX_FIRST,
            'sins' => [
                [
                    'name' => 'зависть',
                    'name_slavonic' => 'за́висть',
                ],
                ['name' => 'недовольство своим положением'],
                ['name' => 'самоедство'],
                ['name' => 'ропот'],
                [
                    'name' => 'лицеприя́тие',
                    'popoverContent' => 'предпочтение одного человека другому (одних людей другим) по причине пристрастного к нему (к ним) отношения.',],
                [
                    'name' => 'печаль',
                    'name_slavonic' => 'печа́ль',
                    'description_sin' => self::DESCRIPTION_SIN_INDEX_FIRST,
                ],
            ],
        ];
    }

    /**
     * @return array<integer, array<string, mixed>>
     */
    public function getDescriptionSins(): array
    {
        return [
            self::DESCRIPTION_SIN_INDEX => [
                'id' => self::DESCRIPTION_SIN_ID_FIRST,
                'name' => self::DESCRIPTION_SIN_NAME_FIRST,
                'text' => <<<TEXT
<p>
    <span style="color:hsl(0,75%,60%);">Гнев</span> – это воспоминание сокровенной ненависти, т. е. памятозлобия. <span style="color:hsl(0,75%,60%);">Гнев</span> – это желание зла огорчившему.
</p>
<p>
    Не должно быть от нас сокрыто, о друзья, и то, что иногда во время гнева лукавые бесы скоро отходят от нас с той целью, чтобы мы о великих страстях вознерадели<sup>1</sup> как бы о маловажных и наконец сделали болезнь свою неисцельной.
</p>
<p>
    &nbsp;
</p>
<p>
    <span style="background-color:rgb(255,255,255);color:rgb(42,42,42);">Пс.4:5 Гневаясь, не согрешайте: размыслите в сердцах ваших на ложах ваших, и утишитесь;</span>
</p>
<p>
    <span style="background-color:rgb(255,255,255);color:rgb(42,42,42);">Пс.36:8 Прекрати гнев и оставь ярость, не подражай лукавству.</span>
</p>
<hr>
<ol>
    <li>
        <span class="text-small">Как некоторые в оправдание свое говорят: “Я хоть вспыльчив, но это у меня скоро проходит”</span>
    </li>
</ol>
TEXT,
            ],
            self::DESCRIPTION_SIN_INDEX_FIRST => [
                'name' => 'Печаль',
                'text' => <<<TEXT
<p>Печаль, скорбь, уныние, склонность к земному. Печаль мира сего - бывает от потери земных польза, или от зависти о чужом добре. Такая печаль бесполезна и порочна.</p><p><span style="background-color:rgb(255,255,255);color:rgb(42,42,42);">Мк.4:17 </span><span style="background-color:rgb(255,255,255);color:rgb(42,42,42);font-family:'Ponomar Unicode';">и҆ не и҆́мꙋтъ коре́нїѧ въ себѣ̀, но привре́менни сꙋ́ть: та́же бы́вшей печа́ли и҆лѝ гоне́нїю словесѐ ра́ди, а҆́бїе соблажнѧ́ютсѧ.</span></p>
TEXT,
            ],
        ];
    }
}
