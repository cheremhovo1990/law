<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Video;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Ulid;

class VideoFixture extends Fixture
{
    public const ID_FIRST = '01G6K4BTGT2DR8Y1002E180A5W';

    public function load(ObjectManager $manager): void
    {
        foreach ($this->getData() as $datum) {
            $video = new Video(
                $datum['title'],
                $datum['link'],
                $datum['description'] ?? '',
            );
            $video->setId(new Ulid($datum['id'] ?? null));
            $video->setPublishedAt($datum['publishedAt'] ?? null);
            $manager->persist($video);
        }
        $manager->flush();
    }

    /**
     * @return array<integer, array<string, mixed>>
     */
    public function getData(): iterable
    {
        yield [
            'id' => self::ID_FIRST,
            'title' => 'ЛЕСТВИЦА. 1 СТУПЕНЬ. ОБ ОТРЕЧЕНИИ ОТ МИРСКОГО ЖИТИЯ',
            'link' => 'https://www.youtube.com/watch?v=KY55qKblrNg',
            'publishedAt' => new \DateTimeImmutable(),
        ];
        yield [
            'title' => 'ЛЕСТВИЦА. 2 СТУПЕНЬ. О БЕСПРИСТРАСТИИ',
            'link' => 'https://www.youtube.com/watch?v=s1l5xAy6flM',
            'publishedAt' => new \DateTimeImmutable(),
        ];
        yield [
            'title' => 'ЛЕСТВИЦА. 3 СТУПЕНЬ. О СТРАННИЧЕСТВЕ',
            'link' => 'https://www.youtube.com/watch?v=0SL2Qeu9WNE',
            'publishedAt' => new \DateTimeImmutable(),
        ];
        yield [
            'title' => 'ЛЕСТВИЦА. 4 СТУПЕНЬ. О ПОСЛУШАНИИ',
            'link' => 'https://www.youtube.com/watch?v=2GTJkgHUBew',
            'publishedAt' => new \DateTimeImmutable(),
        ];
        yield [
            'title' => 'ЛЕСТВИЦА. 5 СТУПЕНЬ. ПОКАЯНИЕ',
            'link' => 'https://www.youtube.com/watch?v=CNPsuGyW4ng',
            'publishedAt' => new \DateTimeImmutable(),
        ];
        yield [
            'title' => 'ЛЕСТВИЦА. 6 СТУПЕНЬ. ПАМЯТЬ О СМЕРТИ',
            'link' => 'https://www.youtube.com/watch?v=QSTNTOIImg8',
            'publishedAt' => new \DateTimeImmutable(),
        ];
        yield [
            'title' => 'ЛЕСТВИЦА. 7 СТУПЕНЬ. О ПЛАЧЕ',
            'link' => 'https://www.youtube.com/watch?v=zZ45SBeaoLg',
            'publishedAt' => new \DateTimeImmutable(),
        ];
        yield [
            'title' => 'ЛЕСТВИЦА. 8 СТУПЕНЬ. О БЕЗГНЕВИИ И КРОТОСТИ',
            'link' => 'https://www.youtube.com/watch?v=8AnXYJaXSQM',
            'publishedAt' => new \DateTimeImmutable(),
        ];
        yield [
            'title' => 'ЛЕСТВИЦА. 9 СТУПЕНЬ. О ПАМЯТОЗЛОБИИ',
            'link' => 'https://www.youtube.com/watch?v=bee92JMtnhw',
            'publishedAt' => new \DateTimeImmutable(),
        ];
        yield [
            'title' => 'ЛЕСТВИЦА. 10 СТУПЕНЬ. О ЗЛОСЛОВИИ И КЛЕВЕТЕ',
            'link' => 'https://www.youtube.com/watch?v=ybntR38YbSQ',
            'publishedAt' => new \DateTimeImmutable(),
        ];
        yield [
            'title' => 'ВЕРИТЬ ЛИ ПРОГНОЗАМ АСТРОЛОГОВ? / ВЕРА И МАГИЯ',
            'link' => 'https://www.youtube.com/watch?v=6_JimFcZb2U',
            'publishedAt' => new \DateTimeImmutable(),
        ];
    }
}
