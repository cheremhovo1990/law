<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Symbol;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Ulid;

class SymbolFixture extends Fixture
{
    public const CODE_COMBINING_INVERTED_BREVE = 'COMBINING_INVERTED_BREVE';
    public const CODE_CYRILLIC_TITLO = 'CYRILLIC_TITLO';
    public const CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA = 'COMBINING_CYRILLIC_PSILI_PNEUMATA';
    public const CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA_COMBINING_GRAVE_ACCENT = 'CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA_COMBINING_GRAVE_ACCENT';
    public const CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA_AND_COMBINING_ACUTE_ACCENT = 'COMBINING_CYRILLIC_PSILI_PNEUMATA_AND_COMBINING_ACUTE_ACCENT';
    public const CODE_COMBINING_ACUTE_ACCENT = 'COMBINING_ACUTE_ACCENT';
    public const CODE_COMBINING_GRAVE_ACCENT = 'COMBINING_GRAVE_ACCENT';
    public const CODE_CYRILLIC_CAPITAL_LETTER_A = 'CYRILLIC_CAPITAL_LETTER_A';
    public const CODE_CYRILLIC_CAPITAL_LETTER_A_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT = 'CYRILLIC_CAPITAL_LETTER_A_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT';
    public const CODE_CYRILLIC_CAPITAL_LETTER_A_AND_PSILI_PNEUMATA = 'CYRILLIC_CAPITAL_LETTER_A_AND_PSILI_PNEUMATA';
    public const CODE_CYRILLIC_SMALL_LETTER_A = 'CYRILLIC_SMALL_LETTER_A';
    public const CODE_CYRILLIC_SMALL_LETTER_A_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT = 'CODE_CYRILLIC_SMALL_LETTER_A_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_A_AND_PSILI_PNEUMATA = 'CODE_CYRILLIC_SMALL_LETTER_A_AND_PSILI_PNEUMATA';
    public const CODE_CYRILLIC_SMALL_LETTER_A_AND_GRAVE_ACCENT = 'CODE_CYRILLIC_SMALL_LETTER_A_AND_GRAVE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_A_AND_ACUTE_ACCENT = 'CODE_CYRILLIC_SMALL_LETTER_A_AND_ACUTE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_A_AND_INVERTED_BREVE = 'CYRILLIC_SMALL_LETTER_A_AND_INVERTED_BREVE';
    public const CODE_CYRILLIC_THOUSANDS_SIGN = 'CYRILLIC_THOUSANDS_SIGN';
    public const CODE_COMBINING_CYRILLIC_LETTER_ES_AND_POKRYTIE = 'COMBINING_CYRILLIC_LETTER_ES_AND_POKRYTIE';
    public const CODE_COMBINING_CYRILLIC_LETTER_ER_AND_POKRYTIE = 'COMBINING_CYRILLIC_LETTER_ER_AND_POKRYTIE';
    public const CODE_COMBINING_CYRILLIC_LETTER_GHE_AND_POKRYTIE = 'COMBINING_CYRILLIC_LETTER_GHE_AND_POKRYTIE';
    public const CODE_COMBINING_CYRILLIC_LETTER_O_AND_POKRYTIE = 'COMBINING_CYRILLIC_LETTER_O_AND_POKRYTIE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_BE = 'CYRILLIC_CAPITAL_LETTER_BE';
    public const CODE_CYRILLIC_SMALL_LETTER_BE = 'CYRILLIC_SMALL_LETTER_BE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_VE = 'CYRILLIC_CAPITAL_LETTER_VE';
    public const CODE_CYRILLIC_SMALL_LETTER_VE = 'CYRILLIC_SMALL_LETTER_VE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_GHE = 'CYRILLIC_CAPITAL_LETTER_GHE';
    public const CODE_CYRILLIC_SMALL_LETTER_GHE = 'CYRILLIC_SMALL_LETTER_GHE';
    public const CODE_CYRILLIC_SMALL_LETTER_GHE_AND_COMBINING_CYRILLIC_DE = 'CYRILLIC_SMALL_LETTER_GHE_AND_COMBINING_CYRILLIC_DE';
    public const CODE_CYRILLIC_SMALL_LETTER_GHE_AND_TITLO = 'CYRILLIC_SMALL_LETTER_GHE_AND_TITLO';

    public const CODE_CYRILLIC_CAPITAL_LETTER_DE = 'CYRILLIC_CAPITAL_LETTER_DE';
    public const CODE_CYRILLIC_SMALL_LETTER_DE = 'CYRILLIC_SMALL_LETTER_DE';
    public const CODE_CYRILLIC_SMALL_LETTER_DE_AND_LETTER_ES_AND_POKRYTIE = 'CYRILLIC_SMALL_LETTER_DE_AND_LETTER_ES_AND_POKRYTIE';
    public const CODE_COMBINING_CYRILLIC_DE = 'COMBINING_CYRILLIC_DE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_IE = 'CYRILLIC_CAPITAL_LETTER_IE';
    public const CODE_CYRILLIC_SMALL_LETTER_IE = 'CYRILLIC_SMALL_LETTER_IE';
    public const CODE_CYRILLIC_SMALL_LETTER_UKRAINIAN_IE = 'CYRILLIC_SMALL_LETTER_UKRAINIAN_IE';
    public const CODE_CYRILLIC_SMALL_LETTER_IE_AND_COMBINING_ACUTE_ACCENT = 'CYRILLIC_SMALL_LETTER_UKRAINIAN_IE_COMBINING_ACUTE_ACCENT';

    public const CODE_CYRILLIC_CAPITAL_LETTER_ZHE = 'CYRILLIC_CAPITAL_LETTER_ZHE';
    public const CODE_CYRILLIC_SMALL_LETTER_ZHE = 'CYRILLIC_SMALL_LETTER_ZHE';
    public const CODE_CYRILLIC_SMALL_LETTER_ZHE_AND_CYRILLIC_TITLO = 'CYRILLIC_SMALL_LETTER_ZHE_AND_CYRILLIC_TITLO';
    public const CODE_CYRILLIC_SMALL_LETTER_ZHE_AND_LETTER_ES_AND_POKRYTIE = 'CYRILLIC_SMALL_LETTER_ZHE_AND_LETTER_ES_AND_POKRYTIE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_DZE = 'CYRILLIC_CAPITAL_LETTER_DZE';
    public const CODE_CYRILLIC_SMALL_LETTER_DZE = 'CYRILLIC_SMALL_LETTER_DZE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_ZE = 'CYRILLIC_CAPITAL_LETTER_ZE';
    public const CODE_CYRILLIC_SMALL_LETTER_ZE = 'CYRILLIC_SMALL_LETTER_ZE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_I = 'CYRILLIC_CAPITAL_LETTER_I';
    public const CODE_CYRILLIC_CAPITAL_LETTER_SHORT_I = 'CYRILLIC_CAPITAL_LETTER_SHORT_I';
    public const CODE_CYRILLIC_SMALL_LETTER_SHORT_I = 'CYRILLIC_SMALL_LETTER_SHORT_I';
    public const CODE_CYRILLIC_SMALL_LETTER_I = 'CYRILLIC_SMALL_LETTER_I';
    public const CODE_CYRILLIC_SMALL_LETTER_I_AND_COMBINING_ACUTE_ACCENT = 'CYRILLIC_SMALL_LETTER_I_AND_COMBINING_ACUTE_ACCENT';

    public const CODE_CYRILLIC_CAPITAL_LETTER_BYELORUSSIAN_UKRAINIAN_I = 'CYRILLIC_CAPITAL_LETTER_BYELORUSSIAN_UKRAINIAN_I';
    public const CODE_CYRILLIC_SMALL_LETTER_BYELORUSSIAN_UKRAINIAN_I = 'CYRILLIC_SMALL_LETTER_BYELORUSSIAN_UKRAINIAN_I';
    public const CODE_CYRILLIC_CAPITAL_LETTER_YI = 'CYRILLIC_CAPITAL_LETTER_YI';
    public const CODE_CYRILLIC_CAPITAL_LETTER_YI_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA = 'CYRILLIC_CAPITAL_LETTER_YI_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA';
    public const CODE_CYRILLIC_SMALL_LETTER_YI = 'CYRILLIC_SMALL_LETTER_YI';
    public const CODE_CYRILLIC_SMALL_LETTER_YI_AND_COMBINING_ACUTE_ACCENT = 'CYRILLIC_SMALL_LETTER_YI_AND_COMBINING_ACUTE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_YI_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA = 'CYRILLIC_SMALL_LETTER_YI_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA';

    public const CODE_CYRILLIC_CAPITAL_LETTER_KA = 'CYRILLIC_CAPITAL_LETTER_KA';
    public const CODE_CYRILLIC_SMALL_LETTER_KA = 'CYRILLIC_SMALL_LETTER_KA';

    public const CODE_CYRILLIC_CAPITAL_LETTER_EL = 'CYRILLIC_CAPITAL_LETTER_EL';
    public const CODE_CYRILLIC_SMALL_LETTER_EL = 'CYRILLIC_SMALL_LETTER_EL';
    public const CODE_CYRILLIC_SMALL_LETTER_EL_AND_CYRILLIC_TITLO = 'CYRILLIC_SMALL_LETTER_EL_AND_CYRILLIC_TITLO';
    public const CODE_CYRILLIC_SMALL_LETTER_EL_AND_COMBINING_CYRILLIC_DE = 'CYRILLIC_SMALL_LETTER_EL_AND_COMBINING_CYRILLIC_DE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_EM = 'CYRILLIC_CAPITAL_LETTER_EM';
    public const CODE_CYRILLIC_SMALL_LETTER_EM = 'CYRILLIC_SMALL_LETTER_EM';

    public const CODE_CYRILLIC_CAPITAL_LETTER_EN = 'CYRILLIC_CAPITAL_LETTER_EN';
    public const CODE_CYRILLIC_SMALL_LETTER_EN = 'CYRILLIC_SMALL_LETTER_EN';

    public const CODE_CYRILLIC_CAPITAL_LETTER_O = 'CYRILLIC_CAPITAL_LETTER_O';
    public const CODE_CYRILLIC_CAPITAL_LETTER_ROUND_OMEGA = 'CYRILLIC_CAPITAL_LETTER_ROUND_OMEGA';
    public const CODE_CYRILLIC_SMALL_LETTER_ROUND_OMEGA = 'CYRILLIC_SMALL_LETTER_ROUND_OMEGA';
    public const CODE_CYRILLIC_CAPITAL_LETTER_O_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA = 'CODE_CYRILLIC_CAPITAL_LETTER_O_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA';
    public const CODE_CYRILLIC_SMALL_LETTER_O = 'CYRILLIC_SMALL_LETTER_O';
    public const CODE_CYRILLIC_SMALL_LETTER_O_AND_COMBINING_ACUTE_ACCENT = 'CYRILLIC_SMALL_LETTER_O_AND_COMBINING_ACUTE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_O_AND_PSILI_PNEUMATA_AND_COMBINING_ACUTE_ACCENT = 'CYRILLIC_SMALL_LETTER_O_AND_PSILI_PNEUMATA_AND_COMBINING_ACUTE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_O_AND_COMBINING_GRAVE_ACCENT = 'CYRILLIC_SMALL_LETTER_O_AND_COMBINING_GRAVE_ACCENT';

    public const CODE_CYRILLIC_CAPITAL_LETTER_PE = 'CYRILLIC_CAPITAL_LETTER_PE';
    public const CODE_CYRILLIC_SMALL_LETTER_PE = 'CYRILLIC_SMALL_LETTER_PE';
    public const CODE_CYRILLIC_SMALL_LETTER_PE_AND_LETTER_ES_AND_POKRYTIE = 'CYRILLIC_SMALL_LETTER_PE_AND_LETTER_ES_AND_POKRYTIE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_ER = 'CYRILLIC_CAPITAL_LETTER_ER';
    public const CODE_CYRILLIC_SMALL_LETTER_ER = 'CYRILLIC_SMALL_LETTER_ER';
    public const CODE_CYRILLIC_SMALL_LETTER_ER_AND_COMBINING_CYRILLIC_DE = 'CYRILLIC_SMALL_LETTER_ER_AND_COMBINING_CYRILLIC_DE';
    public const CODE_CYRILLIC_SMALL_LETTER_ER_AND_CYRILLIC_TITLO = 'CYRILLIC_SMALL_LETTER_ER_AND_CYRILLIC_TITLO';
    public const CODE_CYRILLIC_SMALL_LETTER_ER_AND_LETTER_ES_AND_POKRYTIE = 'CYRILLIC_SMALL_LETTER_ER_AND_LETTER_ES_AND_POKRYTIE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_ES = 'CYRILLIC_CAPITAL_LETTER_ES';
    public const CODE_CYRILLIC_SMALL_LETTER_ES = 'CYRILLIC_SMALL_LETTER_ES';

    public const CODE_CYRILLIC_CAPITAL_LETTER_TE = 'CYRILLIC_CAPITAL_LETTER_TE';
    public const CODE_CYRILLIC_SMALL_LETTER_TE = 'CYRILLIC_SMALL_LETTER_TE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_UK = 'CYRILLIC_CAPITAL_LETTER_UK';
    public const CODE_CYRILLIC_CAPITAL_LETTER_UK_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT = 'CYRILLIC_CAPITAL_LETTER_UK_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_UK = 'CYRILLIC_SMALL_LETTER_UK';
    public const CODE_CYRILLIC_SMALL_LETTER_MONOGRAPH_UK = 'CYRILLIC_SMALL_LETTER_MONOGRAPH_UK';
    public const CODE_CYRILLIC_SMALL_LETTER_MONOGRAPH_UK_AND_COMBINING_ACUTE_ACCENT = 'CYRILLIC_SMALL_LETTER_MONOGRAPH_UK_AND_COMBINING_ACUTE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_MONOGRAPH_UK_AND_GRAVE_ACCENT = 'CYRILLIC_SMALL_LETTER_MONOGRAPH_UK_AND_GRAVE_ACCENT';

    public const CODE_CYRILLIC_CAPITAL_LETTER_U = 'CYRILLIC_CAPITAL_LETTER_U';
    public const CODE_CYRILLIC_SMALL_LETTER_U = 'CYRILLIC_SMALL_LETTER_U';

    public const CODE_CYRILLIC_CAPITAL_LETTER_EF = 'CYRILLIC_CAPITAL_LETTER_EF';
    public const CODE_CYRILLIC_SMALL_LETTER_EF = 'CYRILLIC_SMALL_LETTER_EF';

    public const CODE_CYRILLIC_CAPITAL_LETTER_HA = 'CYRILLIC_CAPITAL_LETTER_HA';
    public const CODE_CYRILLIC_SMALL_LETTER_HA = 'CYRILLIC_SMALL_LETTER_HA';

    public const CODE_CYRILLIC_CAPITAL_LETTER_OMEGA = 'CYRILLIC_CAPITAL_LETTER_OMEGA';
    public const CODE_CYRILLIC_SMALL_LETTER_OMEGA = 'CYRILLIC_SMALL_LETTER_OMEGA';
    public const CODE_CYRILLIC_SMALL_LETTER_OMEGA_AND_COMBINING_ACUTE_ACCENT = 'CYRILLIC_SMALL_LETTER_OMEGA_AND_COMBINING_ACUTE_ACCENT';

    public const CODE_CYRILLIC_CAPITAL_LETTER_OT = 'CYRILLIC_CAPITAL_LETTER_OT';
    public const CODE_CYRILLIC_SMALL_LETTER_OT = 'CYRILLIC_SMALL_LETTER_OT';

    public const CODE_CYRILLIC_CAPITAL_LETTER_TSE = 'CYRILLIC_CAPITAL_LETTER_TSE';
    public const CODE_CYRILLIC_SMALL_LETTER_TSE = 'CYRILLIC_SMALL_LETTER_TSE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_CHE = 'CYRILLIC_CAPITAL_LETTER_CHE';
    public const CODE_CYRILLIC_SMALL_LETTER_CHE = 'CYRILLIC_SMALL_LETTER_CHE';
    public const CODE_CYRILLIC_SMALL_LETTER_CHE_AND_LETTER_ES_AND_POKRYTIE = 'CYRILLIC_SMALL_LETTER_CHE_AND_LETTER_ES_AND_POKRYTIE';

    public const CODE_CYRILLIC_CAPITAL_LETTER_SHA = 'CYRILLIC_CAPITAL_LETTER_SHA';
    public const CODE_CYRILLIC_SMALL_LETTER_SHA = 'CYRILLIC_SMALL_LETTER_SHA';

    public const CODE_CYRILLIC_CAPITAL_LETTER_SHCHA = 'CYRILLIC_CAPITAL_LETTER_SHCHA';
    public const CODE_CYRILLIC_SMALL_LETTER_SHCHA = 'CYRILLIC_SMALL_LETTER_SHCHA';

    public const CODE_CYRILLIC_SMALL_LETTER_HARD_SIGN = 'CYRILLIC_SMALL_LETTER_HARD_SIGN';
    public const CODE_COMBINING_VERTICAL_TILDE = 'COMBINING_VERTICAL_TILDE';
    public const CODE_CYRILLIC_SMALL_LETTER_SOFT_SIGN = 'CYRILLIC_SMALL_LETTER_SOFT_SIGN';
    public const CODE_CYRILLIC_SMALL_LETTER_YERU = 'CYRILLIC_SMALL_LETTER_YERU';
    public const CODE_CYRILLIC_SMALL_LETTER_YERU_AND_ACUTE_ACCENT = 'CYRILLIC_SMALL_LETTER_YERU_AND_ACUTE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_TSHE = 'CYRILLIC_SMALL_LETTER_TSHE';
    public const CODE_CYRILLIC_SMALL_LETTER_TSHE_AND_ACUTE_ACCENT = 'CYRILLIC_SMALL_LETTER_TSHE_AND_ACUTE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_TSHE_AND_GRAVE_ACCENT = 'CYRILLIC_SMALL_LETTER_TSHE_AND_GRAVE_ACCENT';

    public const CODE_CYRILLIC_CAPITAL_LETTER_YU = 'CYRILLIC_CAPITAL_LETTER_YU';
    public const CODE_CYRILLIC_SMALL_LETTER_YU = 'CYRILLIC_SMALL_LETTER_YU';
    public const CODE_CYRILLIC_CAPITAL_LETTER_YUS = 'CYRILLIC_CAPITAL_LETTER_YUS';
    public const CODE_CYRILLIC_SMALL_LETTER_YUS = 'CYRILLIC_SMALL_LETTER_YUS';
    public const CODE_CYRILLIC_SMALL_LETTER_YUS_AND_COMBINING_GRAVE_ACCENT = 'CYRILLIC_SMALL_LETTER_YUS_AND_COMBINING_GRAVE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_YUS_AND_ACUTE_ACCENT = 'CYRILLIC_SMALL_LETTER_YUS_AND_ACUTE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_YUS_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT = 'CYRILLIC_SMALL_LETTER_YUS_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_YUS_AND_PSILI_PNEUMATA = 'CYRILLIC_SMALL_LETTER_YUS_AND_PSILI_PNEUMATA';

    public const CODE_CYRILLIC_CAPITAL_LETTER_IOTIFIED_A = 'CYRILLIC_CAPITAL_LETTER_IOTIFIED_A';
    public const CODE_CYRILLIC_SMALL_LETTER_IOTIFIED_A = 'CYRILLIC_SMALL_LETTER_IOTIFIED_A';

    public const CODE_CYRILLIC_CAPITAL_LETTER_KSI = 'CYRILLIC_CAPITAL_LETTER_KSI';
    public const CODE_CYRILLIC_SMALL_LETTER_KSI = 'CYRILLIC_SMALL_LETTER_KSI';

    public const CODE_CYRILLIC_CAPITAL_LETTER_PSI = 'CYRILLIC_CAPITAL_LETTER_PSI';
    public const CODE_CYRILLIC_SMALL_LETTER_PSI = 'CYRILLIC_SMALL_LETTER_PSI';

    public const CODE_CYRILLIC_CAPITAL_LETTER_FITA = 'CYRILLIC_CAPITAL_LETTER_FITA';
    public const CODE_CYRILLIC_SMALL_LETTER_FITA = 'CYRILLIC_SMALL_LETTER_FITA';

    public const CODE_CYRILLIC_CAPITAL_LETTER_IZHITSA = 'CYRILLIC_CAPITAL_LETTER_IZHITSA';
    public const CODE_CYRILLIC_SMALL_LETTER_IZHITSA = 'CYRILLIC_SMALL_LETTER_IZHITSA';
    public const CODE_CYRILLIC_SMALL_LETTER_IZHITSA_AND_DOUBLE_GRAVE_ACCENT = 'CYRILLIC_SMALL_LETTER_IZHITSA_AND_DOUBLE_GRAVE_ACCENT';
    public const CODE_CYRILLIC_SMALL_LETTER_IZHITSA_AND_COMBINING_CYRILLIC_LETTER_GHE_AND_POKRYTIE = 'CYRILLIC_SMALL_LETTER_IZHITSA_AND_COMBINING_CYRILLIC_LETTER_GHE_AND_POKRYTIE';

    public function load(ObjectManager $manager): void
    {
        foreach ($this->getData() as $datum) {
            $entity = new Symbol(
                code: $datum['code'],
                name: $datum['name'],
            );
            $entity->setId(new Ulid($datum['id'] ?? null));
            $manager->persist($entity);
        }
        $manager->flush();
    }

    /**
     * @return array<integer, array<string, string>>
     */
    public function getData(): iterable
    {
        yield [
            'id' => '01GA4DZY23RJXQQ2FP627E9ZDS',
            'code' => self::CODE_COMBINING_INVERTED_BREVE,
            'name' => 'Дуга',
        ];

        yield [
            'id' => '01G9AHRMS3XVXG7EDKY3F741BD',
            'code' => self::CODE_CYRILLIC_TITLO,
            'name' => 'Титло',
        ];

        yield [
            'id' => '01G9J7P8BJ0GJ30ET8RC6S3VBG',
            'code' => self::CODE_CYRILLIC_THOUSANDS_SIGN,
            'name' => 'Тисяча',
        ];
        yield [
            'id' => '01G9J27N5ZJXZ41ZYB5QD1PYVW',
            'code' => self::CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA,
            'name' => 'Придыхание',
        ];
        yield [
            'id' => '01GCC5NVXCG9NM25R3WB601MV3',
            'code' => self::CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA_COMBINING_GRAVE_ACCENT,
            'name' => 'Придыхание и ударение тяжелое',
        ];
        yield [
            'id' => '01GBAQ97GTAES0MSCG8D5KSDWT',
            'code' => self::CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA_AND_COMBINING_ACUTE_ACCENT,
            'name' => 'Придыхание и ударение острое',
        ];
        yield [
            'id' => '01G9J2VEDTCHR0GQ9TQP5BGTMP',
            'code' => self::CODE_COMBINING_ACUTE_ACCENT,
            'name' => 'Ударение острое',
        ];
        yield [
            'id' => '01G9J8EXHEAX1HVMQ8ESD8XFD9',
            'code' => self::CODE_COMBINING_GRAVE_ACCENT,
            'name' => 'Ударение тяжелое',
        ];
        yield [
            'id' => '01G8ZP66JMSSCMD83F514TYPCZ',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_A,
            'name' => 'Заглавная буква азь',
        ];
        yield [
            'id' => '01G9QB8KD8WM0H0QND9H7A1VDR',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_A_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT,
            'name' => 'Заглавная буква азь c предыханием и острым ударением',
        ];
        yield [
            'id' => '01GA4AFYKRAT4CNQ63XCECE8V6',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_A_AND_PSILI_PNEUMATA,
            'name' => 'Заглавная буква азь c придыханием',
        ];
        yield [
            'id' => '01G909V4YN9N4BYTPGN42MDC04',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_A,
            'name' => 'Маленькая буква азь',
        ];
        yield [
            'id' => '01GA6YDJAYAAPPZ7Q3AGXAPVFD',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_A_AND_INVERTED_BREVE,
            'name' => 'Маленькая буква азь c дугой',
        ];
        yield [
            'id' => '01GA9558HD71XF8DABB26S0HEN',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_A_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT,
            'name' => 'Маленькая буква азь c придыханием и острым ударением',
        ];
        yield [
            'id' => '01GA97FD6A0ZHV04R25ERHJPXS',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_A_AND_PSILI_PNEUMATA,
            'name' => 'Маленькая буква азь c придыханием',
        ];
        yield [
            'id' => '01GA97F1XFRWV0ECW5MJW2P0FQ',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_A_AND_GRAVE_ACCENT,
            'name' => 'Маленькая буква азь c тяжелым ударением',
        ];
        yield [
            'id' => '01GA96GT1KFBX7YF5YNZCZGC2A',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_A_AND_ACUTE_ACCENT,
            'name' => 'Маленькая буква азь c острым ударением',
        ];
        yield [
            'id' => '01G8ZPNW8A8SFWB9J493NWWNT0',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_BE,
            'name' => 'Заглавная буква буки',
        ];
        yield [
            'id' => '01G90CMAWQPXPS618BFEVEQP18',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_BE,
            'name' => 'Маленькая буква буки',
        ];
        yield [
            'id' => '01G8ZQBQVJSPQCQZBBKEB0M9KW',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_VE,
            'name' => 'Заглавная буква веди',
        ];
        yield [
            'id' => '01G90CPHKB9Y401P0W8TYH19RW',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_VE,
            'name' => 'Маленькая буква веди',
        ];
        yield [
            'id' => '01G8ZQW64R2BZER12C29P48AHT',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_GHE,
            'name' => 'Заглавная буква глагол',
        ];
        yield [
            'id' => '01G90CQZPJRRT5TM3FVTPM9YNB',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_GHE,
            'name' => 'Маленькая буква глагол',
        ];
        yield [
            'id' => '01GB8G16ETR1WX39P4FTQNKAM9',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_GHE_AND_COMBINING_CYRILLIC_DE,
            'name' => 'Маленькая буква глагол и над строчная буква добро',
        ];
        yield [
            'id' => '01GB3AK5YJG9WN33YF0VSGBHEY',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_GHE_AND_TITLO,
            'name' => 'Маленькая буква глагол c титлом',
        ];
        yield [
            'id' => '01G9MN1M6H1M3JBH3ZSYTMFA7K',
            'code' => self::CODE_COMBINING_CYRILLIC_LETTER_GHE_AND_POKRYTIE,
            'name' => 'Над строчная буква глагол',
        ];
        yield [
            'id' => '01G8ZRYE96WZZ79X1ZCR12NRKT',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_DE,
            'name' => 'Заглавная буква добро',
        ];
        yield [
            'id' => '01G90CT70E9NJQGPCV0W5TAC9H',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_DE,
            'name' => 'Маленькая буква добро',
        ];
        yield [
            'id' => '01GBAMWP02ZZWYG7GY1YAVQH60',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_DE_AND_LETTER_ES_AND_POKRYTIE,
            'name' => 'Маленькая буква добро и над строчная буква слово',
        ];
        yield [
            'id' => '01G9JCRWPPJZQTXRFH8K32YXHT',
            'code' => self::CODE_COMBINING_CYRILLIC_DE,
            'name' => 'Над строчная буква добро',
        ];
        yield [
            'id' => '01G8ZS4TS8RYXA6466MSTSS4K2',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_IE,
            'name' => 'Заглавная буква есть',
        ];
        yield [
            'id' => '01G90CWXKAEHXWFCP399MFPJQC',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_IE,
            'name' => 'Маленькая буква есть',
        ];
        yield [
            'id' => '01G9ACCMPTCVS1YBNB6NJQXXMB',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_UKRAINIAN_IE,
            'name' => 'Маленькая буква есть',
        ];
        yield [
            'id' => '01GB847Z5VAB8R68FQF7BC65W9',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_IE_AND_COMBINING_ACUTE_ACCENT,
            'name' => 'Маленькая буква есть и ударение острое',
        ];

        yield [
            'id' => '01G8ZS7Z04WQ31XHJAR502MM81',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_ZHE,
            'name' => 'Заглавная буква живите',
        ];
        yield [
            'id' => '01G90D06JZ7J8JZFTXJPKR7EAT',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_ZHE,
            'name' => 'Маленькая буква живите',
        ];
        yield [
            'id' => '01GBZ4DBTAVMDW3YFB24DP0T6H',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_ZHE_AND_CYRILLIC_TITLO,
            'name' => 'Маленькая буква живите и титло',
        ];
        yield [
            'id' => '01GBANDQQRG9KH2DXXQ0Y8VD5Z',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_ZHE_AND_LETTER_ES_AND_POKRYTIE,
            'name' => 'Маленькая буква живите и над строчная буква слово',
        ];
        yield [
            'id' => '01G8ZS9KHNP2GVSJYV1GS8XDPN',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_DZE,
            'name' => 'Заглавная буква зело',
        ];
        yield [
            'id' => '01G90D422SC7ESQ0TS1K2AKE3J',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_DZE,
            'name' => 'Маленькая буква зело',
        ];
        yield [
            'id' => '01G909XX09TS70AJKDN9MYJ0JS',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_ZE,
            'name' => 'Заглавная буква земля',
        ];
        yield [
            'id' => '01G90D8ETZGND9GEERPFP6GKXQ',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_ZE,
            'name' => 'Маленькая буква земля',
        ];
        yield [
            'id' => '01G8ZSHNNECD6N1S793HC62F8P',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_I,
            'name' => 'Заглавная буква иже',
        ];
        yield [
            'id' => '01GBMSW13FHNGH329BWQ9HX70X',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_SHORT_I,
            'name' => 'Заглавная буква и краткая',
        ];
        yield [
            'id' => '01GC70SXY77BFZVWH64ZNJAPHZ',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_SHORT_I,
            'name' => 'Маленькая буква и краткая',
        ];
        yield [
            'id' => '01G90DC8J0SCYFG91D7V59NEFD',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_I,
            'name' => 'Маленькая буква иже',
        ];
        yield [
            'id' => '01GBAGRSW7KA3VZRD4M8NWSH9R',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_I_AND_COMBINING_ACUTE_ACCENT,
            'name' => 'Маленькая буква иже и ударение острое',
        ];
        yield [
            'id' => '01G8ZSKVDT0E2G4F72BP65VQXR',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_BYELORUSSIAN_UKRAINIAN_I,
            'name' => 'Заглавная буква и',
        ];
        yield [
            'id' => '01G90EAB3689HPPS2GRMEWNY3A',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_BYELORUSSIAN_UKRAINIAN_I,
            'name' => 'Маленькая буква и',
        ];
        yield [
            'id' => '01G9D8389S8B9W9MP9RB2CAW8S',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_YI,
            'name' => 'Заглавная буква и десятеричное',
        ];
        yield [
            'id' => '01GBDEXJTEBFQ4RNN3V15ZDMKA',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_YI_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA,
            'name' => 'Заглавная буква и десятеричное и придыхание',
        ];
        yield [
            'id' => '01G9D83G9TETG66CH52GYNN719',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_YI,
            'name' => 'Маленькая буква и десятеричное',
        ];
        yield [
            'id' => '01GBAXRVZ2SSQWEVB8Q6X8MDMM',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_YI_AND_COMBINING_ACUTE_ACCENT,
            'name' => 'Маленькая буква и десятеричное и ударение острое',
        ];
        yield [
            'id' => '01GBN60T99JK08G78QDV7H7MZC',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_YI_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA,
            'name' => 'Маленькая буква и десятеричное и придыхание',
        ];
        yield [
            'id' => '01G8ZTP8AVFRVZBTKQ5HMPBWEK',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_KA,
            'name' => 'Заглавная буква како',
        ];
        yield [
            'id' => '01G90E9XNHP81SFBGG2DNG3WEW',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_KA,
            'name' => 'Маленькая буква како',
        ];
        yield [
            'id' => '01G8ZTRYKD03S9TJBCP8WJTEFB',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_EL,
            'name' => 'Заглавная буква люди',
        ];
        yield [
            'id' => '01G90E9BC9WT40X7DK2GBTHTMW',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_EL,
            'name' => 'Маленькая буква люди',
        ];
        yield [
            'id' => '01GCC0D1JV2XHYN2TMHSHEE5CC',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_EL_AND_CYRILLIC_TITLO,
            'name' => 'Маленькая буква люди и титло',
        ];
        yield [
            'id' => '01GBAFR84B6EEXK9RSPQTJG8QZ',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_EL_AND_COMBINING_CYRILLIC_DE,
            'name' => 'Маленькая буква люди и над строчная буква добро',
        ];
        yield [
            'id' => '01G8ZTTZ9TM4EWVAF8FXNPSMPZ',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_EM,
            'name' => 'Заглавная буква мыслите',
        ];
        yield [
            'id' => '01G90EB6A9TTSDRKP6EMNR943T',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_EM,
            'name' => 'Маленькая буква мыслите',
        ];
        yield [
            'id' => '01G8ZTX4AHNKNMK4NTDDW4MCT5',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_EN,
            'name' => 'Заглавная буква нашь',
        ];
        yield [
            'id' => '01G90ECA1GQMYQBPEG1VHWR8KS',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_EN,
            'name' => 'Маленькая буква нашь',
        ];
        yield [
            'id' => '01G8ZTYMHWGHCARRF2G1RY6KR6',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_O,
            'name' => 'Заглавная буква он',
        ];
        yield [
            'id' => '01GFK4NFDMT46D5NTY4W8HY9MW',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_ROUND_OMEGA,
            'name' => 'Заглавная буква он',
        ];
        yield [
            'id' => '01GFK4MY74J7RA7AWSB6SZ91EZ',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_ROUND_OMEGA,
            'name' => 'Заглавная буква он',
        ];
        yield [
            'id' => '01GCC7B74136VS4G6DEZGVTATH',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_O_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA,
            'name' => 'Заглавная буква он и придыхание',
        ];
        yield [
            'id' => '01G90EE0NK2DA9V9H2G9RTTZZZ',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_O,
            'name' => 'Маленькая буква он',
        ];
        yield [
            'id' => '01GB8BTZEBP2H480VH5QJ8FD3A',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_O_AND_COMBINING_ACUTE_ACCENT,
            'name' => 'Маленькая буква он и ударение острое',
        ];
        yield [
            'id' => '01GBSYQAVAK3A86HNHTK5GKK9Q',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_O_AND_PSILI_PNEUMATA_AND_COMBINING_ACUTE_ACCENT,
            'name' => 'Маленькая буква он с придыханием и ударение острое',
        ];
        yield [
            'id' => '01GBANR80TJ8E844C35DT21Z5R',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_O_AND_COMBINING_GRAVE_ACCENT,
            'name' => 'Маленькая буква он и ударение тяжелое',
        ];
        yield [
            'id' => '01G9MNZK7XCXJXE7M6P39P2FKT',
            'code' => self::CODE_COMBINING_CYRILLIC_LETTER_O_AND_POKRYTIE,
            'name' => 'Над строчная буква он',
        ];
        yield [
            'id' => '01G8ZV3XH3BM9B9X67BXHNTNHJ',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_PE,
            'name' => 'Заглавная буква покой',
        ];
        yield [
            'id' => '01G90EEV1BF6VTZN3MMZ6E9TJZ',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_PE,
            'name' => 'Маленькая буква покой',
        ];
        yield [
            'id' => '01GB5H1F5R9HJRRSR7Z47GNS9R',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_PE_AND_LETTER_ES_AND_POKRYTIE,
            'name' => 'Маленькая буква покой и над строчная буква слово',
        ];
        yield [
            'id' => '01G8ZV61GNQZ7ZND1X3C8SFRP4',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_ER,
            'name' => 'Заглавная буква рцы',
        ];
        yield [
            'id' => '01G90EJYT7WT2ZM8JD67PHP7D6',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_ER,
            'name' => 'Маленькая буква рцы',
        ];
        yield [
            'id' => '01GBAG10BSDCRH0F9G89MNWNWG',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_ER_AND_COMBINING_CYRILLIC_DE,
            'name' => 'Маленькая буква рцы и над строчная буква добро',
        ];
        yield [
            'id' => '01GBN6BW0W8FTX6BKFB52VS8ND',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_ER_AND_CYRILLIC_TITLO,
            'name' => 'Маленькая буква рцы и титло',
        ];
        yield [
            'id' => '01GB8CTBRRKS2FF05N8EJWHQHK',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_ER_AND_LETTER_ES_AND_POKRYTIE,
            'name' => 'Маленькая буква рцы и над строчная буква слово',
        ];
        yield [
            'id' => '01G9MMC69JPP3A0G7WQVF2R0HE',
            'code' => self::CODE_COMBINING_CYRILLIC_LETTER_ER_AND_POKRYTIE,
            'name' => 'Над строчная буква рцы',
        ];
        yield [
            'id' => '01G8ZV6HMAEEW2JEYWYC5WE7ZY',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_ES,
            'name' => 'Заглавная буква слово',
        ];
        yield [
            'id' => '01G925KEEENQJT15K6N5P4SPBK',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_ES,
            'name' => 'Маленькая буква слово',
        ];
        yield [
            'id' => '01G9JDS96FW9PENBAKXE7GP673',
            'code' => self::CODE_COMBINING_CYRILLIC_LETTER_ES_AND_POKRYTIE,
            'name' => 'Над строчная буква слово',
        ];
        yield [
            'id' => '01G8ZV7WM620MMMEX61KSZ9GEG',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_TE,
            'name' => 'Заглавная буква твердо',
        ];
        yield [
            'id' => '01G925MGQ9707B6E51QNW5TZG1',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_TE,
            'name' => 'Маленькая буква твердо',
        ];
        yield [
            'id' => '01G8ZV9AAGZ1QKBZH17EFZ1E93',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_UK,
            'name' => 'Заглавная буква укь',
        ];
        yield [
            'id' => '01GBN81YVER41AA988DAMH910V',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_UK_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT,
            'name' => 'Заглавная буква укь c предыханием и острым ударением',
        ];
        yield [
            'id' => '01GBN7VZMXNBBXE211A8HEW885',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_UK,
            'name' => 'Маленькая буква укь',
        ];
        yield [
            'id' => '01G9FNCMP6RZ6389Y8TN9386HN',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_MONOGRAPH_UK,
            'name' => 'Маленькая буква укь',
        ];
        yield [
            'id' => '01GBAY43KZAR72RRRJ5ZSJYNRY',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_MONOGRAPH_UK_AND_COMBINING_ACUTE_ACCENT,
            'name' => 'Маленькая буква укь и ударение острое',
        ];
        yield [
            'id' => '01GBWMVJ2NNWM4GHRW4PKTVD9F',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_MONOGRAPH_UK_AND_GRAVE_ACCENT,
            'name' => 'Маленькая буква укь и ударение тяжелое',
        ];
        yield [
            'id' => '01G9FFKV0QGY8FGJ8E9HBRC1X2',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_U,
            'name' => 'Заглавная буква икь',
        ];
        yield [
            'id' => '01G9FFM4CMRNQZB6WTYVWAY22Z',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_U,
            'name' => 'Маленькая буква икь',
        ];
        yield [
            'id' => '01G8ZVBGAAJ0SP9QV46BKS1A45',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_EF,
            'name' => 'Заглавная буква ферть',
        ];
        yield [
            'id' => '01G925Q3KMBZ842CN5BPZBGAMS',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_EF,
            'name' => 'Маленькая буква ферть',
        ];
        yield [
            'id' => '01G8ZZ9K16T5ENW0TF1KRVVCCM',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_HA,
            'name' => 'Заглавная буква херь',
        ];
        yield [
            'id' => '01G925R0547XTEYSZQ8ZG55Z2R',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_HA,
            'name' => 'Маленькая буква херь',
        ];
        yield [
            'id' => '01G8ZZAYE5N01KMSBA54NZ28VX',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_OMEGA,
            'name' => 'Заглавная буква омега',
        ];
        yield [
            'id' => '01G925SEBT4PAENXNVE6W74EZ1',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_OMEGA,
            'name' => 'Маленькая буква омега',
        ];
        yield [
            'id' => '01GBDFAM1PER4GBH5BQK6Y032T',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_OMEGA_AND_COMBINING_ACUTE_ACCENT,
            'name' => 'Маленькая буква омега и ударение острое',
        ];
        yield [
            'id' => '01G8ZZDQ4VK8CCJEKXVRQM40MZ',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_OT,
            'name' => 'Заглавная буква оть',
        ];
        yield [
            'id' => '01G925T0CJMEZZMQRVM66R2R1X',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_OT,
            'name' => 'Маленькая буква оть',
        ];
        yield [
            'id' => '01G8ZZNSTRDB9WNZRSSYZJ2ZV6',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_TSE,
            'name' => 'Заглавная буква цы',
        ];
        yield [
            'id' => '01G925WTNTZQ171XQR9K38DVKM',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_TSE,
            'name' => 'Маленькая буква цы',
        ];
        yield [
            'id' => '01G8ZZPRH5PC23S5A2N0TQ7AMK',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_CHE,
            'name' => 'Заглавная буква червь',
        ];
        yield [
            'id' => '01G925XZ3RAF9324CCKT7VMJ03',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_CHE,
            'name' => 'Маленькая буква червь',
        ];
        yield [
            'id' => '01GBAFBJNVBQFWH7EXF820XDVA',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_CHE_AND_LETTER_ES_AND_POKRYTIE,
            'name' => 'Маленькая буква червь и над строчная буква слово',
        ];
        yield [
            'id' => '01G8ZZR47NC2JVPKBDKRJPNADC',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_SHA,
            'name' => 'Заглавная буква ша',
        ];
        yield [
            'id' => '01G9264HFTAS11XED2B8981AFX',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_SHA,
            'name' => 'Маленькая буква ша',
        ];
        yield [
            'id' => '01G8ZZSB91JBE0VP4SH5HJZD5T',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_SHCHA,
            'name' => 'Заглавная буква шта',
        ];
        yield [
            'id' => '01G92682DFFTQP1BE5HFE5M9BN',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_SHCHA,
            'name' => 'Маленькая буква шта',
        ];
        yield [
            'id' => '01G99WEVD2YSJCGWQDEAAGAY3E',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_HARD_SIGN,
            'name' => 'Маленькая буква еръ',
        ];
        yield [
            'id' => '01G9FVECPHBAFHMHVPQQER9SJ1',
            'code' => self::CODE_COMBINING_VERTICAL_TILDE,
            'name' => 'Ерик или ерчик, паерок, ерок',
        ];
        yield [
            'id' => '01G9A9JYMR767MTTRPTAHABS17',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_SOFT_SIGN,
            'name' => 'Маленькая буква ерь',
        ];
        yield [
            'id' => '01G9A24M72ZQ8XY1RHSYZNAG68',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_YERU,
            'name' => 'Маленькая буква еры',
        ];
        yield [
            'id' => '01GBAMG2Y86BY60NQ8ATJZTEPA',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_YERU_AND_ACUTE_ACCENT,
            'name' => 'Маленькая буква еры и ударение острое',
        ];
        yield [
            'id' => '01G9D57BAWNR14HKM0RCHW8VGR',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_TSHE,
            'name' => 'Маленькая буква ять',
        ];
        yield [
            'id' => '01GBAWB1JHRHBCJTSGKJ2QFB3A',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_TSHE_AND_ACUTE_ACCENT,
            'name' => 'Маленькая буква ять и ударение острое',
        ];
        yield [
            'id' => '01GBT2MCWS7GBXRD0TGR2E2H0Y',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_TSHE_AND_GRAVE_ACCENT,
            'name' => 'Маленькая буква ять и ударение тяжелое',
        ];
        yield [
            'id' => '01G9265VND1VWWZA4NJS68H211',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_YU,
            'name' => 'Заглавная буква ю',
        ];
        yield [
            'id' => '01G9FQ0KH1PPDYJ3JGAZ1VK3HJ',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_YU,
            'name' => 'Маленькая буква ю',
        ];
        yield [
            'id' => '01G909WQREAGJJCJFDWGNH9ZM5',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_YUS,
            'name' => 'Маленькая буква юсь малый',
        ];
        yield [
            'id' => '01GFGVAQKY4V315XBSDE4WYK9P',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_YUS,
            'name' => 'Заглавная буква юсь малый',
        ];
        yield [
            'id' => '01GBQHYG3NVGF22K08NTQCNQ45',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_YUS_AND_COMBINING_GRAVE_ACCENT,
            'name' => 'Маленькая буква юсь малый и ударение тяжелое',
        ];
        yield [
            'id' => '01GBB1Y68GS6MM7E6XZ74RCBNE',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_YUS_AND_ACUTE_ACCENT,
            'name' => 'Маленькая буква юсь малый и ударение острое',
        ];
        yield [
            'id' => '01GBQCHCATBF1JMN56FCQ0ZTRR',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_YUS_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT,
            'name' => 'Маленькая буква юсь малый с придыханием и ударение острое',
        ];
        yield [
            'id' => '01GBQHE9YCHX0CPHBJ2QVAHRRS',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_YUS_AND_PSILI_PNEUMATA,
            'name' => 'Маленькая буква юсь малый с придыханием',
        ];
        yield [
            'id' => '01G8ZZX6R9Q2BMEEP5P5TWYN9K',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_IOTIFIED_A,
            'name' => 'Заглавная буква я',
        ];
        yield [
            'id' => '01G926BZC2PXSQJS27PH4YXD7C',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_IOTIFIED_A,
            'name' => 'Маленькая буква я',
        ];
        yield [
            'id' => '01G9000WQ75TB8C46ZQ771FQ4T',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_KSI,
            'name' => 'Заглавная буква кси',
        ];
        yield [
            'id' => '01G9268VBF5677CTZTC7BH2ZMW',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_KSI,
            'name' => 'Маленькая буква кси',
        ];
        yield [
            'id' => '01G90022CYF0KGG2J6RHWBWG0V',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_PSI,
            'name' => 'Заглавная буква пси',
        ];
        yield [
            'id' => '01G9269J3K25KBR746ABERDK9H',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_PSI,
            'name' => 'Маленькая буква пси',
        ];
        yield [
            'id' => '01G9003CSYF4YQB703H81NMB5G',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_FITA,
            'name' => 'Заглавная буква фита',
        ];
        yield [
            'id' => '01G926A5ZC2JSYSWPXBFQ5FRX6',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_FITA,
            'name' => 'Маленькая буква фита',
        ];
        yield [
            'id' => '01G90059VHFB52132F84RBV679',
            'code' => self::CODE_CYRILLIC_CAPITAL_LETTER_IZHITSA,
            'name' => 'Заглавная буква ижица',
        ];
        yield [
            'id' => '01G926B2MHC8YW7CYG5BPSNYFN',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_IZHITSA,
            'name' => 'Маленькая буква ижица',
        ];
        yield [
            'id' => '01GBD89HFXT6T29G4FBB3CF4DQ',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_IZHITSA_AND_DOUBLE_GRAVE_ACCENT,
            'name' => 'Маленькая буква ижица и двойное тяжелое ударение',
        ];
        yield [
            'id' => '01GBAXFYECE2J7Q18M1ESA3CG3',
            'code' => self::CODE_CYRILLIC_SMALL_LETTER_IZHITSA_AND_COMBINING_CYRILLIC_LETTER_GHE_AND_POKRYTIE,
            'name' => 'Маленькая буква ижица и над строчная буква глагол',
        ];
    }
}
