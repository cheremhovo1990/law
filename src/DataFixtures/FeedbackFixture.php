<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Feedback;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class FeedbackFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        foreach ($this->getData() as $data) {
            $feedback = new Feedback(
                firstName: $data['firstName'],
                email: $data['email'],
                theme: $data['theme'],
                message: $data['message'],
            );
            $feedback
                ->setPhone($data['phone'])
                ->setClientIp($data['clientId'])
            ;

            $manager->persist($feedback);
        }
        $manager->flush();
    }

    /**
     * @return array<integer, array<string, mixed>>
     */
    public function getData(): iterable
    {
        yield [
            'firstName' => 'Попов',
            'email' => 'email@test.com',
            'phone' => '79500000000',
            'theme' => 'Тестовая тема',
            'message' => 'Текст сообщения',
            'clientId' => '127.0.0.1',
        ];
    }
}
