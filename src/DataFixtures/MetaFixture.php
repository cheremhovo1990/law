<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Meta;
use App\Entity\MetaKeyEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MetaFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        foreach ($this->getData() as $datum) {
            $meta = new Meta(
                $datum['key'],
                $datum['title'] ?? '',
                $datum['description'],
            );

            $manager->persist($meta);
        }

        $manager->flush();
    }

    /**
     * @return array<integer, array<string, mixed>>
     */
    public function getData(): iterable
    {
        yield [
            'key' => MetaKeyEnum::MAIN->value,
            'description' => 'Десять заповедей христианства изреченное Господом Богом народу через избранника Своего и пророка Моисея на Синайской горе. С перечислением грехов.',
        ];
    }
}
