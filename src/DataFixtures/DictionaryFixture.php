<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Dictionary;
use App\Entity\Font;
use App\Services\DictionaryService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Ulid;

class DictionaryFixture extends Fixture implements DependentFixtureInterface
{
    public const ID_FIRST = '01G6NMKMW8J82ZRVJ5HT897CKC';
    private DictionaryService $dictionaryService;

    public function __construct(
        DictionaryService $dictionaryService,
    ) {
        $this->dictionaryService = $dictionaryService;
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->getData() as $datum) {
            $dictionary = new Dictionary(
                name: $datum['name'],
                description: $datum['description'],
                isTitlo: $datum['isTitlo'] ?? false,
                groupName: $this->dictionaryService->getGroup($datum['name'], Font::CODE_PONOMAR_UNICODE),
            );
            $dictionary->setId(new Ulid($datum['id'] ?? null));
            $dictionary->setPublishedAt(new \DateTimeImmutable());
            $dictionary->setNormalizeName($datum['normalizeName'] ?? null);
            $manager->persist($dictionary);
        }
        $manager->flush();
    }

    /**
     * @return array<integer, array<string, mixed>>
     */
    public function getData(): iterable
    {
        require_once __DIR__ . '/dictionary/get_list.php';
        yield from getList();
    }

    public function getDependencies(): array
    {
        return [
            SymbolMapFixture::class,
        ];
    }
}
