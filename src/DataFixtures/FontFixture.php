<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Font;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Ulid;

class FontFixture extends Fixture
{
    public const CODE_PONOMAR_UNICODE = Font::CODE_PONOMAR_UNICODE;
    public const CODE_HIRMOS_IE_UCS = Font::CODE_HIRMOS_IE_UCS;

    public function load(ObjectManager $manager): void
    {
        foreach ($this->getData() as $datum) {
            $entity = new Font(
                code: $datum['code'],
                name: $datum['name'],
            );
            $entity->setId(new Ulid($datum['id'] ?? null));
            $manager->persist($entity);
        }
        $manager->flush();
    }

    /**
     * @return array<integer, array<string, string>>
     */
    public function getData(): iterable
    {
        yield [
            'id' => '01G9MWFK0J6X0R7S5XS0VCTS8B',
            'code' => Font::CODE_PONOMAR_UNICODE,
            'name' => 'Ponomar Unicode',
        ];

        yield [
            'id' => '01G926PJYBYB38SHQWVF0G5994',
            'code' => Font::CODE_HIRMOS_IE_UCS,
            'name' => 'Hirmos ieUcs',
        ];
    }
}
