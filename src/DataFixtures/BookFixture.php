<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Doctrine\Collections\BookLinkCollection;
use App\Doctrine\Collections\TextCollection;
use App\Doctrine\Models\BookLinkTypeEnum;
use App\Doctrine\Models\Image as ImageModel;
use App\Doctrine\Models\Meta;
use App\Entity\Book;
use App\Entity\Image;
use App\Entity\LanguageOfThePublicationEnum;
use App\Repository\ImageRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Ulid;

class BookFixture extends Fixture implements DependentFixtureInterface
{
    public const ID_FIRST = '01G60YZXMAA9C2PABNM4X8B3GF';
    protected ObjectManager $manager;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        foreach ($this->getData() as $datum) {
            $book = new Book(
                name: $datum['name'],
                description: $datum['description'] ?? '',
                pageSize: $datum['pageSize'],
                languageOfThePublication: $datum['languageOfPublication'],
                dateOfWriting: $datum['dateOfWriting'] ?? null,
                dateOfPublished: $datum['dateOfPublished'] ?? null,
                codeOfPublishingCouncil: $datum['codeOfPublishingCouncil'] ?? null,
                blessedByPatriarchKirill: $datum['blessedByPatriarchKirill'] ?? false,
                recommendedByPublishingCouncilRoc: $datum['recommendedByPublishingCouncilRoc'] ?? false,
                recommendedByPublishingCouncilBoc: $datum['recommendedByPublishingCouncilBoc'] ?? false,
            );
            $book->setId(new Ulid($datum['id'] ?? null));
            $book->setIsbnCollection(new TextCollection($datum['isbn']));
            $book->setLinks(new BookLinkCollection($datum['links']));
            $book->setMeta(new Meta());
            $image = $this->getImageRepository()->find($this->getImage($datum['coverImage'])->getId());
            $image->setPivots(clone $image->getPivots()->push($book));
            $imageModel = new ImageModel((string) $image->getId(), $datum['coverImage']);
            $book->setCoverImage($imageModel);
            $book->setPublishedAt($datum['publishedAt'] ?? null);
            $manager->persist($book);
        }
        $manager->flush();
    }

    public function getImageRepository(): ImageRepository
    {
        return $this->manager->getRepository(Image::class);
    }

    /**
     * @return array<integer, array<string, mixed>>
     */
    public function getData(): iterable
    {
        yield [
            'id' => self::ID_FIRST,
            'name' => 'Помоги, Господи, не унывать',
            'links' => [
                [
                    'link' => 'https://www.litres.ru/igumen-mitrofan-gudkov/pomogi-gospodi-ne-unyvat/',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
                [
                    'link' => 'https://www.ozon.ru/product/pomogi-gospodi-ne-unyvat-525486598/?asb=vJFtyNGu439RhsXTE7HfvpeKfkcfC87bbZhGdZ7n2bc%253D&asb2=iAd2tAS5mniGskjNOgsgaFhxjoWJg6nSLHbQqXdJ2PvEtoVwUvFakuaIgyQj63kR&keywords=Помоги%2C+Господи%2C+не+унывать&sh=H3Hg8Q8s2g',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
            ],
            'coverImage' => ImageFixture::NAMES_FIRST,
            'isbn' => [['text' => '978-5-91362-213-6']],
            'dateOfWriting' => \DateTime::createFromFormat('Y', '2009'),
            'pageSize' => 270,
            'publishedAt' => new \DateTimeImmutable(),
            'description' => '<p><span style="color:rgb(0,0,0);">Уныние – один из смертных грехов. Святые отцы считали его великой опасностью на пути ко спасению. Святитель Иоанн Златоуст пишет: «Уныние и непрестанная печаль могут сокрушить силу души христианина и довести ее до крайнего изнеможения». Эта духовная болезнь очень сильна и врачуется непрестанным пребыванием сердца верующего в Боге. В сборнике представлены поучения, проповеди, наставления святых отцов, подвижников благочестия и современных священников о причинах уныния и способах борьбы с ним.</span></p>',
            'languageOfPublication' => LanguageOfThePublicationEnum::RUSSIAN->value,
        ];
        yield [
            'name' => 'Помоги, Господи, изжить гордыню',
            'links' => [
                [
                    'link' => 'https://www.litres.ru/igumen-mitrofan-gudkov/pomogi-gospodi-izzhit-gordynu/',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
            ],
            'coverImage' => ImageFixture::NAMES_SECOND,
            'isbn' => [['text' => '978-5-91362-185-6']],
            'dateOfWriting' => \DateTime::createFromFormat('Y', '2009'),
            'pageSize' => 180,
            'publishedAt' => new \DateTimeImmutable(),
            'languageOfPublication' => LanguageOfThePublicationEnum::RUSSIAN->value,
        ];

        yield [
            'name' => 'Помоги, Господи, изжить моё сребролюбие',
            'links' => [
                [
                    'link' => 'https://www.litres.ru/igumen-mitrofan-gudkov/pomogi-gospodi-izzhit-moe-srebrolubie/',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
            ],
            'coverImage' => ImageFixture::NAMES_THIRD,
            'isbn' => [['text' => '978-5-91362-393-5']],
            'dateOfWriting' => \DateTime::createFromFormat('Y', '2011'),
            'pageSize' => 190,
            'publishedAt' => new \DateTimeImmutable(),
            'languageOfPublication' => LanguageOfThePublicationEnum::RUSSIAN->value,
        ];

        yield [
            'name' => 'Господи, исцели блудные страсти мои',
            'links' => [
                [
                    'link' => 'https://www.litres.ru/igumen-mitrofan-gudkov/gospodi-isceli-bludnye-strasti-moi/',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
            ],
            'coverImage' => ImageFixture::NAMES_FOURTH,
            'isbn' => [['text' => '978-5-91362-829-9']],
            'dateOfWriting' => \DateTime::createFromFormat('Y', '2014'),
            'pageSize' => 150,
            'publishedAt' => new \DateTimeImmutable(),
            'languageOfPublication' => LanguageOfThePublicationEnum::RUSSIAN->value,
        ];

        yield [
            'name' => 'Избави, Господи, душу мою от гнева',
            'links' => [
                [
                    'link' => 'https://www.litres.ru/igumen-mitrofan-gudkov/izbavi-gospodi-dushu-mou-ot-gneva/',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
            ],
            'coverImage' => ImageFixture::NAMES_FIFTH,
            'isbn' => [['text' => '978-5-906793-67-6']],
            'dateOfWriting' => \DateTime::createFromFormat('Y', '2016'),
            'pageSize' => 120,
            'publishedAt' => new \DateTimeImmutable(),
            'languageOfPublication' => LanguageOfThePublicationEnum::RUSSIAN->value,
        ];

        yield [
            'name' => 'Лествица, возводящая на небо',
            'links' => [
                [
                    'link' => 'https://www.litres.ru/prepodobnyy-ioann-lestvichnik/lestvica-vozvodyaschaya-na-nebo/',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
            ],
            'coverImage' => ImageFixture::NAMES_SIXTH,
            'isbn' => [['text' => '978-5-485-00436-1']],
            'dateOfWriting' => \DateTime::createFromFormat('Y', '2013'),
            'pageSize' => 340,
            'publishedAt' => new \DateTimeImmutable(),
            'languageOfPublication' => LanguageOfThePublicationEnum::RUSSIAN->value,
        ];
        yield [
            'name' => 'Часослов (на церковнославянском языке)',
            'links' => [
                [
                    'link' => 'https://www.ozon.ru/product/chasoslov-na-tserkovnoslavyanskom-yazyke-477219662/?sh=H3Hg8Z0TQQ',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
            ],
            'coverImage' => ImageFixture::NAMES_SEVEN,
            'isbn' => [['text' => '978-5-00127-262-5']],
            // 'dateOfPublished' => \DateTime::createFromFormat('Y', '1831'),
            'pageSize' => 336,
            'publishedAt' => new \DateTimeImmutable(),
            'blessedByPatriarchKirill' => true,
            'codeOfPublishingCouncil' => 'Р21-123-3340',
            'languageOfPublication' => LanguageOfThePublicationEnum::CHURCH_SLAVIC->value,
        ];
        yield [
            'name' => 'Молитвослов на церковнославянском языке',
            'links' => [
                [
                    'link' => 'https://www.ozon.ru/product/molitvoslov-na-tserkovnoslavyanskom-yazyke-546812190/?sh=H3Hg8XbXSQ',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
            ],
            'coverImage' => ImageFixture::NAMES_EIGHT,
            'isbn' => [['text' => '978-5-906793-90-4']],
            'pageSize' => 336,
            'publishedAt' => new \DateTimeImmutable(),
            'recommendedByPublishingCouncilRoc' => true,
            'codeOfPublishingCouncil' => 'Р16-555-3801',
            'languageOfPublication' => LanguageOfThePublicationEnum::CHURCH_SLAVIC->value,
        ];
        yield [
            'name' => 'Православный молитвослов Правило ко Причащению',
            'links' => [
                [
                    'link' => 'https://www.ozon.ru/product/pravoslavnyy-molitvoslov-pravilo-ko-prichashcheniyu-250912449/?sh=H3Hg8Qu6uA',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
                [
                    'link' => 'http://izdatsovet.by/index.php/katalogi/katalog-izdatsovet-bpc/1738-pravoslavnyj-molitvoslov-pravilo-ko-prichashcheniyu',
                    'type' => BookLinkTypeEnum::UNKNOWN->value,
                ],
            ],
            'coverImage' => ImageFixture::NAMES_NINE,
            'recommendedByPublishingCouncilBoc' => true,
            'isbn' => [['text' => '978-985-7124-07-7']],
            'pageSize' => 112,
            'publishedAt' => new \DateTimeImmutable(),
            'codeOfPublishingCouncil' => 'Б15-501-0006',
            'languageOfPublication' => LanguageOfThePublicationEnum::RUSSIAN->value,
        ];
        yield [
            'name' => 'Православный молитвослов для мирян полный по уставу Церкви',
            'links' => [
                [
                    'link' => 'https://www.ozon.ru/product/pravoslavnyy-molitvoslov-dlya-miryan-polnyy-po-ustavu-tserkvi-avtor-ne-ukazan-533224815/?sh=H3Hg8cm1HQ',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
            ],
            'coverImage' => ImageFixture::NAMES_TEN,
            'recommendedByPublishingCouncilRoc' => true,
            'isbn' => [['text' => '978-5-89101-626-2']],
            'pageSize' => 528,
            'publishedAt' => new \DateTimeImmutable(),
            'codeOfPublishingCouncil' => 'Р18-814-3285',
            'languageOfPublication' => LanguageOfThePublicationEnum::RUSSIAN->value,
        ];
        yield [
            'name' => 'Часослов. на церковнославянском языке',
            'links' => [
                [
                    'link' => 'https://www.ozon.ru/product/chasoslov-svyato-elisavetinskiy-m-265874898/?asb=xY1XKzPIAnm1RoMRu2YUIOepnq0Ip%252BCyM8D9Rq%252BIJxk%253D&asb2=RgkZakL6KX9ge56xQyvyTC1vMy-KB3acZN5JaH77kvG3YojRXdvoWA3I1QJahZOc&keywords=978-985-7200-80-1&sh=H3Hg8ce_MQ',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
                [
                    'link' => 'https://www.ozon.ru/product/chasoslov-na-tserkovnoslavyanskom-yazyke-325248013/?asb=t1c3G7SgAlIx76KXV%252BvtblGOWlJ0d6G%252BwBhoXVDl8gI%253D&asb2=PCA5XoLPVkRDnLg9j6V-zeQjOrscUjR5EBu3jmB9daHHok9sWBF46VAlrzhn19fC&keywords=978-985-7200-80-1&sh=H3Hg8e3C8g',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
            ],
            'coverImage' => ImageFixture::NAMES_ELEVEN,
            'recommendedByPublishingCouncilBoc' => true,
            'isbn' => [['text' => '978-985-7200-80-1']],
            'pageSize' => 288,
            'publishedAt' => new \DateTimeImmutable(),
            'codeOfPublishingCouncil' => 'Б20-007-0058',
            'languageOfPublication' => LanguageOfThePublicationEnum::CHURCH_SLAVIC->value,
        ];
        yield [
            'name' => 'Псалтирь с переводом на русском языке. 3-е изд',
            'links' => [
                [
                    'link' => 'https://www.ozon.ru/product/psaltir-s-perevodom-na-russkom-yazyke-3-e-izd-256739785/?asb=z8eul1RObaQ2UGPkz3iafbggd3qn4rXqYQZX8Hgj7iazY7nDqeAwVb9n24wdKCuK&asb2=s5sOofxnZxq5RTK7UyFeEM_RupkreFWp1kJP6hrxKj_wAHQLNGJ7NZHoo1frZSXh8otfbcwXGuAPuSFs5RVNXcx8laAUQmIKpQLDJ9_-scaMrN8kgk6F-mUiUATSiO8CmKq88WC4G0831QqZv5uYww&keywords=978-985-7232-55-0&sh=H3Hg8Ttprg',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
            ],
            'dateOfPublished' => \DateTime::createFromFormat('Y', '2020'),
            'coverImage' => ImageFixture::NAMES_TWELVE,
            'recommendedByPublishingCouncilBoc' => true,
            'isbn' => [['text' => '978-985-7232-55-0']],
            'pageSize' => 560,
            'publishedAt' => new \DateTimeImmutable(),
            'codeOfPublishingCouncil' => 'Б20-008-0068',
            'languageOfPublication' => LanguageOfThePublicationEnum::CHURCH_SLAVIC->value,
        ];
        yield [
            'name' => 'Полный церковно-славянский словарь | Протоиерей Григорий Дьяченко',
            'links' => [
                [
                    'link' => 'https://www.ozon.ru/product/polnyy-tserkovno-slavyanskiy-slovar-protoierey-grigoriy-dyachenko-320058518',
                    'type' => BookLinkTypeEnum::BUY->value,
                ],
            ],
            'dateOfWriting' => \DateTime::createFromFormat('Y', '1900'),
            'coverImage' => ImageFixture::NAMES_THIRTEEN,
            'recommendedByPublishingCouncilRoc' => true,
            'isbn' => [['text' => '978-5-370-04688-9']],
            'pageSize' => 1168,
            'publishedAt' => new \DateTimeImmutable(),
            'codeOfPublishingCouncil' => '13-312-1963',
            'languageOfPublication' => LanguageOfThePublicationEnum::CHURCH_SLAVIC->value,
        ];
    }

    protected function getImage(string $name): Image
    {
        return $this->getReference(sprintf('%s-%s', ImageFixture::class, $name));
    }

    public function getDependencies()
    {
        return [
            ImageFixture::class,
        ];
    }
}
