<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ImageFixture extends Fixture
{
    public const NAMES_FIRST = 'fixture/book-1.jpg';
    public const NAMES_SECOND = 'fixture/book-2.jpg';
    public const NAMES_THIRD = 'fixture/book-3.jpg';
    public const NAMES_FOURTH = 'fixture/book-4.jpg';
    public const NAMES_FIFTH = 'fixture/book-5.jpg';
    public const NAMES_SIXTH = 'fixture/book-6.jpg';
    public const NAMES_SEVEN = 'fixture/book-7.webp';
    public const NAMES_EIGHT = 'fixture/book-8.webp';
    public const NAMES_NINE = 'fixture/book-9.webp';
    public const NAMES_TEN = 'fixture/book-10.webp';
    public const NAMES_ELEVEN = 'fixture/book-11.webp';
    public const NAMES_TWELVE = 'fixture/book-12.webp';
    public const NAMES_THIRTEEN = 'fixture/book-13.webp';

    public function load(ObjectManager $manager): void
    {
        foreach ($this->getData() as $datum) {
            $image = new Image(
                name: $datum['name'],
            );
            $manager->persist($image);
            $this->addReference(sprintf('%s-%s', self::class, $datum['name']), $image);
        }
        $manager->flush();
    }

    /**
     * @return array<integer, array<string, mixed>>
     */
    public function getData(): iterable
    {
        yield [
            'name' => self::NAMES_FIRST,
        ];

        yield [
            'name' => self::NAMES_SECOND,
        ];

        yield [
            'name' => self::NAMES_THIRD,
        ];

        yield [
            'name' => self::NAMES_FOURTH,
        ];

        yield [
            'name' => self::NAMES_FIFTH,
        ];

        yield [
            'name' => self::NAMES_SIXTH,
        ];

        yield [
            'name' => self::NAMES_SEVEN,
        ];
        yield [
            'name' => self::NAMES_EIGHT,
        ];
        yield [
            'name' => self::NAMES_NINE,
        ];
        yield [
            'name' => self::NAMES_TEN,
        ];
        yield [
            'name' => self::NAMES_ELEVEN,
        ];
        yield [
            'name' => self::NAMES_TWELVE,
        ];
        yield [
            'name' => self::NAMES_THIRTEEN,
        ];
    }
}
