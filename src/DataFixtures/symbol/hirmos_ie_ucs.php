<?php

declare(strict_types=1);

use App\DataFixtures\SymbolFixture;
use App\Entity\Font;

/**
 * @return iterable<integer, mixed>
 */
function hirmosIeUcs(): iterable
{
    yield [
        'id' => '01G9Q9HN1A0XK4QXDJ3ZJ0QFPP',
        'unicode' => '&#1040;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_A,
        'groupName' => mb_chr(1040),
        'groupNamePosition' => 1,
    ];
    yield [
        'id' => '01GA4AHZKMBJ5DXVMVKT0GB0GE',
        'unicode' => '&#1168;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_A_AND_PSILI_PNEUMATA,
        'groupName' => mb_chr(1040),
        'groupNamePosition' => 1,
    ];
    yield [
        'id' => '01GA4AKN3PAWD9Y0RRTYF2H5AR',
        'unicode' => '&#1027;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_A_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT,
        'groupName' => mb_chr(1040),
        'groupNamePosition' => 1,
    ];
    yield [
        'id' => '01GA6Y460AZ6YNR6S9RQRG4J9E',
        'unicode' => '&#1072;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_A,
    ];
    yield [
        'id' => '01GA6YHHA3SJ5TXQ4TK199PGFV',
        'unicode' => '&#1072;&#54;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_A_AND_INVERTED_BREVE,
    ];

    yield [
        'id' => '01GA951C6C968ZH9EP8YY0642H',
        'unicode' => '&#1107;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_A_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GA96V0XN0A89EP5NTFVXT88Q',
        'unicode' => '&#1169;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_A_AND_PSILI_PNEUMATA,
    ];

    yield [
        'id' => '01GA96V93VQMRGGTZ1GK9NG6BT',
        'unicode' => '&#65;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_A_AND_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01GA96DR3EPT4WHCBSTFGSRK19',
        'unicode' => '&#97;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_A_AND_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GA1W942TK8BXXGTX2KDSQZDG',
        'unicode' => '&#1079;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ZE,
    ];

    yield [
        'id' => '01GA1WMWN0D51EZEKR6J2BQAGD',
        'unicode' => '&#1098;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_HARD_SIGN,
    ];

    yield [
        'id' => '01GB37KG1K6X4FA5P43FTP08TY',
        'unicode' => '&#1075;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_GHE,
    ];

    yield [
        'id' => '01GB3A9Y3NN8KYTHJDZT8SNX9S',
        'unicode' => '&#71;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_GHE_AND_TITLO,
    ];

    yield [
        'id' => '01GB598RJCEPAJ97DDXEDJ87JM',
        'unicode' => '&#1083;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_EL,
    ];

    yield [
        'id' => '01GB5A4NFP46VG4SDYN0RJ7FP9',
        'unicode' => '&#1085;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_EN,
    ];

    yield [
        'id' => '01GB5A8ZQFCJXAMQV87NWCZ7YM',
        'unicode' => '&#1077;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_IE,
    ];

    yield [
        'id' => '01GB5AXHNMMRXK0N1T76RF8VAQ',
        'unicode' => '&#1100;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_SOFT_SIGN,
    ];

    yield [
        'id' => '01GB5CC9KHTJWPK7CNDZXMYN0D',
        'unicode' => '&#1089;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ES,
    ];

    yield [
        'id' => '01GB5FVN522S1NHVFK1541J3SN',
        'unicode' => '&#1082;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_KA,
    ];
    yield [
        'id' => '01GB5FZRR1VKNKBZFP4CAA5G5V',
        'unicode' => '&#1110;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YI,
    ];
    yield [
        'id' => '01GB5G4Q9N13DEJWZT0898G7E0',
        'unicode' => '&#1081;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_SHORT_I,
    ];
    yield [
        'id' => '01GB5GCPMNZMRA1ZHG1MBRYS24',
        'unicode' => '&#1088;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ER,
    ];
    yield [
        'id' => '01GB5GKPAB86PD6MES56CBBYZQ',
        'unicode' => '&#1093;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_HA,
    ];
    yield [
        'id' => '01GB5GT9BD0N1ME0W42AJ6ZP1D',
        'unicode' => '&#1087;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_PE,
    ];
    yield [
        'id' => '01GB5HC57S8RBP759AVSZXBD36',
        'unicode' => '&#1087;&#99;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_PE_AND_LETTER_ES_AND_POKRYTIE,
    ];
    yield [
        'id' => '01GB5RYHK6611MFPTFPEHHEQQH',
        'unicode' => '&#1080;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_I,
    ];
    yield [
        'id' => '01GB5SKEJBEBDPV70H0BQJC6MT',
        'unicode' => '&#1090;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_TE,
    ];
    yield [
        'id' => '01GB5XN7JCHXPE1Y6G8NWC87WE',
        'unicode' => '&#1086;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_O,
    ];

    yield [
        'id' => '01GB83ZGPPZ4RGHZ0KG9WZNPE4',
        'unicode' => '&#1042;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_VE,
        'groupName' => mb_chr(1042),
        'groupNamePosition' => 3,
    ];

    yield [
        'id' => '01GB846FXJYP8ECRD72AYD45RT',
        'unicode' => '&#1077;&#49;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_IE_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GB84R1YHKKVADAW6K7P0G0Q5',
        'unicode' => '&#1084;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_EM,
    ];

    yield [
        'id' => '01GB85FM7WADMBQHH8JM4EMKYG',
        'unicode' => '&#122;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YUS,
        'groupName' => mb_chr(122),
        'groupNamePosition' => 18,
    ];

    yield [
        'id' => '01GB8ASTE1CEA943EWARP65QRT',
        'unicode' => '&#1041;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_BE,
        'groupName' => mb_chr(1041),
        'groupNamePosition' => 2,
    ];

    yield [
        'id' => '01GB8AWJPMBKCSSE9Q8A5VAEWQ',
        'unicode' => '&#57;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ZHE_AND_CYRILLIC_TITLO,
    ];

    yield [
        'id' => '01GB8AZ17W9X0JKW6TGHFJNK5V',
        'unicode' => '&#1074;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_VE,
    ];

    yield [
        'id' => '01GB8B5HNWR944DJKNDKH3M5X2',
        'unicode' => '&#1086;&#50;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_O_AND_COMBINING_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01GB8BXJWQ091M84R4H1HA838X',
        'unicode' => '&#1094;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_TSE,
    ];

    yield [
        'id' => '01GB8CKR519ECVAFKETP5KE16Q',
        'unicode' => '&#1094;&#100;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ER_AND_COMBINING_CYRILLIC_DE,
    ];

    yield [
        'id' => '01GB8F4X0A9V0Z56BVXJV3APKY',
        'unicode' => '&#1073;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_BE,
    ];

    yield [
        'id' => '01GB8F9BF72FR39RHFDDSMVDA3',
        'unicode' => '&#1076;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_DE,
    ];

    yield [
        'id' => '01GB8FC8S70W4NKCKH157625CK',
        'unicode' => '&#108;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_EL_AND_CYRILLIC_TITLO,
    ];

    yield [
        'id' => '01GB8FQXXB1K3TWBV2TZV5F8BS',
        'unicode' => '&#1078;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ZHE,
    ];

    yield [
        'id' => '01GB8FZA4VR0MF742NTC15A13P',
        'unicode' => '&#1075;&#100;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_GHE_AND_COMBINING_CYRILLIC_DE,
    ];

    yield [
        'id' => '01GBAF71E8REYWV39FE77F7JFK',
        'unicode' => '&#1095;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_CHE,
    ];

    yield [
        'id' => '01GBAF8VQ9C964889448TMC03T',
        'unicode' => '&#1095;&#99;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_CHE_AND_LETTER_ES_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBAFHN6TERN63WGNZVV1PQJD',
        'unicode' => '&#119;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_OMEGA,
    ];

    yield [
        'id' => '01GBAFV9SZPGEJRBNZHTSTJ39V',
        'unicode' => '&#76;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_EL_AND_COMBINING_CYRILLIC_DE,
    ];

    yield [
        'id' => '01GBAFYYFSQ11ETJ9EW5BWZRHS',
        'unicode' => '&#114;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ER_AND_LETTER_ES_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBAG5MBD5ABFXZTAFH4FWG7R',
        'unicode' => '&#1057;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_ES,
        'groupName' => mb_chr(1057),
        'groupNamePosition' => 18,
    ];

    yield [
        'id' => '01GBAGQ5MEHPB31TTDPC6NBHCY',
        'unicode' => '&#1080;&#49;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_I_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBAMAPK6M9FY830XRQA28TTJ',
        'unicode' => '&#1099;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YERU,
    ];

    yield [
        'id' => '01GBAMNQY1B65TRRAY6P1R0PWJ',
        'unicode' => '&#104;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YERU_AND_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBAMQMCHXZ3J88BRMH3C0Y6P',
        'unicode' => '&#1043;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_GHE,
        'groupName' => mb_chr(1043),
        'groupNamePosition' => 4,
    ];

    yield [
        'id' => '01GBAMSPHRAM78HH35B1J49PRV',
        'unicode' => '&#68;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_DE_AND_LETTER_ES_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBANAGSWFQX0RKTEWY59AFVW',
        'unicode' => '&#1078;&#99;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ZHE_AND_LETTER_ES_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBANNCY2ARZRWH3NEW21CX42',
        'unicode' => '&#1086;&#49;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_O_AND_COMBINING_ACUTE_ACCENT,
    ];
    yield [
        'id' => '01GBANYX8JTH4H65V4FGJTTA1Y',
        'unicode' => '&#55;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_TITLO,
    ];
    yield [
        'id' => '01GBAPAMD2SYB8NDBZN74X7WVE',
        'unicode' => '&#1044;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_DE,
        'groupName' => mb_chr(1044),
        'groupNamePosition' => 5,
    ];
    yield [
        'id' => '01GBAPCPTQYWCWXN55X8X7M301',
        'unicode' => '&#1096;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_SHA,
    ];
    yield [
        'id' => '01GBAQ5EFJZTSXDJ0PTQ4BM5EG',
        'unicode' => '&#52;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA_AND_COMBINING_ACUTE_ACCENT,
    ];
    yield [
        'id' => '01GBAQTD6AGWX3XE577G1QT94V',
        'unicode' => '&#1091;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_MONOGRAPH_UK,
    ];
    yield [
        'id' => '01GBAT94Y66JD70ED4VBMV95ZH',
        'unicode' => '&#1101;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_TSHE,
    ];
    yield [
        'id' => '01GBATB8DHH2XCX9HZRRP5V9Q7',
        'unicode' => '&#1047;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_ZE,
        'groupName' => mb_chr(1047),
        'groupNamePosition' => 8,
    ];
    yield [
        'id' => '01GBAW7RZWFXE5EQBFWQ6TZSV2',
        'unicode' => '&#1105;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_TSHE_AND_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBAWJQ3BX7JZX8RQPV7ACAW4',
        'unicode' => '&#1045;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_IE,
        'groupName' => mb_chr(1045),
        'groupNamePosition' => 6,
    ];

    yield [
        'id' => '01GBAWP52VQBCYQ16KQ9ETDDR6',
        'unicode' => '&#35;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA,
    ];

    yield [
        'id' => '01GBAXCPYT12P25BJ187NZV0K6',
        'unicode' => '&#1106;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_IZHITSA_AND_COMBINING_CYRILLIC_LETTER_GHE_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBAXR15PXK6YHMT5P9BZ9P2N',
        'unicode' => '&#106;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YI_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBAY07P4GVTSG6PR153E01SG',
        'unicode' => '&#121;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_MONOGRAPH_UK_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBAYAM9WYXF74HTWW4790RWA',
        'unicode' => '&#86;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_IZHITSA,
        'groupName' => mb_chr(86),
        'groupNamePosition' => 34,
    ];

    yield [
        'id' => '01GBAYES7XQDS6R3AY79XJFJM3',
        'unicode' => '&#1055;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_PE,
        'groupName' => mb_chr(1055),
        'groupNamePosition' => 16,
    ];

    yield [
        'id' => '01GBAYGPFQS8TNZ43714ND4D2X',
        'unicode' => '&#1097;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_SHCHA,
    ];

    yield [
        'id' => '01GBAYNAS3H1EBMHGSBNXGSTYG',
        'unicode' => '&#36;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBB1H7GZT89D3975J6C0XK7F',
        'unicode' => '&#1046;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_ZHE,
        'groupName' => mb_chr(1046),
        'groupNamePosition' => 7,
    ];

    yield [
        'id' => '01GBB1MMX7SZR5QVPTQB82BECJ',
        'unicode' => '&#1061;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_HA,
        'groupName' => mb_chr(1061),
        'groupNamePosition' => 22,
    ];

    yield [
        'id' => '01GBB1RXNXWAM9J7KWD288HM4S',
        'unicode' => '&#115;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YUS_AND_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBB23FCCG77XQYF3D07G08KV',
        'unicode' => '&#1029;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_DZE,
        'groupName' => mb_chr(1029),
        'groupNamePosition' => 7,
    ];
    yield [
        'id' => '01GBB26Y5ZCZM3DQFQEVDCJEB7',
        'unicode' => '&#50;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_GRAVE_ACCENT,
    ];
    yield [
        'id' => '01GBD77PPMKFZ6T0AFES2MV56D',
        'unicode' => '&#1108;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_UKRAINIAN_IE,
    ];

    yield [
        'id' => '01GBD7BR7WYXK33132PCR5PSD4',
        'unicode' => '&#51;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA,
    ];

    yield [
        'id' => '01GBD7EKDG4JX1HTCYWFGS1YNR',
        'unicode' => '&#1034;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_OMEGA,
        'groupName' => mb_chr(87),
        'groupNamePosition' => 23,
    ];

    yield [
        'id' => '01GBD7GTXF505EHAQA41SB1F7C',
        'unicode' => '&#1052;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_EM,
        'groupName' => mb_chr(1052),
        'groupNamePosition' => 13,
    ];

    yield [
        'id' => '01GBD8DY4Y0BVXN2N9XFW52H1Y',
        'unicode' => '&#109;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_IZHITSA_AND_DOUBLE_GRAVE_ACCENT,
    ];
    yield [
        'id' => '01GBD9N5H99E73236JQTW7F0GN',
        'unicode' => '&#87;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_OMEGA,
        'groupName' => mb_chr(87),
        'groupNamePosition' => 23,
    ];
    yield [
        'id' => '01GBD9QDNTN29GEQ1J4CYXWVDP',
        'unicode' => '&#84;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_OT,
        'groupName' => mb_chr(84),
        'groupNamePosition' => 24,
    ];
    yield [
        'id' => '01GBDA3MK6XS2NHDVZT8FDRED2',
        'unicode' => '&#1051;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_EL,
        'groupName' => mb_chr(1051),
        'groupNamePosition' => 12,
    ];
    yield [
        'id' => '01GBDENCXG7VSQZBZ8ATW03SED',
        'unicode' => '&#1053;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_EN,
        'groupName' => mb_chr(1053),
        'groupNamePosition' => 14,
    ];
    yield [
        'id' => '01GBDER3FARZ82MZQCAHJ43KFV',
        'unicode' => '&#1048;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_I,
        'groupName' => mb_chr(1048),
        'groupNamePosition' => 9,
    ];
    yield [
        'id' => '01GBDEV1HZV4R2YW18C96R6BSP',
        'unicode' => '&#1031;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_YI_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA,
        'groupName' => mb_chr(1031),
        'groupNamePosition' => 10,
    ];
    yield [
        'id' => '01GBDFCSBQXD2AC6DYA2WME6QS',
        'unicode' => '&#72;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_OMEGA_AND_COMBINING_ACUTE_ACCENT,
    ];
    yield [
        'id' => '01GBDFQAXDTBVXCQTSNJ8QZ5W9',
        'unicode' => '&#1050;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_KA,
        'groupName' => mb_chr(1050),
        'groupNamePosition' => 11,
    ];
    yield [
        'id' => '01GBDFTQSG16NQWT14BKG2ZSKG',
        'unicode' => '&#1056;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_ER,
        'groupName' => mb_chr(1056),
        'groupNamePosition' => 17,
    ];

    yield [
        'id' => '01GBDGSRB909B34E6XDJ30XW75',
        'unicode' => '&#100;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_DE,
    ];

    yield [
        'id' => '01GBDGWF2S802R07ECS9GKBWSA',
        'unicode' => '&#99;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_LETTER_ES_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBDGZ0MS4GCQKD1WP1QC3FAR',
        'unicode' => '&#49;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBMSN04HRVJ05W1MSPG1ESXQ',
        'unicode' => '&#1049;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_SHORT_I,
        'groupName' => mb_chr(1048),
        'groupNamePosition' => 9,
    ];

    yield [
        'id' => '01GBMT6XNCY1FYF4R1NHAD31J0',
        'unicode' => '&#78;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_O_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA,
        'groupName' => mb_chr(79),
        'groupNamePosition' => 15,
    ];

    yield [
        'id' => '01GBMT9Y9SD4PW5KRYPVQRS7CF',
        'unicode' => '&#98;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_LETTER_O_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBMTDYFPW3GW9BC76W5222RN',
        'unicode' => '&#62;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_LETTER_ER_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBMZRPHCW8F8KJ61XKJ557VV',
        'unicode' => '&#1038;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_UK,
        'groupName' => mb_chr(1144),
        'groupNamePosition' => 20,
    ];

    yield [
        'id' => '01GBMZXNFGBW8DX430TZ7ZECGJ',
        'unicode' => '&#102;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_FITA,
    ];

    yield [
        'id' => '01GBN04ADJ5XGEKBAYNC7HWYAZ',
        'unicode' => '&#1058;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_TE,
        'groupName' => mb_chr(1058),
        'groupNamePosition' => 19,
    ];

    yield [
        'id' => '01GBN5YF65Z8ANR2NKK2BQER55',
        'unicode' => '&#1111;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YI_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA,
    ];

    yield [
        'id' => '01GBN6AC2K7EQQXB8QGY46ENDQ',
        'unicode' => '&#82;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ER_AND_CYRILLIC_TITLO,
    ];

    yield [
        'id' => '01GBN7T244AHPMSKRPX1WPR19Z',
        'unicode' => '&#1036;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_UK_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT,
        'groupName' => mb_chr(1144),
        'groupNamePosition' => 20,
    ];
    yield [
        'id' => '01GBN8757MDD13KC05P60HM2ER',
        'unicode' => '&#1060;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_EF,
        'groupName' => mb_chr(1060),
        'groupNamePosition' => 21,
    ];
    yield [
        'id' => '01GBN8BXN3931YX32Y5CN7QYD8',
        'unicode' => '&#120;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_KSI,
    ];

    yield [
        'id' => '01GBQBG77KBRMHH43B3WYJ73N5',
        'unicode' => '&#1102;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YU,
        'groupName' => mb_chr(1070),
        'groupNamePosition' => 29,
    ];

    yield [
        'id' => '01GBQBMJG0ZTT3EHC5JNPPFZ4M',
        'unicode' => '&#1062;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_TSE,
        'groupName' => mb_chr(1062),
        'groupNamePosition' => 25,
    ];

    yield [
        'id' => '01GBQBQ75YQC3GWCFX269T1CV0',
        'unicode' => '&#1063;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_CHE,
        'groupName' => mb_chr(1063),
        'groupNamePosition' => 26,
    ];

    yield [
        'id' => '01GBQBTN72WD50PE9Q32E2QCA9',
        'unicode' => '&#1065;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_SHCHA,
        'groupName' => mb_chr(1065),
        'groupNamePosition' => 28,
    ];

    yield [
        'id' => '01GBQCHTHDQWSB8A2BEMPQ455K',
        'unicode' => '&#1115;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YUS_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBQHA1KTHQMR3MGNK4BSC55B',
        'unicode' => '&#1071;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_IOTIFIED_A,
        'groupName' => mb_chr(1071),
        'groupNamePosition' => 30,
    ];

    yield [
        'id' => '01GBQHD699ASD5AD11002D68JB',
        'unicode' => '&#1113;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YUS_AND_PSILI_PNEUMATA,
    ];

    yield [
        'id' => '01GBQHSQ4Q5T7PMYB9K2TZK1AE',
        'unicode' => '&#124;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YUS_AND_COMBINING_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01GBQJ3GPE4ZDXMX54CW0SQV08',
        'unicode' => '&#80;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_PSI,
        'groupName' => mb_chr(80),
        'groupNamePosition' => 32,
    ];

    yield [
        'id' => '01GBQJHB0RZ3TZQKMZJP59T6AC',
        'unicode' => '&#118;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_IZHITSA,
    ];

    yield [
        'id' => '01GBQJKFS1NETSGZ68PEDP0JTV',
        'unicode' => '&#56;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_VERTICAL_TILDE,
    ];

    yield [
        'id' => '01GBSYFX3RZPAD3JT4W5KM1VHB',
        'unicode' => '&#1039;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_O_AND_PSILI_PNEUMATA_AND_COMBINING_ACUTE_ACCENT,
        'groupName' => mb_chr(79),
        'groupNamePosition' => 15,
    ];

    yield [
        'id' => '01GBSYVCNVFHMRY0HXN8R90H9V',
        'unicode' => '&#1064;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_SHA,
        'groupName' => mb_chr(1064),
        'groupNamePosition' => 27,
    ];

    yield [
        'id' => '01GBT2GS1ZVB8P1FCGMFJVBH9E',
        'unicode' => '&#64;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_TSHE_AND_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01GBWMPZRTB3HSSYRAXFHBC07Y',
        'unicode' => '&#89;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_MONOGRAPH_UK_AND_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01GCC5B9XKJSVDMR5PEQ5ZVHEN',
        'unicode' => '&#37;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA_COMBINING_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01GCEGZTK8TSY22NB7CZXW2PQN',
        'unicode' => '&#54;',
        'font' => Font::CODE_HIRMOS_IE_UCS,
        'symbol' => SymbolFixture::CODE_COMBINING_INVERTED_BREVE,
    ];
}
