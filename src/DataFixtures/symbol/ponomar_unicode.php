<?php

declare(strict_types=1);

use App\DataFixtures\SymbolFixture;
use App\Entity\Font;

/**
 * @return iterable<integer, mixed>
 */
function ponomarUnicode(): iterable
{
    $position = [
        '1040' => 1,
        '1041' => 2,
        '1042' => 3,
        '1043' => 4,
        '1044' => 5,
        '1045' => 6,
        '1046' => 7,
        '1029' => 8,
        '1047' => 9,
        '1048' => 10,
        '1031' => 11,
        '1050' => 12,
        '1051' => 13,
        '1052' => 14,
        '1053' => 15,
        '1054' => 16,
        '1055' => 17,
        '1056' => 18,
        '1057' => 19,
        '1058' => 20,
        '1144' => 21,
        '1060' => 22,
        '1061' => 23,
        '1150' => 24,
        '1062' => 25,
        '1063' => 26,
        '1064' => 27,
        '1065' => 28,
        '1070' => 29,
        '42582' => 30,
        '1120' => 31,
        '1126' => 32,
        '1134' => 33,
        '1136' => 34,
        '1138' => 35,
        '1140' => 36,
    ];
    yield [
        'id' => '01GA4EFQ7KQJ4F3N0FPRX04GB7',
        'unicode' => '&#785;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_INVERTED_BREVE,
    ];

    yield [
        'id' => '01G9AHTKAYTENDGJ46Y63P0VE5',
        'unicode' => '&#1155;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_TITLO,
    ];

    yield [
        'id' => '01G9J7QJ19P33PSMJ29HX2ZJTK',
        'unicode' => '&#1154;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_THOUSANDS_SIGN,
    ];

    yield [
        'id' => '01G9J28G9NXDJ14RAFTMTK0Y5B',
        'unicode' => '&#1158;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA,
    ];

    yield [
        'id' => '01G9J2XH0BDZVMY07K0762E0D5',
        'unicode' => '&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01G9J8M1EBZ1XGKMTFYRE3FJAV',
        'unicode' => '&#768;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01G927QF5ARNGCNCZFSE3AZCZJ',
        'unicode' => '&#1040;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_A,
        'groupName' => mb_chr(1040),
        'groupNamePosition' => $position[1040],
    ];

    yield [
        'id' => '01G92EHGVF8P2JHNGBEMA1J3N2',
        'unicode' => '&#1072;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_A,
    ];

    yield [
        'id' => '01GA6YFKV87YKQ97N60J70CCEY',
        'unicode' => '&#1072;&#785;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_A_AND_INVERTED_BREVE,
    ];

    yield [
        'id' => '01G92EYSTAZ16N1H71F43G7ZGY',
        'unicode' => '&#1041;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_BE,
        'groupName' => mb_chr(1041),
        'groupNamePosition' => $position[1041],
    ];

    yield [
        'id' => '01G92EZ2YWXMY10WZJC61KMNBB',
        'unicode' => '&#1073;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_BE,
    ];

    yield [
        'id' => '01G92F1B5XDX19AV8PNC7M2JX4',
        'unicode' => '&#1042;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_VE,
        'groupName' => mb_chr(1042),
        'groupNamePosition' => $position[1042],
    ];

    yield [
        'id' => '01G92F1M99SKKXRAT4650V16SN',
        'unicode' => '&#1074;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_VE,
    ];

    yield [
        'id' => '01G92FJQ33TJVM84MEBRK62P90',
        'unicode' => '&#1043;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_GHE,
        'groupName' => mb_chr(1043),
        'groupNamePosition' => $position[1043],
    ];

    yield [
        'id' => '01G92FK7EBV6P8YAA9GY5FQBCC',
        'unicode' => '&#1075;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_GHE,
    ];

    yield [
        'id' => '01G92FKMBEZBSJCWRVJQFVGWN0',
        'unicode' => '&#1044;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_DE,
        'groupName' => mb_chr(1044),
        'groupNamePosition' => $position[1044],
    ];

    yield [
        'id' => '01G92FKZRFX24P38YWB1S992R8',
        'unicode' => '&#1076;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_DE,
    ];
    yield [
        'id' => '01G9JCXWS89BME5X8BSA7Q8A3Q',
        'unicode' => '&#11747;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_DE,
    ];
    yield [
        'id' => '01G9JCWV44RPT0F97ETQCHJR0X',
        'unicode' => '&#1045;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_IE,
        'groupName' => mb_chr(1045),
        'groupNamePosition' => $position[1045],
    ];

    yield [
        'id' => '01G92FMGV3PE0HKGAWNQW8W81X',
        'unicode' => '&#1077;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_IE,
    ];

    yield [
        'id' => '01G9ACG6YDKEABZXYEKYE06Q3A',
        'unicode' => '&#1108;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_UKRAINIAN_IE,
        'groupName' => mb_chr(1045),
        'groupNamePosition' => $position[1045],
    ];

    yield [
        'id' => '01G9JDSQQH3K14JY8ZX1CC7AVB',
        'unicode' => '&#11757;&#1159;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_LETTER_ES_AND_POKRYTIE,
    ];

    yield [
        'id' => '01G9MMJJQKN8B871KX22MFKQ45',
        'unicode' => '&#11756;&#1159;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_LETTER_ER_AND_POKRYTIE,
    ];

    yield [
        'id' => '01G9MN4XR1J6RGW8CCW81NVA6Z',
        'unicode' => '&#11746;&#1159;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_LETTER_GHE_AND_POKRYTIE,
    ];

    yield [
        'id' => '01G9MP0TFH855G61B3686JY83D',
        'unicode' => '&#11754;&#1159;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_LETTER_O_AND_POKRYTIE,
    ];

    yield [
        'id' => '01G92HEFR2PS6GEEH5YC659CDB',
        'unicode' => '&#1046;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_ZHE,
        'groupName' => mb_chr(1046),
        'groupNamePosition' => $position[1046],
    ];

    yield [
        'id' => '01G92HERKYWSSBSVA8G2YQANWJ',
        'unicode' => '&#1078;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ZHE,
    ];

    yield [
        'id' => '01G92HH0JWCM9BPD3RP791MGFT',
        'unicode' => '&#1029;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_DZE,
        'groupName' => mb_chr(1029),
        'groupNamePosition' => $position[1029],
    ];

    yield [
        'id' => '01G92HH9KW23P0BW81WRFRS66Y',
        'unicode' => '&#1109;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_DZE,
    ];

    yield [
        'id' => '01G92MR9WN7G9BW36NRXWS7GF1',
        'unicode' => '&#1047;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_ZE,
        'groupName' => mb_chr(1047),
        'groupNamePosition' => $position[1047],
    ];

    yield [
        'id' => '01G92MSVKJGSD2MB17WHAY3VN8',
        'unicode' => '&#1079;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ZE,
    ];

    yield [
        'id' => '01G92JS9C2E93285H6XXTCJ12K',
        'unicode' => '&#1048;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_I,
        'groupName' => mb_chr(1048),
        'groupNamePosition' => $position[1048],
    ];

    yield [
        'id' => '01G92JSMA2KJ5XN1RA01X3NHC8',
        'unicode' => '&#1080;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_I,
    ];

    yield [
        'id' => '01GC70YRA224CR59B3DEFYBT9E',
        'unicode' => '&#1081;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_SHORT_I,
    ];

    yield [
        'id' => '01G92K1AJKJ93NWPC4JMR0X225',
        'unicode' => '&#1030;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_BYELORUSSIAN_UKRAINIAN_I,
        'groupName' => mb_chr(1031),
        'groupNamePosition' => $position[1031],
    ];

    yield [
        'id' => '01G92K1KA38Q9H4JP1PE4CDGNV',
        'unicode' => '&#1110;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_BYELORUSSIAN_UKRAINIAN_I,
        'groupName' => mb_chr(1031),
        'groupNamePosition' => $position[1031],
    ];

    yield [
        'id' => '01G9D8C6SPHZQJ4TFHSHXXXGPC',
        'unicode' => '&#1031;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_YI,
        'groupName' => mb_chr(1031),
        'groupNamePosition' => $position[1031],
    ];

    yield [
        'id' => '01G9D8B955YM8RZE8Y2AZQSJ52',
        'unicode' => '&#1111;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YI,
    ];

    yield [
        'id' => '01G92MDJ9TPBQKJDFG04Y74MXM',
        'unicode' => '&#1050;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_KA,
        'groupName' => mb_chr(1050),
        'groupNamePosition' => $position[1050],
    ];

    yield [
        'id' => '01G92MDTHZY8TNBENBQJRGDXPX',
        'unicode' => '&#1082;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_KA,
    ];

    yield [
        'id' => '01G92MJ8JZZJ9VFQPKSD3XZH6V',
        'unicode' => '&#1051;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_EL,
        'groupName' => mb_chr(1051),
        'groupNamePosition' => $position[1051],
    ];

    yield [
        'id' => '01G92MJHY4KA1F9GD2690577WA',
        'unicode' => '&#1083;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_EL,
    ];

    yield [
        'id' => '01G92MZ9N4XYBTDE104P6KYQ8X',
        'unicode' => '&#1052;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_EM,
        'groupName' => mb_chr(1052),
        'groupNamePosition' => $position[1052],
    ];

    yield [
        'id' => '01G92MZKFJA8FKG3VPZC2QYHN2',
        'unicode' => '&#1084;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_EM,
    ];

    yield [
        'id' => '01G99FTC1Q0EBTJAQ0YMVZ5XNR',
        'unicode' => '&#1053;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_EN,
        'groupName' => mb_chr(1053),
        'groupNamePosition' => $position[1053],
    ];

    yield [
        'id' => '01G99FTQZZJKHV3F04YTK02QNY',
        'unicode' => '&#1085;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_EN,
    ];

    yield [
        'id' => '01G92PHGAX82R2G5650Q1FVZTH',
        'unicode' => '&#1054;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_O,
        'groupName' => mb_chr(1054),
        'groupNamePosition' => $position[1054],
    ];

    yield [
        'id' => '01GFK4V6RFVS7FEGFB1AMBJ9T3',
        'unicode' => '&#1146;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_ROUND_OMEGA,
        'groupName' => mb_chr(1054),
        'groupNamePosition' => $position[1054],
    ];

    yield [
        'id' => '01GFK4VEHS0P2A7M5S8M27VR5N',
        'unicode' => '&#1147;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ROUND_OMEGA,
        'groupName' => mb_chr(1054),
        'groupNamePosition' => $position[1054],
    ];

    yield [
        'id' => '01G92PHVH1A6HEENA4940HJVXA',
        'unicode' => '&#1086;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_O,
    ];

    yield [
        'id' => '01G92PN5S1TR4SFDF5BNNM0GED',
        'unicode' => '&#1055;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_PE,
        'groupName' => mb_chr(1055),
        'groupNamePosition' => $position[1055],
    ];

    yield [
        'id' => '01G92PNDTBBBW1QVQ1SP8TGN0Q',
        'unicode' => '&#1087;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_PE,
    ];

    yield [
        'id' => '01G92PTV5DBC6NBZKWTQ7Q0C3X',
        'unicode' => '&#1056;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_ER,
        'groupName' => mb_chr(1056),
        'groupNamePosition' => $position[1056],
    ];

    yield [
        'id' => '01G92PV3VGSYK0MWS6BPJJ0GX8',
        'unicode' => '&#1088;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ER,
    ];

    yield [
        'id' => '01G95090FTBX5KB28NP76TV6CZ',
        'unicode' => '&#1057;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_ES,
        'groupName' => mb_chr(1057),
        'groupNamePosition' => $position[1057],
    ];

    yield [
        'id' => '01G9509AS60BS9V2CHR0MEVED7',
        'unicode' => '&#1089;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ES,
    ];

    yield [
        'id' => '01G950H9GSKTX7GS1K41KW076M',
        'unicode' => '&#1058;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_TE,
        'groupName' => mb_chr(1058),
        'groupNamePosition' => $position[1058],
    ];

    yield [
        'id' => '01G950HNMGDTYS73WCZXDX961T',
        'unicode' => '&#1090;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_TE,
    ];

    yield [
        'id' => '01G950HZ6V63S06RV4MW733MT6',
        'unicode' => '&#1144;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_UK,
        'groupName' => mb_chr(1144),
        'groupNamePosition' => $position[1144],
    ];

    yield [
        'id' => '01G950J774MJYSZZEMV9YGQP7S',
        'unicode' => '&#1145;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_UK,
    ];

    yield [
        'id' => '01G9FNT98DEZP936HE2QHTSCBE',
        'unicode' => '&#42571;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_MONOGRAPH_UK,
    ];

    yield [
        'id' => '01G9FFVVD993D4KJDH6TYG1HZ3',
        'unicode' => '&#1059;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_U,
    ];

    yield [
        'id' => '01G9FFW38PENHXXGX48S7TBM9F',
        'unicode' => '&#1091;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_U,
    ];

    yield [
        'id' => '01G950V96G6AN6R3ZD0QRDCW1E',
        'unicode' => '&#1060;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_EF,
        'groupName' => mb_chr(1060),
        'groupNamePosition' => $position[1060],
    ];

    yield [
        'id' => '01G950W0GMHVCC9TM5DPCHPRA5',
        'unicode' => '&#1092;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_EF,
    ];

    yield [
        'id' => '01G950WTG27VJ74NJTA4E6CFPK',
        'unicode' => '&#1061;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_HA,
        'groupName' => mb_chr(1061),
        'groupNamePosition' => $position[1061],
    ];

    yield [
        'id' => '01G950X7K1RHBPY4SY600ARMYY',
        'unicode' => '&#1093;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_HA,
    ];

    yield [
        'id' => '01G951GH0WBBB4JS3DCG03W7TF',
        'unicode' => '&#1120;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_OMEGA,
        'groupName' => mb_chr(1120),
        'groupNamePosition' => $position[1120],
    ];

    yield [
        'id' => '01G9532476GT74RZ6T2QHT5KMX',
        'unicode' => '&#1121;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_OMEGA,
    ];

    yield [
        'id' => '01G953274KT8R5FK9BT4RR4S0Y',
        'unicode' => '&#1150;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_OT,
        'groupName' => mb_chr(1150),
        'groupNamePosition' => $position[1150],
    ];

    yield [
        'id' => '01G951GTCHS04HP13897XF63S9',
        'unicode' => '&#1151;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_OT,
    ];

    yield [
        'id' => '01G951M6N1MZSGKER080EHGK8R',
        'unicode' => '&#1062;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_TSE,
        'groupName' => mb_chr(1062),
        'groupNamePosition' => $position[1062],
    ];

    yield [
        'id' => '01G951MEV77BWG1F69JD3CH8S6',
        'unicode' => '&#1094;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_TSE,
    ];

    yield [
        'id' => '01G951QJ186FWNYD3RV1PDJPP9',
        'unicode' => '&#1063;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_CHE,
        'groupName' => mb_chr(1063),
        'groupNamePosition' => $position[1063],
    ];

    yield [
        'id' => '01G951QVC4MF57FNG1EF96ME0V',
        'unicode' => '&#1095;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_CHE,
    ];

    yield [
        'id' => '01G951TXKHGPVC021NMH7RVAS7',
        'unicode' => '&#1064;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_SHA,
        'groupName' => mb_chr(1064),
        'groupNamePosition' => $position[1064],
    ];

    yield [
        'id' => '01G951V5NS1JGZ2FAYVQB4793R',
        'unicode' => '&#1096;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_SHA,
    ];

    yield [
        'id' => '01G95277BQ1TV735A0SM7M80PJ',
        'unicode' => '&#1065;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_SHCHA,
        'groupName' => mb_chr(1065),
        'groupNamePosition' => $position[1065],
    ];

    yield [
        'id' => '01G95278420R1DRPXDBT5S5ZR1',
        'unicode' => '&#1097;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_SHCHA,
    ];

    yield [
        'id' => '01G99WJPPPX0561JA3W09T608D',
        'unicode' => '&#1098;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_HARD_SIGN,
    ];

    yield [
        'id' => '01G9J0DG4A2594QKHKWN5JVPER',
        'unicode' => '&#830;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_VERTICAL_TILDE,
    ];

    yield [
        'id' => '01G9A9MZEJ4EVN24A7B7DAG724',
        'unicode' => '&#1100;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_SOFT_SIGN,
    ];

    yield [
        'id' => '01G9A27YTK9PMXDNQP45T389PQ',
        'unicode' => '&#1099;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YERU,
    ];

    yield [
        'id' => '01GBAMN4H9PDE8WSCEEBEMP7WP',
        'unicode' => '&#1099;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YERU_AND_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01G9D5AJ20H4K2ETTMSGYF4CVY',
        'unicode' => '&#1123;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_TSHE,
    ];

    yield [
        'id' => '01G952DVM56NJTCVFZQ7DW1PWA',
        'unicode' => '&#1070;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_YU,
        'groupName' => mb_chr(1070),
        'groupNamePosition' => $position[1070],
    ];

    yield [
        'id' => '01G952E6PZ4SSNGFPQKCMRFRJY',
        'unicode' => '&#1102;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YU,
        'groupName' => mb_chr(1070),
        'groupNamePosition' => $position[1070],
    ];

    yield [
        'id' => '01GFGV7Z9YA142K2Z726MKTDQW',
        'unicode' => '&#1126;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_YUS,
        'groupName' => mb_chr(1126),
        'groupNamePosition' => $position[1126],
    ];

    yield [
        'id' => '01G9FQ3VC6K6RKTEDJJYTZZX43',
        'unicode' => '&#1127;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YUS,
        'groupName' => mb_chr(1126),
        'groupNamePosition' => $position[1126],
    ];

    yield [
        'id' => '01G95639BPW5PZ9NVAS2RC6N3V',
        'unicode' => '&#42582;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_IOTIFIED_A,
        'groupName' => mb_chr(42582),
        'groupNamePosition' => $position[42582],
    ];

    yield [
        'id' => '01G9563KK3K55PTWGZ1DYQB8XT',
        'unicode' => '&#42583;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_IOTIFIED_A,
    ];

    yield [
        'id' => '01G9568NK4RKADKDQTA225F5SA',
        'unicode' => '&#1134;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_KSI,
        'groupName' => mb_chr(1134),
        'groupNamePosition' => $position[1134],
    ];

    yield [
        'id' => '01G95697BJC3H878GNHK7EE5FW',
        'unicode' => '&#1135;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_KSI,
        'groupName' => mb_chr(1134),
        'groupNamePosition' => $position[1134],
    ];

    yield [
        'id' => '01G9569QJW4CSGS2YNDDYCXPAJ',
        'unicode' => '&#1136;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_PSI,
        'groupName' => mb_chr(1136),
        'groupNamePosition' => $position[1136],
    ];

    yield [
        'id' => '01G956QBCZMT524P3YEVWKM8TK',
        'unicode' => '&#1137;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_PSI,
    ];

    yield [
        'id' => '01G956A1R7G1APZQA09ZD4032X',
        'unicode' => '&#1138;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_FITA,
        'groupName' => mb_chr(1138),
        'groupNamePosition' => $position[1138],
    ];

    yield [
        'id' => '01G956AG31MZS06FMJD2M7Z3JT',
        'unicode' => '&#1139;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_FITA,
        'groupName' => mb_chr(1138),
        'groupNamePosition' => $position[1138],
    ];

    yield [
        'id' => '01G956AXDPSVB063PNY8E7CWCJ',
        'unicode' => '&#1140;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_IZHITSA,
        'groupName' => mb_chr(1140),
        'groupNamePosition' => $position[1140],
    ];

    yield [
        'id' => '01G956B6WGJYEGBH67QWBHHE4B',
        'unicode' => '&#1141;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_IZHITSA,
        'groupName' => mb_chr(1140),
        'groupNamePosition' => $position[1140],
    ];

    yield [
        'id' => '01GA1R8VNFNQ3A6ZAFMYRS20F1',
        'unicode' => '&#1040;&#1158;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_A_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT,
        'groupName' => mb_chr(1040),
        'groupNamePosition' => $position[1040],
    ];

    yield [
        'id' => '01GA6VGHSTM0R282TYTVEA24PV',
        'unicode' => '&#1040;&#1158;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_A_AND_PSILI_PNEUMATA,
        'groupName' => mb_chr(1040),
        'groupNamePosition' => $position[1040],
    ];

    yield [
        'id' => '01GA95B51WC5QM01MY4ZF8G1PK',
        'unicode' => '&#1072;&#1158;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_A_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GA96HWZG1YST2TH3V08446V9',
        'unicode' => '&#1072;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_A_AND_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GA97S8HNNKW7C740EW00XHF7',
        'unicode' => '&#1072;&#1158;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_A_AND_PSILI_PNEUMATA,
    ];

    yield [
        'id' => '01GA97SFD35WF6R5G7XMNZ68PJ',
        'unicode' => '&#1072;&#768;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_A_AND_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01GB3AGK4K1RRKXVJPGE86BZ4V',
        'unicode' => '&#1075;&#1155;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_GHE_AND_TITLO,
    ];

    yield [
        'id' => '01GB5H9F5WBC7VKBS7KGXTRDV0',
        'unicode' => '&#1087;&#11757;&#1159;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_PE_AND_LETTER_ES_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GB84AYRN7R1EVJMZAD6Q9570',
        'unicode' => '&#1077;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_IE_AND_COMBINING_ACUTE_ACCENT,
        'groupName' => mb_chr(1045),
        'groupNamePosition' => $position[1045],
    ];

    yield [
        'id' => '01GB8BSH39Z8TTFKQRYZDT4D98',
        'unicode' => '&#1086;&#768;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_O_AND_COMBINING_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01GB8CTS8VKP2GG2ZSKD816HST',
        'unicode' => '&#1094;&#11747;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ER_AND_COMBINING_CYRILLIC_DE,
    ];

    yield [
        'id' => '01GB8G3V62E74S1W0M46SH1G9Z',
        'unicode' => '&#1075;&#11747;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_GHE_AND_COMBINING_CYRILLIC_DE,
    ];

    yield [
        'id' => '01GBAFFAY228S4KK55QGQPYB7W',
        'unicode' => '&#1095;&#99;&#11756;&#1159;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_CHE_AND_LETTER_ES_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBAFRZ7KYKZZRK97XVHWXXBQ',
        'unicode' => '&#1083;&#11747;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_EL_AND_COMBINING_CYRILLIC_DE,
    ];

    yield [
        'id' => '01GBAG2JGD87BCM099X32DEV6W',
        'unicode' => '&#1088;&#11757;&#1159;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ER_AND_LETTER_ES_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBAGZQFP4YS2TK7W1SMGCJ7Y',
        'unicode' => '&#1080;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_I_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBAN181AYMV04VXJRKP4ZY1A',
        'unicode' => '&#1076;&#11757;&#1159;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_DE_AND_LETTER_ES_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBANFK0PXEJ8KEV78VE1Z568',
        'unicode' => '&#1078;&#11757;&#1159;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ZHE_AND_LETTER_ES_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBANV3AW1K9J2RNX6A5PCDPZ',
        'unicode' => '&#1086;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_O_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBAQCEQ4VJTH2BC7H817J72M',
        'unicode' => '&#1158;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBAQNBRMSP101BF9QPJPRXEQ',
        'unicode' => '&#1091;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBAWBXEV5054M6366NCZ16D5',
        'unicode' => '&#1123;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_TSHE_AND_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBAXJM0PWH6SXJ90HZBX4171',
        'unicode' => '&#1141;&#11746;&#1159;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_IZHITSA_AND_COMBINING_CYRILLIC_LETTER_GHE_AND_POKRYTIE,
    ];

    yield [
        'id' => '01GBAXTQSR1N0NTTX4GR8259RD',
        'unicode' => '&#1110;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YI_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBAY5VNB2D71PAMW9DE5NT5H',
        'unicode' => '&#42571;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_MONOGRAPH_UK_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBB21A8YZ7FNVW7N6847GPX0',
        'unicode' => '&#1127;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YUS_AND_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBD8BKZJHXRZ2DY1D4DV43Y3',
        'unicode' => '&#1143;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_IZHITSA_AND_DOUBLE_GRAVE_ACCENT,
    ];
    yield [
        'id' => '01GBDF1D2TNWVMRQAM0KTK33VA',
        'unicode' => '&#1030;&#1158;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_YI_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA,
        'groupName' => mb_chr(1031),
        'groupNamePosition' => $position[1031],
    ];

    yield [
        'id' => '01GBDFE3AHYX4T349EMJAN865W',
        'unicode' => '&#1121;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_OMEGA_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBMSPTVX7NY96T27EHFER3G1',
        'unicode' => '&#1049;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_SHORT_I,
        'groupName' => mb_chr(1048),
        'groupNamePosition' => $position[1048],
    ];

    yield [
        'id' => '01GBN656A8KDWJXE2JCAPWDP63',
        'unicode' => '&#1110;&#1158;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YI_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA,
    ];

    yield [
        'id' => '01GBN6K99DZT9BAYRGGW0QKSZT',
        'unicode' => '&#1088;&#1155;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ER_AND_CYRILLIC_TITLO,
    ];

    yield [
        'id' => '01GBN83A207HV63C8F3EZFEA3E',
        'unicode' => '&#1144;&#1158;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_UK_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT,
        'groupName' => mb_chr(1144),
        'groupNamePosition' => $position[1144],
    ];

    yield [
        'id' => '01GBQC78EKZDX4D643R4WEY2BF',
        'unicode' => '&#1127;&#1158;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YUS_AND_PSILI_PNEUMATA_AND_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBQHGA9CEDBW085PT3KJJ8QT',
        'unicode' => '&#1127;&#1158;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YUS_AND_PSILI_PNEUMATA,
    ];

    yield [
        'id' => '01GBQHXQ9SCX36HDKWJVFTVXHB',
        'unicode' => '&#1127;&#1158;&#768;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_YUS_AND_COMBINING_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01GBSYRGEBXGW1C78248F62ASV',
        'unicode' => '&#1054;&#1158;&#769;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_O_AND_PSILI_PNEUMATA_AND_COMBINING_ACUTE_ACCENT,
    ];

    yield [
        'id' => '01GBT2PQVEKZVXCXGWNGWEN09Z',
        'unicode' => '&#768;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_TSHE_AND_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01GBWMWMMMK7DDBVY51GE1ZEDY',
        'unicode' => '&#42571;&#768;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_MONOGRAPH_UK_AND_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01GBZ4HB9MCYKXXHZH24365W1D',
        'unicode' => '&#1078;&#1155;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_ZHE_AND_CYRILLIC_TITLO,
    ];

    yield [
        'id' => '01GCC0G7M0DP2D4BD78412AV8V',
        'unicode' => '&#1083;&#1155;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_SMALL_LETTER_EL_AND_CYRILLIC_TITLO,
    ];

    yield [
        'id' => '01GCC5VC3RQR63S7TKVWMD3QNN',
        'unicode' => '&#1158;&#768;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_CYRILLIC_PSILI_PNEUMATA_COMBINING_GRAVE_ACCENT,
    ];

    yield [
        'id' => '01GCC7DDYDEJS50FK0H7K79JTH',
        'unicode' => '&#1054;&#1158;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_CYRILLIC_CAPITAL_LETTER_O_AND_COMBINING_CYRILLIC_PSILI_PNEUMATA,
    ];

    yield [
        'id' => '01GCEH1FMA6YCY963YFVPDGHG6',
        'unicode' => '&#785;',
        'font' => Font::CODE_PONOMAR_UNICODE,
        'symbol' => SymbolFixture::CODE_COMBINING_INVERTED_BREVE,
    ];
}
