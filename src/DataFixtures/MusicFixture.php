<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Music;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\AbstractUid;
use Symfony\Component\Uid\Ulid;

class MusicFixture extends Fixture
{
    public const ID_FIRST = '01G5SAQ9XCNZRMSFXTDHPZJDSF';

    public function load(ObjectManager $manager): void
    {
        foreach ($this->getData() as $datum) {
            $music = new Music(
                title: $datum['title'],
                link: $datum['link'],
                description: $datum['description'] ?? '',
            );
            if ($datum['id'] instanceof AbstractUid) {
                $music->setId($datum['id']);
            }
            $music->setPublishedAt(new \DateTimeImmutable());
            $manager->persist($music);
        }
        $manager->flush();
    }

    /**
     * @return array<integer, array<string, mixed>>
     */
    public function getData(): iterable
    {
        yield [
            'id' => Ulid::fromString(self::ID_FIRST),
            'title' => 'Не грусти, не рыдай',
            'link' => 'https://youtu.be/l31c9qY4tNI',
            'publishedAt' => new \DateTime(),
        ];
    }
}
