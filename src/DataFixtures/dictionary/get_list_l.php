<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListL(): iterable
{
    yield [
        'name' => 'Лѣ́то',
        'description' => 'год',
    ];
    yield [
        'name' => 'Льстѧ́й',
        'description' => 'льстящий',
    ];
    yield [
        'name' => 'Лїтꙋргі́ѧ',
        'description' => '(греч. общее дело) - Главная служба христиан с тайнством причащения',
    ];
    yield [
        'name' => 'Лѣ́пый',
        'description' => 'красивый, изящный',
    ];
    yield [
        'name' => 'Лѣ́пѡ',
        'description' => 'прекрасно, Красиво',
    ];
    yield [
        'name' => 'Лежа́щїй',
        'description' => 'лежачий',
    ];
}
