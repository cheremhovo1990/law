<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListWord(): iterable
{
    yield [
        'name' => 'Са́дъ',
        'description' => 'растение, дерево, лес',
    ];

    yield [
        'name' => 'Си́це',
        'description' => 'так',
    ];

    yield [
        'name' => 'Ст҃ы́й дх҃ъ',
        'description' => 'святый дух',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Сꙋгꙋ́бъ',
        'description' => 'двойствен',
    ];

    yield [
        'name' => 'Слы́шай',
        'description' => 'слышащий',
    ];

    yield [
        'name' => 'Сни́де',
        'description' => 'пошел',
    ];

    yield [
        'name' => 'Со́нмъ',
        'description' => 'Собрание, совокупность, соединение',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Сотворѝ',
        'description' => 'Сотворил',
    ];

    yield [
        'name' => 'Сн҃ъ О҆чь',
        'description' => 'Сын Отечь (Сын Отчии - о Христе)',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Со́нмище',
        'description' => 'Судебное собрание верующих',
    ];

    yield [
        'name' => 'Ст҃ъ',
        'description' => 'Свят',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Сщ҃е́нникъ',
        'description' => 'Священнник',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Сл҃нце',
        'description' => 'Солнце',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Срⷣце',
        'description' => 'Сердце',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Стрⷭ҇ть',
        'description' => 'Страсть (страдания Господа и мучеников)',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Сп҃съ',
        'description' => 'Спас',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Сп҃си́тель',
        'description' => 'Спаситель',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Слы́шахъ',
        'description' => 'Я слышал',
    ];

    yield [
        'name' => 'Стоѧ́щїй',
        'description' => 'Стоячий',
    ];

    yield [
        'name' => 'Сый',
        'description' => 'Сущий',
    ];

    yield [
        'name' => 'Свѣ́дый',
        'description' => 'Знающий, ведающий',
    ];

    yield [
        'name' => 'Стезѧ̀',
        'description' => 'Путь',
    ];

    yield [
        'name' => 'Стꙋ́днѡ грѣ́хъ содѣва́юще',
        'description' => 'Сотворя постыдный грех',
    ];

    yield [
        'name' => 'Сыта́',
        'description' => 'Вода, подслащенная медом, или медовый взвар на воде',
    ];

    yield [
        'name' => 'Село',
        'description' => 'поле',
    ];

    yield [
        'name' => 'Сꙋ́щихъ',
        'description' => 'бывших',
    ];

    yield [
        'name' => 'Сокрꙋше́нїе',
        'description' => 'разрушение',
    ];
}
