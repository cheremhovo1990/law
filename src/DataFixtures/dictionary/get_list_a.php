<?php

declare(strict_types=1);

use App\DataFixtures\DictionaryFixture;

/**
 * @return iterable<integer, mixed>
 */
function getListA(): iterable
{
    yield [
        'name' => 'А҆́гг҃лъ',
        'description' => 'ангел - посланный, вестник, дух бесплотный',
        'isTitlo' => true,
        'normalizeName' => 'А҆́нгелъ',
    ];
    yield [
        'name' => 'А҆́ггель',
        'description' => 'бес',
    ];
    yield [
        'name' => 'А҆́гг҃льскїй',
        'description' => 'ангельский',
        'normalizeName' => 'А҆́нгельскїй',
        'isTitlo' => true,
    ];
    yield [
        'id' => DictionaryFixture::ID_FIRST,
        'name' => 'А҆́зъ',
        'description' => 'я',
    ];
    yield [
        'name' => 'А҆рха́гг҃лъ',
        'description' => 'архангел',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'А҆рха́гг҃лъскїй',
        'description' => 'архангельский',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'А҆пⷭ҇лъ или А҆пⷭ҇толъ',
        'description' => 'апостол',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'А҆рхїепⷭ҇копъ',
        'description' => 'архиепископ',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'А҆рхїепⷭ҇копскїй',
        'description' => 'архиепископский',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'А҆́бїе',
        'description' => 'Вскоре, тотчас, вдруг',
    ];

    yield [
        'name' => 'А҆пока́лѷѱїсъ',
        'description' => 'пророческая книга о втором пришествии Христа',
    ];
}
