<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListAnd(): iterable
{
    yield [
        'name' => 'Исповѣ́дати, Исповѣ́даю',
        'description' => 'объявить, признавать, поведаю, рассказываю',
    ];
    yield [
        'name' => 'Исповѣ́датисѧ, Исповѣ́даніе',
        'description' => 'славить, воздавать хвалу, прославлять',
    ];
    yield [
        'name' => 'И҆жди́хъ',
        'description' => 'Я прожил, промотал',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'И҆́миже',
        'description' => 'Которыми',
        'isTitlo' => false,
    ];
}
