<?php

declare(strict_types=1);

require_once __DIR__ . '/get_list_and.php';
require_once __DIR__ . '/get_list_zelo.php';
require_once __DIR__ . '/get_list_i.php';
require_once __DIR__ . '/get_list_l.php';
require_once __DIR__ . '/get_list_t.php';
require_once __DIR__ . '/get_list_word.php';
require_once __DIR__ . '/get_list_rest.php';
require_once __DIR__ . '/get_list_land.php';
require_once __DIR__ . '/get_list_uk.php';
require_once __DIR__ . '/get_list_vedi.php';
require_once __DIR__ . '/get_list_worm.php';
require_once __DIR__ . '/get_list_speak.php';
require_once __DIR__ . '/get_list_on.php';
require_once __DIR__ . '/get_list_psi.php';
require_once __DIR__ . '/get_list_buki.php';
require_once __DIR__ . '/get_list_a.php';

/**
 * @return iterable<integer, mixed>
 */
function getList(): iterable
{
    yield [
        'name' => 'Гдⷭ҇ь',
        'description' => 'Господь',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Гдⷭ҇и',
        'description' => 'Господи',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Гжⷭ҇а',
        'description' => 'Госпожа',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Гл҃го́лы',
        'description' => 'глаголы',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Дш҃е и҆́стины',
        'description' => 'душе Истины',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Дв҃дъ',
        'description' => 'Давид (царь, пророк, псалмопевец)',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Дв҃а',
        'description' => 'Дева (о Богородице)',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Е҆ѵⷢ҇лїе',
        'description' => 'евангелие',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Е҆ѵⷢ҇лі́стъ',
        'description' => 'евангелист',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Ѵ҆поста́сь',
        'description' => 'Лицо, личность',
    ];

    yield [
        'name' => 'Е҆ди́нъ',
        'description' => 'один',
    ];
    yield [
        'name' => 'Е҆ле́нь',
        'description' => 'олень',
    ];
    yield [
        'name' => 'Е҆́сень',
        'description' => 'осень',
    ];
    yield [
        'name' => 'Е҆́зеро',
        'description' => 'озеро',
    ];
    yield [
        'name' => 'Живѣ́те',
        'description' => 'живите',
    ];

    yield [
        'name' => 'Жа́ждай',
        'description' => 'жаждущий',
    ];
    yield [
        'name' => 'Хотѧ́й',
        'description' => 'хотящий',
    ];

    yield [
        'name' => 'Ѡбле́клсѧ є҆сѝ',
        'description' => 'Ты облачился, оделся',
    ];
    yield [
        'name' => 'Мѡѷсе́й',
        'description' => 'Моисей',
    ];

    yield [
        'name' => 'Даде́',
        'description' => 'дал',
    ];
    yield [
        'name' => 'Ѡ҆́бласть',
        'description' => 'власть',
    ];
    yield [
        'name' => 'Ѿве́рзе',
        'description' => 'открыл',
    ];

    yield [
        'name' => 'Жꙋ́пельно',
        'description' => 'серное',
    ];
    yield [
        'name' => 'Ниже́',
        'description' => 'а также не',
    ];

    yield [
        'name' => 'И҆́же',
        'description' => 'который, тот самый, "тот, кто ..."',
    ];
    yield [
        'name' => 'И҆̀же',
        'description' => 'которые, именно те, те самые',
    ];

    yield [
        'name' => 'Нападо́ша',
        'description' => 'напали',
    ];

    yield [
        'name' => 'І҆и҃ль',
        'description' => 'Израиль',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'І҆и҃льскїй',
        'description' => 'Израильский',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'І҆и҃съ',
        'description' => 'Иисус',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'І҆ерⷭ҇ли́мъ',
        'description' => 'Иерусалим',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Хїтѡ́нъ',
        'description' => 'Одежда, в первую очередь монашеская, также риза Спасителя',
    ];
    yield [
        'name' => 'Ми́ръ',
        'description' => 'Спокойствие, тишина',
    ];
    yield [
        'name' => 'Мі́ръ',
        'description' => 'Вселенная',
    ];
    yield [
        'name' => 'Крⷭ҇тъ',
        'description' => 'Крест',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Крⷭ҇ще́нїе',
        'description' => 'Крещение (Господне)',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Крⷭ҇ти́тель',
        'description' => 'Креститель (об Иоанне Предтече)',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Мрⷣость',
        'description' => 'Мудрость',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Млⷭ҇ть',
        'description' => 'Милость',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Млⷭ҇рдїе',
        'description' => 'Милосердие',
    ];
    yield [
        'name' => 'Мл҃тва',
        'description' => 'Молитва',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Мт҃и',
        'description' => 'Мати',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Млⷣнцъ',
        'description' => 'Младенец (О Христе)',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Мч҃никъ',
        'description' => 'Мученик',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Мрє́жи',
        'description' => 'Сети',
    ];

    yield [
        'name' => 'Йдо́ста',
        'description' => 'Пошли',
    ];
    yield [
        'name' => 'Нб҃о',
        'description' => 'Небо',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'На нб҃сѣ́хъ',
        'description' => 'На небесех',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Нб҃ный',
        'description' => 'Небесный',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Хотѣти',
        'description' => 'Желать, любить',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Имⷬ҇къ',
        'description' => 'Имя рек, то есть сказав, вставив имя или имена тех, за кого молимся',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Мамѡ́нѣ',
        'description' => 'Богатству',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Мамѡ́нѣ',
        'description' => 'Богатству',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'і҆.н҃.ц҃.і҆ - і҆исъ назѡрѧни́нъ цр҃ь і҆ꙋде́йскїй',
        'description' => 'Иисус Назорей царь иудейский',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Фі́нїѯъ',
        'description' => 'Пальма',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Фело́нь',
        'description' => 'Верхняя риза священника',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Хрⷭ҇то́съ',
        'description' => 'Христос',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Хрⷭ҇тѐ',
        'description' => '(звать.) Христе',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Хрⷭ҇тїани́нъ',
        'description' => 'Христианин',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Хрⷭ҇тїа́не',
        'description' => 'Христианине',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Цр҃ь',
        'description' => 'Царь',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Цр҃ица',
        'description' => 'Царица',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Цр҃ковь',
        'description' => 'Церковь',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Црⷭ҇тво',
        'description' => 'Царство',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Црⷭ҇кїй',
        'description' => 'Царский',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Цѣсаре́вичъ',
        'description' => 'Наследник престола',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Ке́сарь',
        'description' => 'Правитель',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Цѣ́лъ',
        'description' => 'Здоровый',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'чⷭ҇ть',
        'description' => 'Честь',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'чⷭ҇тенъ',
        'description' => 'Честен',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'чⷭ҇тый',
        'description' => 'Чистый',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Дш҃е',
        'description' => 'Душе (к Святому Духу)',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Дꙋшѐ',
        'description' => 'Душе (к человеческой душе)',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Ще́дрый',
        'description' => 'Милостивый, сострадательный',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Ще́дрити',
        'description' => 'Жалеть, миловать',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Мо́щи',
        'description' => 'Мочи нет',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Но́щь',
        'description' => 'Ночь',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Ко́лющїй',
        'description' => 'Колючий',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'ѧ҆́ти',
        'description' => 'брать, взять',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Ꙗзы́къ',
        'description' => 'народ',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'ѧ҆зы́къ',
        'description' => 'Дар речи, орган речи',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'ѧ҆̀',
        'description' => 'Местоимение их',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Гла́съ',
        'description' => 'Смысл речи',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Кі́йждо',
        'description' => 'Всякий, каждый',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Нѣ́цын',
        'description' => 'Некоторых',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Ка́мѡ',
        'description' => 'Куда',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'є҆да',
        'description' => 'Разве',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'ѵ҆ссѡ́пъ',
        'description' => 'Кропильная трава',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Жнво́ть',
        'description' => 'Жизнь',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'И҆́же Прⷭ҇то́лъ',
        'description' => 'Тот самый Престол',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'І҆и҃са Хрⷭ҇та мѧ̀ вз̾ѧ́ша',
        'description' => 'Иисуса Христа Меня взяли',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Нѣ́сть',
        'description' => 'нет',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Шата́нїѧ бѣсо́вскагѡ',
        'description' => 'Блуждания суеты бесовской',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Горѣ̀',
        'description' => 'Вверх',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Да́ждь',
        'description' => 'Дай',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Е҆́же',
        'description' => 'То самое, которое',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Херꙋві́мъ',
        'description' => 'Херувимы - высший чин Ангелов Божиих',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Ѡбрати́ша сѧ',
        'description' => 'Обратились',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Цѣли́ши',
        'description' => 'Исцеляешь',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Шестокрила́тїи',
        'description' => 'Херувимы и Серафимы (огненные, пламенные) высшие чины Ангелов Божиих',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'ю҆́же',
        'description' => 'Которую',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Ꙗвѣ',
        'description' => 'Явственно',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Грꙋзи́мꙋ',
        'description' => 'Погружаему',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Десни́цꙋ',
        'description' => 'Правую руку',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Е҆гда̀',
        'description' => 'Когда',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Е҆сѝ',
        'description' => '(2-е лицо ед. числа от глагола-связки Быти) - Ты (есть)',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Ко́вникъ',
        'description' => 'Коварный',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Кончева́ѧ',
        'description' => 'Заканчивая',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Мздои́мче',
        'description' => '(Звательная форма) - Мздоимец (о Господе), Воздающий праведную мзду',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Мытарю̀',
        'description' => 'Сборщик податей, лихоимец, притеснитель',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Не свѣ́мъ',
        'description' => 'Не знаю',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Ѡдержи́мъ',
        'description' => 'Одержимый',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Ѡскверни́хъ',
        'description' => 'Я осквернил',
        'isTitlo' => false,
    ];

    yield [
        'name' => 'Шє́ствїѧ мѝ',
        'description' => 'Мой шествия, движение по жизни',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Шестокри́лнїи',
        'description' => 'Шестокрылые серафимы, самый высший и близкий к Богу чин из девяти аглеских чинов',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'Ꙗви́ти та̑наѧ',
        'description' => 'Открыть мои тайные дела',
        'isTitlo' => false,
    ];
    yield [
        'name' => 'ѧ҆́коже',
        'description' => 'как',
        'isTitlo' => false,
    ];

    yield from getListAnd();
    yield from getListZelo();
    yield from getListI();
    yield from getListL();
    yield from getListT();
    yield from getListWord();
    yield from getListRest();
    yield from getListLand();
    yield from getListUk();
    yield from getListVedi();
    yield from getListWorm();
    yield from getListSpeak();
    yield from getListOn();
    yield from getListPsi();
    yield from getListBuki();
    yield from getListA();
}
