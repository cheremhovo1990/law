<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListRest(): iterable
{
    yield [
        'name' => 'Пресꙋ́щественный',
        'description' => 'существовавший всегда, прежде всего и превыше всего',
    ];

    yield [
        'name' => 'Прїидо́ша',
        'description' => 'пришли',
    ];

    yield [
        'name' => 'Паде́сѧ',
        'description' => 'пала',
    ];

    yield [
        'name' => 'Пррⷪ҇къ',
        'description' => 'Пророк',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Прмрⷭ҇ть',
        'description' => 'Премудрость',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Прⷭ҇то́лъ',
        'description' => 'Престол',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Прⷣпбенъ',
        'description' => 'Преподобен',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Прабⷣнъ',
        'description' => 'Праведен',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Прⷭ҇но',
        'description' => 'Присно (всегда, непрестанно)',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Пало́мникъ',
        'description' => 'Богомолец, путешествующий по святым местам',
    ];

    yield [
        'name' => 'Панагі́а',
        'description' => 'Образ Божией Матери, носимый архиереем на груди',
    ];

    yield [
        'name' => 'Паникади́ло',
        'description' => 'Подвесной многосвечный светильник',
    ];

    yield [
        'name' => 'Про́стъ',
        'description' => 'Прямой, ясный',
    ];

    yield [
        'name' => 'Пе́щь',
        'description' => 'Печь',
    ];

    yield [
        'name' => 'Пе́ртъ',
        'description' => 'Персть',
    ];

    yield [
        'name' => 'Пе́рть',
        'description' => 'Прах',
    ];

    yield [
        'name' => 'Патери́къ',
        'description' => 'Сборник житий святых',
    ];

    yield [
        'name' => 'Поти́р',
        'description' => 'Чаша; богослужебный сосуд, в котором во время Божественной литургии возносятся Святые Дары',
    ];

    yield [
        'name' => 'Попере́тъ',
        'description' => 'Победит',
    ];

    yield [
        'name' => 'Преложи́ти',
        'description' => 'Переменять, переставлять, перемещать',
    ];

    yield [
        'name' => 'Пропѧ́ша',
        'description' => 'Распяли',
    ];

    yield [
        'name' => 'Преложѝ',
        'description' => 'Перемени',
    ];

    yield [
        'name' => 'Прⷭ҇нѡ',
        'description' => 'Всегда',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Прїѧ́ти',
        'description' => 'Принять',
    ];
    yield [
        'name' => 'Простра́нно',
        'description' => 'Просторное, великое',
    ];
    yield [
        'name' => 'Прїѧ́тъ',
        'description' => 'Приял',
    ];

    yield [
        'name' => 'Пе́рстъ',
        'description' => 'палец',
    ];

    yield [
        'name' => 'Порфѵ́ра',
        'description' => 'Красная одежда',
    ];
}
