<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListBuki(): iterable
{
    yield [
        'name' => 'Бг҃ъ',
        'description' => 'бог',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Бж҃е',
        'description' => 'боже',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Бж҃тво̀',
        'description' => 'божество',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Бцⷣа или бг҃оро̀дица',
        'description' => 'богородица',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Бл҃же́нъ',
        'description' => 'блажен',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Бл҃гослове́нъ',
        'description' => 'благословен',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Блгⷣть или бл҃года́ть',
        'description' => 'благодать',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Бл҃гочⷭ҇тнѡ',
        'description' => 'благочестно',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Бі́блїа',
        'description' => '(греч. книги) - Библия',
    ];

    yield [
        'name' => 'Бѣ́ста',
        'description' => 'Были',
    ];

    yield [
        'name' => 'Бы́сть',
        'description' => 'Стал',
    ];

    yield [
        'name' => 'Бѣ́совѡ́мъ',
        'description' => 'Бесам',
    ];

    yield [
        'name' => 'Блазни́тъ',
        'description' => 'Соблазняет, совращает',
    ];
}
