<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListOn(): iterable
{
    yield [
        'name' => 'О҆ц҃ъ',
        'description' => 'Отец (Небесны)',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'О҆ч҃ество',
        'description' => 'Отечество (Небесное)',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'О҆́цта',
        'description' => 'Уксуса',
    ];

    yield [
        'name' => 'О҆́дръ',
        'description' => 'постель',
    ];

    yield [
        'name' => 'О҆́ко',
        'description' => 'глаз',
    ];

    yield [
        'name' => 'О҆́трокъ',
        'description' => 'ребёнок, сын',
    ];
}
