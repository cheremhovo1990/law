<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListI(): iterable
{
    yield [
        'name' => 'І҆́косъ',
        'description' => 'богослужебное песнопение',
    ];
}
