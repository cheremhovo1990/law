<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListPsi(): iterable
{
    yield [
        'name' => 'Ѱалти́рь',
        'description' => 'Струнный музыкальный инструмент, на котором играли припевая',
    ];
    yield [
        'name' => 'Ѱалти́рь',
        'description' => 'Книга псалмов, получившая название от музыкального инструмента',
    ];

    yield [
        'name' => 'Ѱало́мъ',
        'description' => 'песнь Богу',
    ];
}
