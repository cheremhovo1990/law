<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListLand(): iterable
{
    yield [
        'name' => 'Заповѣ́да',
        'description' => 'заповедал',
    ];

    yield [
        'name' => 'Зави́ди',
        'description' => 'завидовать',
    ];

    yield [
        'name' => 'Занѐ',
        'description' => 'потому что',
    ];

    yield [
        'name' => 'Зижди́тель',
        'description' => 'Создатель, Творец',
    ];

    yield [
        'name' => 'Зѣлѡ̀',
        'description' => 'Очень',
    ];
}
