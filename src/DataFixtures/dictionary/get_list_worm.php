<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListWorm(): iterable
{
    yield [
        'name' => 'Чл҃вѣ́къ',
        'description' => 'Человек',
    ];
    yield [
        'name' => 'Чл҃вѣ́чь',
        'description' => 'Человечь',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Чл҃вѣ́ческїй',
        'description' => 'Человеческий',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Червлени́ца',
        'description' => 'Багряная одежда',
    ];

    yield [
        'name' => 'Червле́ный',
        'description' => 'Багряный, ярко-малиновый цвет из особых окрашивающих червей',
    ];

    yield [
        'name' => 'Чинѡ́м',
        'description' => 'К чинам',
    ];

    yield [
        'name' => 'Ча́до',
        'description' => 'Дитя',
    ];

    yield [
        'name' => 'Ра́звѣ',
        'description' => 'Кроме',
    ];

    yield [
        'name' => 'Ра́звѣ тебє̀',
        'description' => 'Кроме Тебя',
    ];

    yield [
        'name' => 'Ра́звѣ тебє̀',
        'description' => 'Кроме Тебя',
    ];
}
