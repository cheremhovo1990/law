<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListSpeak(): iterable
{
    yield [
        'name' => 'Ро́ждїе',
        'description' => 'Розги, ветви',
    ];

    yield [
        'name' => 'Ра́ка',
        'description' => 'Гроб с мощами',
    ];

    yield [
        'name' => 'Рцы́',
        'description' => 'говори',
    ];
    yield [
        'name' => 'Рце́мъ',
        'description' => 'Скажем',
    ];
    yield [
        'name' => 'Речѐ',
        'description' => 'Сказал',
    ];

    yield [
        'name' => 'Рака̀',
        'description' => 'Пустой человек',
    ];

    yield [
        'name' => 'Растерзающаѧ',
        'description' => 'Разрывающая',
    ];

    yield [
        'name' => 'Рѣ́хъ',
        'description' => 'я сказал',
    ];

    yield [
        'name' => 'Рꙋио̀',
        'description' => 'шерсть',
    ];
}
