<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListT(): iterable
{
    yield [
        'name' => 'Тро́сть',
        'description' => 'тростинка, палочка, камыши',
    ];

    yield [
        'name' => 'Трⷪ҇ца',
        'description' => 'троица',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Трⷪ҇ческъ',
        'description' => 'троическ',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Трⷪ҇ченъ',
        'description' => 'троичен',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Трист҃о́е',
        'description' => 'трисвятое',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Тѝ',
        'description' => 'тебе',
    ];

    yield [
        'name' => 'Тишинꙋ̀',
        'description' => 'покой',
    ];

    yield [
        'name' => 'Та́ть',
        'description' => 'вор',
    ];

    yield [
        'name' => 'Тропа́рь',
        'description' => 'церковное песнопение',
    ];

    yield [
        'name' => 'Тлѧ̀',
        'description' => 'тление',
    ];
}
