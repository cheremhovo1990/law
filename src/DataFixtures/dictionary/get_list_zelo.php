<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListZelo(): iterable
{
    yield [
        'name' => 'Ѕѣ́ница',
        'description' => 'глаз, зрачок',
    ];
    yield [
        'name' => 'Ѕѣлѡ̀',
        'description' => 'очень, высоко',
    ];
    yield [
        'name' => 'Ѕлѣ̀',
        'description' => 'сильно',
    ];

    yield [
        'name' => 'Ѕе́лїе',
        'description' => 'трава',
    ];

    yield [
        'name' => 'Ѕла́къ',
        'description' => 'зелень, трава',
    ];

    yield [
        'name' => 'Ѕло̀',
        'description' => 'зло',
    ];
}
