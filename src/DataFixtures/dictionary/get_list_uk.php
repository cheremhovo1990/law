<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListUk(): iterable
{
    yield [
        'name' => 'Ѹро́ѳе',
        'description' => 'Безумный',
    ];

    yield [
        'name' => 'Ѹтвердѝ',
        'description' => 'Он утвердил',
    ];

    yield [
        'name' => 'Ѹ҆́же',
        'description' => 'Веревка',
    ];

    yield [
        'name' => 'Ѹмно́жишасѧ',
        'description' => 'Умножились',
    ];

    yield [
        'name' => 'Ѹ҆́не',
        'description' => 'Лучше',
    ];

    yield [
        'name' => 'Ѹмы',
        'description' => 'умыл',
    ];

    yield [
        'name' => 'Ѹтолѝ',
        'description' => 'Останови',
    ];

    yield [
        'name' => 'Ѹ҆ста̀',
        'description' => 'губы',
    ];
}
