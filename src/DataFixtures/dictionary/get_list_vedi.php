<?php

declare(strict_types=1);

/**
 * @return iterable<integer, mixed>
 */
function getListVedi(): iterable
{
    yield [
        'name' => 'Взе́млѧй',
        'description' => 'берущий',
    ];

    yield [
        'name' => 'Влⷣка',
        'description' => 'владыка',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Влⷣчца',
        'description' => 'владычица',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Влⷣчество',
        'description' => 'владычество',
        'isTitlo' => true,
    ];
    yield [
        'name' => 'Воскрⷭ҇нїе',
        'description' => 'воскресение',
        'isTitlo' => true,
    ];

    yield [
        'name' => 'Взме́тъ или покры́тїе',
        'description' => 'титло',
    ];

    yield [
        'name' => 'Вкꙋпѣ',
        'description' => 'вместе',
    ];

    yield [
        'name' => 'Возвели́чилсѧ є҆сѝ',
        'description' => 'Ты возвеличился',
    ];

    yield [
        'name' => 'Вознесе́',
        'description' => 'вознес',
    ];

    yield [
        'name' => 'Ви́дѣхъ',
        'description' => 'я видел',
    ];

    yield [
        'name' => 'Возвѣ́ѧшѧ',
        'description' => 'возвеяли',
    ];

    yield [
        'name' => 'Воста̀',
        'description' => 'Восстал, воскрес',
    ];

    yield [
        'name' => 'Вѣне́цъ лѣ́та',
        'description' => 'Вершина времен',
    ];

    yield [
        'name' => 'Ви́сѧщїй',
        'description' => 'Висячий',
    ];

    yield [
        'name' => 'Воз̾ блага҆ѧ',
        'description' => 'За благое',
    ];

    yield [
        'name' => 'Вотщѐ',
        'description' => 'Напрасно',
    ];

    yield [
        'name' => 'Вѣ́мъ',
        'description' => 'Я знаю',
    ];

    yield [
        'name' => 'Во мра́цѣ',
        'description' => 'Во мраке',
    ];

    yield [
        'name' => 'Вѷссоиъ',
        'description' => 'Дорогая одежда',
    ];

    yield [
        'name' => 'Сѷиаго́га',
        'description' => 'Молитвенный дом иудеев',
    ];
}
