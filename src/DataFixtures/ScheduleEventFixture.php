<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\ScheduleEvent;
use Carbon\Carbon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Uuid;

class ScheduleEventFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        foreach ($this->getData() as $datum) {
            $scheduleEvent = new ScheduleEvent(
                calendarId: $datum['calendarId'],
                eventUid: $datum['eventUid'],
                startDateTime: $datum['startDateTime'],
                endDateTime: $datum['endDateTime'],
                summary: $datum['summary'],
                rule: $datum['rule'],
            );
            $manager->persist($scheduleEvent);
        }
        $manager->flush();
    }

    /**
     * @return array<integer, array<string, mixed>>
     */
    public function getData(): iterable
    {
        $timezone = 'Asia/Irkutsk';
        $datePrevOfWeek = Carbon::now()
            ->setTimezone($timezone)
            ->startOfWeek()
            ->subDays(3)
        ;

        yield [
            'summary' => 'Середина недели',
            'calendarId' => Uuid::v4()->toRfc4122(),
            'startDateTime' => $datePrevOfWeek->clone()->setTime(5, 10)->toDateTimeImmutable(),
            'endDateTime' => $datePrevOfWeek->clone()->setTime(7, 40)->toDateTimeImmutable(),
            'eventUid' => Uuid::v4()->toRfc4122(),
            'timezone' => $timezone,
            'rule' => 'BEGIN:VCALENDAR',
        ];

        $timezone = 'Asia/Irkutsk';
        $dateStartOfWeek = Carbon::now()
            ->setTimezone($timezone)
            ->startOfWeek()
        ;

        yield [
            'summary' => 'Одни день начало',
            'calendarId' => Uuid::v4()->toRfc4122(),
            'startDateTime' => $dateStartOfWeek->clone()->setTime(1, 20)->toDateTimeImmutable(),
            'endDateTime' => $dateStartOfWeek->clone()->setTime(3, 00)->toDateTimeImmutable(),
            'eventUid' => Uuid::v4()->toRfc4122(),
            'timezone' => $timezone,
            'rule' => 'BEGIN:VCALENDAR',
        ];
        yield [
            'summary' => 'Два дня',
            'calendarId' => Uuid::v4()->toRfc4122(),
            'startDateTime' => $dateStartOfWeek->clone()->setTime(2, 20)->toDateTimeImmutable(),
            'endDateTime' => $dateStartOfWeek->clone()->addDays(1)->setTime(2, 40)->toDateTimeImmutable(),
            'eventUid' => Uuid::v4()->toRfc4122(),
            'timezone' => $timezone,
            'rule' => 'BEGIN:VCALENDAR',
        ];
        yield [
            'summary' => 'Один день конец',
            'calendarId' => Uuid::v4()->toRfc4122(),
            'startDateTime' => $dateStartOfWeek->clone()->addDays(1)->setTime(4, 20)->toDateTimeImmutable(),
            'endDateTime' => $dateStartOfWeek->clone()->addDays(1)->setTime(5, 30)->toDateTimeImmutable(),
            'eventUid' => Uuid::v4()->toRfc4122(),
            'timezone' => $timezone,
            'rule' => 'BEGIN:VCALENDAR',
        ];

        $dateEndOfWeek = Carbon::now()
            ->setTimezone($timezone)
            ->endOfWeek()
        ;
        yield [
            'summary' => 'Два дня начало',
            'calendarId' => Uuid::v4()->toRfc4122(),
            'startDateTime' => $dateEndOfWeek->clone()->subDay()->setTime(1, 20)->toDateTimeImmutable(),
            'endDateTime' => $dateEndOfWeek->clone()->setTime(3, 00)->toDateTimeImmutable(),
            'eventUid' => Uuid::v4()->toRfc4122(),
            'timezone' => $timezone,
            'rule' => 'BEGIN:VCALENDAR',
        ];
        yield [
            'summary' => 'Одни день середина',
            'calendarId' => Uuid::v4()->toRfc4122(),
            'startDateTime' => $dateEndOfWeek->clone()->setTime(2, 20)->toDateTimeImmutable(),
            'endDateTime' => $dateEndOfWeek->clone()->setTime(2, 40)->toDateTimeImmutable(),
            'eventUid' => Uuid::v4()->toRfc4122(),
            'timezone' => $timezone,
            'rule' => 'BEGIN:VCALENDAR',
        ];
        yield [
            'summary' => 'перенос',
            'calendarId' => Uuid::v4()->toRfc4122(),
            'startDateTime' => $dateEndOfWeek->clone()->subDay()->setTime(4, 20)->toDateTimeImmutable(),
            'endDateTime' => $dateEndOfWeek->clone()->addDays(2)->setTime(5, 30)->toDateTimeImmutable(),
            'eventUid' => Uuid::v4()->toRfc4122(),
            'timezone' => $timezone,
            'rule' => 'BEGIN:VCALENDAR',
        ];

        $dateNextOfWeek = Carbon::now()
            ->setTimezone($timezone)
            ->endOfWeek()
            ->addDays(3)
        ;

        yield [
            'summary' => 'Все неделю',
            'calendarId' => Uuid::v4()->toRfc4122(),
            'startDateTime' => $dateNextOfWeek->clone()->setTime(2, 20)->toDateTimeImmutable(),
            'endDateTime' => $dateNextOfWeek->clone()->addWeeks(1)->setTime(2, 40)->toDateTimeImmutable(),
            'eventUid' => Uuid::v4()->toRfc4122(),
            'timezone' => $timezone,
            'rule' => 'BEGIN:VCALENDAR',
        ];

        yield [
            'summary' => 'Одни день середина',
            'calendarId' => Uuid::v4()->toRfc4122(),
            'startDateTime' => $dateNextOfWeek->clone()->addDays(2)->setTime(2, 20)->toDateTimeImmutable(),
            'endDateTime' => $dateNextOfWeek->clone()->addDays(2)->setTime(2, 40)->toDateTimeImmutable(),
            'eventUid' => Uuid::v4()->toRfc4122(),
            'timezone' => $timezone,
            'rule' => 'BEGIN:VCALENDAR',
        ];
    }
}
