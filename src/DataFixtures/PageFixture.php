<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Doctrine\Models\Meta;
use App\Entity\Page;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Ulid;

class PageFixture extends Fixture
{
    public const ID_FIRST = '01G8JZ0QJTMF8HWC2EBXCZBRRG';
    public const ID_SECOND = '01G80W59K9BYGJMD27SH2B66FX';
    public const ID_THREE = '01G8QDS0XRC2ZMY82PGF0FQH2F';

    public function load(ObjectManager $manager): void
    {
        $parents = [];
        foreach ($this->getData() as $datum) {
            $page = new Page(
                content: $datum['content'] ?? '',
                title: $datum['title'],
            );
            $page->setId(new Ulid($datum['id'] ?? null));
            $page->setMeta(new Meta());
            $parents[$datum['id']] = $page;
            if (isset($datum['parent'])) {
                $page->setParent($parents[$datum['parent']]);
            }
            $page->setPosition($datum['position']);
            $manager->persist($page);
        }
        $manager->flush();
    }

    /**
     * @return iterable<integer ,mixed>
     */
    public function getData(): iterable
    {
        yield [
            'id' => self::ID_THREE,
            'title' => 'Имя существительное',
            'position' => 1,
        ];

        yield [
            'id' => self::ID_FIRST,
            'title' => '1-е склонение имен существительных',
            'content' => file_get_contents(sprintf(__DIR__ . '/page/%s.html', self::ID_FIRST)),
            'parent' => self::ID_THREE,
            'position' => 1,
        ];

        yield [
            'id' => self::ID_SECOND,
            'title' => '2-e слонение имен существительных',
            'content' => file_get_contents(sprintf(__DIR__ . '/page/%s.html', self::ID_SECOND)),
            'parent' => self::ID_THREE,
            'position' => 2,
        ];
    }
}
