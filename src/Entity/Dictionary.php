<?php

declare(strict_types=1);

namespace App\Entity;

use App\Controller\Api\Cp\DictionariesQueryController;
use App\Controller\Api\Cp\DictionaryQueryController;
use App\Doctrine\UlidTrait;
use App\Repository\DictionaryRepository;
use App\Services\Entity\TimestampTrait;
use App\Services\Publication\PublicationTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'dictionaries')]
#[ORM\Entity(repositoryClass: DictionaryRepository::class)]
class Dictionary
{
    use PublicationTrait;
    use TimestampTrait;
    use UlidTrait;

    #[ORM\Column(type: 'string', length: 1000, nullable: false)]
    #[Groups([
        DictionariesQueryController::class,
        DictionaryQueryController::class,
    ])]
    protected string $name;

    #[ORM\Column(type: 'string', length: 1000, nullable: true)]
    #[Groups([
        DictionariesQueryController::class,
        DictionaryQueryController::class,
    ])]
    protected null|string $normalizeName;

    #[ORM\Column(type: 'string', length: 1000, nullable: false)]
    protected string $groupName;

    #[ORM\Column(type: 'text', nullable: false)]
    #[Groups([
        DictionariesQueryController::class,
        DictionaryQueryController::class,
    ])]
    protected string $description;

    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => true])]
    #[Groups([
        DictionariesQueryController::class,
        DictionaryQueryController::class,
    ])]
    protected bool $titlo = false;

    public function __construct(
        string $name,
        string $description,
        bool $isTitlo,
        string $groupName,
    ) {
        $this->createdAt = new \DateTimeImmutable();
        $this->groupName = $groupName;
        $this->name = $name;
        $this->description = $description;
        $this->titlo = $isTitlo;
    }

    public function update(
        string $name,
        string $description,
        bool $isTitlo,
        string $groupName,
    ): void {
        $this->groupName = $groupName;
        $this->name = $name;
        $this->description = $description;
        $this->titlo = $isTitlo;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isTitlo(): bool
    {
        return $this->titlo;
    }

    public function setTitlo(bool $titlo): void
    {
        $this->titlo = $titlo;
    }

    public function getNormalizeName(): null|string
    {
        return $this->normalizeName;
    }

    public function getGroupName(): string
    {
        return $this->groupName;
    }

    public function setNormalizeName(?string $normalizeName): void
    {
        $this->normalizeName = $normalizeName;
    }
}
