<?php

declare(strict_types=1);

namespace App\Entity;

use App\Controller\Api\Cp\MetaOneQueryController;
use App\Controller\Api\Cp\MetaQueryController;
use App\Repository\MetaRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(
    name: 'meta',
)]
#[ORM\Entity(repositoryClass: MetaRepository::class)]
class Meta
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', nullable: false)]
    #[ORM\GeneratedValue('NONE')]
    #[Groups([
        MetaQueryController::class,
        MetaOneQueryController::class,
    ])]
    protected string $key;

    #[ORM\Column(type: 'string', length: 500, nullable: false, options: ['default' => ''])]
    #[Groups([
        MetaQueryController::class,
        MetaOneQueryController::class,
    ])]
    protected string $title;

    #[ORM\Column(type: 'string', length: 1000, nullable: false)]
    #[Groups([
        MetaQueryController::class,
        MetaOneQueryController::class,
    ])]
    protected string $description;

    public function __construct(
        string $key,
        string $title,
        string $description,
    ) {
        $this->key = MetaKeyEnum::from($key)->value;
        $this->title = $title;
        $this->description = $description;
    }

    public static function createEmpty(): self
    {
        return new self('', '', '');
    }

    public function update(
        string $key,
        string $title,
        string $description,
    ): void {
        $this->key = MetaKeyEnum::from($key)->value;
        $this->title = $title;
        $this->description = $description;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
