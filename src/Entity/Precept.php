<?php

declare(strict_types=1);

namespace App\Entity;

use App\Controller\Api\Cp\DescriptionSinQueryController as CpDescriptionSinQueryController;
use App\Controller\Api\Cp\PreceptQueryController;
use App\Controller\Api\Cp\PreceptsQueryController as PreceptsQueryControllerCp;
use App\Controller\Api\DescriptionSinQueryController;
use App\Controller\Api\PreceptsQueryController;
use App\Doctrine\UlidTrait;
use App\Repository\PreceptRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'precepts')]
#[ORM\Entity(repositoryClass: PreceptRepository::class, readOnly: false)]
class Precept
{
    use UlidTrait;

    #[ORM\Column(type: 'string', length: 1000, nullable: false)]
    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        PreceptsQueryControllerCp::class,
        PreceptQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected string $name;

    #[ORM\Column(type: 'string', length: 1000, nullable: false)]
    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        PreceptsQueryControllerCp::class,
        PreceptQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected string $nameSlavonic;

    #[ORM\Column(type: 'string', length: 500, nullable: false)]
    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        PreceptsQueryControllerCp::class,
        PreceptQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected string $link;

    #[ORM\Column(type: 'string', length: 15, nullable: false)]
    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        PreceptsQueryControllerCp::class,
        PreceptQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected string $verse;

    #[ORM\Column(type: 'integer')]
    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        PreceptsQueryControllerCp::class,
        PreceptQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected int $position;

    /**
     * @var Collection<integer, Sin>|Sin[]
     */
    #[ORM\OneToMany(mappedBy: 'precept', targetEntity: Sin::class)]
    #[ORM\OrderBy(['position' => 'asc'])]
    #[Groups([
        PreceptsQueryController::class,
    ])]
    protected Collection $sins;

    public function __construct(
        string $name,
        string $nameSlavonic,
        string $link,
        string $verse,
        int $position,
    ) {
        $this->name = $name;
        $this->nameSlavonic = $nameSlavonic;
        $this->link = $link;
        $this->verse = $verse;
        $this->position = $position;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function getVerse(): string
    {
        return $this->verse;
    }

    public function setVerse(string $verse): void
    {
        $this->verse = $verse;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function addSin(Sin $sin): void
    {
        if (!$this->sins->contains($sin)) {
            $this->sins[] = $sin;
            $sin->setPrecept($this);
        }
    }

    /**
     * @return ArrayCollection|Collection<integer, Sin>
     */
    public function getSins(): ArrayCollection|Collection
    {
        return $this->sins;
    }

    public function update(
        string $link,
        string $verse,
    ): void {
        $this->link = $link;
        $this->verse = $verse;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    public function getNameSlavonic(): ?string
    {
        return $this->nameSlavonic;
    }
}
