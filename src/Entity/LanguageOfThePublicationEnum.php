<?php

declare(strict_types=1);

namespace App\Entity;

enum LanguageOfThePublicationEnum: string
{
    case RUSSIAN = 'russian';
    case CHURCH_SLAVIC = 'church_slavic';

    /**
     * @return array<string, string>
     */
    public static function getChoices(): array
    {
        return [
            self::RUSSIAN->value => self::RUSSIAN->value,
            self::CHURCH_SLAVIC->value => self::CHURCH_SLAVIC->value,
        ];
    }
}
