<?php

declare(strict_types=1);

namespace App\Entity;

use App\Controller\Api\Cp\FeedbacksQueryController;
use App\Doctrine\UlidTrait;
use App\Repository\FeedbackRepository;
use App\Services\Entity\TimestampTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'feedbacks')]
#[ORM\Entity(repositoryClass: FeedbackRepository::class)]
class Feedback
{
    use TimestampTrait;
    use UlidTrait;

    #[ORM\Column(type: 'string', nullable: false, length: 50)]
    #[Groups([
        FeedbacksQueryController::class,
    ])]
    protected string $firstName;

    #[ORM\Column(type: 'string', nullable: true, length: 20)]
    #[Groups([
        FeedbacksQueryController::class,
    ])]
    protected ?string $phone = null;

    #[ORM\Column(type: 'string', nullable: false, length: 50)]
    #[Groups([
        FeedbacksQueryController::class,
    ])]
    protected string $email;

    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups([
        FeedbacksQueryController::class,
    ])]
    protected string $theme;

    #[ORM\Column(type: 'text', nullable: false)]
    #[Groups([
        FeedbacksQueryController::class,
    ])]
    protected string $message;

    #[ORM\Column(name: 'client_ip', type: 'string', nullable: false, length: 15)]
    #[Groups([
        FeedbacksQueryController::class,
    ])]
    protected string $clientIp;

    public function __construct(
        string $firstName,
        string $email,
        string $theme,
        string $message,
    ) {
        $this->createdAt = new \DateTimeImmutable();
        $this
            ->setEmail($email)
            ->setFirstName($firstName)
            ->setTheme($theme)
            ->setMessage($message)
        ;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getClientIp(): string
    {
        return $this->clientIp;
    }

    public function setClientIp(string $clientIp): self
    {
        $this->clientIp = $clientIp;

        return $this;
    }

    public function getTheme(): string
    {
        return $this->theme;
    }

    public function setTheme(string $theme): self
    {
        $this->theme = $theme;

        return $this;
    }
}
