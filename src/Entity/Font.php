<?php

declare(strict_types=1);

namespace App\Entity;

use App\Doctrine\UlidTrait;
use App\Repository\FontRepository;
use App\Services\Entity\TimestampTrait;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'fonts')]
#[ORM\Entity(repositoryClass: FontRepository::class)]
class Font
{
    use TimestampTrait;
    use UlidTrait;
    public const CODE_PONOMAR_UNICODE = 'PONOMAR_UNICODE';
    public const CODE_HIRMOS_IE_UCS = 'HIRMOS_IE_UCS';

    #[ORM\Column(type: 'string', nullable: false, length: 50, unique: true)]
    protected string $code;

    #[ORM\Column(type: 'string', nullable: false, length: 200)]
    protected string $name;

    public function __construct(
        string $code,
        string $name,
    ) {
        $this->code = $code;
        $this->name = $name;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
