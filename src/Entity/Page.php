<?php

declare(strict_types=1);

namespace App\Entity;

use App\Controller\Api\Cp\PageQueryController;
use App\Controller\Api\Cp\PagesQueryController;
use App\Controller\Api\Cp\PagesRootTreeController;
use App\Doctrine\UlidTrait;
use App\Repository\PageRepository;
use App\Services\Entity\TimestampTrait;
use App\Services\IdInterface;
use App\Services\Meta\MetaInterface;
use App\Services\Meta\MetaTrait;
use App\Services\Publication\PublicationInterface;
use App\Services\Publication\PublicationTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table('page')]
#[ORM\Entity(repositoryClass: PageRepository::class)]
class Page implements IdInterface, PublicationInterface, MetaInterface
{
    use MetaTrait;
    use PublicationTrait;
    use TimestampTrait;
    use UlidTrait;

    #[ORM\Column(type: 'string', length: 500, nullable: false, options: ['default' => ''])]
    #[Groups([
        PageQueryController::class,
        PagesQueryController::class,
        PagesRootTreeController::class,
    ])]
    protected string $title = '';

    #[ORM\Column(type: 'text', nullable: false)]
    #[Groups([
        PageQueryController::class,
        PagesQueryController::class,
    ])]
    protected string $content;

    #[ORM\Column(type: 'integer', nullable: false, options: ['default' => 0])]
    #[Groups([
        PageQueryController::class,
        PagesQueryController::class,
        PagesRootTreeController::class,
    ])]
    protected int $deep = 0;

    #[ORM\Column(type: 'integer', nullable: false, options: ['default' => 0])]
    #[Groups([
        PageQueryController::class,
        PagesQueryController::class,
        PagesRootTreeController::class,
    ])]
    protected int $position = 0;

    #[ORM\ManyToOne(inversedBy: 'children', targetEntity: self::class, )]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    #[Groups([
        PageQueryController::class,
    ])]
    protected null|Page $parent = null;

    /** @var Collection<integer, Page> */
    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class, )]
    #[ORM\OrderBy(['position' => 'ASC'])]
    #[Groups([
        PagesRootTreeController::class,
    ])]
    protected Collection $children;

    public function __construct(
        string $content,
        string $title,
        self $parent = null,
        int $position = 0,
    ) {
        $this->content = $content;
        $this->title = $title;
        $this->createdAt = new \DateTimeImmutable();
        $this->parent = $parent;
        $this->children = new ArrayCollection();
        $this->position = $position;
    }

    public function update(
        string $content,
        string $title,
        self $parent = null,
    ): void {
        $this->content = $content;
        $this->title = $title;
        $this->setParent($parent);
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getDeep(): int
    {
        return $this->deep;
    }

    /**
     * @return Collection<integer, Page>
     */
    public function getChildren(): ArrayCollection|Collection
    {
        return $this->children;
    }

    public function setParent(null|Page $parent): void
    {
        $this->parent = $parent;
        if (null === $parent) {
            $this->deep = 0;
        } else {
            $this->deep = $parent->getDeep() + 1;
        }
    }

    public function getParent(): null|Page
    {
        return $this->parent;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): void
    {
        $this->position = $position;
    }
}
