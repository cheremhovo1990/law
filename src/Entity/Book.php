<?php

declare(strict_types=1);

namespace App\Entity;

use App\Controller\Api\Book\BookQueryController;
use App\Controller\Api\Book\BooksQueryController;
use App\Controller\Api\Cp\BookQueryController as BookQueryControllerCp;
use App\Controller\Api\Cp\BooksQueryController as BooksQueryControllerCp;
use App\Doctrine\Collections\BookLinkCollection;
use App\Doctrine\Collections\TextCollection;
use App\Doctrine\Models\BookLink;
use App\Doctrine\Models\Image as ImageModel;
use App\Doctrine\Types\BookLinks;
use App\Doctrine\Types\Texts;
use App\Doctrine\UlidTrait;
use App\Repository\BookRepository;
use App\Services\Entity\TimestampTrait;
use App\Services\IdInterface;
use App\Services\Meta\MetaInterface;
use App\Services\Meta\MetaTrait;
use App\Services\Publication\PublicationInterface;
use App\Services\Publication\PublicationTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'books')]
#[ORM\Entity(repositoryClass: BookRepository::class)]
class Book implements IdInterface, PublicationInterface, MetaInterface
{
    use MetaTrait;
    use PublicationTrait;
    use TimestampTrait;
    use UlidTrait;

    #[ORM\Column(type: 'string', length: 500, nullable: false)]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected string $name;

    #[ORM\Column(type: BookLinks::NAME, length: 1000, nullable: false)]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected BookLinkCollection $links;

    #[ORM\Column(type: 'text', nullable: false, options: ['default' => ''])]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected string $description = '';

    #[ORM\Column(type: \App\Doctrine\Types\Image::NAME, nullable: false, options: ['jsonb' => true])]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected ImageModel $coverImage;

    /**
     * ISBN.
     */
    #[ORM\Column(type: Texts::NAME, nullable: true)]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected TextCollection $isbnCollection;

    #[ORM\Column(name: 'date_of_writing', type: 'date', nullable: true)]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected ?\DateTimeInterface $dateOfWriting = null;

    #[ORM\Column(name: 'date_of_published', type: 'date', nullable: true)]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    private ?\DateTimeInterface $dateOfPublished = null;

    #[ORM\Column(name: 'page_size', type: 'integer', nullable: false)]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected int $pageSize;

    /**
     * По благословению Святейшего Патриарха Московского и всея Руси Кирилла.
     */
    #[ORM\Column(
        name: 'blessed_by_patriach_kirill',
        type: 'boolean',
        nullable: false,
        options: ['default' => false],
    )]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected bool $blessedByPatriarchKirill = false;

    /**
     * Рекомендовано к публикации Издательским советом Русской Православной Церкви.
     */
    #[ORM\Column(
        name: 'recommended_by_publishing_council_roc',
        type: 'boolean',
        nullable: false,
        options: ['default' => false],
    )]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected bool $recommendedByPublishingCouncilRoc = false;

    /**
     * Рекомендовано к публикации Издательским советом Белорусской Православной Церкви.
     */
    #[ORM\Column(
        name: 'recommended_by_publishing_council_boc',
        type: 'boolean',
        nullable: false,
        options: ['default' => false],
    )]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected bool $recommendedByPublishingCouncilBoc = false;
    /**
     * Код Издательского Совета.
     */
    #[ORM\Column(
        name: 'code_of_publishing_council',
        type: 'string',
        length: 100,
        nullable: true,
    )]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected ?string $codeOfPublishingCouncil = null;

    /**
     * Язык издания.
     */
    #[ORM\Column(
        name: 'language_of_publication',
        type: 'string',
        length: 100,
        nullable: false,
        options: ['check' => "CHECK(language_of_publication IN ('russian'::text, 'church_slavic'::text))"],
    )]
    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    protected string $languageOfThePublication;

    public function __construct(
        string $name,
        string $description,
        int $pageSize,
        string $languageOfThePublication,
        \DateTimeInterface $dateOfWriting = null,
        \DateTimeInterface $dateOfPublished = null,
        string $codeOfPublishingCouncil = null,
        bool $blessedByPatriarchKirill = false,
        bool $recommendedByPublishingCouncilRoc = false,
        bool $recommendedByPublishingCouncilBoc = false,
    ) {
        $this->name = $name;
        $this->languageOfThePublication = LanguageOfThePublicationEnum::from($languageOfThePublication)->value;
        $this->description = $description;
        $this->dateOfWriting = $dateOfWriting;
        $this->pageSize = $pageSize;
        $this->createdAt = new \DateTimeImmutable();
        $this->codeOfPublishingCouncil = $codeOfPublishingCouncil;
        $this->blessedByPatriarchKirill = $blessedByPatriarchKirill;
        $this->recommendedByPublishingCouncilRoc = $recommendedByPublishingCouncilRoc;
        $this->recommendedByPublishingCouncilBoc = $recommendedByPublishingCouncilBoc;
        $this->dateOfPublished = $dateOfPublished;
    }

    public function update(
        string $name,
        string $description,
        int $pageSize,
        string $languageOfThePublication,
        \DateTimeInterface $dateOfWriting = null,
        \DateTimeInterface $dateOfPublished = null,
        string $codeOfPublishingCouncil = null,
        bool $blessedByPatriarchKirill = false,
        bool $recommendedByPublishingCouncilRoc = false,
        bool $recommendedByPublishingCouncilBoc = false,
    ): void {
        $this->languageOfThePublication = LanguageOfThePublicationEnum::from($languageOfThePublication)->value;
        $this->name = $name;
        $this->description = $description;
        $this->dateOfWriting = $dateOfWriting;
        $this->dateOfPublished = $dateOfPublished;
        $this->pageSize = $pageSize;
        $this->codeOfPublishingCouncil = $codeOfPublishingCouncil;
        $this->blessedByPatriarchKirill = $blessedByPatriarchKirill;
        $this->recommendedByPublishingCouncilRoc = $recommendedByPublishingCouncilRoc;
        $this->recommendedByPublishingCouncilBoc = $recommendedByPublishingCouncilBoc;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCoverImage(): ImageModel
    {
        return $this->coverImage;
    }

    public function setCoverImage(ImageModel $coverImage): void
    {
        $this->coverImage = $coverImage;
    }

    public function getIsbnCollection(): TextCollection
    {
        return $this->isbnCollection;
    }

    public function setIsbnCollection(TextCollection $isbnCollection): void
    {
        $this->isbnCollection = $isbnCollection;
    }

    public function getDateOfWriting(): ?\DateTimeInterface
    {
        return $this->dateOfWriting;
    }

    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    public function isRecommendedByPublishingCouncilRoc(): bool
    {
        return $this->recommendedByPublishingCouncilRoc;
    }

    public function isBlessedByPatriarchKirill(): bool
    {
        return $this->blessedByPatriarchKirill;
    }

    public function getCodeOfPublishingCouncil(): null|string
    {
        return $this->codeOfPublishingCouncil;
    }

    public function getDateOfPublished(): ?\DateTimeInterface
    {
        return $this->dateOfPublished;
    }

    #[Groups([
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        BookQueryController::class,
    ])]
    public function getFirstLinkBuy(): string
    {
        /** @var BookLink $link */
        $link = $this->links->filter(function (BookLink $link) {
            if ($link->isTypeBuy()) {
                return true;
            }

            return false;
        })->first();

        return $link->getLink();
    }

    public function getLinks(): BookLinkCollection
    {
        return $this->links;
    }

    public function setLinks(BookLinkCollection $links): void
    {
        $this->links = $links;
    }

    public function isRecommendedByPublishingCouncilBoc(): bool
    {
        return $this->recommendedByPublishingCouncilBoc;
    }

    public function getLanguageOfThePublication(): string
    {
        return $this->languageOfThePublication;
    }
}
