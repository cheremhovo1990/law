<?php

declare(strict_types=1);

namespace App\Entity;

use App\Controller\Api\Cp\DescriptionSinQueryController as CpDescriptionSinQueryController;
use App\Controller\Api\Cp\DescriptionSinsQueryController;
use App\Controller\Api\DescriptionSinQueryController;
use App\Controller\Api\PreceptsQueryController;
use App\Doctrine\UlidTrait;
use App\Repository\DescriptionSinRepository;
use App\Services\Entity\TimestampTrait;
use App\Services\Meta\MetaInterface;
use App\Services\Meta\MetaTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'description_sins')]
#[ORM\Entity(repositoryClass: DescriptionSinRepository::class)]
class DescriptionSin implements MetaInterface
{
    use MetaTrait;
    use TimestampTrait;
    use UlidTrait;

    #[ORM\Column(type: 'string', length: 500, nullable: false)]
    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        DescriptionSinsQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected string $name;

    #[ORM\Column(type: 'text', nullable: false)]
    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        DescriptionSinsQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected string $text;

    /** @var Collection<integer, Sin>|ArrayCollection|Sin[] */
    #[ORM\OneToMany(mappedBy: 'descriptionSin', targetEntity: Sin::class)]
    #[Groups([
        DescriptionSinQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected Collection $sins;

    #[ORM\ManyToOne(targetEntity: Precept::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([
        DescriptionSinQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected Precept $precept;

    public function __construct(
        string $name,
        string $text,
    ) {
        $this->name = $name;
        $this->text = $text;
        $this->createdAt = new \DateTimeImmutable();
        $this->sins = new ArrayCollection();
    }

    public function update(
        string $name,
        string $text,
        Precept $precept,
    ): void {
        $this->name = $name;
        $this->text = $text;
        $this->precept = $precept;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getPrecept(): Precept
    {
        return $this->precept;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Collection<integer, Sin>|Sin[]
     */
    public function getSins(): Collection
    {
        return $this->sins;
    }

    public function setPrecept(Precept $precept): void
    {
        $this->precept = $precept;
    }
}
