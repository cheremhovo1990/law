<?php

declare(strict_types=1);

namespace App\Entity;

use App\Controller\Api\Cp\MusicOneQueryController;
use App\Controller\Api\Cp\MusicQueryController;
use App\Doctrine\Models\Meta;
use App\Doctrine\UlidTrait;
use App\Repository\MusicRepository;
use App\Services\Entity\TimestampTrait;
use App\Services\Meta\MetaInterface;
use App\Services\Meta\MetaTrait;
use App\Services\Publication\PublicationInterface;
use App\Services\Publication\PublicationTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'music')]
#[ORM\Entity(repositoryClass: MusicRepository::class)]
class Music implements PublicationInterface, MetaInterface
{
    use MetaTrait;
    use PublicationTrait;
    use TimestampTrait;
    use UlidTrait;

    #[ORM\Column(type: 'string', length: 500, nullable: false)]
    #[Groups([
        MusicQueryController::class,
        MusicOneQueryController::class,
    ])]
    protected string $title;

    #[ORM\Column(type: 'string', length: 500, nullable: false)]
    #[Groups([
        MusicQueryController::class,
        MusicOneQueryController::class,
    ])]
    protected string $link;

    #[ORM\Column(type: 'text', nullable: false)]
    #[Groups([
        MusicQueryController::class,
        MusicOneQueryController::class,
    ])]
    protected string $description;

    public function __construct(
        string $title,
        string $link,
        string $description,
    ) {
        $this->title = $title;
        $this->link = $link;
        $this->description = $description;
        $this->meta = new Meta();
        $this->createdAt = new \DateTimeImmutable();
    }

    public function update(
        string $title,
        string $link,
        string $description,
    ): void {
        $this->title = $title;
        $this->link = $link;
        $this->description = $description;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
