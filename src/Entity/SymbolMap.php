<?php

declare(strict_types=1);

namespace App\Entity;

use App\Controller\Api\Cp\SymbolsQueryController;
use App\Doctrine\UlidTrait;
use App\Helpers\SymbolHelper;
use App\Repository\SymbolMapRepository;
use App\Services\Entity\TimestampTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'symbol_maps')]
#[ORM\Entity(repositoryClass: SymbolMapRepository::class)]
class SymbolMap
{
    use TimestampTrait;
    use UlidTrait;

    #[ORM\Column(type: 'string', length: 30, nullable: false)]
    #[Groups([
        SymbolsQueryController::class,
    ])]
    protected string $unicode;

    #[ORM\Column(type: 'string', length: 30, nullable: false)]
    #[Groups([
        SymbolsQueryController::class,
    ])]
    protected string $character;

    #[ORM\Column(type: 'string', length: 30, nullable: true)]
    protected null|string $groupName = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    protected null|int $groupNamePosition = null;

    #[ORM\ManyToOne(targetEntity: Font::class)]
    #[ORM\JoinColumn(nullable: false, onDelete: 'RESTRICT')]
    protected Font $font;

    #[ORM\ManyToOne(targetEntity: Symbol::class, inversedBy: 'symbolMaps')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'RESTRICT')]
    protected Symbol $symbol;

    #[ORM\Column(type: 'integer', nullable: false)]
    protected int $countUnicode;

    public function __construct(
        string $unicode,
        Font $font,
        Symbol $symbol,
        string $groupName = null,
        int $groupNamePosition = null,
    ) {
        $this->unicode = $unicode;
        $this->character = SymbolHelper::convertToCharacter($unicode);
        $this->countUnicode = mb_substr_count($unicode, ';');
        $this->font = $font;
        $this->symbol = $symbol;
        $this->groupName = $groupName;
        $this->groupNamePosition = $groupNamePosition;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getUnicode(): string
    {
        return $this->unicode;
    }

    #[Groups([
        SymbolsQueryController::class,
    ])]
    public function getSymbolCode(): string
    {
        return $this->symbol->getCode();
    }

    public function getSymbol(): Symbol
    {
        return $this->symbol;
    }

    public function getFont(): Font
    {
        return $this->font;
    }

    public function getCharacter(): string
    {
        return $this->character;
    }

    public function getGroupName(): ?string
    {
        return $this->groupName;
    }
}
