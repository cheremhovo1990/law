<?php

declare(strict_types=1);

namespace App\Entity;

use App\Doctrine\UlidTrait;
use App\Repository\SymbolRepository;
use App\Services\Entity\TimestampTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'symbols')]
#[ORM\Entity(repositoryClass: SymbolRepository::class)]
class Symbol
{
    use TimestampTrait;
    use UlidTrait;

    #[ORM\Column(type: 'string', nullable: false, length: 100, unique: true)]
    protected string $code;

    #[ORM\Column(type: 'string', nullable: false, length: 200)]
    protected string $name;

    /**
     * @var Collection<integer, SymbolMap>
     */
    #[ORM\OneToMany(mappedBy: 'symbol', targetEntity: SymbolMap::class)]
    protected Collection $symbolMaps;

    public function __construct(
        string $code,
        string $name,
    ) {
        $this->code = $code;
        $this->name = $name;
        $this->createdAt = new \DateTimeImmutable();
        $this->symbolMaps = new ArrayCollection();
    }

    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return Collection<integer, SymbolMap>
     */
    public function getSymbolMaps(): Collection
    {
        return $this->symbolMaps;
    }
}
