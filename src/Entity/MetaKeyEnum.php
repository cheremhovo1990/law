<?php

declare(strict_types=1);

namespace App\Entity;

enum MetaKeyEnum: string
{
    case EMPTY = '';
    case MAIN = 'main';
    case BOOKS = 'books';

    /**
     * @return array<string, string>
     */
    public static function getDropDown(): array
    {
        return [
            self::MAIN->value => self::MAIN->value,
            self::BOOKS->value => self::BOOKS->value,
        ];
    }

    /**
     * @return array<string, string>
     */
    public static function getChoices(): array
    {
        return [
            self::MAIN->value => self::MAIN->value,
            self::BOOKS->value => self::BOOKS->value,
        ];
    }
}
