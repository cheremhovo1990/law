<?php

declare(strict_types=1);

namespace App\Entity;

use App\Doctrine\Collections\ImagePivotCollection;
use App\Doctrine\Types\ImagePivots;
use App\Doctrine\UlidTrait;
use App\Repository\ImageRepository;
use App\Services\Entity\TimestampTrait;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(
    'images',
)]
#[ORM\Entity(
    repositoryClass: ImageRepository::class,
)]
class Image
{
    use TimestampTrait;
    use UlidTrait;

    #[ORM\Column(type: 'string', nullable: false, length: 1000)]
    protected string $name;

    #[ORM\Column(type: ImagePivots::NAME, nullable: true, options: ['jsonb' => true])]
    private ImagePivotCollection $pivots;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->createdAt = new \DateTimeImmutable();
        $this->pivots = new ImagePivotCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPivots(): ImagePivotCollection
    {
        return $this->pivots;
    }

    public function setPivots(ImagePivotCollection $pivots): self
    {
        $this->pivots = $pivots;

        return $this;
    }
}
