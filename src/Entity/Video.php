<?php

declare(strict_types=1);

namespace App\Entity;

use App\Controller\Api\Cp\VideoOneQueryController;
use App\Controller\Api\Cp\VideoQueryController;
use App\Doctrine\Models\Meta;
use App\Doctrine\UlidTrait;
use App\Repository\VideoRepository;
use App\Services\Entity\TimestampTrait;
use App\Services\Meta\MetaInterface;
use App\Services\Meta\MetaTrait;
use App\Services\Publication\PublicationInterface;
use App\Services\Publication\PublicationTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'video')]
#[ORM\Entity(repositoryClass: VideoRepository::class)]
class Video implements PublicationInterface, MetaInterface
{
    use MetaTrait;
    use PublicationTrait;
    use TimestampTrait;
    use UlidTrait;

    #[ORM\Column(type: 'string', length: 500, nullable: false)]
    #[Groups([
        VideoQueryController::class,
        VideoOneQueryController::class,
    ])]
    protected string $title;

    #[ORM\Column(type: 'string', length: 500, nullable: false)]
    #[Groups([
        VideoQueryController::class,
        VideoOneQueryController::class,
    ])]
    protected string $link;

    #[ORM\Column(type: 'text', nullable: false)]
    #[Groups([
        VideoQueryController::class,
        VideoOneQueryController::class,
    ])]
    protected string $description;

    public function __construct(
        string $title,
        string $link,
        string $description,
    ) {
        $this->title = $title;
        $this->link = $link;
        $this->description = $description;
        $this->createdAt = new \DateTimeImmutable();
        $this->meta = new Meta();
    }

    public function update(
        string $title,
        string $link,
        string $description,
    ): void {
        $this->title = $title;
        $this->link = $link;
        $this->description = $description;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
