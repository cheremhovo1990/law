<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ViewFromSymbolToSymbolRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'view_from_symbol_to_symbol')]
#[ORM\Entity(repositoryClass: ViewFromSymbolToSymbolRepository::class, readOnly: true)]
class ViewFromSymbolToSymbol
{
    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    protected string $id;
    #[ORM\Column(type: 'string')]
    protected string $fontFromCode;
    #[ORM\Column(type: 'string')]
    protected string $fontToCode;
    #[ORM\Column(type: 'string')]
    protected string $unicodeFrom;
    #[ORM\Column(type: 'string')]
    protected string $unicodeTo;
    #[ORM\Column(type: 'string')]
    protected string $characterFrom;
    #[ORM\Column(type: 'string')]
    protected string $characterTo;
    #[ORM\Column(type: 'integer')]
    protected int $countUnicode;

    public function getUnicodeFrom(): string
    {
        return $this->unicodeFrom;
    }

    public function getUnicodeTo(): string
    {
        return $this->unicodeTo;
    }

    public function getCharacterFrom(): string
    {
        return $this->characterFrom;
    }

    public function getCharacterTo(): string
    {
        return $this->characterTo;
    }
}
