<?php

declare(strict_types=1);

namespace App\Entity;

use App\Controller\Api\Cp\DescriptionSinQueryController as CpDescriptionSinQueryController;
use App\Controller\Api\Cp\SinQueryController;
use App\Controller\Api\Cp\SinsQueryController;
use App\Controller\Api\DescriptionSinQueryController;
use App\Controller\Api\PreceptsQueryController;
use App\Doctrine\UlidTrait;
use App\Repository\SinRepository;
use App\Services\Entity\TimestampTrait;
use App\Services\Publication\PublicationInterface;
use App\Services\Publication\PublicationTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'sins')]
#[ORM\Entity(repositoryClass: SinRepository::class)]
class Sin implements PublicationInterface
{
    use PublicationTrait;
    use TimestampTrait;
    use UlidTrait;

    #[ORM\Column(type: 'string', length: 500, nullable: false)]
    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        SinsQueryController::class,
        SinQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected string $name;

    #[ORM\Column(type: 'string', length: 500, nullable: true)]
    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        SinsQueryController::class,
        SinQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected null|string $nameSlavonic;

    #[ORM\Column(type: 'string', length: 1000, nullable: false, options: ['default' => ''])]
    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        SinsQueryController::class,
        SinQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected string $popoverContent = '';

    #[ORM\Column(type: 'integer', nullable: false)]
    #[Groups([
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        SinsQueryController::class,
        SinQueryController::class,
        CpDescriptionSinQueryController::class,
    ])]
    protected int $position;

    #[ORM\ManyToOne(targetEntity: Precept::class, inversedBy: 'sins')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([
        SinsQueryController::class,
        SinQueryController::class,
    ])]
    protected Precept $precept;

    #[ORM\ManyToOne(targetEntity: DescriptionSin::class, inversedBy: 'sins')]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    #[Groups([
        PreceptsQueryController::class,
        SinQueryController::class,
    ])]
    protected ?DescriptionSin $descriptionSin;

    public function __construct(
        string $name,
        Precept $precept,
        int $position,
        string $popoverContent,
        null|string $nameSlavonic = null,
    ) {
        $this->name = $name;
        $this->precept = $precept;
        $this->position = $position;
        $this->createdAt = new \DateTimeImmutable();
        $this->popoverContent = $popoverContent;
        $this->nameSlavonic = $nameSlavonic;
    }

    public function update(
        string $name,
        Precept $precept,
        int $position,
        string $popoverContent,
        null|string $nameSlavonic = null,
    ): void {
        $this->name = $name;
        $this->precept = $precept;
        $this->position = $position;
        $this->popoverContent = $popoverContent;
        $this->nameSlavonic = $nameSlavonic;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function getPrecept(): Precept
    {
        return $this->precept;
    }

    public function getPopoverContent(): string
    {
        return $this->popoverContent;
    }

    public function getDescriptionSin(): ?DescriptionSin
    {
        return $this->descriptionSin;
    }

    public function setDescriptionSin(?DescriptionSin $descriptionSin): void
    {
        $this->descriptionSin = $descriptionSin;
    }

    public function setPrecept(Precept $precept): void
    {
        $this->precept = $precept;
    }

    public function getNameSlavonic(): ?string
    {
        return $this->nameSlavonic;
    }
}
