<?php

declare(strict_types=1);

namespace App\Entity;

use App\Controller\Api\Cp\ScheduleMonthController;
use App\Doctrine\Types\DateTimeZone;
use App\Doctrine\UlidTrait;
use App\Repository\ScheduleEventRepository;
use App\Services\Entity\TimestampTrait;
use Carbon\CarbonImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(
    name: 'schedule_events',
)]
#[ORM\Entity(
    repositoryClass: ScheduleEventRepository::class,
)]
class ScheduleEvent
{
    use TimestampTrait;
    use UlidTrait;

    #[ORM\Column(name: 'calendar_id', type: 'string', length: 100, nullable: false)]
    protected string $calendarId;

    #[ORM\Column(name: 'event_uid', type: 'string', length: 100, nullable: false)]
    protected string $eventUid;

    #[ORM\Column(name: 'summary', type: 'string', length: 250, nullable: false)]
    protected string $summary;

    #[ORM\Column(name: 'description', type: 'string', length: 250, nullable: true)]
    protected null|string $description = null;

    #[ORM\Column(name: 'start_date_time', type: 'datetimetz_immutable', nullable: false)]
    protected \DateTimeImmutable $startDateTime;

    #[ORM\Column(name: 'end_date_time', type: 'datetimetz_immutable', nullable: false)]
    #[Groups([
        ScheduleMonthController::class,
    ])]
    protected \DateTimeImmutable $endDateTime;

    #[ORM\Column(name: 'timezone', type: DateTimeZone::NAME, length: 50, nullable: false)]
    protected \DateTimeZone $timezone;

    #[ORM\Column(name: 'rule', type: 'string', length: 250, nullable: true)]
    protected null|string $rule = null;

    #[ORM\Column(name: 'raw', type: 'text', length: 250, nullable: true)]
    protected null|string $raw = null;

    public function __construct(
        string $calendarId,
        string $eventUid,
        \DateTimeImmutable $startDateTime,
        \DateTimeImmutable $endDateTime,
        string $summary,
        null|string $description = null,
        null|string $rule = null,
    ) {
        $this->timezone = $startDateTime->getTimezone();
        $this->calendarId = $calendarId;
        $this->eventUid = $eventUid;
        $this->startDateTime = $startDateTime->setTimezone(new \DateTimeZone('UTC'));
        $this->endDateTime = $endDateTime->setTimezone(new \DateTimeZone('UTC'));
        $this->summary = $summary;
        $this->description = $description;
        $this->createdAt = new \DateTimeImmutable();

        $this->rule = $rule;
    }

    public function startBetween(\DateTimeImmutable $start, \DateTimeImmutable $end): bool
    {
        return $this->getCarbonStartDateTime()->greaterThanOrEqualTo($start) && $this->getCarbonStartDateTime()->lessThanOrEqualTo($end);
    }

    public function setRule(null|string $rule): self
    {
        $this->rule = $rule;

        return $this;
    }

    public function setRaw(null|string $raw): self
    {
        $this->raw = $raw;

        return $this;
    }

    public function getCarbonEndDateTime(): CarbonImmutable
    {
        return new CarbonImmutable($this->endDateTime);
    }

    public function getCarbonStartDateTime(): CarbonImmutable
    {
        return new CarbonImmutable($this->startDateTime);
    }

    public function getEndDateTime(): \DateTimeImmutable
    {
        return $this->endDateTime;
    }

    public function getStartDateTime(): \DateTimeImmutable
    {
        return $this->startDateTime;
    }

    public function getSummary(): string
    {
        return $this->summary;
    }
}
