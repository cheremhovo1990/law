<?php

declare(strict_types=1);

namespace App\Controller\Cp;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    public const NAME = 'cp.main';

    #[Route('/cp', name: self::NAME)]
    public function __invoke(): Response
    {
        return $this->render('cp/main.html.twig');
    }
}
