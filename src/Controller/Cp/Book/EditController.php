<?php

declare(strict_types=1);

namespace App\Controller\Cp\Book;

use App\Repository\BookRepository;
use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    public const NAME = 'cp.book.edit';
    private BookRepository $bookRepository;
    private CommonService $commonService;

    public function __construct(
        BookRepository $bookRepository,
        CommonService $commonService,
    ) {
        $this->bookRepository = $bookRepository;
        $this->commonService = $commonService;
    }

    #[Route('/cp/book/{id}/edit', name: self::NAME, options: ['expose' => true])]
    public function __invoke(string $id): Response
    {
        $book = $this->bookRepository->find($id);
        $this->commonService->createNotFoundException($book);

        return $this->render('cp/book/edit.html.twig', [
            'bookId' => $id,
        ]);
    }
}
