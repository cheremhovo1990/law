<?php

declare(strict_types=1);

namespace App\Controller\Cp\Book;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    public const NAME = 'cp.books';

    #[Route('/cp/books', name: self::NAME)]
    public function __invoke(): Response
    {
        return $this->render('cp/book/books.html.twig');
    }
}
