<?php

declare(strict_types=1);

namespace App\Controller\Cp\Book;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    public const NAME = 'cp.book.create';

    #[Route('/cp/book/create', name: self::NAME)]
    public function __invoke(Request $request): Response
    {
        return $this->render(
            'cp/book/create.html.twig',
        );
    }
}
