<?php

declare(strict_types=1);

namespace App\Controller\Cp\Schedule;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MonthController extends AbstractController
{
    public const NAME = 'cp.schedule.month';

    #[Route('/cp/schedule/month', name: self::NAME, options: ['expose' => true])]
    public function __invoke(): Response
    {
        return $this->render('cp/schedule/month.html.twig');
    }
}
