<?php

declare(strict_types=1);

namespace App\Controller\Cp\Sin;

use App\Repository\SinRepository;
use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    public const NAME = 'cp.sin.edit';
    private CommonService $commonService;
    private SinRepository $sinRepository;

    public function __construct(
        CommonService $commonService,
        SinRepository $sinRepository,
    ) {
        $this->commonService = $commonService;
        $this->sinRepository = $sinRepository;
    }

    #[Route('/cp/precept/{preceptId}/sin/{id}/edit', name: self::NAME, options: ['expose' => true])]
    public function __invoke(string $id): Response
    {
        $sin = $this->sinRepository->find($id);
        $this->commonService->createNotFoundException($sin);

        return $this->render('cp/sin/mutation.html.twig', [
            'precept' => $sin->getPrecept(),
        ]);
    }
}
