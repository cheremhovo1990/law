<?php

declare(strict_types=1);

namespace App\Controller\Cp\Sin;

use App\Repository\PreceptRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    public const NAME = 'cp.sin.create';

    private PreceptRepository $preceptRepository;

    public function __construct(
        PreceptRepository $preceptRepository,
    ) {
        $this->preceptRepository = $preceptRepository;
    }

    #[Route('/cp/precept/sin/create', name: self::NAME)]
    public function __invoke(Request $request, ?int $preceptId): Response
    {
        $precept = null;
        if (null !== $preceptId) {
            $precept = $this->preceptRepository->find($request->get('preceptId'));
        }

        return $this->render('cp/sin/mutation.html.twig', [
            'precept' => $precept,
        ]);
    }
}
