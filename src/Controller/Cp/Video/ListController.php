<?php

declare(strict_types=1);

namespace App\Controller\Cp\Video;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    public const NAME = 'cp.video';

    #[Route('/cp/video', name: self::NAME)]
    public function __invoke(): Response
    {
        return $this->render('cp/video/video.html.twig');
    }
}
