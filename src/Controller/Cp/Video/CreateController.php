<?php

declare(strict_types=1);

namespace App\Controller\Cp\Video;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    public const NAME = 'cp.video.create';

    #[Route('/cp/video/create', name: self::NAME)]
    public function __invoke(Request $request): Response
    {
        return $this->render(
            'cp/video/mutation.html.twig',
        );
    }
}
