<?php

declare(strict_types=1);

namespace App\Controller\Cp\Video;

use App\Repository\VideoRepository;
use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    public const NAME = 'cp.video.edit';

    private CommonService $commonService;
    private VideoRepository $videoRepository;

    public function __construct(
        CommonService $commonService,
        VideoRepository $videoRepository,
    ) {
        $this->commonService = $commonService;
        $this->videoRepository = $videoRepository;
    }

    #[Route('/cp/video/{id}/edit', name: self::NAME, options: ['expose' => true])]
    public function __invoke(string $id): Response
    {
        $entity = $this->videoRepository->find($id);
        $this->commonService->createNotFoundException($entity);

        return $this->render(
            'cp/video/mutation.html.twig',
        );
    }
}
