<?php

declare(strict_types=1);

namespace App\Controller\Cp\DescriptionSin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    public const NAME = 'cp.description_sins';

    #[Route('/cp/description/sins', name: self::NAME)]
    public function __invoke(): Response
    {
        return $this->render(
            'cp/descriptionSin/description-sins.html.twig',
        );
    }
}
