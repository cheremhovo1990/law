<?php

declare(strict_types=1);

namespace App\Controller\Cp\DescriptionSin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    public const NAME = 'cp.description_sin.create';

    #[Route('/cp/precept/description/sin/create', name: self::NAME)]
    public function __invoke(Request $request): Response
    {
        return $this->render(
            'cp/descriptionSin/mutation.html.twig',
        );
    }
}
