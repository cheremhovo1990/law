<?php

declare(strict_types=1);

namespace App\Controller\Cp\DescriptionSin;

use App\Repository\DescriptionSinRepository;
use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    public const NAME = 'cp.description.sin.edit';
    private DescriptionSinRepository $descriptionSinRepository;
    private CommonService $commonService;

    public function __construct(
        DescriptionSinRepository $descriptionSinRepository,
        CommonService $commonService,
    ) {
        $this->descriptionSinRepository = $descriptionSinRepository;
        $this->commonService = $commonService;
    }

    #[Route('/cp/description/sin/{id}/edit', name: self::NAME, options: ['expose' => true])]
    public function __invoke(Request $request, string $id): Response
    {
        $descriptionSin = $this->descriptionSinRepository->find($id);
        $this->commonService->createNotFoundException($descriptionSin);

        return $this->render(
            'cp/descriptionSin/mutation.html.twig',
        );
    }
}
