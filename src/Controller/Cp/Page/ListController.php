<?php

declare(strict_types=1);

namespace App\Controller\Cp\Page;

use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    protected CommonService $commonService;

    public function __construct(
        CommonService $commonService,
    ) {
        $this->commonService = $commonService;
    }

    public const NAME = 'cp.pages';

    #[Route('/cp/pages', name: self::NAME)]
    public function __invoke(): Response
    {
        return $this->render('cp/page/pages.html.twig');
    }
}
