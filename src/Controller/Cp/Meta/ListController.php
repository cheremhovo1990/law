<?php

declare(strict_types=1);

namespace App\Controller\Cp\Meta;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    public const NAME = 'cp.meta';

    #[Route('/cp/meta', name: self::NAME)]
    public function __invoke(): Response
    {
        return $this->render('cp/meta/meta.html.twig');
    }
}
