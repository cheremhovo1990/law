<?php

declare(strict_types=1);

namespace App\Controller\Cp\Meta;

use App\Repository\MetaRepository;
use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    public const NAME = 'cp.meta.edit';

    private CommonService $commonService;
    private MetaRepository $metaRepository;

    public function __construct(
        CommonService $commonService,
        MetaRepository $metaRepository,
    ) {
        $this->commonService = $commonService;
        $this->metaRepository = $metaRepository;
    }

    #[Route('/cp/meta/{key}/edit', name: self::NAME, options: ['expose' => true])]
    public function __invoke(string $key): Response
    {
        $entity = $this->metaRepository->find($key);
        $this->commonService->createNotFoundException($entity);

        return $this->render('cp/meta/mutation.twig');
    }
}
