<?php

declare(strict_types=1);

namespace App\Controller\Cp\Meta;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    public const NAME = 'cp.meta.create';

    #[Route('/cp/meta/create', name: self::NAME)]
    public function __invoke(): Response
    {
        return $this->render('cp/meta/mutation.twig');
    }
}
