<?php

declare(strict_types=1);

namespace App\Controller\Cp\Music;

use App\Repository\MusicRepository;
use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    public const NAME = 'cp.music.edit';
    private MusicRepository $musicRepository;
    private CommonService $commonService;

    public function __construct(
        MusicRepository $musicRepository,
        CommonService $commonService,
    ) {
        $this->musicRepository = $musicRepository;
        $this->commonService = $commonService;
    }

    #[Route('/cp/music/{id}/edit', name: self::NAME, options: ['expose' => true])]
    public function __invoke(Request $request, string $id): Response
    {
        $entity = $this->musicRepository->find($id);
        $this->commonService->createNotFoundException($entity);

        return $this->render(
            'cp/music/mutation.html.twig',
        );
    }
}
