<?php

declare(strict_types=1);

namespace App\Controller\Cp\Music;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    public const NAME = 'cp.music';

    #[Route('/cp/music', name: self::NAME)]
    public function __invoke(): Response
    {
        return $this->render('cp/music/music.html.twig');
    }
}
