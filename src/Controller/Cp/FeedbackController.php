<?php

declare(strict_types=1);

namespace App\Controller\Cp;

use App\Repository\FeedbackRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FeedbackController extends AbstractController
{
    private FeedbackRepository $feedbackRepository;

    public const NAME = 'cp.feedbacks';

    public function __construct(
        FeedbackRepository $feedbackRepository,
    ) {
        $this->feedbackRepository = $feedbackRepository;
    }

    #[Route('/cp/feedbacks', name: self::NAME)]
    public function __invoke(): Response
    {
        return $this->render(
            'cp/feedback/feedbacks.html.twig',
            [
                'feedbacks' => $this->feedbackRepository->findAll(),
            ],
        );
    }
}
