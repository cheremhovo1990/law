<?php

declare(strict_types=1);

namespace App\Controller\Cp\Precept;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    public const NAME = 'cp.precepts';

    #[Route('/cp/precepts', name: self::NAME)]
    public function __invoke(): Response
    {
        return $this->render('cp/precept/precepts.html.twig');
    }
}
