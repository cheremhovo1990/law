<?php

declare(strict_types=1);

namespace App\Controller\Cp\Precept;

use App\Repository\PreceptRepository;
use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    public const NAME = 'cp.precept.edit';

    private PreceptRepository $preceptRepository;
    private CommonService $commonService;

    public function __construct(
        PreceptRepository $preceptRepository,
        CommonService $commonService,
    ) {
        $this->preceptRepository = $preceptRepository;
        $this->commonService = $commonService;
    }

    #[Route('/cp/precept/{id}/edit', name: self::NAME, options: ['expose' => true])]
    public function __invoke(Request $request, string $id): Response
    {
        $precept = $this->preceptRepository->find($id);
        $this->commonService->createNotFoundException($precept);

        return $this->render('cp/precept/edit.html.twig', [
            'precept' => $precept,
        ]);
    }
}
