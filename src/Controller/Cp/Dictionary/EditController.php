<?php

declare(strict_types=1);

namespace App\Controller\Cp\Dictionary;

use App\Repository\DictionaryRepository;
use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    public const NAME = 'cp.dictionary.edit';
    private DictionaryRepository $dictionaryRepository;
    private CommonService $commonService;

    public function __construct(
        DictionaryRepository $dictionaryRepository,
        CommonService $commonService,
    ) {
        $this->dictionaryRepository = $dictionaryRepository;
        $this->commonService = $commonService;
    }

    #[Route('/cp/dictionary/{id}/edit', name: self::NAME, options: ['expose' => true])]
    public function __invoke(Request $request, string $id): Response
    {
        $dictionary = $this->dictionaryRepository->find($id);
        $this->commonService->createNotFoundException($dictionary);

        return $this->render('cp/dictionary/create.html.twig');
    }
}
