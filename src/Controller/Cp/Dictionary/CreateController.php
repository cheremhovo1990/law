<?php

declare(strict_types=1);

namespace App\Controller\Cp\Dictionary;

use App\Form\Types\Cp\DictionaryType;
use App\UseCase\EntityManager\DictionaryManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    public const NAME = 'cp.dictionary.create';
    private DictionaryManagerUseCase $dictionaryManagerUseCase;

    public function __construct(DictionaryManagerUseCase $dictionaryManagerUseCase)
    {
        $this->dictionaryManagerUseCase = $dictionaryManagerUseCase;
    }

    #[Route('/cp/dictionary/create', name: self::NAME)]
    public function __invoke(Request $request): Response
    {
        $form = $this->createForm(DictionaryType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $dictionary = $this->dictionaryManagerUseCase->create($data);
            $this->addFlash('success', 'Success');

            return $this->redirectToRoute(EditController::NAME, ['id' => $dictionary->getId()]);
        }

        return $this->render(
            'cp/dictionary/create.html.twig',
            [
                'form' => $form->createView(),
            ],
        );
    }
}
