<?php

declare(strict_types=1);

namespace App\Controller\Cp\Dictionary;

use App\Repository\DictionaryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    public const NAME = 'cp.dictionaries';

    #[Route('/cp/dictionaries', name: self::NAME)]
    public function __invoke(DictionaryRepository $dictionaryRepository): Response
    {
        return $this->render('cp/dictionary/dictionaries.html.twig');
    }
}
