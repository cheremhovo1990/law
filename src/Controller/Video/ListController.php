<?php

declare(strict_types=1);

namespace App\Controller\Video;

use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    public const NAME = 'video';
    private VideoRepository $videoRepository;

    public function __construct(
        VideoRepository $videoRepository,
    ) {
        $this->videoRepository = $videoRepository;
    }

    #[Route('/video', name: self::NAME)]
    public function __invoke(): Response
    {
        return $this->render(
            'video/list.html.twig',
            [
                'video' => $this->videoRepository->findAllByPublishAt(),
            ],
        );
    }
}
