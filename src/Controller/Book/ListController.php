<?php

declare(strict_types=1);

namespace App\Controller\Book;

use App\Entity\MetaKeyEnum;
use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    public const NAME = 'books';
    private CommonService $commonService;

    public function __construct(
        CommonService $commonService,
    ) {
        $this->commonService = $commonService;
    }

    #[Route('/books', name: self::NAME)]
    public function __invoke(): Response
    {
        $meta = $this->commonService->findOneMetaByKey(MetaKeyEnum::BOOKS->value);

        return $this->render(
            'book/list.html.twig',
            [
                'meta' => $meta,
            ],
        );
    }
}
