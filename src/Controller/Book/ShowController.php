<?php

declare(strict_types=1);

namespace App\Controller\Book;

use App\Repository\BookRepository;
use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShowController extends AbstractController
{
    public const NAME = 'book';
    private BookRepository $bookRepository;
    private CommonService $commonService;

    public function __construct(
        BookRepository $bookRepository,
        CommonService $commonService,
    ) {
        $this->bookRepository = $bookRepository;
        $this->commonService = $commonService;
    }

    #[Route('/book/{id}', name: self::NAME, options: ['expose' => true])]
    public function __invoke(string $id): Response
    {
        $book = $this->bookRepository->findOneByPublishedAt($id);
        $this->commonService->createNotFoundException($book);

        return $this->render('book/show.html.twig', [
            'book' => $book,
        ]);
    }
}
