<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Repository\MetaRepository;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MetaQueryController extends AbstractController
{
    private MetaRepository $metaRepository;

    public function __construct(
        MetaRepository $metaRepository,
    ) {
        $this->metaRepository = $metaRepository;
    }

    #[Route('/api/cp/meta', name: 'api.cp.meta', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(): JsonResponse
    {
        return $this->json([
            'data' => $this->metaRepository->findAllWithOrderKey(),
        ]);
    }
}
