<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Services\CommonService;
use App\UseCase\EntityManager\BookManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class BookDeleteMutationController extends AbstractController
{
    private CommonService $commonService;
    private BookManagerUseCase $bookManagerUseCase;

    public function __construct(
        BookManagerUseCase $bookManagerUseCase,
        CommonService $commonService,
    ) {
        $this->commonService = $commonService;
        $this->bookManagerUseCase = $bookManagerUseCase;
    }

    #[Route('/api/cp/book/{id}/delete/mutation', name: 'api.cp.book.delete-mutation')]
    public function __invoke(string $id): JsonResponse
    {
        try {
            $this->bookManagerUseCase->delete($id);
            $this->commonService->showMessageSuccess('Deleted successfully');
        } catch (\Throwable $exception) {
            $this->commonService->warning('Deleted Failed');
            throw $exception;
        }

        return $this->json([
            'success' => true,
        ]);
    }
}
