<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Form\Types\Cp\PageType;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\PageManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PageCreateMutationController extends AbstractController
{
    private PageManagerUseCase $pageManagerUseCase;
    private CommonService $commonService;

    public function __construct(
        PageManagerUseCase $pageManagerUseCase,
        CommonService $commonService,
    ) {
        $this->pageManagerUseCase = $pageManagerUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/page/create/mutation', name: 'api.cp.page.create-mutation', methods: 'post')]
    #[SerializeToJson]
    public function __invoke(Request $request): JsonResponse|FormInterface
    {
        $form = $this->createForm(PageType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $entity = $this->pageManagerUseCase->create($data);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'id' => (string) $entity->getId(),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
