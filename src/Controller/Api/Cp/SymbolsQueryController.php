<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Entity\Font;
use App\Entity\SymbolMap;
use App\Repository\SymbolMapRepository;
use App\Repository\SymbolRepository;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SymbolsQueryController extends AbstractController
{
    protected SymbolRepository $symbolRepository;
    protected SymbolMapRepository $symbolMapRepository;

    public function __construct(
        SymbolRepository $symbolRepository,
        SymbolMapRepository $symbolMapRepository,
    ) {
        $this->symbolRepository = $symbolRepository;
        $this->symbolMapRepository = $symbolMapRepository;
    }

    /**
     * @return array<integer, SymbolMap>
     */
    #[Route('/api/cp/symbols', name: 'api.cp.symbols')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(): array
    {
        return $this->symbolMapRepository->findAllByFont(Font::CODE_PONOMAR_UNICODE);
    }
}
