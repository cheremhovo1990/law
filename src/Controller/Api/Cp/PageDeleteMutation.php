<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\PageManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PageDeleteMutation extends AbstractController
{
    private PageManagerUseCase $pageManagerUseCase;
    private CommonService $commonService;

    public function __construct(
        PageManagerUseCase $pageManagerUseCase,
        CommonService $commonService,
    ) {
        $this->pageManagerUseCase = $pageManagerUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/page/{id}/delete/mutation', name: 'api.cp.page.delete-mutation', methods: 'post')]
    #[SerializeToJson]
    public function __invoke(Request $request, string $id): JsonResponse
    {
        try {
            $this->pageManagerUseCase->delete($id);
            $this->commonService->showMessageSuccess('Deleted successfully');
        } catch (\Throwable $exception) {
            $this->commonService->warning('Deleted Failed');
            throw $exception;
        }

        return $this->json([
            'success' => true,
        ]);
    }
}
