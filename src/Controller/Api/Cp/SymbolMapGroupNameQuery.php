<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Entity\Font;
use App\Repository\SymbolMapRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class SymbolMapGroupNameQuery extends AbstractController
{
    private SymbolMapRepository $symbolMapRepository;

    public function __construct(
        SymbolMapRepository $symbolMapRepository,
    ) {
        $this->symbolMapRepository = $symbolMapRepository;
    }

    #[Route('/api/cp/symbol/map/group/name', name: 'api.cp.symbol.map.group.name')]
    public function __invoke(): JsonResponse
    {
        $entities = $this->symbolMapRepository->findAllGroupName(Font::CODE_PONOMAR_UNICODE);

        return $this->json($entities);
    }
}
