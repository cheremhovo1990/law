<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Controller\Cp\Dictionary\EditController;
use App\Form\Types\Cp\DictionaryType;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\DictionaryManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DictionaryCreateMutationController extends AbstractController
{
    private DictionaryManagerUseCase $dictionaryManagerUseCase;
    private CommonService $commonService;

    public function __construct(
        DictionaryManagerUseCase $dictionaryManagerUseCase,
        CommonService $commonService,
    ) {
        $this->dictionaryManagerUseCase = $dictionaryManagerUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/dictionary/create/mutation', name: 'api.cp.dictionary.create-mutation')]
    #[SerializeToJson]
    public function __invoke(Request $request): FormInterface|JsonResponse
    {
        $form = $this->createForm(DictionaryType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $dictionary = $this->dictionaryManagerUseCase->create($data);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'redirect' => $this->generateUrl(
                    EditController::NAME,
                    ['id' => $dictionary->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
