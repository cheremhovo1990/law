<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Controller\Cp\Music\EditController;
use App\Form\Types\Cp\MusicType;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\MusicManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MusicCreateMutationController extends AbstractController
{
    private CommonService $commonService;
    private MusicManagerUseCase $musicManagerUseCase;

    public function __construct(
        MusicManagerUseCase $musicManagerUseCase,
        CommonService $commonService,
    ) {
        $this->commonService = $commonService;
        $this->musicManagerUseCase = $musicManagerUseCase;
    }

    #[Route('/api/cp/music/create/mutation', name: 'api.cp.music.create-mutation', methods: 'post')]
    #[SerializeToJson]
    public function __invoke(Request $request): FormInterface|JsonResponse
    {
        $form = $this->createForm(MusicType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $entity = $this->musicManagerUseCase->create($data);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'redirect' => $this->generateUrl(
                    EditController::NAME,
                    ['id' => $entity->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
