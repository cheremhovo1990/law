<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Controller\Cp\Meta\EditController;
use App\Form\Types\Cp\MetaType;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\MetaManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MetaCreateMutationController extends AbstractController
{
    private MetaManagerUseCase $metaManagerUseCase;
    private CommonService $commonService;

    public function __construct(
        MetaManagerUseCase $metaManagerUseCase,
        CommonService $commonService,
    ) {
        $this->metaManagerUseCase = $metaManagerUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/meta/create/mutation', name: 'api.cp.meta.create-mutation', methods: 'post')]
    #[SerializeToJson]
    public function __invoke(Request $request): FormInterface|JsonResponse
    {
        $form = $this->createForm(MetaType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $meta = $this->metaManagerUseCase->create($data);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'redirect' => $this->generateUrl(EditController::NAME, ['key' => $meta->getKey()]),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
