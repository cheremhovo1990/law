<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Controller\Cp\DescriptionSin\EditController;
use App\Form\Types\Cp\DescriptionSinType;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\DescriptionSinManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DescriptionSinCreateMutationController extends AbstractController
{
    private DescriptionSinManagerUseCase $descriptionSinManagerUseCase;
    private CommonService $commonService;

    public function __construct(
        DescriptionSinManagerUseCase $descriptionSinManagerUseCase,
        CommonService $commonService,
    ) {
        $this->descriptionSinManagerUseCase = $descriptionSinManagerUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/description/sin/create/mutation', name: 'api.cp.description.sin.create-mutation')]
    #[SerializeToJson]
    public function __invoke(Request $request): FormInterface|JsonResponse
    {
        $form = $this->createForm(DescriptionSinType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $descriptionSin = $this->descriptionSinManagerUseCase->create($data);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'redirect' => $this->generateUrl(
                    EditController::NAME,
                    ['id' => $descriptionSin->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
