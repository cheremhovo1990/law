<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Repository\DictionaryRepository;
use App\Searches\Cp\DictionarySearch;
use App\Services\PaginatorService;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DictionariesQueryController extends AbstractController
{
    private DictionaryRepository $dictionaryRepository;
    private DictionarySearch $dictionarySearch;

    public function __construct(
        DictionarySearch $dictionarySearch,
        DictionaryRepository $dictionaryRepository,
    ) {
        $this->dictionaryRepository = $dictionaryRepository;
        $this->dictionarySearch = $dictionarySearch;
    }

    #[Route('/api/cp/dictionaries', name: 'api.cp.dictionaries', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(Request $request): PaginatorService
    {
        $search = new ParameterBag($request->query->all('search'));
        if (0 === $search->count()) {
            $qb = $this->dictionaryRepository->createQueryBuilder('d');
        } else {
            $qb = $this->dictionarySearch->search($search);
        }
        $qb
            ->orderBy('d.createdAt', 'desc')
        ;

        return new PaginatorService($qb, $request);
    }
}
