<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Entity\Page;
use App\Repository\PageRepository;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PagesRootTreeController extends AbstractController
{
    protected PageRepository $pageRepository;

    public function __construct(
        PageRepository $pageRepository,
    ) {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @return array<integer, Page>
     */
    #[Route('/api/cp/pages/root/tree', name: 'api.cp.pages.root.tree', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(): array
    {
        return $this->pageRepository->rootTree();
    }
}
