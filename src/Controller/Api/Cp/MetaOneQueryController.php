<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Entity\Meta;
use App\Repository\MetaRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MetaOneQueryController extends AbstractController
{
    private MetaRepository $metaRepository;
    private CommonService $commonService;

    public function __construct(
        MetaRepository $metaRepository,
        CommonService $commonService,
    ) {
        $this->metaRepository = $metaRepository;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/meta/one/{key}', name: 'api.cp.meta.one', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(string $key): Meta
    {
        $meta = $this->metaRepository->find($key);
        $this->commonService->createNotFoundException($meta);

        return $meta;
    }
}
