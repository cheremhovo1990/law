<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\MusicManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MusicDeleteMutationController extends AbstractController
{
    private CommonService $commonService;
    private MusicManagerUseCase $musicManagerUseCase;

    public function __construct(
        MusicManagerUseCase $musicManagerUseCase,
        CommonService $commonService,
    ) {
        $this->commonService = $commonService;
        $this->musicManagerUseCase = $musicManagerUseCase;
    }

    #[Route('/api/cp/music/{id}/delete/mutation', name: 'api.cp.music.delete-mutation', methods: 'post')]
    #[SerializeToJson]
    public function __invoke(Request $request, string $id): JsonResponse
    {
        try {
            $this->musicManagerUseCase->delete($id);
            $this->commonService->showMessageSuccess('Deleted successfully');
        } catch (\Throwable $exception) {
            $this->commonService->warning('Deleted Failed');
            throw $exception;
        }

        return $this->json([
            'success' => true,
        ]);
    }
}
