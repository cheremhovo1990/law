<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Entity\Music;
use App\Repository\MusicRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MusicOneQueryController extends AbstractController
{
    private CommonService $commonService;
    private MusicRepository $musicRepository;

    public function __construct(
        MusicRepository $musicRepository,
        CommonService $commonService,
    ) {
        $this->commonService = $commonService;
        $this->musicRepository = $musicRepository;
    }

    #[Route('/api/cp/music/one/{id}', name: 'api.cp.music.one', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(string $id): Music
    {
        $entity = $this->musicRepository->find($id);
        $this->commonService->createNotFoundException($entity);

        return $entity;
    }
}
