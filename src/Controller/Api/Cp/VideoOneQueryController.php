<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Entity\Video;
use App\Repository\VideoRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class VideoOneQueryController extends AbstractController
{
    private VideoRepository $videoRepository;
    private CommonService $commonService;

    public function __construct(
        VideoRepository $videoRepository,
        CommonService $commonService,
    ) {
        $this->videoRepository = $videoRepository;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/video/one/{id}', name: 'api.cp.video.one', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(string $id): Video
    {
        $video = $this->videoRepository->find($id);
        $this->commonService->createNotFoundException($video);

        return $video;
    }
}
