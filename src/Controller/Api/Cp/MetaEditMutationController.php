<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Commands\Cp\MetaCommand;
use App\Controller\Cp\Meta\EditController;
use App\Form\Types\Cp\MetaType;
use App\Repository\MetaRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\MetaManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MetaEditMutationController extends AbstractController
{
    private MetaManagerUseCase $metaManagerUseCase;
    private MetaRepository $metaRepository;
    private CommonService $commonService;

    public function __construct(
        MetaManagerUseCase $metaManagerUseCase,
        MetaRepository $metaRepository,
        CommonService $commonService,
    ) {
        $this->metaManagerUseCase = $metaManagerUseCase;
        $this->metaRepository = $metaRepository;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/meta/{key}/edit/mutation', name: 'api.cp.meta.edit-mutation', methods: 'post')]
    #[SerializeToJson]
    public function __invoke(Request $request, string $key): FormInterface|JsonResponse
    {
        $meta = $this->metaRepository->findOneByKey($key);
        $this->commonService->createNotFoundException($meta);
        $command = MetaCommand::create($meta);

        $form = $this->createForm(MetaType::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->metaManagerUseCase->update($data, $meta);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'redirect' => $this->generateUrl(EditController::NAME, ['key' => $meta->getKey()]),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
