<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Commands\Cp\ImageCommand;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\ImageManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Image;

class ImageUploadController extends AbstractController
{
    private ImageManagerUseCase $imageManagerUseCase;
    private CommonService $commonService;

    public function __construct(
        ImageManagerUseCase $imageManagerUseCase,
        CommonService $commonService,
    ) {
        $this->imageManagerUseCase = $imageManagerUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/image/upload/mutation', name: 'api.cp.image.upload-mutation')]
    #[SerializeToJson]
    public function __invoke(Request $request): FormInterface|JsonResponse
    {
        $command = new ImageCommand();
        $form =
            $this
                ->createFormBuilder($command, [
                    'csrf_protection' => false,
                ])
                ->add('image', FileType::class, [
                    'required' => true,
                    'constraints' => [
                        new Image(),
                    ],
                ])
                ->getForm()
        ;
        $form->submit($request->files->all());
        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData();
            $image = $this->imageManagerUseCase->create($command);
            $this->commonService->showMessageSuccess('Success');

            return $this->json(['success' => true, 'message' => 'OK', 'data' => $image]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
