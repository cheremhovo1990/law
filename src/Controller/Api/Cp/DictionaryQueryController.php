<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Entity\Dictionary;
use App\Repository\DictionaryRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DictionaryQueryController extends AbstractController
{
    private CommonService $commonService;
    private DictionaryRepository $dictionaryRepository;

    public function __construct(
        DictionaryRepository $dictionaryRepository,
        CommonService $commonService,
    ) {
        $this->commonService = $commonService;
        $this->dictionaryRepository = $dictionaryRepository;
    }

    #[Route('/api/cp/dictionary/{id}', name: 'api.cp.dictionary')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(string $id): Dictionary
    {
        $book = $this->dictionaryRepository->find($id);
        $this->commonService->createNotFoundException($book);

        return $book;
    }
}
