<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Services\CommonService;
use App\UseCase\EntityManager\DescriptionSinManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class DescriptionSinDeleteMutation extends AbstractController
{
    private DescriptionSinManagerUseCase $descriptionSinManagerUseCase;
    private CommonService $commonService;

    public function __construct(
        DescriptionSinManagerUseCase $descriptionSinManagerUseCase,
        CommonService $commonService,
    ) {
        $this->descriptionSinManagerUseCase = $descriptionSinManagerUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/description/sin/{id}/delete/mutation', name: 'api.cp.description.sin.delete-mutation')]
    public function __invoke(string $id): JsonResponse
    {
        try {
            $this->descriptionSinManagerUseCase->delete($id);
            $this->commonService->showMessageSuccess('Deleted successfully');
        } catch (\Throwable $exception) {
            $this->commonService->warning('Deleted Failed');
            throw $exception;
        }

        return $this->json([
            'success' => true,
        ]);
    }
}
