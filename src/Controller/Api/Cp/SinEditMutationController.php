<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Commands\Cp\SinCommand;
use App\Controller\Cp\Sin\EditController;
use App\Form\Types\Cp\SinType;
use App\Repository\SinRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\SinManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SinEditMutationController extends AbstractController
{
    private SinManagerUseCase $sinManagerUseCase;
    private SinRepository $sinRepository;
    private CommonService $commonService;

    public function __construct(
        SinManagerUseCase $sinManagerUseCase,
        SinRepository $sinRepository,
        CommonService $commonService,
    ) {
        $this->sinManagerUseCase = $sinManagerUseCase;
        $this->sinRepository = $sinRepository;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/sin/{id}/edit/mutation', name: 'api.cp.sin.edit-mutation')]
    #[SerializeToJson]
    public function __invoke(Request $request, string $id): FormInterface|JsonResponse
    {
        $sin = $this->sinRepository->find($id);
        $this->commonService->createNotFoundException($sin);
        $command = SinCommand::create($sin);
        $form = $this->createForm(SinType::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->sinManagerUseCase->update($data, $sin);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'redirect' => $this->generateUrl(
                    EditController::NAME,
                    [
                        'preceptId' => $sin->getPrecept()->getId(),
                        'id' => $sin->getId(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
