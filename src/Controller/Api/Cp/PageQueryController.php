<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Entity\Page;
use App\Repository\PageRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PageQueryController extends AbstractController
{
    private PageRepository $pageRepository;
    private CommonService $commonService;

    public function __construct(
        PageRepository $pageRepository,
        CommonService $commonService,
    ) {
        $this->pageRepository = $pageRepository;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/page/{id}', name: 'api.cp.page', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(string $id): Page
    {
        $video = $this->pageRepository->find($id);
        $this->commonService->createNotFoundException($video);

        return $video;
    }
}
