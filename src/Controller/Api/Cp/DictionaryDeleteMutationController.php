<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Services\CommonService;
use App\UseCase\EntityManager\DictionaryManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class DictionaryDeleteMutationController extends AbstractController
{
    private DictionaryManagerUseCase $dictionaryManagerUseCase;
    private CommonService $commonService;

    public function __construct(
        CommonService $commonService,
        DictionaryManagerUseCase $dictionaryManagerUseCase,
    ) {
        $this->dictionaryManagerUseCase = $dictionaryManagerUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/dictionary/{id}/delete/mutation', name: 'api.cp.dictionary.delete-mutation')]
    public function __invoke(string $id): JsonResponse
    {
        try {
            $this->dictionaryManagerUseCase->delete($id);
            $this->commonService->showMessageSuccess('Deleted successfully');
        } catch (\Throwable $exception) {
            $this->commonService->warning('Deleted Failed');
            throw $exception;
        }

        return $this->json(['success' => true]);
    }
}
