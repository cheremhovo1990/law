<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Commands\Cp\PageCommand;
use App\Form\Types\Cp\PageType;
use App\Repository\PageRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\PageManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PageEditMutationController extends AbstractController
{
    private CommonService $commonService;
    private PageRepository $pageRepository;
    private PageManagerUseCase $pageManagerUseCase;

    public function __construct(
        PageRepository $pageRepository,
        PageManagerUseCase $pageManagerUseCase,
        CommonService $commonService,
    ) {
        $this->commonService = $commonService;
        $this->pageRepository = $pageRepository;
        $this->pageManagerUseCase = $pageManagerUseCase;
    }

    #[Route('/api/cp/page/{id}/edit/mutation', name: 'api.cp.page.edit-mutation', methods: 'post')]
    #[SerializeToJson]
    public function __invoke(Request $request, string $id): FormInterface|JsonResponse
    {
        $page = $this->pageRepository->find($id);
        $this->commonService->createNotFoundException($page);
        $command = PageCommand::create($page);
        $form = $this->createForm(PageType::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->pageManagerUseCase->update($data, $page);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
