<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\VideoManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class VideoDeleteMutation extends AbstractController
{
    private VideoManagerUseCase $videoManagerUseCase;
    private CommonService $commonService;

    public function __construct(
        VideoManagerUseCase $videoManagerUseCase,
        CommonService $commonService,
    ) {
        $this->videoManagerUseCase = $videoManagerUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/video/{id}/delete/mutation', name: 'api.cp.video.delete-mutation', methods: 'post')]
    #[SerializeToJson]
    public function __invoke(Request $request, string $id): JsonResponse
    {
        try {
            $this->videoManagerUseCase->delete($id);
            $this->commonService->showMessageSuccess('Deleted successfully');
        } catch (\Throwable $exception) {
            $this->commonService->warning('Deleted Failed');
            throw $exception;
        }

        return $this->json([
            'success' => true,
        ]);
    }
}
