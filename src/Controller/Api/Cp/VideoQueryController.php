<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Repository\VideoRepository;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class VideoQueryController extends AbstractController
{
    private VideoRepository $videoRepository;

    public function __construct(
        VideoRepository $videoRepository,
    ) {
        $this->videoRepository = $videoRepository;
    }

    #[Route('/api/cp/video', name: 'api.cp.video', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(): JsonResponse
    {
        return $this->json([
            'data' => $this->videoRepository->findAllWithOrder(),
        ]);
    }
}
