<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Entity\Sin;
use App\Repository\SinRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SinQueryController extends AbstractController
{
    private SinRepository $sinRepository;
    private CommonService $commonService;

    public function __construct(
        SinRepository $sinRepository,
        CommonService $commonService,
    ) {
        $this->sinRepository = $sinRepository;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/sin/{id}', name: 'api.cp.sin')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(string $id): Sin
    {
        $sin = $this->sinRepository->find($id);
        $this->commonService->createNotFoundException($sin);

        return $sin;
    }
}
