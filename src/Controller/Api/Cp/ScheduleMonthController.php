<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Repository\ScheduleEventRepository;
use App\Services\Schedule\Factory;
use App\Services\Serialize\SerializeToJson;
use Carbon\CarbonImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ScheduleMonthController extends AbstractController
{
    private ScheduleEventRepository $scheduleEventRepository;

    public function __construct(
        ScheduleEventRepository $scheduleEventRepository,
    ) {
        $this->scheduleEventRepository = $scheduleEventRepository;
    }

    #[Route('/api/cp/schedule/month', name: 'api.cp.schedule.month')]
    #[SerializeToJson([
        self::class,
    ])]
    public function __invoke(Request $request): JsonResponse
    {
        $startDate = $request->query->get('startDate');
        $endDate = $request->query->get('endDate');
        $timezone = new \DateTimeZone('Asia/Irkutsk');
        $start = CarbonImmutable::createFromFormat('Y-m-d H:i:s', sprintf('%s 00:00:00', $startDate), $timezone);
        $end = CarbonImmutable::createFromFormat('Y-m-d H:i:s', sprintf('%s 23:59:59', $endDate), $timezone);

        $events = $this->scheduleEventRepository->findAllByStartAndEnd(
            $start->format('Y-m-d H:i'),
            $end->format('Y-m-d H:i'),
        );
        $factory = new Factory(
            $events,
            $start,
            $end,
            $timezone,
        );

        return $this->json($factory->buildMonth()->resolve());
    }
}
