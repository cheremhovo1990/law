<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Repository\FeedbackRepository;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FeedbacksQueryController extends AbstractController
{
    private FeedbackRepository $feedbackRepository;

    public function __construct(
        FeedbackRepository $feedbackRepository,
    ) {
        $this->feedbackRepository = $feedbackRepository;
    }

    /**
     * @return array<string, array<int, mixed>>
     */
    #[Route('/api/cp/feedbacks', name: 'api.cp.feedbacks', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(): array
    {
        return [
            'data' => $this->feedbackRepository->findAll(),
        ];
    }
}
