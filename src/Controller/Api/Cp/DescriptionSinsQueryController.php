<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Entity\DescriptionSin;
use App\Repository\DescriptionSinRepository;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DescriptionSinsQueryController extends AbstractController
{
    private DescriptionSinRepository $descriptionSinRepository;

    public function __construct(
        DescriptionSinRepository $descriptionSinRepository,
    ) {
        $this->descriptionSinRepository = $descriptionSinRepository;
    }

    /**
     * @return array<string, array<DescriptionSin>>
     */
    #[Route('/api/cp/description/sins', name: 'api.cp.description.sins', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(): array
    {
        return [
            'data' => $this->descriptionSinRepository->findAllWithOrder(),
        ];
    }
}
