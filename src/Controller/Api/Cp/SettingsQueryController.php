<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Doctrine\Models\BookLinkTypeEnum;
use App\Entity\LanguageOfThePublicationEnum;
use App\Entity\MetaKeyEnum;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class SettingsQueryController extends AbstractController
{
    #[Route('/api/cp/settings', name: 'api.cp.settings')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(): JsonResponse
    {
        return $this->json([
            'meta' => [
                'key' => [
                    'options' => MetaKeyEnum::getDropDown(),
                ],
            ],
            'book' => [
                'languageOfThePublication' => [
                    'options' => LanguageOfThePublicationEnum::getChoices(),
                ],
                'links' => [
                    'options' => BookLinkTypeEnum::getChoices(),
                ],
            ],
        ]);
    }
}
