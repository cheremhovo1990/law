<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Repository\MusicRepository;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MusicQueryController extends AbstractController
{
    private MusicRepository $musicRepository;

    public function __construct(
        MusicRepository $musicRepository,
    ) {
        $this->musicRepository = $musicRepository;
    }

    /**
     * @return array<string, array<int, mixed>>
     */
    #[Route('/api/cp/music', name: 'api.cp.music', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(): array
    {
        return [
            'data' => $this->musicRepository->findAllWithOrder(),
        ];
    }
}
