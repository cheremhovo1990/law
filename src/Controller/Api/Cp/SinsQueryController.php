<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Repository\SinRepository;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SinsQueryController extends AbstractController
{
    private SinRepository $sinRepository;

    public function __construct(
        SinRepository $sinRepository,
    ) {
        $this->sinRepository = $sinRepository;
    }

    /**
     * @return array<string, array<integer, mixed>>
     */
    #[Route('/api/cp/sins', methods: 'get', name: 'api.cp.sins')]
    #[Route('/api/cp/precept/{preceptId}/sins', methods: 'get', name: 'api.cp.sins.by.precept')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(string $preceptId = null): array
    {
        if (null !== $preceptId) {
            $data = [
                'data' => $this->sinRepository->findAllByPrecept($preceptId),
            ];
        } else {
            $data = [
                'data' => $this->sinRepository->findAll(),
            ];
        }

        return $data;
    }
}
