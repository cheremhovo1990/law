<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Commands\Cp\DictionaryCommand;
use App\Controller\Cp\Dictionary\EditController;
use App\Form\Types\Cp\DictionaryType;
use App\Repository\DictionaryRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\DictionaryManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DictionaryEditMutationController extends AbstractController
{
    private DictionaryManagerUseCase $dictionaryManagerUseCase;
    private CommonService $commonService;
    private DictionaryRepository $dictionaryRepository;

    public function __construct(
        DictionaryManagerUseCase $dictionaryManagerUseCase,
        DictionaryRepository $dictionaryRepository,
        CommonService $commonService,
    ) {
        $this->dictionaryManagerUseCase = $dictionaryManagerUseCase;
        $this->commonService = $commonService;
        $this->dictionaryRepository = $dictionaryRepository;
    }

    #[Route('/api/cp/dictionary/{id}/edit/mutation', name: 'api.cp.dictionary.edit-mutation')]
    #[SerializeToJson]
    public function __invoke(Request $request, string $id): FormInterface|JsonResponse
    {
        $dictionary = $this->dictionaryRepository->find($id);
        $this->commonService->createNotFoundException($dictionary);
        $command = DictionaryCommand::create($dictionary);
        $form = $this->createForm(DictionaryType::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->dictionaryManagerUseCase->update($data, $dictionary);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'redirect' => $this->generateUrl(
                    EditController::NAME,
                    ['id' => $dictionary->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
