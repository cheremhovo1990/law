<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Commands\Cp\DescriptionSinCommand;
use App\Controller\Cp\DescriptionSin\EditController;
use App\Form\Types\Cp\DescriptionSinType;
use App\Repository\DescriptionSinRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\DescriptionSinManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DescriptionSinEditMutationController extends AbstractController
{
    private DescriptionSinManagerUseCase $descriptionSinManagerUseCase;
    private DescriptionSinRepository $descriptionSinRepository;
    private CommonService $commonService;

    public function __construct(
        DescriptionSinRepository $descriptionSinRepository,
        DescriptionSinManagerUseCase $descriptionSinManagerUseCase,
        CommonService $commonService,
    ) {
        $this->descriptionSinManagerUseCase = $descriptionSinManagerUseCase;
        $this->descriptionSinRepository = $descriptionSinRepository;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/description/sin/{id}/edit/mutation', name: 'api.cp.description.sin.edit-mutation')]
    #[SerializeToJson]
    public function __invoke(Request $request, string $id): JsonResponse|FormInterface
    {
        $descriptionSin = $this->descriptionSinRepository->find($id);
        $this->commonService->createNotFoundException($descriptionSin);
        $command = DescriptionSinCommand::create(
            $descriptionSin,
        );

        $form = $this->createForm(DescriptionSinType::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->descriptionSinManagerUseCase->update($data, $descriptionSin);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'redirect' => $this->generateUrl(
                    EditController::NAME,
                    ['id' => $descriptionSin->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
