<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Services\CommonService;
use App\UseCase\EntityManager\SinManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class SinDeleteMutationController extends AbstractController
{
    private SinManagerUseCase $sinManagerUseCase;
    private CommonService $commonService;

    public function __construct(
        SinManagerUseCase $sinManagerUseCase,
        CommonService $commonService,
    ) {
        $this->sinManagerUseCase = $sinManagerUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/sin/{id}/delete/mutation', name: 'api.cp.sin.delete-mutation', methods: 'post')]
    public function __invoke(string $id): JsonResponse
    {
        try {
            $this->sinManagerUseCase->delete($id);
            $this->commonService->showMessageSuccess('Deleted successfully');
        } catch (\Throwable $exception) {
            $this->commonService->warning('Deleted Failed');
            throw $exception;
        }

        return $this->json([
            'success' => true,
        ]);
    }
}
