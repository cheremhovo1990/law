<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Controller\Cp\Book\EditController;
use App\Form\Types\Cp\BookType;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\BookManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BookCreateMutationController extends AbstractController
{
    private BookManagerUseCase $bookManagerUseCase;
    private CommonService $commonService;

    public function __construct(
        BookManagerUseCase $bookManagerUseCase,
        CommonService $commonService,
    ) {
        $this->bookManagerUseCase = $bookManagerUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/book/create/mutation', name: 'api.cp.book.create-mutation')]
    #[SerializeToJson]
    public function __invoke(Request $request): FormInterface|JsonResponse
    {
        $form = $this->createForm(BookType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $book = $this->bookManagerUseCase->create($data);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'redirect' => $this->generateUrl(
                    EditController::NAME,
                    ['id' => $book->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
