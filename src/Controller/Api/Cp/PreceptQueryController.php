<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Entity\Precept;
use App\Repository\PreceptRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PreceptQueryController extends AbstractController
{
    private PreceptRepository $preceptRepository;
    private CommonService $commonService;

    public function __construct(
        PreceptRepository $preceptRepository,
        CommonService $commonService,
    ) {
        $this->preceptRepository = $preceptRepository;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/precept/{id}', name: 'api.cp.precept')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(string $id): Precept
    {
        $precept = $this->preceptRepository->find($id);
        $this->commonService->createNotFoundException($precept);

        return $precept;
    }
}
