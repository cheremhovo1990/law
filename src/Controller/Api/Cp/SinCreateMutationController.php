<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Controller\Cp\Sin\EditController;
use App\Form\Types\Cp\SinType;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\SinManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SinCreateMutationController extends AbstractController
{
    private SinManagerUseCase $sinManagerUseCase;
    private CommonService $commonService;

    public function __construct(
        SinManagerUseCase $sinManagerUseCase,
        CommonService $commonService,
    ) {
        $this->sinManagerUseCase = $sinManagerUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/sin/create/mutation', name: 'api.cp.sin.create-mutation')]
    #[SerializeToJson]
    public function __invoke(Request $request): FormInterface|JsonResponse
    {
        $form = $this->createForm(SinType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $sin = $this->sinManagerUseCase->create($data);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'redirect' => $this->generateUrl(
                    EditController::NAME,
                    [
                        'preceptId' => $sin->getPrecept()->getId(),
                        'id' => $sin->getId(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
