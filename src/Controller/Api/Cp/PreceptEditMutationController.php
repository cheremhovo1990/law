<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Commands\Cp\PreceptCommand;
use App\Controller\Cp\Precept\EditController;
use App\Form\Types\Cp\PreceptType;
use App\Repository\PreceptRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\PreceptManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PreceptEditMutationController extends AbstractController
{
    private PreceptRepository $preceptRepository;
    private CommonService $commonService;
    private PreceptManagerUseCase $preceptManagerUseCase;

    public function __construct(
        PreceptRepository $preceptRepository,
        PreceptManagerUseCase $preceptManagerUseCase,
        CommonService $commonService,
    ) {
        $this->preceptRepository = $preceptRepository;
        $this->commonService = $commonService;
        $this->preceptManagerUseCase = $preceptManagerUseCase;
    }

    #[Route('/api/cp/precept/{id}/edit/mutation', name: 'api.cp.precept.edit-mutation', methods: 'post')]
    #[SerializeToJson]
    public function __invoke(Request $request, string $id): FormInterface|JsonResponse
    {
        $precept = $this->preceptRepository->find($id);

        $this->commonService->createNotFoundException($precept);

        $command = PreceptCommand::create($precept);
        $form = $this->createForm(PreceptType::class, $command);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->preceptManagerUseCase->update($data, $precept);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'redirect' => $this->generateUrl(EditController::NAME, ['id' => $precept->getId()]),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
