<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Commands\Cp\BookCommand;
use App\Controller\Cp\Book\EditController;
use App\Form\Types\Cp\BookType;
use App\Repository\BookRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\BookManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BookEditMutationController extends AbstractController
{
    private BookManagerUseCase $bookManagerUseCase;
    private CommonService $commonService;
    private BookRepository $bookRepository;

    public function __construct(
        BookManagerUseCase $bookManagerUseCase,
        CommonService $commonService,
        BookRepository $bookRepository,
    ) {
        $this->bookManagerUseCase = $bookManagerUseCase;
        $this->commonService = $commonService;
        $this->bookRepository = $bookRepository;
    }

    #[Route('/api/cp/book/{id}/edit/mutation', name: 'api.cp.book.edit-mutation')]
    #[SerializeToJson]
    public function __invoke(Request $request, string $id): FormInterface|JsonResponse
    {
        $book = $this->bookRepository->find($id);
        $this->commonService->createNotFoundException($book);
        $command = BookCommand::create($book);
        $form = $this->createForm(BookType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->bookManagerUseCase->update($data, $book);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'redirect' => $this->generateUrl(
                    EditController::NAME,
                    ['id' => $book->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
