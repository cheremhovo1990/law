<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Commands\Cp\VideoCommand;
use App\Controller\Cp\Video\EditController;
use App\Form\Types\Cp\VideoType;
use App\Repository\VideoRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\VideoManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class VideoEditMutationController extends AbstractController
{
    private VideoManagerUseCase $videoManagerUseCase;
    private VideoRepository $videoRepository;
    private CommonService $commonService;

    public function __construct(
        VideoManagerUseCase $videoManagerUseCase,
        VideoRepository $videoRepository,
        CommonService $commonService,
    ) {
        $this->videoManagerUseCase = $videoManagerUseCase;
        $this->videoRepository = $videoRepository;
        $this->commonService = $commonService;
    }

    #[Route('/api/cp/video/{id}/edit/mutation', name: 'api.cp.video.edit-mutation', methods: 'post')]
    #[SerializeToJson]
    public function __invoke(Request $request, string $id): FormInterface|JsonResponse
    {
        $video = $this->videoRepository->find($id);
        $this->commonService->createNotFoundException($video);
        $command = VideoCommand::create($video);
        $form = $this->createForm(VideoType::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->videoManagerUseCase->update($data, $video);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'redirect' => $this->generateUrl(
                    EditController::NAME,
                    ['id' => $video->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
