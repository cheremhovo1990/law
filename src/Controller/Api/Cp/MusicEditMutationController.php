<?php

declare(strict_types=1);

namespace App\Controller\Api\Cp;

use App\Commands\Cp\MusicCommand;
use App\Controller\Cp\Music\EditController;
use App\Form\Types\Cp\MusicType;
use App\Repository\MusicRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\EntityManager\MusicManagerUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MusicEditMutationController extends AbstractController
{
    private CommonService $commonService;
    private MusicManagerUseCase $musicManagerUseCase;
    private MusicRepository $musicRepository;

    public function __construct(
        MusicManagerUseCase $musicManagerUseCase,
        MusicRepository $musicRepository,
        CommonService $commonService,
    ) {
        $this->commonService = $commonService;
        $this->musicManagerUseCase = $musicManagerUseCase;
        $this->musicRepository = $musicRepository;
    }

    #[Route('/api/cp/music/{id}/edit/mutation', name: 'api.cp.music.edit-mutation', methods: 'post')]
    #[SerializeToJson]
    public function __invoke(Request $request, string $id): FormInterface|JsonResponse
    {
        $video = $this->musicRepository->find($id);
        $this->commonService->createNotFoundException($video);
        $command = MusicCommand::create($video);
        $form = $this->createForm(MusicType::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->musicManagerUseCase->update($data, $video);
            $this->commonService->showMessageSuccess('Success');

            return $this->json([
                'success' => true,
                'message' => 'OK',
                'redirect' => $this->generateUrl(
                    EditController::NAME,
                    ['id' => $video->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ),
            ]);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
