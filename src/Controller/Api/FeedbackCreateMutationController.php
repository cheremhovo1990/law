<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Form\Types\FeedbackType;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use App\UseCase\Feedback\FeedbackUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FeedbackCreateMutationController extends AbstractController
{
    private FeedbackUseCase $feedbackUseCase;
    private CommonService $commonService;

    public function __construct(
        FeedbackUseCase $feedbackUseCase,
        CommonService $commonService,
    ) {
        $this->feedbackUseCase = $feedbackUseCase;
        $this->commonService = $commonService;
    }

    #[Route('/api/feedback/create/mutation', name: 'api.feedback-mutation', methods: 'post')]
    #[SerializeToJson]
    public function __invoke(Request $request): FormInterface|JsonResponse
    {
        $form = $this->createForm(FeedbackType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            $command->setClientIp($request->getClientIp());
            $this->feedbackUseCase->save($command);
            $this->commonService->showMessageSuccess('Success');

            return $this->json(['success' => true, 'message' => 'OK']);
        }

        $this->commonService->warning('Validation Failed');

        return $form;
    }
}
