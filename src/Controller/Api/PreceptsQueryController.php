<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\Precept;
use App\Repository\PreceptRepository;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PreceptsQueryController extends AbstractController
{
    /**
     * @return array<integer, Precept>
     */
    #[Route('/api/precepts', name: 'api.precepts', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(PreceptRepository $preceptRepository): array
    {
        return $preceptRepository->findAllWithOrderByPosition();
    }
}
