<?php

declare(strict_types=1);

namespace App\Controller\Api\Book;

use App\Entity\Book;
use App\Repository\BookRepository;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BooksQueryController extends AbstractController
{
    private BookRepository $bookRepository;

    public function __construct(
        BookRepository $bookRepository,
    ) {
        $this->bookRepository = $bookRepository;
    }

    /**
     * @return array<integer, Book>
     */
    #[Route('/api/books', name: 'api.books', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(): array
    {
        return $this->bookRepository->findAllByDateOfWritingWithOrder();
    }
}
