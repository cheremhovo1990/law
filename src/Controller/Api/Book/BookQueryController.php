<?php

declare(strict_types=1);

namespace App\Controller\Api\Book;

use App\Entity\Book;
use App\Repository\BookRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BookQueryController extends AbstractController
{
    private BookRepository $bookRepository;
    private CommonService $commonService;

    public function __construct(
        BookRepository $bookRepository,
        CommonService $commonService,
    ) {
        $this->bookRepository = $bookRepository;
        $this->commonService = $commonService;
    }

    #[Route('/api/book/{id}', name: 'api.book', methods: 'get')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(string $id): Book
    {
        $book = $this->bookRepository->findOneByPublishedAt($id);
        $this->commonService->createNotFoundException($book);

        return $book;
    }
}
