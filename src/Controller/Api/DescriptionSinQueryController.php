<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\DescriptionSin;
use App\Repository\DescriptionSinRepository;
use App\Services\CommonService;
use App\Services\Serialize\SerializeToJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DescriptionSinQueryController extends AbstractController
{
    private DescriptionSinRepository $descriptionSinRepository;
    private CommonService $commonService;

    public function __construct(
        DescriptionSinRepository $descriptionSinRepository,
        CommonService $commonService,
    ) {
        $this->descriptionSinRepository = $descriptionSinRepository;
        $this->commonService = $commonService;
    }

    #[Route('api/precept/{preceptId}/description/sin/{id}', name: 'api.description.sin')]
    #[SerializeToJson(
        groups: [
            self::class,
        ],
    )]
    public function __invoke(string $preceptId, string $id): DescriptionSin
    {
        $descriptionSin = $this->descriptionSinRepository->findOneByPreceptAndId($preceptId, $id);
        $this->commonService->createNotFoundException($descriptionSin);

        return $descriptionSin;
    }
}
