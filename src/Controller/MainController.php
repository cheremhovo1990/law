<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\MetaKeyEnum;
use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    public const NAME = 'main';
    public const NAME_CHURCH_SLAVONIC = 'main.church_slavonic';
    private CommonService $commonService;

    public function __construct(
        CommonService $commonService,
    ) {
        $this->commonService = $commonService;
    }

    #[Route('/', name: self::NAME)]
    #[Route('/cs', name: self::NAME_CHURCH_SLAVONIC)]
    public function __invoke(): Response
    {
        $meta = $this->commonService->findOneMetaByKey(MetaKeyEnum::MAIN->value);

        return $this->render(
            'main.html.twig',
            [
                'meta' => $meta,
            ],
        );
    }
}
