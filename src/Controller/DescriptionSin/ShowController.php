<?php

declare(strict_types=1);

namespace App\Controller\DescriptionSin;

use App\Repository\DescriptionSinRepository;
use App\Services\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShowController extends AbstractController
{
    public const NAME = 'description.sin';
    private DescriptionSinRepository $descriptionSinRepository;
    private CommonService $commonService;

    public function __construct(
        DescriptionSinRepository $descriptionSinRepository,
        CommonService $commonService,
    ) {
        $this->descriptionSinRepository = $descriptionSinRepository;
        $this->commonService = $commonService;
    }

    #[Route('precept/{preceptId}/description/sin/{id}', name: self::NAME, options: ['expose' => true])]
    public function __invoke(string $preceptId, string $id): Response
    {
        $descriptionSin = $this->descriptionSinRepository->findOneByPreceptAndId($preceptId, $id);
        $this->commonService->createNotFoundException($descriptionSin);

        return $this->render(
            'descriptionSin/show.html.twig',
        );
    }
}
