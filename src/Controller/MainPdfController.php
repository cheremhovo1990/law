<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\PreceptRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainPdfController extends AbstractController
{
    public const NAME = 'main.pdf';

    #[Route('/main/pdf', name: self::NAME)]
    public function __invoke(PreceptRepository $preceptRepository): Response
    {
        return $this->render(
            'main.pdf.html.twig',
            [
                'precepts' => $preceptRepository->findAllWithOrderByPosition(),
            ],
        );
    }
}
