<?php

declare(strict_types=1);

namespace App\Helpers;

class SymbolHelper
{
    public static function convertToCharacter(string $unicode): string
    {
        return preg_replace_callback('~&#(?<code>\d+);~', function ($match) {
            return mb_chr((int) $match['code']);
        }, $unicode);
    }
}
