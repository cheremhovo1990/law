<?php

declare(strict_types=1);

namespace App\Helpers;

enum MimeTypeResponseEnumHelper: string
{
    case ACCEPT = 'application/json';
    case CONTENT_TYPE = 'text/html; charset=UTF-8';
}
