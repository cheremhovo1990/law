<?php

declare(strict_types=1);

namespace App\Helpers;

enum MimeTypeRequestEnumHelper: string
{
    case CONTENT_TYPE = 'application/json';
}
