<?php

declare(strict_types=1);

namespace App\Command;

use App\Services\SitemapService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:create:sitemap')]
class CreateSitemapCommand extends Command
{
    private SitemapService $sitemapService;

    public function __construct(
        SitemapService $sitemapService,
        string $name = null,
    ) {
        parent::__construct($name);
        $this->sitemapService = $sitemapService;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->sitemapService->run();
        $output->writeln('success');

        return Command::SUCCESS;
    }
}
