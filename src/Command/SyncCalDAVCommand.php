<?php

declare(strict_types=1);

namespace App\Command;

use App\UseCase\EntityManager\CalendarEventManagerUseCase;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:sync:caldav')]
class SyncCalDAVCommand extends Command
{
    private CalendarEventManagerUseCase $calendarEventManagerUseCase;

    public function __construct(
        CalendarEventManagerUseCase $calendarEventManagerUseCase,
        string $name = null,
    ) {
        parent::__construct($name);
        $this->calendarEventManagerUseCase = $calendarEventManagerUseCase;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->calendarEventManagerUseCase->init();
        $this->calendarEventManagerUseCase->setOutput($output);
        $this->calendarEventManagerUseCase->create();
        $output->writeln('success');

        return Command::SUCCESS;
    }
}
