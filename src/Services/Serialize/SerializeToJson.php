<?php

declare(strict_types=1);

namespace App\Services\Serialize;

#[\Attribute(\Attribute::TARGET_METHOD)]
class SerializeToJson
{
    /**
     * @param array<integer, string>|null $groups
     */
    public function __construct(
        protected null|array $groups = null,
    ) {
    }

    /**
     * @return array<integer, string>|null
     */
    public function getGroups(): null|array
    {
        return $this->groups;
    }
}
