<?php

declare(strict_types=1);

namespace App\Services\Serialize;

class SerializeRuntimeException extends \RuntimeException
{
}
