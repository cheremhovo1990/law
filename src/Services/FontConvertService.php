<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\NotFoundCharacterException;
use App\Repository\ViewFromSymbolToSymbolRepository;

class FontConvertService
{
    private ViewFromSymbolToSymbolRepository $viewFromSymbolToSymbolRepository;
    public string $output;

    public function __construct(
        ViewFromSymbolToSymbolRepository $viewFromSymbolToSymbolRepository,
    ) {
        $this->viewFromSymbolToSymbolRepository = $viewFromSymbolToSymbolRepository;
    }

    public function canConvert(string $result, string $fontFrom, string $fontTo): bool
    {
        $from = mb_str_split($result);
        $queue = new \SplQueue();
        foreach ($from as $symbol) {
            $queue->enqueue($symbol);
        }
        while ($queue->count()) {
            $character = $queue->dequeue();
            $begin = $character;
            if (in_array(mb_ord($character), [32, 46, 45])) {
                continue;
            }
            $symbols = $this->viewFromSymbolToSymbolRepository->findAllByLikeCharacterAndFont($character, $fontFrom, $fontTo);
            $tmpQueue = clone $queue;
            while (count($symbols) > 1) {
                if ($tmpQueue->count()) {
                    $character .= $tmpQueue->dequeue();
                    $symbols = $this->viewFromSymbolToSymbolRepository->findAllByLikeCharacterAndFont($character, $fontFrom, $fontTo);
                    if (1 === count($symbols)) {
                        $queue = clone $tmpQueue;
                    }
                } else {
                    break;
                }
            }
            if (0 === count($symbols)) {
                $symbols = $this->viewFromSymbolToSymbolRepository->findAllByLikeCharacterAndFont($begin, $fontFrom, $fontTo, '%s');
                if (0 === count($symbols)) {
                    throw NotFoundCharacterException::create($character);
                }
            }
        }

        return true;
    }

    public function convert(string $result, string $fontFrom, string $fontTo): string
    {
        $this->output = '';
        $this->output .= sprintf('input: %s' . PHP_EOL, $result);
        $symbols = $this->viewFromSymbolToSymbolRepository->findAllByFont($fontFrom, $fontTo);
        $charters = mb_str_split($result);
        foreach ($charters as &$charter) {
            $exists = '';
            foreach ($symbols as $symbol) {
                $unicodeFrom = $symbol->getCharacterFrom();
                $unicodeFrom = preg_quote($unicodeFrom);
                $pattern = sprintf('~(?<char>%s)~', $unicodeFrom);
                if (1 === preg_match($pattern, $exists, $match)) {
                    continue;
                }
                $charter = preg_replace_callback($pattern, function () use ($symbol, &$exists) {
                    $exists .= $symbol->getCharacterTo();
                    $this->output .= sprintf('input: "%s" - %s, output: "%s" - %s' . PHP_EOL, $symbol->getCharacterFrom(), $symbol->getUnicodeFrom(), $symbol->getCharacterTo(), $symbol->getUnicodeTo());

                    return $symbol->getCharacterTo();
                }, $charter);
            }
        }
        $result = implode('', $charters);
        $this->output .= sprintf('result: %s' . PHP_EOL, $result);

        return $result;
    }
}
