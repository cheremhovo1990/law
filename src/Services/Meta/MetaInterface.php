<?php

declare(strict_types=1);

namespace App\Services\Meta;

use App\Doctrine\Models\Meta;

interface MetaInterface
{
    public function setMeta(Meta $meta): static;

    public function getMeta(): Meta;
}
