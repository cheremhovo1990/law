<?php

declare(strict_types=1);

namespace App\Services\Meta;

interface MetaCommandInterface
{
    /**
     * @return array<string, string>
     */
    public function getMeta(): array;
}
