<?php

declare(strict_types=1);

namespace App\Services\Meta;

use App\Controller\Api\Book\BookQueryController;
use App\Controller\Api\Cp\BookQueryController as BookQueryControllerCp;
use App\Controller\Api\Cp\DescriptionSinQueryController as CpDescriptionSinQueryController;
use App\Controller\Api\Cp\DescriptionSinsQueryController;
use App\Controller\Api\Cp\MusicOneQueryController;
use App\Controller\Api\Cp\MusicQueryController;
use App\Controller\Api\Cp\PageQueryController;
use App\Controller\Api\Cp\PagesQueryController;
use App\Controller\Api\Cp\VideoOneQueryController;
use App\Controller\Api\DescriptionSinQueryController;
use App\Controller\Api\PreceptsQueryController;
use App\Doctrine\Models\Meta;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

trait MetaTrait
{
    #[ORM\Column(type: \App\Doctrine\Types\Meta::NAME, nullable: true, options: ['default' => '[]', 'jsonb' => true])]
    #[Groups([
        VideoOneQueryController::class,
        MusicQueryController::class,
        MusicOneQueryController::class,
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        DescriptionSinsQueryController::class,
        CpDescriptionSinQueryController::class,
        VideoOneQueryController::class,
        BookQueryControllerCp::class,
        BookQueryController::class,
        PageQueryController::class,
        PagesQueryController::class,
    ])]
    protected Meta $meta;

    public function getMeta(): Meta
    {
        return $this->meta;
    }

    public function setMeta(Meta $meta): static
    {
        $this->meta = $meta;

        return $this;
    }
}
