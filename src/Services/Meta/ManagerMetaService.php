<?php

declare(strict_types=1);

namespace App\Services\Meta;

use App\Doctrine\Models\Meta;

class ManagerMetaService
{
    public function add(MetaInterface $entity, MetaCommandInterface $command): void
    {
        $meta = $command->getMeta();
        $entity->setMeta(new Meta($meta['title'], $meta['description']));
    }

    public function update(MetaInterface $entity, MetaCommandInterface $command): void
    {
        $meta = $command->getMeta();
        if (
            $entity->getMeta()->getTitle() !== $meta['title']
            || $entity->getMeta()->getDescription() !== $meta['description']
        ) {
            $entity->setMeta(new Meta($meta['title'], $meta['description']));
        }
    }
}
