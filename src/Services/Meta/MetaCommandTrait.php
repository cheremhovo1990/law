<?php

declare(strict_types=1);

namespace App\Services\Meta;

trait MetaCommandTrait
{
    /**
     * @var array<string, string>
     */
    protected array $meta = [];

    /**
     * @return array<string, string>
     */
    public function getMeta(): array
    {
        return $this->meta;
    }

    /**
     * @param array<string, string> $meta
     */
    public function setMeta(array $meta): self
    {
        $this->meta = $meta;

        return $this;
    }
}
