<?php

declare(strict_types=1);

namespace App\Services\Pinba;

class PinbaService
{
    public static function init(): void
    {
        if (extension_loaded('pinba')) {
            /* @noinspection PhpUndefinedFunctionInspection */
            /* @phpstan-ignore-next-line */
            pinba_server_name_set($_SERVER['HTTP_HOST']);
            /* @noinspection PhpUndefinedFunctionInspection */
            /* @phpstan-ignore-next-line */
            pinba_script_name_set($_SERVER['DOCUMENT_URI']);
        }
    }

    public function extensionLoaded(): bool
    {
        return extension_loaded('pinba');
    }

    public function getExtensionFuncs(): mixed
    {
        return get_extension_funcs('pinba');
    }

    public function timerStart(mixed $arg): mixed
    {
        /* @noinspection PhpUndefinedFunctionInspection */
        /* @phpstan-ignore-next-line */
        return pinba_timer_start($arg);
    }

    public function timerStop(mixed $timer): void
    {
        /* @noinspection PhpUndefinedFunctionInspection */
        /* @phpstan-ignore-next-line */
        pinba_timer_stop($timer);
    }

    public function isTimerStarted(mixed $timer): bool
    {
        /* @noinspection PhpUndefinedFunctionInspection */
        /* @phpstan-ignore-next-line */
        return pinba_timer_get_info($timer)['started'];
    }
}
