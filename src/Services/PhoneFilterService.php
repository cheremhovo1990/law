<?php

declare(strict_types=1);

namespace App\Services;

class PhoneFilterService
{
    public function normalize(string $phone): string
    {
        $first = '';
        if ('+' == $phone[0]) {
            $first = '+';
            $phone = substr($phone, 1);
        }
        $phone = preg_replace('~\D~', '', $phone);

        return $first . $phone;
    }
}
