<?php

declare(strict_types=1);

namespace App\Services\Google;

use App\Exceptions\FailedVerificationRuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RecapthcaService
{
    private HttpClientInterface $client;
    private ParameterBagInterface $parameterBag;

    public function __construct(
        HttpClientInterface $client,
        ParameterBagInterface $parameterBag,
    ) {
        $this->client = $client;
        $this->parameterBag = $parameterBag;
    }

    public function verify(string $token = null): bool
    {
        if (null === $token) {
            throw FailedVerificationRuntimeException::verifyNull();
        }
        $body = [
            'secret' => $this->parameterBag->get('captchaSecretKey'),
            'response' => $token,
        ];
        $response = $this->client->request('POST', 'recaptcha/api/siteverify', [
            'body' => $body,
        ]);
        $content = $response->toArray();
        if (($content['success'] ?? false) === false) {
            throw FailedVerificationRuntimeException::notVerify();
        }

        return true;
    }

    public function setClient(HttpClientInterface $client): void
    {
        $this->client = $client;
    }
}
