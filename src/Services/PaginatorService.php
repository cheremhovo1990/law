<?php

declare(strict_types=1);

namespace App\Services;

use App\Controller\Api\Cp\DictionariesQueryController;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @implements \IteratorAggregate<integer, mixed>
 */
class PaginatorService implements \Countable, \IteratorAggregate, PaginatorInterface
{
    /**
     * @var Paginator<mixed>
     */
    protected Paginator $paginator;

    public function __construct(
        private QueryBuilder $query,
        private Request $request,
    ) {
        $query
            ->setFirstResult($this->getFirstResult())
            ->setMaxResults($this->getLimit())
        ;

        $this->paginator = new Paginator($this->query);
    }

    public function getPage(): int
    {
        return $this->request->query->getInt('page', 1);
    }

    #[Groups([
        DictionariesQueryController::class,
    ])]
    public function getLimit(): int
    {
        return $this->request->query->getInt('limit', 100);
    }

    public function getFirstResult(): int
    {
        if (1 === $this->getPage()) {
            $firstResult = 0;
        } else {
            $firstResult = $this->getLimit() * ($this->getPage() - 1);
        }

        return $firstResult;
    }

    public function getTotal(): int
    {
        return count($this->paginator);
    }

    public function getMaxPage(): int
    {
        return (int) round($this->getTotal() / $this->getLimit());
    }

    public function havePagination(): bool
    {
        return $this->getMaxPage() > 1;
    }

    /**
     * @return Paginator<mixed>
     */
    public function getPaginator(): Paginator
    {
        return $this->paginator;
    }

    public function count(): int
    {
        return count($this->paginator);
    }

    /**
     * @return \ArrayIterator<integer, mixed>
     *
     * @throws \Exception
     */
    public function getIterator(): \ArrayIterator
    {
        return $this->paginator->getIterator();
    }
}
