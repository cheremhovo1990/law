<?php

declare(strict_types=1);

namespace App\Services\MonitoringSstatistics;

use App\Helpers\JsonHelper;
use Symfony\Component\Stopwatch\Stopwatch;

class MonitoringStatisticsService
{
    protected static string $host;
    protected static int $port;
    protected static Stopwatch $stopwatch;
    protected static float $startTime;

    public static function init(
        string $host,
        int $port,
    ): void {
        static::$host = $host;
        static::$port = $port;
        static::$startTime = microtime(true);
    }

    public static function send(): void
    {
        $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);

        $data = [];
        $data['server'] = $_SERVER['HOSTNAME'];
        $data['name'] = $_SERVER['HTTP_HOST'];
        $data['uri'] = $_SERVER['DOCUMENT_URI'];
        $data['startTime'] = self::$startTime;
        $data['endTime'] = microtime(true);
        $data['request_time'] = $data['endTime'] - self::$startTime;
        $data['status'] = http_response_code();
        $data['source'] = 'web';
        $data['memory_peak_usage'] = memory_get_peak_usage(true);
        $data['load_avg'] = sys_getloadavg();
        $sendData = JsonHelper::encode($data);
        socket_sendto($socket, $sendData, strlen($sendData), $flags = 0, static::$host, static::$port);
    }

    public static function setStopwatch(Stopwatch $stopwatch): void
    {
        self::$stopwatch = $stopwatch;
    }
}
