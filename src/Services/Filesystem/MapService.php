<?php

declare(strict_types=1);

namespace App\Services\Filesystem;

use League\Flysystem\FilesystemOperator;

class MapService
{
    private FilesystemOperator $bookStorage;

    public function __construct(
        FilesystemOperator $bookStorage,
    ) {
        $this->bookStorage = $bookStorage;
    }

    public function getBookStorage(): FilesystemOperator
    {
        return $this->bookStorage;
    }
}
