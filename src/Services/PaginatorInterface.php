<?php

declare(strict_types=1);

namespace App\Services;

interface PaginatorInterface
{
    public function getTotal(): int;

    public function getPage(): int;

    public function getMaxPage(): int;

    public function getLimit(): int;
}
