<?php

declare(strict_types=1);

namespace App\Services;

use App\Services\Filesystem\MapService;
use League\Flysystem\FilesystemOperator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageService
{
    protected FilesystemOperator $bookStorage;

    public function __construct(
        MapService $mapService,
    ) {
        $this->bookStorage = $mapService->getBookStorage();
    }

    public function write(UploadedFile $image): string
    {
        $name = sprintf('%s.%s', uniqid(), $image->getClientOriginalExtension());
        $this->bookStorage->write($name, $image->getContent());

        return $name;
    }
}
