<?php

declare(strict_types=1);

namespace App\Services\Identity;

use Symfony\Component\Uid\Uuid;

class IdentityService
{
    protected null|string $identity = null;

    public function setIdentity(string $identity): void
    {
        $this->identity = $identity;
    }

    public function getIdentity(): string
    {
        if (null === $this->identity) {
            $this->identity = Uuid::v4()->toRfc4122();
        }

        return $this->identity;
    }
}
