<?php

declare(strict_types=1);

namespace App\Services;

interface ToArrayInterface
{
    /**
     * @return array<integer|string, mixed>
     */
    public function toArray(): array;
}
