<?php

declare(strict_types=1);

namespace App\Services;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RequestContext;

class GenerateRouteService
{
    private UrlGeneratorInterface $router;

    public function __construct(
        UrlGeneratorInterface $router,
    ) {
        $this->router = $router;

        $context = new RequestContext();
        $context->setHost('ten-laws.ru');
        $context->setScheme('https');
        $this->router->setContext($context);
    }

    public function getRoute(): UrlGeneratorInterface
    {
        return $this->router;
    }
}
