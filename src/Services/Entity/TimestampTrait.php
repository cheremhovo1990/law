<?php

declare(strict_types=1);

namespace App\Services\Entity;

use Doctrine\ORM\Mapping as ORM;

trait TimestampTrait
{
    #[ORM\Column(type: 'datetimetz_immutable', nullable: false)]
    protected \DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetimetz_immutable', nullable: true)]
    protected ?\DateTimeImmutable $deletedAt;
}
