<?php

declare(strict_types=1);

namespace App\Services;

use App\Repository\SymbolMapRepository;

class DictionaryService
{
    private SymbolMapRepository $symbolMapRepository;

    public function __construct(
        SymbolMapRepository $symbolMapRepository,
    ) {
        $this->symbolMapRepository = $symbolMapRepository;
    }

    public function getGroup(string $string, string $font): string
    {
        if (0 === mb_strlen($string)) {
            return '';
        }
        $list = mb_str_split($string);
        $key = array_key_first($list);
        $first = $list[$key];
        $symbol = $this->symbolMapRepository->findOneByCharacterAndFontGroupNameNotNull($first, $font);
        if (null === $symbol) {
            return '';
        }

        return $symbol->getGroupName();
    }
}
