<?php

declare(strict_types=1);

namespace App\Services\Centrifugo;

class JWTTokenService
{
    private ClientService $clientService;

    public function __construct(
        ClientService $clientService,
    ) {
        $this->clientService = $clientService;
    }

    public function generateConnectionToken(string $userId): string
    {
        return $this->clientService->generateConnectionToken($userId);
    }
}
