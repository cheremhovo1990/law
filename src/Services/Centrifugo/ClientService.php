<?php

declare(strict_types=1);

namespace App\Services\Centrifugo;

use phpcent\Client;

class ClientService
{
    protected Client $client;

    public function __construct(
        string $host,
        string $apiKey,
        string $secretKey,
    ) {
        $this->client = new Client($host, $apiKey, $secretKey);
    }

    public function generateConnectionToken(string $userId): string
    {
        return $this->client->generateConnectionToken($userId);
    }

    /**
     * @param array<string, mixed> $data
     */
    public function publish(string $channel, array $data): void
    {
        $this->client->publish($channel, $data);
    }
}
