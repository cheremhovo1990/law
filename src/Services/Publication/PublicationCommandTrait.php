<?php

declare(strict_types=1);

namespace App\Services\Publication;

trait PublicationCommandTrait
{
    protected bool $publish = false;

    public function isPublish(): bool
    {
        return $this->publish;
    }

    public function setPublish(bool $publish): void
    {
        $this->publish = $publish;
    }
}
