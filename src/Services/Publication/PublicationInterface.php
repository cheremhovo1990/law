<?php

declare(strict_types=1);

namespace App\Services\Publication;

interface PublicationInterface
{
    public function isPublish(): bool;

    public function setPublishedAt(null|\DateTimeImmutable $dateTime): void;
}
