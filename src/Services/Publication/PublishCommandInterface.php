<?php

declare(strict_types=1);

namespace App\Services\Publication;

interface PublishCommandInterface
{
    public function isPublish(): bool;
}
