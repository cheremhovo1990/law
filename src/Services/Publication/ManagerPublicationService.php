<?php

declare(strict_types=1);

namespace App\Services\Publication;

class ManagerPublicationService
{
    public function toPublish(PublishCommandInterface $command, PublicationInterface $entity): void
    {
        if ($command->isPublish()) {
            if (!$entity->isPublish()) {
                $entity->setPublishedAt(new \DateTimeImmutable());
            }
        } else {
            $entity->setPublishedAt(null);
        }
    }
}
