<?php

declare(strict_types=1);

namespace App\Services\Publication;

use App\Controller\Api\Book\BooksQueryController;
use App\Controller\Api\Cp\BookQueryController as BookQueryControllerCp;
use App\Controller\Api\Cp\BooksQueryController as BooksQueryControllerCp;
use App\Controller\Api\Cp\DescriptionSinQueryController as CpDescriptionSinQueryController;
use App\Controller\Api\Cp\DictionariesQueryController;
use App\Controller\Api\Cp\DictionaryQueryController;
use App\Controller\Api\Cp\MusicOneQueryController;
use App\Controller\Api\Cp\MusicQueryController;
use App\Controller\Api\Cp\PageQueryController;
use App\Controller\Api\Cp\PagesQueryController;
use App\Controller\Api\Cp\SinQueryController;
use App\Controller\Api\Cp\SinsQueryController;
use App\Controller\Api\Cp\VideoOneQueryController;
use App\Controller\Api\Cp\VideoQueryController;
use App\Controller\Api\DescriptionSinQueryController;
use App\Controller\Api\PreceptsQueryController;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

trait PublicationTrait
{
    #[ORM\Column(type: 'datetimetz_immutable', nullable: true)]
    #[Groups([
        VideoQueryController::class,
        VideoOneQueryController::class,
        MusicQueryController::class,
        MusicOneQueryController::class,
        PreceptsQueryController::class,
        DescriptionSinQueryController::class,
        SinsQueryController::class,
        SinQueryController::class,
        CpDescriptionSinQueryController::class,
        VideoQueryController::class,
        VideoOneQueryController::class,
        DictionariesQueryController::class,
        BooksQueryControllerCp::class,
        BookQueryControllerCp::class,
        BooksQueryController::class,
        PageQueryController::class,
        PagesQueryController::class,
        DictionaryQueryController::class,
    ])]
    protected null|\DateTimeImmutable $publishedAt = null;

    public function isPublish(): bool
    {
        return $this->publishedAt instanceof \DateTimeImmutable;
    }

    public function getPublishedAt(): null|\DateTimeImmutable
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(null|\DateTimeImmutable $publishedAt): void
    {
        $this->publishedAt = $publishedAt;
    }
}
