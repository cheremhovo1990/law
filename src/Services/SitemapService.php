<?php

declare(strict_types=1);

namespace App\Services;

use App\Controller\Book;
use App\Controller\DescriptionSin\ShowController;
use App\Controller\MainController;
use App\Repository\BookRepository;
use App\Repository\DescriptionSinRepository;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SitemapService
{
    protected string $path;
    protected string $temporaryPath;
    protected string $file = 'sitemap.xml';
    protected string $temporaryFile = 'temporary-sitemap.xml';
    protected KernelInterface $kernel;
    private UrlGeneratorInterface $router;
    private BookRepository $bookRepository;
    private DescriptionSinRepository $descriptionSinRepository;

    public function __construct(
        protected string $dir,
        DescriptionSinRepository $descriptionSinRepository,
        KernelInterface $kernel,
        GenerateRouteService $generateRouteService,
        BookRepository $bookRepository,
    ) {
        $this->kernel = $kernel;
        $this->router = $generateRouteService->getRoute();
        $this->bookRepository = $bookRepository;
        $this->descriptionSinRepository = $descriptionSinRepository;
    }

    public function run(): void
    {
        if (!file_exists($this->dir)) {
            mkdir($this->dir, 0777, true);
        }
        $this->temporaryPath = sprintf('%s/%s', $this->dir, $this->temporaryFile);
        $this->path = sprintf('%s/%s', $this->dir, $this->file);
        $this->beginOfDocument();
        $this->body();
        $this->endOfDocument();
        copy($this->temporaryPath, $this->path);
    }

    protected function beginOfDocument(): void
    {
        file_put_contents($this->temporaryPath, '<?xml version="1.0" encoding="UTF-8"?>');
        file_put_contents($this->temporaryPath, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">', FILE_APPEND);
    }

    protected function body(): void
    {
        $this->mainPage();
        $this->descriptionSinPages();
        $this->bookPages();
    }

    protected function mainPage(): void
    {
        $url = <<<URL
   <url>
      <loc>{$this->router->generate(MainController::NAME, referenceType: UrlGeneratorInterface::ABSOLUTE_URL)}</loc>
   </url>
URL;
        file_put_contents($this->temporaryPath, $url, FILE_APPEND);
    }

    protected function bookPages(): void
    {
        $url = <<<URL
   <url>
      <loc>{$this->router->generate(Book\ListController::NAME, referenceType: UrlGeneratorInterface::ABSOLUTE_URL)}</loc>
   </url>
URL;
        file_put_contents($this->temporaryPath, $url, FILE_APPEND);

        $books = $this->bookRepository->findAllByDateOfWritingWithOrder();
        foreach ($books as $book) {
            $url = <<<URL
   <url>
      <loc>{$this->router->generate(Book\ShowController::NAME, ['id' => $book->getId()], referenceType: UrlGeneratorInterface::ABSOLUTE_URL)}</loc>
   </url>
URL;
            file_put_contents($this->temporaryPath, $url, FILE_APPEND);
        }
    }

    protected function descriptionSinPages(): void
    {
        $descriptionSins = $this->descriptionSinRepository->findAll();

        foreach ($descriptionSins as $descriptionSin) {
            $url = <<<URL
   <url>
      <loc>{$this->router->generate(ShowController::NAME, ['preceptId' => $descriptionSin->getPrecept()->getId(), 'id' => $descriptionSin->getId()], UrlGeneratorInterface::ABSOLUTE_URL)}</loc>
   </url>
URL;
            file_put_contents($this->temporaryPath, $url, FILE_APPEND);
        }
    }

    protected function endOfDocument(): void
    {
        file_put_contents($this->temporaryPath, '</urlset>', FILE_APPEND);
    }
}
