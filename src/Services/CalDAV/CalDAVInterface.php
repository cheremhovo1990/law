<?php

declare(strict_types=1);

namespace App\Services\CalDAV;

use Carbon\CarbonImmutable;

interface CalDAVInterface
{
    public function connect(string $url, string $user, string $password, string $calendar = null): void;

    /**
     * @return array<integer, mixed>
     */
    public function findCalendars(): array;

    public function setCalendar(CalDAVCalendar $calendar): void;

    /**
     * @return array<integer, mixed>
     */
    public function getEvents(CarbonImmutable $start, CarbonImmutable $end): array;
}
