<?php

declare(strict_types=1);

namespace App\Services\CalDAV;

use it\thecsea\simple_caldav_client\CalDAVCalendar as Calendar;

class CalDAVCalendar implements CalDAVCalendarInterface
{
    protected Calendar $calendar;

    public function __construct(Calendar $calendar)
    {
        $this->calendar = $calendar;
    }

    public function getCalendar(): Calendar
    {
        return $this->calendar;
    }
}
