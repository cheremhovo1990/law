<?php

declare(strict_types=1);

namespace App\Services\CalDAV;

use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;

class CalDAVRule
{
    public const WEEKLY = 'weekly';
    public const MONTHLY = 'monthly';

    /**
     * @var array<integer, mixed>
     */
    protected array $byDay = [];
    public const DAY_OF_WEEK_MAP = [
        'mon' => 'mo',
        'tue' => 'tu',
        'wed' => 'we',
        'thu' => 'th',
        'fri' => 'fr',
        'sat' => 'sa',
        'sun' => 'su',
    ];

    public function __construct(
        protected string $freg,
        protected string $raw,
        protected null|CarbonImmutable $until = null,
        protected null|string $byDayRaw = null,
        protected null|string $byMonthDayRaw = null,
    ) {
        if ($this->isWeekly() && !empty($this->byDayRaw)) {
            $this->byDay = array_map(
                'strtolower',
                array_map(
                    'trim',
                    explode(',', $this->byDayRaw),
                ),
            );
        }
    }

    public function haveDay(string $day): bool
    {
        return in_array(self::DAY_OF_WEEK_MAP[strtolower($day)], $this->byDay);
    }

    public function getRaw(): string
    {
        return $this->raw;
    }

    public function getUntil(): ?CarbonImmutable
    {
        return $this->until;
    }

    public function isDayOfWeekEqual(CarbonInterface $day, \DateTimeZone $timezone): bool
    {
        return $this->haveDay((clone $day)->setTimezone($timezone)->format('D'));
    }

    public function isWeekly(): bool
    {
        return self::WEEKLY === $this->freg;
    }

    public function isMonthly(): bool
    {
        return self::MONTHLY === $this->freg;
    }
}
