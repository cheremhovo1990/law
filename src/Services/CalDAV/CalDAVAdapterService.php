<?php

declare(strict_types=1);

namespace App\Services\CalDAV;

use Carbon\CarbonImmutable;
use it\thecsea\simple_caldav_client\CalDAVCalendar as Calendar;
use it\thecsea\simple_caldav_client\SimpleCalDAVClient;

class CalDAVAdapterService implements CalDAVInterface
{
    protected SimpleCalDAVClient $simpleCalDAVClient;
    protected ?string $calendar = null;

    public function __construct()
    {
        $this->simpleCalDAVClient = new SimpleCalDAVClient();
    }

    public function connect(string $url, string $user, string $password, string $calendar = null): void
    {
        $this->calendar = $calendar;
        try {
            $this->simpleCalDAVClient->connect($url, $user, $password);
            if (null !== $calendar) {
                $calendars = $this->findCalendars();
                if (!isset($calendars[$calendar])) {
                    throw new CalDAVRuntimeException(sprintf('calendar not found: %s', $calendar));
                }
                $this->setCalendar($calendars[$calendar]);
            }
        } catch (\Throwable $exception) {
            throw new CalDAVRuntimeException($exception->getMessage());
        }
    }

    /**
     * @return CalDAVCalendar[]
     */
    public function findCalendars(): array
    {
        $result = [];
        try {
            $calendars = $this->simpleCalDAVClient->findCalendars();
        } catch (\Throwable $exception) {
            throw new CalDAVRuntimeException($exception->getMessage());
        }
        foreach ($calendars as $key => $calendar) {
            $result[$key] = new CalDAVCalendar($calendar);
        }

        return $result;
    }

    public function setCalendar(CalDAVCalendar $calendar): void
    {
        if (!$calendar->getCalendar() instanceof Calendar) {
            throw new CalDAVRuntimeException('calendar is not a SimpleCaldavCLient calendar');
        }
        try {
            $this->simpleCalDAVClient->setCalendar($calendar->getCalendar());
        } catch (\Throwable $exception) {
            throw new CalDAVRuntimeException($exception->getMessage());
        }
    }

    /**
     * @return array|CalDAVEvent[]
     */
    public function getEvents(CarbonImmutable $start, CarbonImmutable $end): array
    {
        $result = [];
        try {
            $events = $this->simpleCalDAVClient->getEvents($start->format('Ymd\THis\Z'), $end->format('Ymd\THis\Z'));
        } catch (\Throwable $exception) {
            throw new CalDAVRuntimeException($exception->getMessage());
        }
        foreach ($events as $event) {
            $result[] = new CalDAVEvent($event, $start, $end);
        }

        return $result;
    }
}
