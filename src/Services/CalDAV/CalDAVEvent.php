<?php

declare(strict_types=1);

namespace App\Services\CalDAV;

use Carbon\CarbonImmutable;
use it\thecsea\simple_caldav_client\CalDAVObject;

class CalDAVEvent implements CalDAVEventInterface
{
    protected CalDAVObject $event;
    protected CalDAVEventHandleService $calDAVEventHandleService;
    protected \DateTimeZone $timeZone;
    protected ?CalDAVRule $rule = null;
    protected CarbonImmutable $start;
    protected CarbonImmutable $end;

    public function __construct(CalDAVObject $event, CarbonImmutable $start, CarbonImmutable $end)
    {
        $this->event = $event;
        $this->calDAVEventHandleService = new CalDAVEventHandleService();
        $this->timeZone = $this->calDAVEventHandleService->getTimeZone($this);
        $this->start = $start;
        $this->end = $end;
    }

    public function getEvent(): CalDAVObject
    {
        return $this->event;
    }

    public function getData(): string
    {
        return $this->event->getData();
    }

    public function getDateTimeStart(): CarbonImmutable
    {
        return $this->calDAVEventHandleService->getDateTimeStart($this, $this->getTimeZone());
    }

    public function getDateTimeEnd(): CarbonImmutable
    {
        return $this->calDAVEventHandleService->getDateTimeEnd($this, $this->getTimeZone());
    }

    public function getUid(): string
    {
        return $this->calDAVEventHandleService->getUid($this);
    }

    public function getRule(): ?CalDAVRule
    {
        if (null === $this->rule) {
            $this->rule = $this->calDAVEventHandleService->getRule($this, $this->timeZone);
        }

        return $this->rule;
    }

    public function getSummary(): string
    {
        return $this->calDAVEventHandleService->getSummary($this);
    }

    public function getDescription(): null|string
    {
        return $this->calDAVEventHandleService->getDescription($this);
    }

    public function getTimeZone(): \DateTimeZone
    {
        return $this->timeZone;
    }

    public function isWeekly(): bool
    {
        return null !== $this->getRule() && $this->rule->isWeekly();
    }

    public function isMonthly(): bool
    {
        return null !== $this->getRule() && $this->rule->isMonthly();
    }

    public function getUntil(): CarbonImmutable
    {
        $until = $this->end;
        if (null !== $this->getRule()) {
            return $until;
        }
        if (null !== $this->rule->getUntil()) {
            $until = $this->rule->getUntil();
        }

        return $until;
    }

    public function getRaw(): null|string
    {
        if (null == $this->getRule()) {
            return null;
        }

        return $this->rule->getRaw();
    }
}
