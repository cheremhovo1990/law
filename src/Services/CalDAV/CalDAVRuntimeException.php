<?php

declare(strict_types=1);

namespace App\Services\CalDAV;

class CalDAVRuntimeException extends \RuntimeException
{
}
