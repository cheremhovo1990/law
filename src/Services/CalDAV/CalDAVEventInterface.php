<?php

declare(strict_types=1);

namespace App\Services\CalDAV;

use it\thecsea\simple_caldav_client\CalDAVObject;

interface CalDAVEventInterface
{
    public function getEvent(): CalDAVObject;

    public function getData(): string;
}
