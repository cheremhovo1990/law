<?php

declare(strict_types=1);

namespace App\Services\CalDAV;

use it\thecsea\simple_caldav_client\CalDAVCalendar as Calendar;

interface CalDAVCalendarInterface
{
    public function getCalendar(): Calendar;
}
