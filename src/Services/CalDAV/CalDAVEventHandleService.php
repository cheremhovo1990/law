<?php

declare(strict_types=1);

namespace App\Services\CalDAV;

use Carbon\CarbonImmutable;

class CalDAVEventHandleService
{
    public function getDateTimeStart(CalDAVEventInterface $event, \DateTimeZone $timeZone = null): CarbonImmutable
    {
        $pattern = '~^DTSTART;(?<start>.*)$~m';
        if (1 !== preg_match($pattern, $event->getData(), $match)) {
            throw new CalDAVRuntimeException();
        }

        return $this->parseDateTime($match['start'], $timeZone);
    }

    public function getDateTimeEnd(CalDAVEventInterface $event, \DateTimeZone $timeZone = null): CarbonImmutable
    {
        $pattern = '~^DTEND;(?<end>.*)$~m';
        if (1 !== preg_match($pattern, $event->getData(), $match)) {
            throw new CalDAVRuntimeException();
        }

        return $this->parseDateTime($match['end'], $timeZone);
    }

    public function getUid(CalDAVEventInterface $event): string
    {
        $pattern = '~^UID:(?<uid>.*)~m';
        if (1 !== preg_match($pattern, $event->getData(), $match)) {
            throw new CalDAVRuntimeException();
        }

        return $match['uid'];
    }

    public function getRule(CalDAVEventInterface $event, \DateTimeZone $timeZone): ?CalDAVRule
    {
        $rule = $this->getWeeklyRule($event, $timeZone);
        if (null !== $rule) {
            return $rule;
        }

        return $this->getMonthlyRule($event, $timeZone);
    }

    protected function getWeeklyRule(CalDAVEventInterface $event, \DateTimeZone $timeZone): ?CalDAVRule
    {
        $pattern = '~^RRULE:FREQ=WEEKLY;(BYDAY=(?<byday>.+);)?(UNTIL=(?<until>.+);|)INTERVAL=(.*)$~m';
        if (1 === preg_match($pattern, $event->getData(), $match)) {
            $until = null;
            if (!empty($match['until'])) {
                $until = CarbonImmutable::createFromFormat('Ymd\THis\Z', $match['until'], 'UTC')->setTimezone($timeZone);
            }

            return new CalDAVRule(
                freg: 'weekly',
                raw: $match[0],
                until: $until,
                byDayRaw: $match['byday'],
            );
        }

        return null;
    }

    protected function getMonthlyRule(CalDAVEventInterface $event, \DateTimeZone $timeZone): ?CalDAVRule
    {
        $pattern = '~^RRULE:FREQ=MONTHLY;?(BYMONTHDAY=(?<bymonthday>.+);|)?(BYDAY=(?<byday>.+);|)?(UNTIL=(?<until>.+);|)INTERVAL=(.+)$~m';
        if (1 === preg_match($pattern, $event->getData(), $match)) {
            $until = null;
            if (!empty($match['until'])) {
                $until = CarbonImmutable::createFromFormat('Ymd', $match['until'], 'UTC')->setTimezone($timeZone);
            }

            return new CalDAVRule(
                freg: 'monthly',
                raw: $match[0],
                until: $until,
                byDayRaw: $match['byday'] ?? null,
                byMonthDayRaw: $match['bymonthday'] ?? null,
            );
        }

        return null;
    }

    public function getSummary(CalDAVEvent $event): string
    {
        if (1 !== preg_match('~^SUMMARY:(?<summary>.+)$~m', $event->getData(), $match)) {
            throw new CalDAVRuntimeException('SUMMARY not found');
        }

        return $match['summary'];
    }

    public function getDescription(CalDAVEvent $event): null|string
    {
        if (preg_match('~^DESCRIPTION:(?<description>.+)$~m', $event->getData(), $match)) {
            return $match['description'];
        }

        return null;
    }

    public function getTimeZone(CalDAVEvent $event): \DateTimeZone
    {
        if (preg_match('~^TZID:(?<timezone>.+)$~m', $event->getData(), $match)) {
            return new \DateTimeZone($match['timezone']);
        }

        throw new CalDAVRuntimeException();
    }

    protected function parseDateTime(string $raw, \DateTimeZone $timeZone = null): CarbonImmutable
    {
        if (1 == preg_match('~^TZID=(?<timezone>.+):(?<datetime>.+)$~m', $raw, $match)) {
            return CarbonImmutable::createFromFormat('Ymd\THis', $match['datetime'], new \DateTimeZone($match['timezone']));
        } elseif (1 == preg_match('~^VALUE=DATE:(?<date>.+)$~m', $raw, $match)) {
            return CarbonImmutable::createFromFormat('Ymd\THis', $match['date'] . 'T000000', $timeZone);
        } else {
            throw new CalDAVRuntimeException('failed parse datetime');
        }
    }
}
