<?php

declare(strict_types=1);

namespace App\Services\Schedule;

use App\Entity\ScheduleEvent;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;

class Period
{
    protected CarbonImmutable $start;

    protected CarbonImmutable $end;

    /**
     * @var array|Event[]
     */
    protected array $events = [];

    public function __construct(
        CarbonInterface $start,
        protected \DateTimeZone $timeZone,
    ) {
        $this->start = (new CarbonImmutable($start))->setTime(00, 00, 00);
        $this->setEnd($this->start);
    }

    public function resolve(): void
    {
        usort($this->events, function (Event $a, Event $b) {
            $a = strtotime($a->getTime());
            $b = strtotime($b->getTime());

            return $a - $b;
        });
    }

    public function addEvent(ScheduleEvent $event): self
    {
        $this->events[spl_object_hash($event)] = new Event($event, $this->timeZone);

        return $this;
    }

    public function exists(ScheduleEvent $event): bool
    {
        return isset($this->events[spl_object_hash($event)]);
    }

    public function getStartAsString(): string
    {
        return $this->start->format(\DateTime::ATOM);
    }

    public function getEndAsString(): string
    {
        return $this->end->format(\DateTime::ATOM);
    }

    public function getStartWeek(): CarbonImmutable
    {
        return $this->start->endOfWeek();
    }

    public function getDurationToEndWeek(): int
    {
        return $this->getStartWeek()->addMinute()->diffInDays($this->start);
    }

    public function getDuration(): int
    {
        return $this->end->addMinute()->diffInDays($this->start);
    }

    public function getStart(): CarbonImmutable
    {
        return $this->start;
    }

    public function getEnd(): CarbonImmutable
    {
        return $this->end;
    }

    public function setEnd(CarbonImmutable $end): self
    {
        $this->end = $end->setTime(23, 59, 59);

        return $this;
    }

    /**
     * @return array<integer, Event>
     */
    public function getEvents(): array
    {
        return $this->events;
    }

    public function canAddEvent(CarbonImmutable $start, CarbonImmutable $end): bool
    {
        return
            ($start->greaterThanOrEqualTo($this->start) && $start->lessThanOrEqualTo($this->end))
            || ($end->greaterThanOrEqualTo($this->start) && $end->lessThanOrEqualTo($this->end))
            || ($this->end->lessThanOrEqualTo($end) && $this->start->lessThanOrEqualTo($end) && $this->start->greaterThanOrEqualTo($start));
    }
}
