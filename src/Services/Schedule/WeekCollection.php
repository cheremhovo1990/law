<?php

declare(strict_types=1);

namespace App\Services\Schedule;

/**
 * @implements \IteratorAggregate<integer, WeekRow>
 */
class WeekCollection implements \IteratorAggregate
{
    /**
     * @var array<integer, WeekRow>
     */
    protected array $elements = [];

    public function add(WeekRow $weekRow): self
    {
        $this->elements[] = $weekRow;

        return $this;
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->elements);
    }

    public function getWeeksInMonth(): int
    {
        return count($this->elements);
    }

    /**
     * @return array<string, mixed>
     */
    public function resolve(): array
    {
        foreach ($this as $row) {
            $row->resolve();
        }

        return [
            'data' => $this,
            'weeksInMonth' => $this->getWeeksInMonth(),
        ];
    }
}
