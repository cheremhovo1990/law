<?php

declare(strict_types=1);

namespace App\Services\Schedule;

class WeekRow
{
    protected \DateTimeImmutable|null $skipTo = null;

    /** @var array|Period[] */
    protected array $periods;

    public function addPeriod(Period $period): self
    {
        $this->periods[] = $period;

        return $this;
    }

    public function resolve(): self
    {
        foreach ($this->periods as $period) {
            $period->resolve();
        }

        return $this;
    }

    public function getCountPeriods(): int
    {
        return count($this->periods);
    }

    public function getSkipTo(): \DateTimeImmutable|null
    {
        return $this->skipTo;
    }

    public function setSkipTo(?\DateTimeImmutable $skipTo): self
    {
        $this->skipTo = $skipTo;

        return $this;
    }

    /**
     * @return array<integer, Period>
     */
    public function getPeriods(): array
    {
        return $this->periods;
    }
}
