<?php

declare(strict_types=1);

namespace App\Services\Schedule;

use App\Entity\ScheduleEvent;

class Event
{
    public function __construct(
        protected ScheduleEvent $event,
        protected \DateTimeZone $timeZone,
    ) {
    }

    public function getId(): string
    {
        return (string) $this->event->getId();
    }

    public function getTime(): string
    {
        return $this->event->getStartDateTime()->setTimezone($this->timeZone)->format('H:i');
    }

    public function getSummary(): string
    {
        return $this->event->getSummary();
    }

    public function getStartDateTime(): string
    {
        return $this->event->getStartDateTime()->setTimezone($this->timeZone)->format(\DateTime::ATOM);
    }

    public function getEndDateTime(): string
    {
        return $this->event->getEndDateTime()->setTimezone($this->timeZone)->format(\DateTime::ATOM);
    }
}
