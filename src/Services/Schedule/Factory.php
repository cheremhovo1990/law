<?php

declare(strict_types=1);

namespace App\Services\Schedule;

use App\Entity\ScheduleEvent;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;
use Carbon\CarbonPeriod;

class Factory
{
    protected const NUMBER_SUNDAY = 7;

    protected CarbonPeriod $datePeriod;
    protected WeekRow $row;
    protected Period $period;
    protected WeekCollection $result;
    /**
     * @var array|ScheduleEvent[]
     */
    protected array $events;

    /**
     * @param array<integer, ScheduleEvent> $events
     */
    public function __construct(
        array $events,
        protected CarbonImmutable $start,
        protected CarbonImmutable $end,
        protected \DateTimeZone $timeZone,
    ) {
        $this->events = $events;
        $interval = new \DateInterval('P1D');
        $this->datePeriod = new CarbonPeriod($start, $interval, $end);
    }

    public function buildMonth(): WeekCollection
    {
        $this->result = new WeekCollection();
        $this->row = new WeekRow();
        foreach ($this->datePeriod as $day) {
            if (null !== $this->row->getSkipTo()) {
                if ($day->lessThan($this->row->getSkipTo())) {
                    $this->endWeekRow($day);
                    continue;
                }
            }
            $this->period = new Period(
                $day,
                $this->timeZone,
            );
            $this->addEvents();
            $this->row->addPeriod($this->period);
            $this->endWeekRow($day);
        }

        return $this->result;
    }

    public function buildWeek(): WeekRow
    {
        $this->row = new WeekRow();
        foreach ($this->datePeriod as $day) {
            if (null !== $this->row->getSkipTo()) {
                if ($day->lessThan($this->row->getSkipTo())) {
                    continue;
                }
            }
            $this->period = new Period(
                $day,
                $this->timeZone,
            );
            $this->addEvents();
            $this->row->addPeriod($this->period);
        }

        return $this->row;
    }

    public function buildDays(): WeekRow
    {
        $this->row = new WeekRow();
        foreach ($this->datePeriod as $day) {
            $this->period = new Period(
                $day,
                $this->timeZone,
            );
            $this->row->addPeriod($this->period);
            foreach ($this->events as $event) {
                if (
                    $this->period->getStart()->lessThanOrEqualTo($event->getCarbonStartDateTime()->setTimezone($this->timeZone))
                    && $this->period->getEnd()->greaterThanOrEqualTo($event->getCarbonEndDateTime()->setTimezone($this->timeZone))
                ) {
                    $this->period->addEvent($event);
                }
            }
        }

        return $this->row;
    }

    protected function addEvents(): void
    {
        foreach ($this->events as $event) {
            if ($this->period->exists($event)) {
                continue;
            }
            if ($this->period->canAddEvent(
                $event->getCarbonStartDateTime()->setTimezone($this->timeZone),
                $event->getCarbonEndDateTime()->setTimezone($this->timeZone),
            )
            ) {
                $this->period->addEvent($event);
                $end = $event->getCarbonEndDateTime()->setTimezone($this->timeZone);
                if ($this->period->getEnd()->lessThan($end)) {
                    $this->period->setEnd($end);
                    $this->row->setSkipTo($end);
                    $this->addEvents();
                }
            }
        }
    }

    protected function endWeekRow(CarbonInterface $day): void
    {
        if (self::NUMBER_SUNDAY == $day->format('N')) {
            $this->result->add($this->row);
            $this->row = new WeekRow();
        }
    }

    public function getTimeZone(): \DateTimeZone
    {
        return $this->timeZone;
    }
}
