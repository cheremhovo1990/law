<?php

declare(strict_types=1);

namespace App\Services\Alert;

class AlertMessage
{
    public function __construct(
        protected string $message,
        protected string $title,
        protected null|int|float $delay = null, // секунды
        protected null|int|float $duration = null, // секунды
    ) {
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getDelay(): null|int|float
    {
        return $this->delay;
    }

    public function getDuration(): null|int|float
    {
        return $this->duration;
    }
}
