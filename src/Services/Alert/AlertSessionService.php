<?php

declare(strict_types=1);

namespace App\Services\Alert;

use Symfony\Component\HttpFoundation\Exception\SessionNotFoundException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AlertSessionService implements AlertInterface
{
    private RequestStack $requestStack;

    public function __construct(
        RequestStack $requestStack,
    ) {
        $this->requestStack = $requestStack;
    }

    public function success(AlertMessage $message): void
    {
        $this->addFlash('success', $message->getMessage());
    }

    public function info(AlertMessage $message): void
    {
        $this->addFlash('info', $message->getMessage());
    }

    public function warning(AlertMessage $message): void
    {
        $this->addFlash('warning', $message->getMessage());
    }

    public function error(AlertMessage $message): void
    {
        $this->addFlash('error', $message->getMessage());
    }

    protected function addFlash(string $type, mixed $message): void
    {
        try {
            if ($this->getSession() instanceof Session) {
                $this->getSession()->getFlashBag()->add($type, $message);
            }
        } catch (SessionNotFoundException $e) {
            throw new \LogicException('You cannot use the addFlash method if sessions are disabled. Enable them in "config/packages/framework.yaml".', 0, $e);
        }
    }

    protected function getSession(): Session|SessionInterface
    {
        return $this->requestStack->getSession();
    }
}
