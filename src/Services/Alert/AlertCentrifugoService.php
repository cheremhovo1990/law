<?php

declare(strict_types=1);

namespace App\Services\Alert;

use App\Services\Centrifugo\ClientService;
use App\Services\Identity\IdentityService;
use Symfony\Component\Uid\Uuid;

/**
 * @TODO test
 */
class AlertCentrifugoService implements AlertInterface
{
    private ClientService $clientService;
    private IdentityService $identityService;

    public function __construct(
        ClientService $clientService,
        IdentityService $identityService,
    ) {
        $this->clientService = $clientService;
        $this->identityService = $identityService;
    }

    public function success(AlertMessage $message): void
    {
        $this->addMessage($this->getChannel(), $this->createData('success', $message));
    }

    public function info(AlertMessage $message): void
    {
        $this->addMessage($this->getChannel(), $this->createData('info', $message));
    }

    public function warning(AlertMessage $message): void
    {
        $this->addMessage($this->getChannel(), $this->createData('warning', $message));
    }

    public function error(AlertMessage $message): void
    {
        $this->addMessage($this->getChannel(), $this->createData('error', $message));
    }

    /**
     * @return array<string, mixed>
     */
    protected function createData(string $alert, AlertMessage $message): array
    {
        return [
            'alert' => $alert,
            'id' => Uuid::v4()->toRfc4122(),
            'title' => $message->getTitle(),
            'message' => $message->getMessage(),
            'delay' => $message->getDelay(),
            'duration' => $message->getDuration(),
            'created' => (new \DateTime())->format(\DateTime::ATOM),
        ];
    }

    protected function getChannel(): string
    {
        return sprintf('alerts#%s', $this->identityService->getIdentity());
    }

    /**
     * @param array<string, mixed> $message
     */
    protected function addMessage(string $channel, array $message): void
    {
        $this->clientService->publish($channel, $message);
    }
}
