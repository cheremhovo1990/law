<?php

declare(strict_types=1);

namespace App\Services\Alert;

interface AlertInterface
{
    public function success(AlertMessage $message): void;

    public function info(AlertMessage $message): void;

    public function warning(AlertMessage $message): void;

    public function error(AlertMessage $message): void;
}
