<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Meta;
use App\Repository\MetaRepository;
use App\Services\Alert\AlertInterface;
use App\Services\Alert\AlertMessage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class CommonService
{
    private MetaRepository $metaRepository;
    private KernelInterface $kernel;
    private AlertInterface $alert;
    private CsrfTokenManagerInterface $csrfTokenManager;

    public function __construct(
        MetaRepository $metaRepository,
        AlertInterface $alert,
        KernelInterface $kernel,
        CsrfTokenManagerInterface $csrfTokenManager,
    ) {
        $this->metaRepository = $metaRepository;
        $this->kernel = $kernel;
        $this->alert = $alert;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    public function showMessageSuccess(string $message): void
    {
        $message = new AlertMessage(
            message: $message,
            title: 'Success',
            delay: 2.5,
            duration: 25,
        );
        $this->alert->success($message);
    }

    public function warning(string $message): void
    {
        $message = new AlertMessage(
            message: $message,
            title: 'Warning',
        );
        $this->alert->warning($message);
    }

    public function findOneMetaByKey(string $key): Meta
    {
        $meta = $this->metaRepository->findOneByKey($key);
        if (null === $meta) {
            $meta = Meta::createEmpty();
        }

        return $meta;
    }

    public function isTestEnvironment(): bool
    {
        return 'test' === $this->kernel->getEnvironment();
    }

    public function createNotFoundException(mixed $entity = null, string $message = 'Not Found'): void
    {
        if (null === $entity) {
            throw new NotFoundHttpException($message);
        }
    }

    public function generateCsrfToken(string $tokenId): string
    {
        return $this->csrfTokenManager->getToken($tokenId)->getValue();
    }
}
