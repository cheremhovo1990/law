<?php

declare(strict_types=1);

namespace App\Repository;

use App\Doctrine\IdEnum;
use App\Entity\DescriptionSin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DescriptionSin find($id, $lockMode = null, $lockVersion = null)
 * @method DescriptionSin findOneBy(array $criteria, ?array $orderBy = null)
 *
 * @extends ServiceEntityRepository<DescriptionSin>
 */
class DescriptionSinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DescriptionSin::class);
    }

    /**
     * @return DescriptionSin[]
     */
    public function findAllWithOrder(): array
    {
        $qb = $this->createQueryBuilder('d');

        $qb
            ->orderBy('d.createdAt', 'desc')
        ;

        return $qb->getQuery()->getResult();
    }

    public function findOneByPreceptAndId(string $preceptId, string $id): ?DescriptionSin
    {
        $qb = $this->createQueryBuilder('d');

        $qb
            ->andWhere('d.precept = :precept AND d.id = :id')
            ->setParameter('precept', $preceptId, IdEnum::ULID->value)
            ->setParameter('id', $id, IdEnum::ULID->value)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }
}
