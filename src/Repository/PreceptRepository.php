<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Precept;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Precept find($id, $lockMode = null, $lockVersion = null)
 * @method Precept findOneBy(array $criteria, ?array $orderBy = null)
 *
 * @extends ServiceEntityRepository<Precept>
 */
class PreceptRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Precept::class);
    }

    /**
     * @return array|Precept[]
     */
    public function findAllWithOrderByPosition(): array
    {
        $qb = $this->createQueryBuilder('p');
        $qb->addSelect('s');
        $qb->innerJoin('p.sins', 's', Join::WITH, 's.publishedAt IS NOT NULL');
        $qb->orderBy('p.position', 'asc');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array|Precept[]
     */
    public function findAllWithOrder(string $sort = 'p.position', string $order = 'asc'): array
    {
        $qb = $this->createQueryBuilder('p');
        $qb->orderBy($sort, $order);
        $query = $qb->getQuery();

        return $query->getResult();
    }
}
