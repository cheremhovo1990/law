<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Page>
 */
class PageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    /**
     * @return array<integer, Page>|Page[]
     */
    public function findAllWithOrder(): array
    {
        $qb = $this->createQueryBuilder('p');

        $qb
            ->orderBy('p.createdAt', 'desc')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array<integer, Page>|Page[]
     */
    public function rootTree(): array
    {
        $qb = $this->createQueryBuilder('p');

        $qb
            ->where($qb->expr()->isNull('p.parent'))
            ->orderBy('p.position', 'asc')
        ;

        return $qb->getQuery()->getResult();
    }

    public function getMaxPosition(null|Page $page = null): int
    {
        $qb = $this->createQueryBuilder('p');

        $qb
            ->select('MAX(p.position)')
            ->where($qb->expr()->isNull('p.parent'))
        ;

        if ($page instanceof Page) {
            $qb
                ->andWhere('p.parent = :parent')
                ->setParameter('parent', $page->getId(), 'uild')
            ;
        }

        return $qb->getQuery()->getSingleScalarResult();
    }
}
