<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Music;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Music findOneBy(array $criteria, ?array $orderBy = null)
 * @method Music find($id, $lockMode = null, $lockVersion = null)
 *
 * @extends ServiceEntityRepository<Music>
 */
class MusicRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Music::class);
    }

    /**
     * @return array|Music[]
     */
    public function findAllWithOrder(): array
    {
        $qb = $this->createQueryBuilder('m');

        $qb
            ->orderBy('m.createdAt', 'desc')
        ;

        return $qb->getQuery()->getResult();
    }
}
