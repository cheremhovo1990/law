<?php

declare(strict_types=1);

namespace App\Repository;

use App\Doctrine\IdEnum;
use App\Entity\Sin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Ulid;

/**
 * @method Sin find($id, $lockMode = null, $lockVersion = null)
 * @method Sin findOneBy(array $criteria, ?array $orderBy = null)
 *
 * @extends ServiceEntityRepository<Sin>
 *
 * Class SinRepository
 */
class SinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sin::class);
    }

    /**
     * @return array<integer, Sin>
     */
    public function findAllByPrecept(string $preceptId): array
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->andWhere('s.precept = :precept')
            ->setParameter('precept', $preceptId, IdEnum::ULID->value)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array<integer, mixed>
     */
    public function findAllAsArray(): array
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->select('s.name', 's.id')
            ->orderBy('s.createdAt', 'desc')
        ;

        return $qb->getQuery()->getArrayResult();
    }

    public function findOneByIdAndPrecept(string $id, string $preceptId): ?Sin
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->andWhere('s.id = :id AND s.precept = :precept')
            ->setParameter('id', $id, IdEnum::ULID->value)
            ->setParameter('precept', $preceptId, IdEnum::ULID->value)
        ;

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }

    public function maxPositionByPrecept(string $preceptId): int
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->select('MAX(s.position)')
            ->andWhere('s.precept = :precept')
            ->setParameter('precept', $preceptId, IdEnum::ULID->value)
        ;

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }

    /**
     * @param array<integer, string> $ids
     *
     * @return array<integer, Sin>|Sin[]
     */
    public function findAllByInId(array $ids): array
    {
        $ids = array_map(function (string $id) {
            return Ulid::fromString($id)->toRfc4122();
        }, $ids);
        $qb = $this->createQueryBuilder('s');

        $qb
            ->andWhere($qb->expr()->in('s.id', $ids))
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array<integer, string> $names
     *
     * @return array<integer, Sin>|Sin[]
     */
    public function findAllByInName(array $names): array
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->andWhere($qb->expr()->in('s.name', $names))
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array<integer, Sin>|Sin[]
     */
    public function findAllByDescriptionSin(string $descriptionSinId): array
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->andWhere('s.descriptionSin = :descriptionSin')
            ->setParameter('descriptionSin', $descriptionSinId, IdEnum::ULID->value)
        ;

        return $qb->getQuery()->getResult();
    }
}
