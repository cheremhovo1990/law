<?php

declare(strict_types=1);

namespace App\Repository;

use App\Doctrine\IdEnum;
use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book find($id, $lockMode = null, $lockVersion = null)
 * @method Book findOneBy(array $criteria, ?array $orderBy = null)
 *
 * @extends ServiceEntityRepository<Book>
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function findOneByPublishedAt(string $id): ?Book
    {
        $qb = $this->createQueryBuilder('b');

        $qb
            ->andWhere('b.id = :id AND b.publishedAt IS NOT NULL')
            ->setParameter('id', $id, IdEnum::ULID->value)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return Book[]
     */
    public function findAllByDateOfWritingWithOrder(): array
    {
        $qb = $this->createQueryBuilder('b');

        $qb
            ->andWhere('b.publishedAt IS NOT NULL')
            ->orderBy('b.publishedAt', 'desc')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Book[]
     */
    public function findAllWithOrder(): array
    {
        $qb = $this->createQueryBuilder('b');

        $qb
            ->orderBy('b.createdAt', 'desc')
        ;

        return $qb->getQuery()->getResult();
    }
}
