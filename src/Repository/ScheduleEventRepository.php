<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ScheduleEvent;
use Carbon\CarbonImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ScheduleEvent>
 */
class ScheduleEventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ScheduleEvent::class);
    }

    public function deleteByDateStart(CarbonImmutable $start): int
    {
        $qb = $this->createQueryBuilder('ce')
            ->where('ce.startDateTime >= :start')
            ->setParameter('start', $start->format('Y-m-d'))
            ->delete()
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @return array|ScheduleEvent[]
     */
    public function findAllByStartAndEnd(string $start, string $end): array
    {
        $qb = $this->createQueryBuilder('ce');

        $qb
            ->andWhere('DateTimeWithZone(ce.startDateTime, ce.timezone) BETWEEN :start AND :end OR DateTimeWithZone(ce.endDateTime, ce.timezone) BETWEEN :start AND :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
        ;

        return $qb->getQuery()->getResult();
    }
}
