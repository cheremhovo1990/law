<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\SymbolMap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SymbolMap>
 */
class SymbolMapRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SymbolMap::class);
    }

    /**
     * @return array<integer, SymbolMap>|SymbolMap[]>
     */
    public function findAllByFont(string $code): array
    {
        $qb = $this->createQueryBuilder('sm');

        $qb
            ->addSelect('s')
            ->innerJoin('sm.symbol', 's')
            ->innerJoin('sm.font', 'f', Join::WITH, 'f.code = :code')
            ->setParameters([
                'code' => $code,
            ])
        ;

        return $qb->getQuery()->getResult();
    }

    public function findOneByCharacterAndFontGroupNameNotNull(string $character, string $font): null|SymbolMap
    {
        $qb = $this->createQueryBuilder('sm');

        $qb
            ->innerJoin('sm.font', 'f')
            ->andWhere('sm.groupName IS NOT NULL AND sm.character = :character AND f.code = :code')
            ->setParameter('character', $character)
            ->setParameter('code', $font)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return array<integer, string>
     */
    public function findAllGroupName(string $font): array
    {
        $qb = $this->createQueryBuilder('sm');

        $qb
            ->select('sm.groupName, sm.groupNamePosition')
            ->innerJoin('sm.font', 'f')
            ->andWhere('sm.groupName IS NOT NULL AND f.code = :code')
            ->groupBy('sm.groupName')
            ->addGroupBy('sm.groupNamePosition')
            ->orderBy('sm.groupNamePosition', 'asc')
            ->setParameter('code', $font)
        ;

        return $qb->getQuery()->getSingleColumnResult();
    }
}
