<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Dictionary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Dictionary find($id, $lockMode = null, $lockVersion = null)
 * @method Dictionary findOneBy(array $criteria, ?array $orderBy = null)
 *
 * @extends ServiceEntityRepository<Dictionary>
 */
class DictionaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dictionary::class);
    }

    /**
     * @return Dictionary[]
     */
    public function findAllWithOrder(): array
    {
        $qb = $this->createQueryBuilder('d');

        $qb
            ->orderBy('d.createdAt', 'desc')
        ;

        return $qb->getQuery()->getResult();
    }
}
