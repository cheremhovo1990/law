<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Video;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Video findOneBy(array $criteria, ?array $orderBy = null)
 *
 * @extends ServiceEntityRepository<Video>
 */
class VideoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Video::class);
    }

    /**
     * @return Video[]
     */
    public function findAllWithOrder(): array
    {
        $qb = $this->createQueryBuilder('v');

        $qb
            ->orderBy('v.createdAt', 'desc')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array<integer, Video>
     */
    public function findAllByPublishAt(): array
    {
        $qb = $this->createQueryBuilder('v');

        $qb
            ->andWhere('v.publishedAt IS NOT NULL')
            ->orderBy('v.publishedAt', 'desc')
        ;

        return $qb->getQuery()->getResult();
    }
}
