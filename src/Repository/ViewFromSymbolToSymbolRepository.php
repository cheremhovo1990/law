<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ViewFromSymbolToSymbol;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ViewFromSymbolToSymbol>
 */
class ViewFromSymbolToSymbolRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ViewFromSymbolToSymbol::class);
    }

    /**
     * @return array<integer, ViewFromSymbolToSymbol>|ViewFromSymbolToSymbol[]
     */
    public function findAllByFont(string $fontFrom, string $fontTo): array
    {
        $qb = $this->createQueryBuilder('vs');

        $qb
            ->andWhere('vs.fontFromCode = :fontFrom AND vs.fontToCode = :fontTo')
            ->orderBy('vs.characterTo', 'asc')
            ->setParameters([
                'fontFrom' => $fontFrom,
                'fontTo' => $fontTo,
            ])
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array<integer, ViewFromSymbolToSymbol>|ViewFromSymbolToSymbol[]
     */
    public function findAllByLikeCharacterAndFont(string $character, string $fontFrom, string $fontTo, string $pattern = '%s%%'): array
    {
        $qb = $this->createQueryBuilder('vs');

        $qb
            ->andWhere('vs.characterFrom LIKE :character AND vs.fontFromCode = :fontFrom AND vs.fontToCode = :fontTo')
            ->setParameters([
                'character' => sprintf($pattern, addcslashes($character, '%_')),
                'fontFrom' => $fontFrom,
                'fontTo' => $fontTo,
            ])
        ;

        return $qb->getQuery()->getResult();
    }
}
