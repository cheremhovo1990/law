<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Meta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Meta findOneBy(array $criteria, ?array $orderBy = null)
 *
 * @extends ServiceEntityRepository<Meta>
 */
class MetaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Meta::class);
    }

    public function findOneByKey(string $key): ?Meta
    {
        $qb = $this->createQueryBuilder('b');

        $qb
            ->andWhere('b.key = :key')
            ->setParameter('key', $key)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return array<string, Meta>
     */
    public function findAllWithOrderKey(): array
    {
        $qb = $this->createQueryBuilder('b');

        $qb->orderBy('b.key', 'asc');

        return $qb->getQuery()->getResult();
    }
}
