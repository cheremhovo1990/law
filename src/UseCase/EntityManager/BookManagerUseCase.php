<?php

declare(strict_types=1);

namespace App\UseCase\EntityManager;

use App\Commands\Cp\BookCommand;
use App\Doctrine\Collections\BookLinkCollection;
use App\Doctrine\Collections\TextCollection;
use App\Doctrine\Models\Image;
use App\Entity\Book;
use App\Repository\BookRepository;
use App\Repository\ImageRepository;
use App\Services\CommonService;
use App\Services\Meta\ManagerMetaService;
use App\Services\Publication\ManagerPublicationService;
use App\UseCase\EntityManager\Exceptions\CannotFoundImageRuntimeException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Uid\Ulid;

class BookManagerUseCase
{
    private EntityManagerInterface $entityManager;
    private BookRepository $bookRepository;
    private CommonService $commonService;
    private ImageRepository $imageRepository;
    private ManagerPublicationService $managerPublicationService;
    private ManagerMetaService $managerMetaService;

    public function __construct(
        EntityManagerInterface $entityManager,
        CommonService $commonService,
        BookRepository $bookRepository,
        ImageRepository $imageRepository,
        ManagerPublicationService $managerPublicationService,
        ManagerMetaService $managerMetaService,
    ) {
        $this->entityManager = $entityManager;
        $this->bookRepository = $bookRepository;
        $this->commonService = $commonService;
        $this->imageRepository = $imageRepository;
        $this->managerPublicationService = $managerPublicationService;
        $this->managerMetaService = $managerMetaService;
    }

    public function create(BookCommand $command): Book
    {
        $book = new Book(
            name: $command->getName(),
            description: $command->getDescription(),
            pageSize: $command->getPageSize(),
            languageOfThePublication: $command->getLanguageOfThePublication(),
            dateOfWriting: $command->getDateOfWriting(),
            dateOfPublished: $command->getDateOfWriting(),
            codeOfPublishingCouncil: $command->getCodeOfPublishingCouncil(),
            blessedByPatriarchKirill: $command->isBlessedByPatriarchKirill(),
            recommendedByPublishingCouncilRoc: $command->isRecommendedByPublishingCouncilRoc(),
            recommendedByPublishingCouncilBoc: $command->isRecommendedByPublishingCouncilBoc(),
        );
        $book->setId(new Ulid());
        $book->setIsbnCollection(new TextCollection($command->getIsbnCollection()));
        $book->setLinks(new BookLinkCollection($command->getLinks()));
        $this->managerPublicationService->toPublish($command, $book);
        $image = $command->getCoverImage();
        $book->setCoverImage(new Image($image['id'], $image['name']));
        $this->managerMetaService->add($book, $command);
        $this->updateImage($book, $book->getCoverImage());
        $this->entityManager->persist($book);
        $this->entityManager->flush();

        return $book;
    }

    public function update(BookCommand $command, Book $book): bool
    {
        $book->update(
            name: $command->getName(),
            description: $command->getDescription(),
            pageSize: $command->getPageSize(),
            languageOfThePublication: $command->getLanguageOfThePublication(),
            dateOfWriting: $command->getDateOfWriting(),
            dateOfPublished: $command->getDateOfPublished(),
            codeOfPublishingCouncil: $command->getCodeOfPublishingCouncil(),
            blessedByPatriarchKirill: $command->isBlessedByPatriarchKirill(),
            recommendedByPublishingCouncilRoc: $command->isRecommendedByPublishingCouncilRoc(),
            recommendedByPublishingCouncilBoc: $command->isRecommendedByPublishingCouncilBoc(),
        );
        $this->updateIsbnCollection($book, $command->getIsbnCollection());
        $this->updateLinks($book, $command->getLinks());
        $this->managerPublicationService->toPublish($command, $book);
        $image = $command->getCoverImage();
        if (
            $book->getCoverImage()->getId() !== $image['id']
            || $book->getCoverImage()->getName() !== $image['name']
        ) {
            $book->setCoverImage(new Image($image['id'], $image['name']));
        }
        $this->managerMetaService->update($book, $command);
        $this->updateImage($book, $book->getCoverImage());

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param array<integer, array<string, string>> $isbnCollection
     */
    protected function updateIsbnCollection(Book $book, array $isbnCollection): void
    {
        $book->setIsbnCollection($book->getIsbnCollection()->createNewOrReturnThis($isbnCollection));
    }

    /**
     * @param array<integer, mixed> $links
     */
    protected function updateLinks(Book $book, array $links): void
    {
        $book->setLinks($book->getLinks()->createNewOrReturnThis($links));
    }

    protected function updateImage(Book $book, Image $image): void
    {
        $entity = $this->imageRepository->find($image->getId());
        if (null === $entity) {
            throw new CannotFoundImageRuntimeException();
        }
        if ($entity->getPivots()->contains($book)) {
            return;
        }
        $pivots = clone $entity->getPivots();
        $pivots->push($book);
        $entity->setPivots($pivots);
    }

    public function delete(string $id): bool
    {
        $book = $this->bookRepository->find($id);
        $this->commonService->createNotFoundException($book);
        $this->entityManager->remove($book);
        $this->entityManager->flush();

        return true;
    }
}
