<?php

declare(strict_types=1);

namespace App\UseCase\EntityManager;

use App\Commands\Cp\VideoCommand;
use App\Entity\Video;
use App\Repository\VideoRepository;
use App\Services\CommonService;
use App\Services\Meta\ManagerMetaService;
use App\Services\Publication\ManagerPublicationService;
use Doctrine\ORM\EntityManagerInterface;

class VideoManagerUseCase
{
    private EntityManagerInterface $entityManager;
    private VideoRepository $videoRepository;
    private CommonService $commonService;
    private ManagerPublicationService $managerPublicationService;
    private ManagerMetaService $managerMetaService;

    public function __construct(
        VideoRepository $videoRepository,
        CommonService $commonService,
        EntityManagerInterface $entityManager,
        ManagerPublicationService $managerPublicationService,
        ManagerMetaService $managerMetaService,
    ) {
        $this->entityManager = $entityManager;
        $this->videoRepository = $videoRepository;
        $this->commonService = $commonService;
        $this->managerPublicationService = $managerPublicationService;
        $this->managerMetaService = $managerMetaService;
    }

    public function create(VideoCommand $command): Video
    {
        $video = new Video(
            title: $command->getTitle(),
            link: $command->getLink(),
            description: $command->getDescription(),
        );
        $this->managerPublicationService->toPublish($command, $video);
        $this->managerMetaService->add($video, $command);
        $this->entityManager->persist($video);
        $this->entityManager->flush();

        return $video;
    }

    public function update(VideoCommand $command, Video $video): bool
    {
        $video->update(
            title: $command->getTitle(),
            link: $command->getLink(),
            description: $command->getDescription(),
        );

        $this->managerPublicationService->toPublish($command, $video);
        $this->managerMetaService->update($video, $command);

        $this->entityManager->flush();

        return true;
    }

    public function delete(string $id): bool
    {
        $video = $this->videoRepository->find($id);
        $this->commonService->createNotFoundException($video);

        $this->entityManager->remove($video);
        $this->entityManager->flush();

        return true;
    }
}
