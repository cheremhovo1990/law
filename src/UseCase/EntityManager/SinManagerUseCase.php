<?php

declare(strict_types=1);

namespace App\UseCase\EntityManager;

use App\Commands\Cp\SinCommand;
use App\Entity\Sin;
use App\Repository\SinRepository;
use App\Services\CommonService;
use App\Services\Publication\ManagerPublicationService;
use Doctrine\ORM\EntityManagerInterface;

class SinManagerUseCase
{
    private EntityManagerInterface $entityManager;

    private SinRepository $sinRepository;
    private CommonService $commonService;
    private ManagerPublicationService $managerPublicationService;

    public function __construct(
        EntityManagerInterface $entityManager,
        CommonService $commonService,
        SinRepository $sinRepository,
        ManagerPublicationService $managerPublicationService,
    ) {
        $this->entityManager = $entityManager;
        $this->sinRepository = $sinRepository;
        $this->commonService = $commonService;
        $this->managerPublicationService = $managerPublicationService;
    }

    public function create(SinCommand $command): Sin
    {
        $position = $this->getPosition($command);
        $sin = new Sin(
            name: $command->getName(),
            precept: $command->getPrecept(),
            position: $position,
            popoverContent: $command->getPopoverContent(),
            nameSlavonic: $command->getNameSlavonic(),
        );
        $this->managerPublicationService->toPublish($command, $sin);
        $this->entityManager->persist($sin);
        $this->entityManager->flush();

        return $sin;
    }

    public function update(SinCommand $command, Sin $sin): bool
    {
        $position = $this->getPosition($command);
        $sin->update(
            name: $command->getName(),
            precept: $command->getPrecept(),
            position: $position,
            popoverContent: $command->getPopoverContent(),
            nameSlavonic: $command->getNameSlavonic(),
        );
        $this->managerPublicationService->toPublish($command, $sin);
        $this->entityManager->flush();

        return true;
    }

    protected function getPosition(SinCommand $command): int
    {
        if (null === $command->getPosition()) {
            $position = $this->sinRepository->maxPositionByPrecept((string) $command->getPrecept()->getId());
            ++$position;
            $command->setPosition($position);

            return $position;
        } else {
            return $command->getPosition();
        }
    }

    public function delete(string $id): bool
    {
        $sin = $this->sinRepository->find($id);
        $this->commonService->createNotFoundException($sin);
        $this->entityManager->remove($sin);
        $this->entityManager->flush();

        return true;
    }
}
