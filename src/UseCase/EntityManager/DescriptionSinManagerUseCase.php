<?php

declare(strict_types=1);

namespace App\UseCase\EntityManager;

use App\Commands\Cp\DescriptionSinCommand;
use App\Entity\DescriptionSin;
use App\Repository\DescriptionSinRepository;
use App\Repository\SinRepository;
use App\Services\CommonService;
use App\Services\Meta\ManagerMetaService;
use Doctrine\ORM\EntityManagerInterface;

class DescriptionSinManagerUseCase
{
    private EntityManagerInterface $entityManager;
    private SinRepository $sinRepository;
    private ManagerMetaService $managerMetaService;
    private CommonService $commonService;
    private DescriptionSinRepository $descriptionSinRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        SinRepository $sinRepository,
        DescriptionSinRepository $descriptionSinRepository,
        CommonService $commonService,
        ManagerMetaService $managerMetaService,
    ) {
        $this->entityManager = $entityManager;
        $this->sinRepository = $sinRepository;
        $this->managerMetaService = $managerMetaService;
        $this->commonService = $commonService;
        $this->descriptionSinRepository = $descriptionSinRepository;
    }

    public function create(DescriptionSinCommand $command): DescriptionSin
    {
        $descriptionSin = new DescriptionSin(
            name: $command->getName(),
            text: $command->getText(),
        );
        $descriptionSin->setPrecept($command->getPrecept());
        $this->managerMetaService->add($descriptionSin, $command);
        $this->entityManager->persist($descriptionSin);

        if (!empty($command->getSins())) {
            $sins = $this->sinRepository->findAllByInId($command->getSins());
            foreach ($sins as $sin) {
                $sin->setDescriptionSin($descriptionSin);
            }
        }

        $this->entityManager->flush();

        return $descriptionSin;
    }

    public function update(
        DescriptionSinCommand $command,
        DescriptionSin $descriptionSin,
    ): bool {
        $descriptionSin->update(
            name: $command->getName(),
            text: $command->getText(),
            precept: $command->getPrecept(),
        );
        $this->managerMetaService->update($descriptionSin, $command);

        $sins = $this->sinRepository->findAllByDescriptionSin((string) $descriptionSin->getId());

        foreach ($sins as $sin) {
            $sin->setDescriptionSin(null);
        }

        if (!empty($command->getSins())) {
            $sins = $this->sinRepository->findAllByInId($command->getSins());
            foreach ($sins as $sin) {
                $sin->setDescriptionSin($descriptionSin);
            }
        }

        $this->entityManager->flush();

        return true;
    }

    public function delete(
        string $id,
    ): bool {
        $entity = $this->descriptionSinRepository->find($id);
        $this->commonService->createNotFoundException($entity);
        $this->entityManager->remove($entity);
        $this->entityManager->flush();

        return true;
    }
}
