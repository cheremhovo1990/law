<?php

declare(strict_types=1);

namespace App\UseCase\EntityManager;

use App\Commands\Cp\DictionaryCommand;
use App\Entity\Dictionary;
use App\Entity\Font;
use App\Repository\DictionaryRepository;
use App\Services\CommonService;
use App\Services\DictionaryService;
use Doctrine\ORM\EntityManagerInterface;

class DictionaryManagerUseCase
{
    private EntityManagerInterface $entityManager;
    private DictionaryRepository $dictionaryRepository;
    private CommonService $commonService;
    private DictionaryService $dictionaryService;

    public function __construct(
        EntityManagerInterface $entityManager,
        DictionaryService $dictionaryService,
        CommonService $commonService,
        DictionaryRepository $dictionaryRepository,
    ) {
        $this->entityManager = $entityManager;
        $this->dictionaryRepository = $dictionaryRepository;
        $this->commonService = $commonService;
        $this->dictionaryService = $dictionaryService;
    }

    public function create(DictionaryCommand $command): Dictionary
    {
        $dictionary = new Dictionary(
            name: $command->getName(),
            description: $command->getDescription(),
            isTitlo: $command->isTitlo(),
            groupName: $this->dictionaryService->getGroup($command->getName(), Font::CODE_HIRMOS_IE_UCS),
        );
        if (!empty($command->getNormalizeName())) {
            $dictionary->setNormalizeName($command->getNormalizeName());
        } else {
            $dictionary->setNormalizeName(null);
        }

        $this->entityManager->persist($dictionary);
        $this->entityManager->flush();

        return $dictionary;
    }

    public function update(
        DictionaryCommand $command,
        Dictionary $dictionary,
    ): bool {
        $dictionary->update(
            name: $command->getName(),
            description: $command->getDescription(),
            isTitlo: $command->isTitlo(),
            groupName: $this->dictionaryService->getGroup($command->getName(), Font::CODE_HIRMOS_IE_UCS),
        );

        if (!empty($command->getNormalizeName())) {
            $dictionary->setNormalizeName($command->getNormalizeName());
        } else {
            $dictionary->setNormalizeName(null);
        }

        $this->entityManager->flush();

        return true;
    }

    public function delete(string $id): bool
    {
        $entity = $this->dictionaryRepository->find($id);
        $this->commonService->createNotFoundException($entity);
        $this->entityManager->remove($entity);
        $this->entityManager->flush();

        return true;
    }
}
