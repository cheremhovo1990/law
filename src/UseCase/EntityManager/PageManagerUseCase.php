<?php

declare(strict_types=1);

namespace App\UseCase\EntityManager;

use App\Commands\Cp\PageCommand;
use App\Entity\Page;
use App\Repository\PageRepository;
use App\Services\CommonService;
use App\Services\Meta\ManagerMetaService;
use App\Services\Publication\ManagerPublicationService;
use Doctrine\ORM\EntityManagerInterface;

class PageManagerUseCase
{
    private CommonService $commonService;
    private ManagerPublicationService $managerPublicationService;
    private ManagerMetaService $managerMetaService;
    private PageRepository $pageRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        PageRepository $pageRepository,
        CommonService $commonService,
        EntityManagerInterface $entityManager,
        ManagerPublicationService $managerPublicationService,
        ManagerMetaService $managerMetaService,
    ) {
        $this->entityManager = $entityManager;
        $this->commonService = $commonService;
        $this->managerPublicationService = $managerPublicationService;
        $this->managerMetaService = $managerMetaService;
        $this->pageRepository = $pageRepository;
    }

    public function create(PageCommand $command): Page
    {
        if ($command->getParent()) {
            $max = $this->pageRepository->getMaxPosition($command->getParent());
        } else {
            $max = $this->pageRepository->getMaxPosition();
        }
        $entity = new Page(
            content: $command->getContent(),
            title: $command->getTitle(),
            parent: $command->getParent(),
            position: ++$max,
        );
        $this->managerPublicationService->toPublish($command, $entity);
        $this->managerMetaService->add($entity, $command);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity;
    }

    public function update(PageCommand $command, Page $video): bool
    {
        $video->update(
            content: $command->getContent(),
            title: $command->getTitle(),
            parent: $command->getParent(),
        );

        $this->managerPublicationService->toPublish($command, $video);
        $this->managerMetaService->update($video, $command);

        $this->entityManager->flush();

        return true;
    }

    public function delete(string $id): bool
    {
        $video = $this->pageRepository->find($id);
        $this->commonService->createNotFoundException($video);
        $this->entityManager->remove($video);
        $this->entityManager->flush();

        return true;
    }
}
