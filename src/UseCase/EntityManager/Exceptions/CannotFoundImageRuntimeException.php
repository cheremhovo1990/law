<?php

declare(strict_types=1);

namespace App\UseCase\EntityManager\Exceptions;

class CannotFoundImageRuntimeException extends \RuntimeException
{
}
