<?php

declare(strict_types=1);

namespace App\UseCase\EntityManager;

use App\Commands\Cp\MetaCommand;
use App\Entity\Meta;
use Doctrine\ORM\EntityManagerInterface;

class MetaManagerUseCase
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
    ) {
        $this->entityManager = $entityManager;
    }

    public function create(MetaCommand $command): Meta
    {
        $meta = new Meta(
            $command->getKey(),
            $command->getTitle(),
            $command->getDescription(),
        );

        $this->entityManager->persist($meta);
        $this->entityManager->flush();

        return $meta;
    }

    public function update(MetaCommand $command, Meta $meta): bool
    {
        $meta->update(
            $command->getKey(),
            $command->getTitle(),
            $command->getDescription(),
        );
        $this->entityManager->flush();

        return true;
    }
}
