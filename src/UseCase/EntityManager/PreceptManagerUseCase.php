<?php

declare(strict_types=1);

namespace App\UseCase\EntityManager;

use App\Commands\Cp\PreceptCommand;
use App\Entity\Precept;
use Doctrine\ORM\EntityManagerInterface;

class PreceptManagerUseCase
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
    ) {
        $this->entityManager = $entityManager;
    }

    public function update(PreceptCommand $command, Precept $precept): bool
    {
        $precept->update(
            $command->getLink(),
            $command->getVerse(),
        );

        $this->entityManager->flush();

        return true;
    }
}
