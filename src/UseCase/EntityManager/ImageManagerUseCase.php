<?php

declare(strict_types=1);

namespace App\UseCase\EntityManager;

use App\Commands\Cp\ImageCommand;
use App\Entity\Image;
use App\Services\ImageService;
use Doctrine\ORM\EntityManagerInterface;

class ImageManagerUseCase
{
    private ImageService $imageService;
    private EntityManagerInterface $entityManager;

    public function __construct(
        ImageService $imageService,
        EntityManagerInterface $entityManager,
    ) {
        $this->imageService = $imageService;
        $this->entityManager = $entityManager;
    }

    public function create(ImageCommand $command): Image
    {
        $image = new Image($this->imageService->write($command->getImage()));
        $this->entityManager->persist($image);
        $this->entityManager->flush();

        return $image;
    }
}
