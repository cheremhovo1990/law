<?php

declare(strict_types=1);

namespace App\UseCase\EntityManager;

use App\Entity\ScheduleEvent;
use App\Repository\ScheduleEventRepository;
use App\Services\CalDAV\CalDAVEvent;
use App\Services\CalDAV\CalDAVInterface;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Carbon\CarbonPeriod;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CalendarEventManagerUseCase
{
    private CalDAVInterface $calDAVAdapterService;
    protected CarbonImmutable $start;
    protected CarbonImmutable $end;
    protected \DateTimeZone $timezoneUTC;
    private ScheduleEventRepository $scheduleEventRepository;
    private OutputInterface $output;
    private EntityManagerInterface $entityManager;
    private string $calendarId;
    private string $login;
    private string $password;

    public function __construct(
        string $login,
        string $password,
        string $calendarId,
        CalDAVInterface $calDAVAdapterService,
        EntityManagerInterface $entityManager,
        ScheduleEventRepository $scheduleEventRepository,
    ) {
        $this->calendarId = $calendarId;
        $this->calDAVAdapterService = $calDAVAdapterService;
        $this->timezoneUTC = new \DateTimeZone('UTC');
        $this->start = CarbonImmutable::now($this->timezoneUTC)->setTime(0, 0, 0)->subHours(24);
        $this->end = CarbonImmutable::now($this->timezoneUTC)->setTime(0, 0, 0)->addDays(30);
        $this->scheduleEventRepository = $scheduleEventRepository;
        $this->entityManager = $entityManager;
        $this->login = $login;
        $this->password = $password;
    }

    public function init(): void
    {
        $this->calDAVAdapterService->connect(
            sprintf('https://caldav.yandex.ru/calendars/%s', $this->login),
            $this->login,
            $this->password,
            $this->calendarId,
        );
    }

    /**
     * @return array<integer, CalDAVEvent>
     */
    public function getEvents(): array
    {
        return $this->calDAVAdapterService->getEvents($this->start, $this->end);
    }

    public function create(): void
    {
        $events = $this->getEvents();
        $this->scheduleEventRepository->deleteByDateStart($this->start);
        foreach ($events as $event) {
            $this->add($event);
        }
        $this->entityManager->flush();
    }

    public function add(CalDAVEvent $event): void
    {
        $dateStart = $event->getDateTimeStart();
        $dateEnd = $event->getDateTimeEnd();
        $duration = $dateStart->diffInSeconds($dateEnd);
        $this->output->writeln(str_repeat('#', 100));
        $this->output->writeln((string) $duration);
        $this->output->writeln($dateStart->format('Y-m-d H:i:s') . ' ' . $dateEnd->format('Y-m-d H:i:s'));
        if ($event->isWeekly()) {
            $this->output->writeln($event->getRaw());
            $range = $this->getPeriod($dateStart, $event->getUntil());
            foreach ($range as $index => $day) {
                $day->setTimezone($event->getTimeZone());
                $this->output->writeln($day->format('Y-m-d H:i:s D'));
                $rule = $event->getRule();
                if ($rule->isDayOfWeekEqual($day, $this->timezoneUTC)) {
                    $dateEnd = (new Carbon($day))->addSeconds($duration);
                    $scheduleEvent = new ScheduleEvent(
                        $this->calendarId,
                        $event->getUid(),
                        CarbonImmutable::createFromMutable($day->toDateTime()),
                        CarbonImmutable::createFromMutable($dateEnd),
                        $event->getSummary(),
                        $event->getDescription(),
                    );
                    if (0 === $index) {
                        $scheduleEvent->setRule($event->getRaw());
                        $scheduleEvent->setRaw($event->getData());
                    }
                    $this->entityManager->persist($scheduleEvent);
                }
            }
        } elseif ($event->isMonthly()) {
            $this->output->writeln((clone $dateStart)->setTimezone($event->getTimeZone())->format('Y-m-d H:i:s D'));
            $dateEnd = (new Carbon($dateStart))->addSeconds($duration);
            $scheduleEvent = new ScheduleEvent(
                $this->calendarId,
                $event->getUid(),
                $dateStart,
                CarbonImmutable::createFromMutable($dateEnd),
                $event->getSummary(),
                $event->getDescription(),
            );
            $scheduleEvent->setRule($event->getRaw());
            $scheduleEvent->setRaw($event->getData());
            $this->entityManager->persist($scheduleEvent);
        } else {
            $this->output->writeln((clone $dateStart)->setTimezone($event->getTimeZone())->format('Y-m-d H:i:s D'));
            $dateEnd = (new Carbon($dateStart))->addSeconds($duration);
            $scheduleEvent = new ScheduleEvent(
                $this->calendarId,
                $event->getUid(),
                $dateStart,
                CarbonImmutable::createFromMutable($dateEnd),
                $event->getSummary(),
                $event->getDescription(),
            );
            $scheduleEvent->setRaw($event->getData());
            $this->entityManager->persist($scheduleEvent);
        }
    }

    protected function getPeriod(CarbonImmutable $start, CarbonImmutable $end): CarbonPeriod
    {
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new CarbonPeriod();
        $period
            ->setStartDate((clone $start)->setTimezone($this->timezoneUTC), true)
            ->setDateInterval($interval)
            ->setEndDate((clone $end)->setTimezone($this->timezoneUTC), true)
        ;

        return $period;
    }

    public function setOutput(OutputInterface $output): void
    {
        $this->output = $output;
    }
}
