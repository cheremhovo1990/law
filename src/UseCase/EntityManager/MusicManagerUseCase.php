<?php

declare(strict_types=1);

namespace App\UseCase\EntityManager;

use App\Commands\Cp\MusicCommand;
use App\Entity\Music;
use App\Repository\MusicRepository;
use App\Services\CommonService;
use App\Services\Meta\ManagerMetaService;
use App\Services\Publication\ManagerPublicationService;
use Doctrine\ORM\EntityManagerInterface;

class MusicManagerUseCase
{
    private EntityManagerInterface $entityManager;
    private CommonService $commonService;
    private ManagerPublicationService $managerPublicationService;
    private ManagerMetaService $managerMetaService;
    private MusicRepository $musicRepository;

    public function __construct(
        MusicRepository $musicRepository,
        CommonService $commonService,
        EntityManagerInterface $entityManager,
        ManagerPublicationService $managerPublicationService,
        ManagerMetaService $managerMetaService,
    ) {
        $this->entityManager = $entityManager;
        $this->commonService = $commonService;
        $this->managerPublicationService = $managerPublicationService;
        $this->managerMetaService = $managerMetaService;
        $this->musicRepository = $musicRepository;
    }

    public function create(MusicCommand $command): Music
    {
        $entity = new Music(
            title: $command->getTitle(),
            link: $command->getLink(),
            description: $command->getDescription(),
        );
        $this->managerPublicationService->toPublish($command, $entity);
        $this->managerMetaService->add($entity, $command);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity;
    }

    public function update(MusicCommand $command, Music $entity): bool
    {
        $entity->update(
            title: $command->getTitle(),
            link: $command->getLink(),
            description: $command->getDescription(),
        );

        $this->managerPublicationService->toPublish($command, $entity);
        $this->managerMetaService->update($entity, $command);

        $this->entityManager->flush();

        return true;
    }

    public function delete(string $id): bool
    {
        $entity = $this->musicRepository->find($id);
        $this->commonService->createNotFoundException($entity);

        $this->entityManager->remove($entity);
        $this->entityManager->flush();

        return true;
    }
}
