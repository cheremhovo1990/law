<?php

declare(strict_types=1);

namespace App\UseCase\Feedback;

use App\Commands\FeedbackCommand;
use App\Entity\Feedback;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;

class FeedbackUseCase
{
    private EntityManagerInterface $entityManager;
    private MailerInterface $mailer;
    private ParameterBagInterface $parameterBag;

    public function __construct(
        EntityManagerInterface $entityManager,
        ParameterBagInterface $parameterBag,
        MailerInterface $mailer,
    ) {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
    }

    public function save(FeedbackCommand $command): bool
    {
        $feedback = new Feedback(
            firstName: $command->getFirstName(),
            email: $command->getEmail(),
            theme: $command->getTheme(),
            message: $command->getMessage(),
        );

        $feedback
            ->setPhone($command->getPhone())
            ->setClientIp($command->getClientIp())
        ;

        $this->entityManager->persist($feedback);
        $this->entityManager->flush();
        $this->send($feedback);

        return true;
    }

    protected function send(Feedback $feedback): void
    {
        $email = (new TemplatedEmail())
            ->to($this->parameterBag->get('adminEmail'))
            ->subject(sprintf('%s (Обратная связь)', $this->parameterBag->get('appName')))
            ->htmlTemplate('mails/feedback.html.twig')->context([
                'feedback' => $feedback,
            ]);

        $this->mailer->send($email);
    }
}
