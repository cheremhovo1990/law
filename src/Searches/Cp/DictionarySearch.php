<?php

declare(strict_types=1);

namespace App\Searches\Cp;

use App\Repository\DictionaryRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\ParameterBag;

class DictionarySearch
{
    private DictionaryRepository $dictionaryRepository;

    public function __construct(
        DictionaryRepository $dictionaryRepository,
    ) {
        $this->dictionaryRepository = $dictionaryRepository;
    }

    public function search(ParameterBag $params): QueryBuilder
    {
        $qb = $this->dictionaryRepository->createQueryBuilder('d');

        if ($params->getBoolean('have_titlo')) {
            $qb->andWhere('d.titlo = true');
        }
        if ($params->get('description')) {
            $qb->andWhere('LOWER(d.description) LIKE LOWER(:description)');
            $qb->setParameter(':description', sprintf('%%%s%%', $params->get('description')));
        }

        if ($params->get('group')) {
            $qb->andWhere('d.groupName LIKE :group');
            $qb->setParameter(':group', sprintf('%s%%', $params->get('group')));
        }

        return $qb;
    }
}
