<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221003135743 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE books (id UUID NOT NULL, name VARCHAR(500) NOT NULL, links JSON NOT NULL, description TEXT DEFAULT \'\' NOT NULL, cover_image JSONB NOT NULL, isbn_collection JSON DEFAULT NULL, date_of_writing DATE DEFAULT NULL, date_of_published DATE DEFAULT NULL, page_size INT NOT NULL, blessed_by_patriach_kirill BOOLEAN DEFAULT false NOT NULL, recommended_by_publishing_council_roc BOOLEAN DEFAULT false NOT NULL, recommended_by_publishing_council_boc BOOLEAN DEFAULT false NOT NULL, code_of_publishing_council VARCHAR(100) DEFAULT NULL, language_of_publication VARCHAR(100) NOT NULL CHECK(language_of_publication IN (\'russian\'::text, \'church_slavic\'::text)), meta JSONB DEFAULT \'[]\', published_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN books.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN books.links IS \'(DC2Type:json_book_links)\'');
        $this->addSql('COMMENT ON COLUMN books.cover_image IS \'(DC2Type:json_image)\'');
        $this->addSql('COMMENT ON COLUMN books.isbn_collection IS \'(DC2Type:json_texts)\'');
        $this->addSql('COMMENT ON COLUMN books.meta IS \'(DC2Type:json_meta)\'');
        $this->addSql('COMMENT ON COLUMN books.published_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN books.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN books.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE description_sins (id UUID NOT NULL, precept_id UUID NOT NULL, name VARCHAR(500) NOT NULL, text TEXT NOT NULL, meta JSONB DEFAULT \'[]\', created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DBB2D97A3575ABD5 ON description_sins (precept_id)');
        $this->addSql('COMMENT ON COLUMN description_sins.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN description_sins.precept_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN description_sins.meta IS \'(DC2Type:json_meta)\'');
        $this->addSql('COMMENT ON COLUMN description_sins.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN description_sins.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE dictionaries (id UUID NOT NULL, name VARCHAR(1000) NOT NULL, normalize_name VARCHAR(1000) DEFAULT NULL, group_name VARCHAR(1000) NOT NULL, description TEXT NOT NULL, titlo BOOLEAN DEFAULT true NOT NULL, published_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN dictionaries.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN dictionaries.published_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN dictionaries.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN dictionaries.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE feedbacks (id UUID NOT NULL, first_name VARCHAR(50) NOT NULL, phone VARCHAR(20) DEFAULT NULL, email VARCHAR(50) NOT NULL, theme VARCHAR(255) NOT NULL, message TEXT NOT NULL, client_ip VARCHAR(15) NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN feedbacks.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN feedbacks.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN feedbacks.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE fonts (id UUID NOT NULL, code VARCHAR(50) NOT NULL, name VARCHAR(200) NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7303E8FB77153098 ON fonts (code)');
        $this->addSql('COMMENT ON COLUMN fonts.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN fonts.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN fonts.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE images (id UUID NOT NULL, name VARCHAR(1000) NOT NULL, pivots JSONB DEFAULT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN images.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN images.pivots IS \'(DC2Type:json_image_pivots)\'');
        $this->addSql('COMMENT ON COLUMN images.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN images.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE meta (key VARCHAR(255) NOT NULL, title VARCHAR(500) DEFAULT \'\' NOT NULL, description VARCHAR(1000) NOT NULL, PRIMARY KEY(key))');
        $this->addSql('CREATE TABLE music (id UUID NOT NULL, title VARCHAR(500) NOT NULL, link VARCHAR(500) NOT NULL, description TEXT NOT NULL, meta JSONB DEFAULT \'[]\', published_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN music.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN music.meta IS \'(DC2Type:json_meta)\'');
        $this->addSql('COMMENT ON COLUMN music.published_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN music.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN music.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE page (id UUID NOT NULL, parent_id UUID DEFAULT NULL, title VARCHAR(500) DEFAULT \'\' NOT NULL, content TEXT NOT NULL, deep INT DEFAULT 0 NOT NULL, position INT DEFAULT 0 NOT NULL, meta JSONB DEFAULT \'[]\', published_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_140AB620727ACA70 ON page (parent_id)');
        $this->addSql('COMMENT ON COLUMN page.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN page.parent_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN page.meta IS \'(DC2Type:json_meta)\'');
        $this->addSql('COMMENT ON COLUMN page.published_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN page.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN page.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE precepts (id UUID NOT NULL, name VARCHAR(1000) NOT NULL, name_slavonic VARCHAR(1000) NOT NULL, link VARCHAR(500) NOT NULL, verse VARCHAR(15) NOT NULL, position INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN precepts.id IS \'(DC2Type:ulid)\'');
        $this->addSql('CREATE TABLE schedule_events (id UUID NOT NULL, calendar_id VARCHAR(100) NOT NULL, event_uid VARCHAR(100) NOT NULL, summary VARCHAR(250) NOT NULL, description VARCHAR(250) DEFAULT NULL, start_date_time TIMESTAMP(0) WITH TIME ZONE NOT NULL, end_date_time TIMESTAMP(0) WITH TIME ZONE NOT NULL, timezone VARCHAR(50) NOT NULL, rule VARCHAR(250) DEFAULT NULL, raw TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN schedule_events.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN schedule_events.start_date_time IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN schedule_events.end_date_time IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN schedule_events.timezone IS \'(DC2Type:date_time_zone)\'');
        $this->addSql('COMMENT ON COLUMN schedule_events.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN schedule_events.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE sins (id UUID NOT NULL, precept_id UUID NOT NULL, description_sin_id UUID DEFAULT NULL, name VARCHAR(500) NOT NULL, popover_content VARCHAR(1000) DEFAULT \'\' NOT NULL, position INT NOT NULL, published_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2DBA466E3575ABD5 ON sins (precept_id)');
        $this->addSql('CREATE INDEX IDX_2DBA466EA4B1F86A ON sins (description_sin_id)');
        $this->addSql('COMMENT ON COLUMN sins.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN sins.precept_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN sins.description_sin_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN sins.published_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN sins.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN sins.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE symbol_maps (id UUID NOT NULL, font_id UUID NOT NULL, symbol_id UUID NOT NULL, unicode VARCHAR(30) NOT NULL, character VARCHAR(30) NOT NULL, group_name VARCHAR(30) DEFAULT NULL, group_name_position INT DEFAULT NULL, count_unicode INT NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E71896F7D7F7F9EB ON symbol_maps (font_id)');
        $this->addSql('CREATE INDEX IDX_E71896F7C0F75674 ON symbol_maps (symbol_id)');
        $this->addSql('COMMENT ON COLUMN symbol_maps.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN symbol_maps.font_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN symbol_maps.symbol_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN symbol_maps.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN symbol_maps.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE symbols (id UUID NOT NULL, code VARCHAR(100) NOT NULL, name VARCHAR(200) NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DF834D8577153098 ON symbols (code)');
        $this->addSql('COMMENT ON COLUMN symbols.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN symbols.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN symbols.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('CREATE TABLE video (id UUID NOT NULL, title VARCHAR(500) NOT NULL, link VARCHAR(500) NOT NULL, description TEXT NOT NULL, meta JSONB DEFAULT \'[]\', published_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN video.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN video.meta IS \'(DC2Type:json_meta)\'');
        $this->addSql('COMMENT ON COLUMN video.published_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN video.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN video.deleted_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('ALTER TABLE description_sins ADD CONSTRAINT FK_DBB2D97A3575ABD5 FOREIGN KEY (precept_id) REFERENCES precepts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB620727ACA70 FOREIGN KEY (parent_id) REFERENCES page (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sins ADD CONSTRAINT FK_2DBA466E3575ABD5 FOREIGN KEY (precept_id) REFERENCES precepts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sins ADD CONSTRAINT FK_2DBA466EA4B1F86A FOREIGN KEY (description_sin_id) REFERENCES description_sins (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE symbol_maps ADD CONSTRAINT FK_E71896F7D7F7F9EB FOREIGN KEY (font_id) REFERENCES fonts (id) ON DELETE RESTRICT NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE symbol_maps ADD CONSTRAINT FK_E71896F7C0F75674 FOREIGN KEY (symbol_id) REFERENCES symbols (id) ON DELETE RESTRICT NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sins DROP CONSTRAINT FK_2DBA466EA4B1F86A');
        $this->addSql('ALTER TABLE symbol_maps DROP CONSTRAINT FK_E71896F7D7F7F9EB');
        $this->addSql('ALTER TABLE page DROP CONSTRAINT FK_140AB620727ACA70');
        $this->addSql('ALTER TABLE description_sins DROP CONSTRAINT FK_DBB2D97A3575ABD5');
        $this->addSql('ALTER TABLE sins DROP CONSTRAINT FK_2DBA466E3575ABD5');
        $this->addSql('ALTER TABLE symbol_maps DROP CONSTRAINT FK_E71896F7C0F75674');
        $this->addSql('DROP TABLE books');
        $this->addSql('DROP TABLE description_sins');
        $this->addSql('DROP TABLE dictionaries');
        $this->addSql('DROP TABLE feedbacks');
        $this->addSql('DROP TABLE fonts');
        $this->addSql('DROP TABLE images');
        $this->addSql('DROP TABLE meta');
        $this->addSql('DROP TABLE music');
        $this->addSql('DROP TABLE page');
        $this->addSql('DROP TABLE precepts');
        $this->addSql('DROP TABLE schedule_events');
        $this->addSql('DROP TABLE sins');
        $this->addSql('DROP TABLE symbol_maps');
        $this->addSql('DROP TABLE symbols');
        $this->addSql('DROP TABLE video');
    }
}
