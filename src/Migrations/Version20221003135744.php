<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221003135744 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        /*
         * https://postgrespro.ru/docs/postgresql/14/sql-createextension
         * https://postgrespro.ru/docs/postgresql/14/sql-dropextension
         * https://postgrespro.ru/docs/postgresql/14/uuid-ossp
         */
        $this->addSql('CREATE EXTENSION "uuid-ossp"');

        $this->addSql(
            <<<SQL
CREATE VIEW view_from_symbol_to_symbol AS
SELECT
        uuid_generate_v4() AS id,
        f1.code AS font_from_code,
        f2.code AS font_to_code,
        sm1.unicode AS unicode_from,
        sm2.unicode AS unicode_to,
        sm1.count_unicode AS count_unicode,
        sm1.character AS character_from,
        sm2.character AS character_to
    FROM symbols m
        INNER JOIN symbol_maps sm1 on m.id = sm1.symbol_id
        INNER JOIN symbol_maps sm2 on m.id = sm2.symbol_id AND sm1.id <> sm2.id
        INNER JOIN fonts f1 on sm1.font_id = f1.id
        INNER JOIN fonts f2 on sm2.font_id = f2.id
SQL
            ,
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP VIEW view_from_symbol_to_symbol');
        $this->addSql('DROP EXTENSION "uuid-ossp"');
    }
}
