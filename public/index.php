<?php

use App\Kernel;
use App\Services\MonitoringSstatistics\MonitoringStatisticsService;
use App\Services\Pinba\PinbaService;
use \Symfony\Component\HttpFoundation\Request;

require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

Request::setTrustedProxies(
    [$_SERVER['REMOTE_ADDR']],
    Request::HEADER_X_FORWARDED_FOR | Request::HEADER_X_FORWARDED_PORT | Request::HEADER_X_FORWARDED_PROTO
);

MonitoringStatisticsService::init('192.168.110.28', 30003);
register_shutdown_function(MonitoringStatisticsService::send(...));
PinbaService::init();
return function (array $context) {
    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};
