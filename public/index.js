var express = require('express');
var cors = require('cors')
var app = express();

app.use(cors())
app.use(express.json())
app.get('/api', function (req, res) {
    res.json({'success': true});
});
app.post('/api/form/failed', function (req, res) {
    let body = req.body;
    let content = {
        "success":false,
        "message":"Validation failed",
        "errors": {}
    };
    for (const field in body) {
        content["errors"][field] = "This value should not be blank.";
    }
    res.json(content);
});

app.post('/api/form/success', function (req, res) {
    res.json({
        "success": true,
        "message":"Success",
    })
});

app.get('/api/form/data-list', function (req, res) {
    let jsonfile = require('jsonfile');
    jsonfile.readFile(__dirname + '/cities.json').then(function (obj) {
        let result = [];
        if (req.query.search !== undefined) {
            let expression = new RegExp('^'+ req.query.search + '.*', 'i');
            obj.forEach(function (item) {
                if (expression.test(item.city)) {
                    if (result.length === 10) {
                        return;
                    }
                    result.push(item.city);
                }
            });
        } else {
            obj.forEach(function (item) {
                if (result.length === 10) {
                    return;
                }
                result.push(item.city);
            });
        }
        res.json({'success': true, 'data': result});
    });
});
app.get('/api/book/217', function (req, res) {
    res.json({
        "id": "7a22df8a-7cef-49c6-a7ed-2f95fe2ab228",
        "name": "Лествица, возводящая на небо",
        "link": "https://www.litres.ru/prepodobnyy-ioann-lestvichnik/lestvica-vozvodyaschaya-na-nebo/",
        "description": "",
        "coverImage": {
            "id": 62,
            "name": "test/book-6.jpg"
        },
        "isbn": "978-5-485-00436-1",
        "dateOfWriting": "2013-05-22T00:00:00+04:00",
        "dateOfPublished": "1831-05-22T00:00:00+02:30",
        "pageSize": 340,
        "blessedByPatriarchKirill": true,
        "recommendedByPublishingCouncilRoc": true,
        "codeOfPublishingCouncil": 'Р21-123-3340'
    });
})
app.listen(8080, function () {
    console.log('Express server listening on port 8080')
});