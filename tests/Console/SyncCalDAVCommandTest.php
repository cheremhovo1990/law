<?php

declare(strict_types=1);

namespace App\Tests\Console;

use App\Command\SyncCalDAVCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class SyncCalDAVCommandTest extends KernelTestCase
{
    public function testSuccess(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        /** @var SyncCalDAVCommand $command */
        $command = $application->find('app:sync:caldav');
        $commandTester = new CommandTester($command);

        $commandTester->execute([]);

        $commandTester->assertCommandIsSuccessful();
    }
}
