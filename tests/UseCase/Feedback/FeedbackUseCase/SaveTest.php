<?php

declare(strict_types=1);

namespace App\Tests\UseCase\Feedback\FeedbackUseCase;

use App\Commands\FeedbackCommand;
use App\UseCase\Feedback\FeedbackUseCase;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SaveTest extends KernelTestCase
{
    // tests
    public function testSuccess(): void
    {
        $command = new FeedbackCommand();

        $service = $this->grabService();
        Assert::assertTrue($service->save($command));
    }

    public function grabService(): FeedbackUseCase
    {
        return parent::getContainer()->get(FeedbackUseCase::class);
    }
}
