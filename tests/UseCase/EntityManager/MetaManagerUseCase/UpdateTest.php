<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\MetaManagerUseCase;

use App\Commands\Cp\MetaCommand;
use App\Entity\MetaKeyEnum;
use App\Tests\Helpers\DoctrineTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UpdateTest extends KernelTestCase
{
    use DoctrineTrait;
    use ToolTrait;

    public function testSuccess(): void
    {
        $meta = $this->grabMetaByKey(MetaKeyEnum::MAIN->value);
        $command = MetaCommand::create($meta);
        $service = $this->grabService();
        Assert::assertTrue($service->update($command, $meta));
    }
}
