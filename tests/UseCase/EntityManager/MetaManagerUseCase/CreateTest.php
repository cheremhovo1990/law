<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\MetaManagerUseCase;

use App\Commands\Cp\MetaCommand;
use App\Entity\Meta;
use App\Entity\MetaKeyEnum;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreateTest extends KernelTestCase
{
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $command = new MetaCommand();
        $command->setKey(MetaKeyEnum::BOOKS->value);
        $service = $this->grabService();
        Assert::assertInstanceOf(Meta::class, $service->create($command));
    }
}
