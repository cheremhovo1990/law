<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\MetaManagerUseCase;

use App\UseCase\EntityManager\MetaManagerUseCase;

trait ToolTrait
{
    public function grabService(): MetaManagerUseCase
    {
        return parent::getContainer()->get(MetaManagerUseCase::class);
    }
}
