<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\DescriptionSinUseCase;

use App\Commands\Cp\DescriptionSinCommand;
use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\DoctrineTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UpdateTest extends KernelTestCase
{
    use DoctrineTrait;
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $sins = $this->grabSinByNames([PreceptFixtures::SIN_NAME_FIRST, PreceptFixtures::SIN_NAME_SECOND]);
        $ids = [];
        foreach ($sins as $sin) {
            $ids[] = (string) $sin->getId();
        }

        $sin = $this->grabSinByName(PreceptFixtures::SIN_NAME_SECOND);

        $descriptionSin = $sin->getDescriptionSin();
        $command = new DescriptionSinCommand(
            'Name',
            'Text',
            $descriptionSin->getPrecept(),
            sins: $ids,
        );
        $command->setMeta(['title' => 'Title', 'description' => 'Description']);
        $service = $this->grabService();
        Assert::assertTrue($service->update($command, $descriptionSin));
    }
}
