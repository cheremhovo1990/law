<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\DescriptionSinUseCase;

use App\UseCase\EntityManager\DescriptionSinManagerUseCase;

trait ToolTrait
{
    public function grabService(): DescriptionSinManagerUseCase
    {
        return parent::getContainer()->get(DescriptionSinManagerUseCase::class);
    }
}
