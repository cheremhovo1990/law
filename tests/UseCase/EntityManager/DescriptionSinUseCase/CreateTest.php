<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\DescriptionSinUseCase;

use App\Commands\Cp\DescriptionSinCommand;
use App\DataFixtures\PreceptFixtures;
use App\Entity\DescriptionSin;
use App\Tests\Helpers\DoctrineTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreateTest extends KernelTestCase
{
    use DoctrineTrait;
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $precept = $this->grabPreceptById(PreceptFixtures::ID_SIX);
        $sins = $this->grabSinByNames([PreceptFixtures::SIN_NAME_FIRST, PreceptFixtures::SIN_NAME_SECOND]);
        $ids = [];
        foreach ($sins as $sin) {
            $ids[] = (string) $sin->getId();
        }
        $command = new DescriptionSinCommand(
            'Name',
            'Test',
            $precept,
            sins: $ids,
        );
        $command->setMeta(['title' => 'Title', 'description' => 'Description']);
        $service = $this->grabService();
        Assert::assertInstanceOf(DescriptionSin::class, $service->create($command));
    }
}
