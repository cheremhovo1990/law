<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\BookManagerUseCase;

use App\Commands\Cp\BookCommand;
use App\DataFixtures\ImageFixture;
use App\Doctrine\Collections\BookLinkCollection;
use App\Doctrine\Collections\TextCollection;
use App\Doctrine\Models\BookLinkTypeEnum;
use App\Entity\Book;
use App\Entity\LanguageOfThePublicationEnum;
use App\Tests\Helpers\DoctrineTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreateTest extends KernelTestCase
{
    use DoctrineTrait;
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $image = $this->grabImageByName(ImageFixture::NAMES_FIRST);
        $command = new BookCommand(
            links: new BookLinkCollection([['link' => 'https://www.youtube.com/', 'type' => BookLinkTypeEnum::BUY->value]]),
            isbnCollection: new TextCollection([['text' => '978-5-91362-213-6']]),
            dateOfWriting: new \DateTime(),
            pageSize: 100,
            languageOfThePublication: LanguageOfThePublicationEnum::RUSSIAN->value,
        );
        $command->setMeta(['title' => 'Title', 'description' => 'Description']);
        $command->setCoverImage(['id' => (string) $image->getId(), 'name' => $image->getName()]);

        $service = $this->grabService();
        Assert::assertInstanceOf(Book::class, $service->create($command));
    }
}
