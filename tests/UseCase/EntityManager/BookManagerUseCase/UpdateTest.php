<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\BookManagerUseCase;

use App\Commands\Cp\BookCommand;
use App\DataFixtures\BookFixture;
use App\Doctrine\Models\BookLinkTypeEnum;
use App\Tests\Helpers\DoctrineTrait;
use App\UseCase\EntityManager\Exceptions\CannotFoundImageRuntimeException;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UpdateTest extends KernelTestCase
{
    use DoctrineTrait;
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $book = $this->grabBookById(BookFixture::ID_FIRST);
        $command = BookCommand::create($book);
        $command->setLinks([['link' => 'https://www.youtube.com/', 'type' => BookLinkTypeEnum::BUY->value]]);
        $command->setIsbnCollection([['text' => '978-5-91362-213-7']]);
        $coverImage = $book->getCoverImage();
        $command->setCoverImage($coverImage->toArray());
        $service = $this->grabService();
        Assert::assertTrue($service->update($command, $book));
    }

    public function testImageNotFound(): void
    {
        $book = $this->grabBookById(BookFixture::ID_FIRST);
        $command = BookCommand::create($book);
        $coverImage = $book->getCoverImage();
        $command->setCoverImage(['id' => '883ccffe-2b58-4c2a-89c2-f01c16dae6fd'] + $coverImage->toArray());
        $this->expectException(CannotFoundImageRuntimeException::class);
        $service = $this->grabService();
        $service->update($command, $book);
    }
}
