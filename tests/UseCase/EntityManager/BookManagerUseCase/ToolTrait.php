<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\BookManagerUseCase;

use App\UseCase\EntityManager\BookManagerUseCase;

trait ToolTrait
{
    public function grabService(): BookManagerUseCase
    {
        return parent::getContainer()->get(BookManagerUseCase::class);
    }
}
