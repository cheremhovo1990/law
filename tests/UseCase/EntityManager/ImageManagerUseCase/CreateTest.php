<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\ImageManagerUseCase;

use App\Commands\Cp\ImageCommand;
use App\Entity\Image;
use App\UseCase\EntityManager\ImageManagerUseCase;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CreateTest extends KernelTestCase
{
    public function testSuccess(): void
    {
        $command = new ImageCommand();
        $file = new UploadedFile(
            path: '/app/tests/data/book-1.jpg',
            originalName: 'book.jpg',
        );
        $command->setImage($file);
        $service = $this->grabService();
        $book = $service->create($command);
        Assert::assertInstanceOf(Image::class, $book);
    }

    public function grabService(): ImageManagerUseCase
    {
        return parent::getContainer()->get(ImageManagerUseCase::class);
    }
}
