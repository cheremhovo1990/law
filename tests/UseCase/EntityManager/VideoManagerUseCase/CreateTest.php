<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\VideoManagerUseCase;

use App\Commands\Cp\VideoCommand;
use App\Doctrine\Models\Meta;
use App\Entity\Video;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreateTest extends KernelTestCase
{
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $service = $this->grabService();
        $command = new VideoCommand(
            'Title',
            'Link',
            'Description',
            true,
            new Meta('Title', 'Description'),
        );
        $command->setMeta(['title' => 'Title', 'description' => 'description']);
        Assert::assertInstanceOf(Video::class, $service->create($command));
    }
}
