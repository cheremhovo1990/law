<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\VideoManagerUseCase;

use App\DataFixtures\VideoFixture;
use App\Tests\Helpers\DoctrineTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DeleteTest extends KernelTestCase
{
    use DoctrineTrait;
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $service = $this->grabService();
        Assert::assertTrue($service->delete((string) VideoFixture::ID_FIRST));
    }
}
