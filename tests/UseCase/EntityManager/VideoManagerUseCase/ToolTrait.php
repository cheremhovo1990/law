<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\VideoManagerUseCase;

use App\UseCase\EntityManager\VideoManagerUseCase;

trait ToolTrait
{
    public function grabService(): VideoManagerUseCase
    {
        return parent::getContainer()->get(VideoManagerUseCase::class);
    }
}
