<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\DictionaryManagerUseCase;

use App\Commands\Cp\DictionaryCommand;
use App\DataFixtures\DictionaryFixture;
use App\Entity\Dictionary;
use App\Tests\Helpers\DoctrineTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UpdateTest extends KernelTestCase
{
    use DoctrineTrait;
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        /** @var Dictionary $dictionary */
        $dictionary = $this->grabDictionaryById(DictionaryFixture::ID_FIRST);
        $command = new DictionaryCommand('Name', 'Description');
        $service = $this->grabService();
        Assert::assertTrue($service->update($command, $dictionary));
    }
}
