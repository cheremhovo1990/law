<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\DictionaryManagerUseCase;

use App\UseCase\EntityManager\DictionaryManagerUseCase;

trait ToolTrait
{
    public function grabService(): DictionaryManagerUseCase
    {
        return parent::getContainer()->get(DictionaryManagerUseCase::class);
    }
}
