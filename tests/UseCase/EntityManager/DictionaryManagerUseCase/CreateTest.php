<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\DictionaryManagerUseCase;

use App\Commands\Cp\DictionaryCommand;
use App\Entity\Dictionary;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreateTest extends KernelTestCase
{
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $command = new DictionaryCommand(
            'Name',
            'Description',
        );
        $service = $this->grabService();
        Assert::assertInstanceOf(Dictionary::class, $service->create($command));
    }
}
