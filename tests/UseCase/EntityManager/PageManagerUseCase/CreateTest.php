<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\PageManagerUseCase;

use App\Commands\Cp\PageCommand;
use App\Doctrine\Models\Meta;
use App\Entity\Page;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreateTest extends KernelTestCase
{
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $command = new PageCommand(
            content: 'Text',
            title: 'Title',
            publish: true,
            meta: new Meta(title: 'Title', description: 'Description'),
        );

        $service = $this->grabService();
        Assert::assertInstanceOf(Page::class, $service->create($command));
    }
}
