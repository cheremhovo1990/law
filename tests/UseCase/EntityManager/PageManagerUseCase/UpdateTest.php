<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\PageManagerUseCase;

use App\Commands\Cp\PageCommand;
use App\DataFixtures\PageFixture;
use App\Tests\Helpers\DoctrineTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UpdateTest extends KernelTestCase
{
    use DoctrineTrait;
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $entity = $this->grabPageById(PageFixture::ID_FIRST);
        $command = PageCommand::create($entity);
        $command->setTitle('Title 2');
        $command->setContent('Text 2');
        $command->setPublish(false);
        $service = $this->grabService();
        Assert::assertTrue($service->update($command, $entity));
    }
}
