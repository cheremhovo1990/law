<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\PageManagerUseCase;

use App\UseCase\EntityManager\PageManagerUseCase;

trait ToolTrait
{
    public function grabService(): PageManagerUseCase
    {
        return parent::getContainer()->get(PageManagerUseCase::class);
    }
}
