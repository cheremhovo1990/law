<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\SinManagerUseCase;

use App\UseCase\EntityManager\SinManagerUseCase;

trait ToolTrait
{
    public function grabService(): SinManagerUseCase
    {
        return parent::getContainer()->get(SinManagerUseCase::class);
    }
}
