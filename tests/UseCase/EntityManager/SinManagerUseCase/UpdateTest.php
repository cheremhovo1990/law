<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\SinManagerUseCase;

use App\Commands\Cp\SinCommand;
use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\DoctrineTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UpdateTest extends KernelTestCase
{
    use DoctrineTrait;
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $sin = $this->grabSinById(PreceptFixtures::SIN_ID_FIRST);
        $command = SinCommand::create($sin);
        $service = $this->grabService();
        Assert::assertTrue($service->update($command, $sin));
    }
}
