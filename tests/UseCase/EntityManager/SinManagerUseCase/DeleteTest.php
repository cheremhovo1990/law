<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\SinManagerUseCase;

use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\DoctrineTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DeleteTest extends KernelTestCase
{
    use DoctrineTrait;
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $service = $this->grabService();
        Assert::assertTrue($service->delete(PreceptFixtures::SIN_ID_FIRST));
    }

    public function testNotFound(): void
    {
        $service = $this->grabService();
        $this->expectException(NotFoundHttpException::class);
        $service->delete('01G6B198HGBDDHD3F5FDMD6P3D');
    }
}
