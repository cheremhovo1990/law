<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\SinManagerUseCase;

use App\Commands\Cp\SinCommand;
use App\DataFixtures\PreceptFixtures;
use App\Entity\Sin;
use App\Tests\Helpers\DoctrineTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreateTest extends KernelTestCase
{
    use DoctrineTrait;
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $precept = $this->grabPreceptById(PreceptFixtures::ID_SIX);
        $command = new SinCommand(precept: $precept);
        $service = $this->grabService();
        Assert::assertInstanceOf(Sin::class, $service->create($command));
    }
}
