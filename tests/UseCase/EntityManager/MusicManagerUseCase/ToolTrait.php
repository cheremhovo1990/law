<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\MusicManagerUseCase;

use App\UseCase\EntityManager\MusicManagerUseCase;

trait ToolTrait
{
    public function grabService(): MusicManagerUseCase
    {
        return parent::getContainer()->get(MusicManagerUseCase::class);
    }
}
