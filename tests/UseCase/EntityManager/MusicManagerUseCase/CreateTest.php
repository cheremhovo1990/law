<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\MusicManagerUseCase;

use App\Commands\Cp\MusicCommand;
use App\Doctrine\Models\Meta;
use App\Entity\Music;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreateTest extends KernelTestCase
{
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $service = $this->grabService();
        $command = new MusicCommand(
            'Title',
            'Link',
            'Description',
            true,
            new Meta('Title', 'Description'),
        );
        $command->setMeta(['title' => 'Title', 'description' => 'description']);
        Assert::assertInstanceOf(Music::class, $service->create($command));
    }
}
