<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\MusicManagerUseCase;

use App\DataFixtures\MusicFixture;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DeleteTest extends KernelTestCase
{
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $service = $this->grabService();
        Assert::assertTrue($service->delete(MusicFixture::ID_FIRST));
    }
}
