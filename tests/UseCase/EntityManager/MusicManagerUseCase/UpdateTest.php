<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\MusicManagerUseCase;

use App\Commands\Cp\MusicCommand;
use App\DataFixtures\MusicFixture;
use App\Doctrine\Models\Meta;
use App\Tests\Helpers\DoctrineTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UpdateTest extends KernelTestCase
{
    use DoctrineTrait;
    use ToolTrait;

    // tests
    public function testSuccess(): void
    {
        $entity = $this->grabMusicById(MusicFixture::ID_FIRST);
        $command = new MusicCommand(
            'Title',
            'Link',
            'Description',
            true,
            new Meta('Title', 'Description'),
        );
        $command->setMeta(['title' => 'Title', 'description' => 'description']);
        $service = $this->grabService();
        Assert::assertTrue($service->update($command, $entity));
    }
}
