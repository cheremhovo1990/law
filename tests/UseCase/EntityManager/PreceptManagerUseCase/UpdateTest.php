<?php

declare(strict_types=1);

namespace App\Tests\UseCase\EntityManager\PreceptManagerUseCase;

use App\Commands\Cp\PreceptCommand;
use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\DoctrineTrait;
use App\UseCase\EntityManager\PreceptManagerUseCase;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UpdateTest extends KernelTestCase
{
    use DoctrineTrait;

    // tests
    public function testSuccess(): void
    {
        $precept = $this->grabPreceptById(PreceptFixtures::ID_SIX);
        $command = PreceptCommand::create($precept);
        $service = $this->grabService();
        Assert::assertTrue($service->update($command, $precept));
    }

    public function grabService(): PreceptManagerUseCase
    {
        return parent::getContainer()->get(PreceptManagerUseCase::class);
    }
}
