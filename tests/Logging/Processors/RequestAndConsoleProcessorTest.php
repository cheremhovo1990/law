<?php

declare(strict_types=1);

namespace App\Tests\Logging\Processors;

use App\Logging\Processors\RequestAndConsoleProcessor;
use Monolog\Level;
use Monolog\LogRecord;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class RequestAndConsoleProcessorTest extends TestCase
{
    public function testConsoleSuccess(): void
    {
        $process = new RequestAndConsoleProcessor('cli');
        $record = new LogRecord(
            datetime: new \DateTimeImmutable(),
            channel: 'channel',
            level: Level::Info,
            message: 'message',
        );
        $record = $process->__invoke($record);
        Assert::assertSame('console', $record['extra']['app']);
    }

    public function testHttpSuccess(): void
    {
        $process = new RequestAndConsoleProcessor(
            sapi: 'fpm-fcgi',
            server: [
                'REQUEST_URI' => '/',
                'REQUEST_METHOD' => 'POST',
                'HTTP_HOST' => 'ten-laws.local',
                'HTTP_REFERER' => 'http://ten-laws.local/cp',
                'APP_ENV' => 'dev',
            ],
        );
        $record = new LogRecord(
            datetime: new \DateTimeImmutable(),
            channel: 'channel',
            level: Level::Info,
            message: 'message',
        );
        $record = $process->__invoke($record);
        Assert::assertSame('http', $record['extra']['app']);
        Assert::assertSame('/', $record['extra']['REQUEST_URI']);
        Assert::assertSame('ten-laws.local', $record['extra']['HTTP_HOST']);
        Assert::assertSame('dev', $record['extra']['APP_ENV']);
    }
}
