<?php

declare(strict_types=1);

namespace App\Tests\Event\Subscribers;

use App\Event\Subscribers\ExceptionConsoleSubscriber;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

class ExceptionConsoleSubscriberTest extends KernelTestCase
{
    public function testDomainException(): void
    {
        $handler = $this->getSubscriber();
        $input = new ArrayInput([]);
        $output = new NullOutput();
        $exception = new \DomainException();
        $event = new ConsoleErrorEvent($input, $output, $exception);
        $handler->onConsoleEvent($event);
        Assert::assertEquals(0, $event->getExitCode());
    }

    public function testRuntimeException(): void
    {
        $handler = $this->getSubscriber();
        $input = new ArrayInput([]);
        $output = new NullOutput();
        $exception = new \RuntimeException();
        $event = new ConsoleErrorEvent($input, $output, $exception);
        $handler->onConsoleEvent($event);
        Assert::assertEquals(1, $event->getExitCode());
    }

    public function testOtherException(): void
    {
        $handler = $this->getSubscriber();
        $input = new ArrayInput([]);
        $output = new NullOutput();
        $exception = new CommandNotFoundException('Message');
        $event = new ConsoleErrorEvent($input, $output, $exception);
        $handler->onConsoleEvent($event);
        Assert::assertEquals(1, $event->getExitCode());
    }

    protected function getSubscriber(): ExceptionConsoleSubscriber
    {
        return parent::getContainer()->get(ExceptionConsoleSubscriber::class);
    }
}
