<?php

declare(strict_types=1);

namespace App\Tests\Event\Subscribers\SerializerSubscriber;

use App\Event\Subscribers\SerializerSubscriber;

trait ToolTrait
{
    protected function getSubscriber(): SerializerSubscriber
    {
        return parent::getContainer()->get(SerializerSubscriber::class);
    }
}
