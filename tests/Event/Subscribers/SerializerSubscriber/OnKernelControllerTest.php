<?php

declare(strict_types=1);

namespace App\Tests\Event\Subscribers\SerializerSubscriber;

use App\Event\Subscribers\SerializerSubscriber;
use App\Services\Serialize\SerializeToJson;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class OnKernelControllerTest extends KernelTestCase
{
    use ToolTrait;

    public function testSuccessExistsAttribute(): void
    {
        $class = new class() {
            #[SerializeToJson]
            public function __invoke(): void
            {
            }
        };
        $subscriber = $this->getSubscriber();
        $event = new ControllerEvent(
            self::$kernel,
            $class,
            $request = new Request(),
            HttpKernelInterface::MAIN_REQUEST,
        );

        $subscriber->onKernelController($event);
        Assert::assertInstanceOf(
            SerializeToJson::class,
            $request->attributes->get(SerializerSubscriber::KEY_SERIALIZE),
        );
    }

    public function testSuccessNotExistsAttribute(): void
    {
        $class = new class() {
            public function __invoke(): void
            {
            }
        };

        $subscriber = $this->getSubscriber();
        $event = new ControllerEvent(
            self::$kernel,
            $class,
            $request = new Request(),
            HttpKernelInterface::MAIN_REQUEST,
        );

        $subscriber->onKernelController($event);
        Assert::assertNull($request->attributes->get(SerializerSubscriber::KEY_SERIALIZE));
    }
}
