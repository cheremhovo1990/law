<?php

declare(strict_types=1);

namespace App\Tests\Event\Subscribers\SerializerSubscriber;

use App\Event\Subscribers\SerializerSubscriber;
use App\Helpers\MimeTypeResponseEnumHelper;
use App\Services\PaginatorInterface;
use App\Services\Serialize\SerializeRuntimeException;
use App\Services\Serialize\SerializeToJson;
use App\Tests\Helpers\AssertJson;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class OnKernelViewTest extends KernelTestCase
{
    use ToolTrait;

    public function testSuccessNotAttribute(): void
    {
        $subscriber = $this->getSubscriber();
        $request = new Request();
        $event = new ViewEvent(
            self::$kernel,
            $request,
            HttpKernelInterface::MAIN_REQUEST,
            '',
        );
        $subscriber->onKernelView($event);
        Assert::assertNull($event->getResponse());
    }

    public function testSuccessForm(): void
    {
        $subscriber = $this->getSubscriber();
        $request = new Request();
        $request->headers->set(MimeTypeResponseEnumHelper::ACCEPT->name, MimeTypeResponseEnumHelper::ACCEPT->value);
        $request->attributes->set(SerializerSubscriber::KEY_SERIALIZE, new SerializeToJson());
        $event = new ViewEvent(
            self::$kernel,
            $request,
            HttpKernelInterface::MAIN_REQUEST,
            $this->getForm(),
        );
        $subscriber->onKernelView($event);
        AssertJson::assertContainsJson(['success' => false, 'form' => []], $event->getResponse()->getContent());
    }

    public function testSuccessArray(): void
    {
        $subscriber = $this->getSubscriber();
        $request = new Request();
        $request->headers->set(MimeTypeResponseEnumHelper::ACCEPT->name, MimeTypeResponseEnumHelper::ACCEPT->value);
        $request->attributes->set(SerializerSubscriber::KEY_SERIALIZE, new SerializeToJson());
        $event = new ViewEvent(
            self::$kernel,
            $request,
            HttpKernelInterface::MAIN_REQUEST,
            $content = ['success' => true, 'message' => 'OK'],
        );
        $subscriber->onKernelView($event);
        AssertJson::assertContainsJson($content, $event->getResponse()->getContent());
    }

    public function testSuccessNotAcceptJson(): void
    {
        $subscriber = $this->getSubscriber();
        $request = new Request();
        $request->attributes->set(SerializerSubscriber::KEY_SERIALIZE, new SerializeToJson());
        $event = new ViewEvent(
            self::$kernel,
            $request,
            HttpKernelInterface::MAIN_REQUEST,
            $content = ['success' => true, 'message' => 'OK'],
        );
        $this->expectException(SerializeRuntimeException::class);
        $subscriber->onKernelView($event);
    }

    public function testSuccessPaginator(): void
    {
        $subscriber = $this->getSubscriber();
        $request = new Request();
        $request->headers->set(MimeTypeResponseEnumHelper::ACCEPT->name, MimeTypeResponseEnumHelper::ACCEPT->value);
        $request->attributes->set(SerializerSubscriber::KEY_SERIALIZE, new SerializeToJson());
        $event = new ViewEvent(
            self::$kernel,
            $request,
            HttpKernelInterface::MAIN_REQUEST,
            new PaginatorService(),
        );
        $subscriber->onKernelView($event);
        AssertJson::assertContainsJson(
            [
                'data' => [],
                'paginator' => [
                    'limit' => 10,
                    'page' => 1,
                    'maxPage' => 101,
                    'total' => 100,
                ],
            ],
            $event->getResponse()->getContent(),
        );
    }

    protected function getForm(): FormInterface
    {
        return parent::getContainer()->get('form.factory')->createBuilder(FormType::class, null, [])->getForm();
    }
}

/**
 * @implements \IteratorAggregate<integer, mixed>
 */
class PaginatorService implements \Countable, \IteratorAggregate, PaginatorInterface
{
    /**
     * @return \ArrayIterator<integer, mixed>
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator([]);
    }

    public function count(): int
    {
        return 100;
    }

    public function getTotal(): int
    {
        return count($this);
    }

    public function getPage(): int
    {
        return 1;
    }

    public function getMaxPage(): int
    {
        return 101;
    }

    public function getLimit(): int
    {
        return 10;
    }
}
