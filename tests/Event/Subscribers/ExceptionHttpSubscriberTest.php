<?php

declare(strict_types=1);

namespace App\Tests\Event\Subscribers;

use App\Event\Subscribers\ExceptionHttpSubscriber;
use App\Helpers\MimeTypeResponseEnumHelper;
use App\Tests\Helpers\AssertJson;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class ExceptionHttpSubscriberTest extends KernelTestCase
{
    public function testNotJson(): void
    {
        $handler = $this->getSubscriber();
        $request = new Request();
        $exception = new \DomainException();
        $event = new ExceptionEvent(
            self::$kernel,
            $request,
            HttpKernelInterface::MAIN_REQUEST,
            $exception,
        );
        $handler->onException($event);
        Assert::assertNull($event->getResponse());
        Assert::assertFalse($event->isPropagationStopped());
    }

    public function testHttpException(): void
    {
        $handler = $this->getSubscriber();
        $request = new Request();
        $exception = new NotFoundHttpException();
        $event = new ExceptionEvent(
            self::$kernel,
            $request,
            HttpKernelInterface::MAIN_REQUEST,
            $exception,
        );
        $handler->onException($event);
        Assert::assertNull($event->getResponse());
        Assert::assertFalse($event->isPropagationStopped());
    }

    public function testDomainException(): void
    {
        $handler = $this->getSubscriber();
        $request = new Request();
        $request->headers->set(MimeTypeResponseEnumHelper::ACCEPT->name, MimeTypeResponseEnumHelper::ACCEPT->value);
        $exception = new \DomainException('message');
        $event = new ExceptionEvent(
            self::$kernel,
            $request,
            HttpKernelInterface::MAIN_REQUEST,
            $exception,
        );
        $handler->onException($event);
        AssertJson::assertContainsJson(['success' => false, 'message' => 'message'], $event->getResponse()->getContent());
        Assert::assertInstanceOf(JsonResponse::class, $event->getResponse());
        Assert::assertTrue($event->isPropagationStopped());
    }

    public function testRuntimeException(): void
    {
        $handler = $this->getSubscriber();
        $request = new Request();
        $request->headers->set(MimeTypeResponseEnumHelper::ACCEPT->name, MimeTypeResponseEnumHelper::ACCEPT->value);
        $exception = new \RuntimeException();
        $event = new ExceptionEvent(
            self::$kernel,
            $request,
            HttpKernelInterface::MAIN_REQUEST,
            $exception,
        );
        $handler->onException($event);
        Assert::assertInstanceOf(JsonResponse::class, $event->getResponse());
        AssertJson::assertContainsJson(['success' => false, 'message' => 'app.error'], $event->getResponse()->getContent());
        Assert::assertTrue($event->isPropagationStopped());
    }

    protected function getSubscriber(): ExceptionHttpSubscriber
    {
        return parent::getContainer()->get(ExceptionHttpSubscriber::class);
    }
}
