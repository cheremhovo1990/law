<?php

declare(strict_types=1);

namespace App\Tests\Helpers\Constraint;

use App\Tests\Helpers\Util\JsonArray;
use App\Tests\Helpers\Util\JsonType as JsonTypeUtil;
use PHPUnit\Framework\Constraint\Constraint;
use PHPUnit\Framework\ExpectationFailedException;

class JsonType extends Constraint
{
    /**
     * @var array<string|integer, mixed>
     */
    protected array $jsonType;

    private bool $match;

    /**
     * @param array<string|integer, mixed> $jsonType
     */
    public function __construct(array $jsonType, bool $match = true)
    {
        $this->jsonType = $jsonType;
        $this->match = $match;
    }

    /**
     * Evaluates the constraint for parameter $other. Returns true if the
     * constraint is met, false otherwise.
     *
     * @param mixed $jsonArray value or object to evaluate
     */
    protected function matches(mixed $jsonArray): bool
    {
        if ($jsonArray instanceof JsonArray) {
            $jsonArray = $jsonArray->toArray();
        }

        $matched = (new JsonTypeUtil($jsonArray))->matches($this->jsonType);

        if ($this->match) {
            if (true !== $matched) {
                throw new ExpectationFailedException($matched);
            }
        } elseif (true === $matched) {
            $jsonArray = \json_encode($jsonArray, JSON_THROW_ON_ERROR);
            throw new ExpectationFailedException('Unexpectedly response matched: ' . $jsonArray);
        }

        return true;
    }

    /**
     * Returns a string representation of the constraint.
     */
    public function toString(): string
    {
        // unused
        return '';
    }

    protected function failureDescription($other): string
    {
        // unused
        return '';
    }
}
