<?php

declare(strict_types=1);

namespace App\Tests\Helpers\Constraint;

use App\Tests\Helpers\Util\JsonArray;
use PHPUnit\Framework\AssertionFailedError;
use PHPUnit\Framework\Constraint\Constraint;
use PHPUnit\Framework\ExpectationFailedException;
use SebastianBergmann\Comparator\ArrayComparator;
use SebastianBergmann\Comparator\ComparisonFailure;
use SebastianBergmann\Comparator\Factory;

class JsonContains extends Constraint
{
    /**
     * @var array<mixed>
     */
    protected array $expected;

    /**
     * @param array<mixed> $expected
     */
    public function __construct(array $expected)
    {
        $this->expected = $expected;
    }

    /**
     * Evaluates the constraint for parameter $other. Returns true if the
     * constraint is met, false otherwise.
     *
     * @param mixed $other value or object to evaluate
     */
    protected function matches($other): bool
    {
        $jsonResponseArray = new JsonArray($other);
        if (!\is_array($jsonResponseArray->toArray())) {
            throw new AssertionFailedError('JSON response is not an array: ' . $other);
        }
        $jsonArrayContainsArray = $jsonResponseArray->containsArray($this->expected);

        if ($jsonArrayContainsArray) {
            return true;
        }

        $comparator = new ArrayComparator();
        $comparator->setFactory(new Factory());
        try {
            $comparator->assertEquals($this->expected, $jsonResponseArray->toArray());
        } catch (ComparisonFailure $failure) {
            throw new ExpectationFailedException("Response JSON does not contain the provided JSON\n", $failure);
        }

        return false;
    }

    /**
     * Returns a string representation of the constraint.
     */
    public function toString(): string
    {
        // unused
        return '';
    }

    protected function failureDescription($other): string
    {
        // unused
        return '';
    }
}
