<?php

declare(strict_types=1);

namespace App\Tests\Helpers;

use PHPUnit\Framework\Assert;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormInterface;

trait FormTrait
{
    /**
     * @param array<string, mixed> $submittedData
     */
    public function submit(string $type, array $submittedData, mixed $data = null): FormInterface
    {
        $form = $this->create($type, $data);
        $form->submit($submittedData);
        Assert::assertTrue($form->isSubmitted());

        return $form;
    }

    public function create(string $type, mixed $data = null): FormInterface
    {
        return parent::getContainer()->get('form.factory')->create($type, $data, ['csrf_protection' => false]);
    }

    /**
     * @return array<integer, string>
     */
    public function findNamesByCode(FormInterface $form, string $code): array
    {
        $errors = $form->getErrors(true);
        $items = $errors->findByCodes($code);

        return $this->getNames($items);
    }

    /**
     * @return array<integer, string>
     */
    protected function getNames(FormErrorIterator $errors): array
    {
        $names = [];
        foreach ($errors as $error) {
            $names[] = $error->getOrigin()->getName();
        }

        return $names;
    }
}
