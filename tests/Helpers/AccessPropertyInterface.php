<?php

declare(strict_types=1);

namespace App\Tests\Helpers;

interface AccessPropertyInterface
{
    public function createService(object $object): Mock;
}
