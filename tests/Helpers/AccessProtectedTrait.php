<?php

declare(strict_types=1);

namespace App\Tests\Helpers;

use App\Services\CalDAV\CalDAVEventInterface;

trait AccessProtectedTrait
{
    public function createService(object $object): Mock
    {
        return new Mock($object);
    }
}

/**
 * @method getMonthlyRule(CalDAVEventInterface $event, \DateTimeZone $timeZone)
 * @method getWeeklyRule(CalDAVEventInterface $event, \DateTimeZone $timeZone)
 */
class Mock
{
    protected object $object;
    /** @var array|\ReflectionMethod[] */
    protected array $methods;

    public function __construct(object $object)
    {
        $this->object = $object;
        $reflection = new \ReflectionClass($object);
        $this->methods = $reflection->getMethods();
    }

    /**
     * @param array<string, mixed> $arguments
     */
    public function __call(string $name, array $arguments): mixed
    {
        foreach ($this->methods as $method) {
            if ($method->getName() === $name) {
                if ($method->isPrivate() || $method->isProtected()) {
                    $method->setAccessible(true);
                }

                return $method->invokeArgs($this->object, $arguments);
            }
        }
        throw new \RuntimeException();
    }
}
