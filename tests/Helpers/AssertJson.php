<?php

declare(strict_types=1);

namespace App\Tests\Helpers;

use App\Tests\Helpers\Constraint\JsonContains;
use App\Tests\Helpers\Constraint\JsonType;
use App\Tests\Helpers\Util\JsonArray;
use PHPUnit\Framework\Assert;

class AssertJson
{
    /**
     * https://codeception.com/docs/modules/REST#seeResponseContainsJson.
     *
     * @param array<integer|string, mixed> $expect
     */
    public static function assertContainsJson(array $expect, string $actual): void
    {
        Assert::assertThat(
            $actual,
            new JsonContains($expect),
        );
    }

    /**
     * https://codeception.com/docs/modules/REST#seeResponseMatchesJsonType.
     *
     * @param array<integer|string, mixed> $jsonType
     */
    public static function assertMatchesJsonType(array $jsonType, string $actual, string $jsonPath = null): void
    {
        $jsonArray = new JsonArray($actual);
        if ($jsonPath) {
            $jsonArray = $jsonArray->filterByJsonPath($jsonPath);
        }

        Assert::assertThat($jsonArray, new JsonType($jsonType));
    }
}
