<?php

declare(strict_types=1);

namespace App\Tests\Helpers\SymbolHelper;

use App\Helpers\SymbolHelper;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ConvertToCharacterTest extends KernelTestCase
{
    public function testSuccess(): void
    {
        $result = SymbolHelper::convertToCharacter('&#1040;&#1158;&#769;');
        Assert::assertEquals('А҆́', $result);
    }
}
