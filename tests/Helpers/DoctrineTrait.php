<?php

declare(strict_types=1);

namespace App\Tests\Helpers;

use App\Entity\Book;
use App\Entity\DescriptionSin;
use App\Entity\Dictionary;
use App\Entity\Image;
use App\Entity\Meta;
use App\Entity\Music;
use App\Entity\Page;
use App\Entity\Precept;
use App\Entity\Sin;
use App\Entity\Video;
use App\Repository\BookRepository;
use App\Repository\DescriptionSinRepository;
use App\Repository\DictionaryRepository;
use App\Repository\ImageRepository;
use App\Repository\MetaRepository;
use App\Repository\MusicRepository;
use App\Repository\PageRepository;
use App\Repository\PreceptRepository;
use App\Repository\SinRepository;
use App\Repository\VideoRepository;

trait DoctrineTrait
{
    public function grabPreceptById(string $id): Precept
    {
        /** @var PreceptRepository $rep */
        $rep = parent::getContainer()->get(PreceptRepository::class);

        return $rep->find($id);
    }

    public function grabSinById(string $id): Sin
    {
        /** @var SinRepository $rep */
        $rep = parent::getContainer()->get(SinRepository::class);

        return $rep->find($id);
    }

    public function grabSinByName(string $name): Sin
    {
        /** @var SinRepository $rep */
        $rep = parent::getContainer()->get(SinRepository::class);

        return $rep->findOneBy(['name' => $name]);
    }

    /**
     * @param array<integer, string> $names
     *
     * @return array<integer, Sin>
     */
    public function grabSinByNames(array $names): array
    {
        /** @var SinRepository $rep */
        $rep = parent::getContainer()->get(SinRepository::class);

        return $rep->findAllByInName($names);
    }

    public function grabVideoByLink(string $link): Video
    {
        /** @var VideoRepository $rep */
        $rep = parent::getContainer()->get(VideoRepository::class);

        return $rep->findOneBy(
            ['link' => $link],
        );
    }

    public function grabVideoById(string $id): Video
    {
        /** @var VideoRepository $rep */
        $rep = parent::getContainer()->get(VideoRepository::class);

        return $rep->find($id);
    }

    public function grabMusicById(string $id): Music
    {
        /** @var MusicRepository $rep */
        $rep = parent::getContainer()->get(MusicRepository::class);

        return $rep->find($id);
    }

    public function grabMetaByKey(string $key): Meta
    {
        /** @var MetaRepository $rep */
        $rep = parent::getContainer()->get(MetaRepository::class);

        return $rep->findOneBy(['key' => $key]);
    }

    public function grabBookById(string $id): Book
    {
        /** @var BookRepository $rep */
        $rep = parent::getContainer()->get(BookRepository::class);

        return $rep->find($id);
    }

    public function grabDictionaryByDescription(string $description): Dictionary
    {
        /** @var DictionaryRepository $rep */
        $rep = parent::getContainer()->get(DictionaryRepository::class);

        return $rep->findOneBy(['description' => $description]);
    }

    public function grabDictionaryById(string $id): Dictionary
    {
        /** @var DictionaryRepository $rep */
        $rep = parent::getContainer()->get(DictionaryRepository::class);

        return $rep->find($id);
    }

    public function grabDescriptionSinByName(string $name): DescriptionSin
    {
        /** @var DescriptionSinRepository $rep */
        $rep = parent::getContainer()->get(DescriptionSinRepository::class);

        return $rep->findOneBy(['name' => $name]);
    }

    public function grabDescriptionSinById(string $id): DescriptionSin
    {
        /** @var DescriptionSinRepository $rep */
        $rep = parent::getContainer()->get(DescriptionSinRepository::class);

        return $rep->find($id);
    }

    public function grabImageByName(string $name): Image
    {
        /** @var ImageRepository $rep */
        $rep = parent::getContainer()->get(ImageRepository::class);

        return $rep->findOneBy(['name' => $name]);
    }

    public function grabPageById(string $id): Page
    {
        /** @var PageRepository $rep */
        $rep = parent::getContainer()->get(PageRepository::class);

        return $rep->find($id);
    }
}
