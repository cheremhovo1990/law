<?php

declare(strict_types=1);

namespace App\Tests\Services\Meta;

use App\Doctrine\Models\Meta;
use App\Services\Meta\ManagerMetaService;
use App\Services\Meta\MetaCommandInterface;
use App\Services\Meta\MetaCommandTrait;
use App\Services\Meta\MetaInterface;
use App\Services\Meta\MetaTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ManagerMetaServiceTest extends KernelTestCase
{
    public function testAddSuccess(): void
    {
        $service = $this->grabService();
        $command = new Command();
        $command->setMeta(['title' => 'Title', 'description' => 'Description']);
        $entity = new Entity();
        $service->add($entity, $command);
        Assert::assertInstanceOf(Meta::class, $entity->getMeta());
    }

    public function testUpdateReplace(): void
    {
        $service = $this->grabService();
        $command = new Command();
        $command->setMeta(['title' => 'Title', 'description' => 'Description']);
        $entity = new Entity();
        $meta = new Meta();
        $entity->setMeta($meta);
        $service->update($entity, $command);
        Assert::assertNotEquals($meta, $entity->getMeta());
    }

    public function testUpdateNotReplace(): void
    {
        $service = $this->grabService();
        $command = new Command();
        $command->setMeta(['title' => 'Title', 'description' => 'Description']);
        $entity = new Entity();
        $meta = new Meta(
            title: 'Title',
            description: 'Description',
        );
        $entity->setMeta($meta);
        $service->update($entity, $command);
        Assert::assertEquals($meta, $entity->getMeta());
    }

    protected function grabService(): ManagerMetaService
    {
        return parent::getContainer()->get(ManagerMetaService::class);
    }
}

class Command implements MetaCommandInterface
{
    use MetaCommandTrait;
}

class Entity implements MetaInterface
{
    use MetaTrait;
}
