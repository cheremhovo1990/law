<?php

declare(strict_types=1);

namespace App\Tests\Services\CalDav\CalDAVEventHandleService;

use App\Services\CalDAV\CalDAVRule;
use App\Tests\Helpers\AccessPropertyInterface;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GetMonthlyRuleTest extends KernelTestCase implements AccessPropertyInterface
{
    use \App\Tests\Helpers\AccessProtectedTrait;
    use ToolTrait;

    /**
     * @dataProvider getDataProvider
     */
    public function testSuccess(string $data): void
    {
        $event = $this->createEvent($data);
        $service = $this->grabService();

        /* @phpstan-ignore-next-line */
        $rule = $service->getMonthlyRule($event, new \DateTimeZone('UTC'));
        Assert::assertInstanceOf(CalDAVRule::class, $rule);
        Assert::assertTrue($rule->isMonthly());
    }

    /**
     * @return array<integer, array<string, string>>
     */
    protected function getDataProvider(): iterable
    {
        yield [
            'data' => <<<DATA
TZID:Asia/Irkutsk
URL:https://calendar.yandex.ru/event?event_id=1701424530
RRULE:FREQ=MONTHLY;BYDAY=3TH;UNTIL=20220710;INTERVAL=1
TRANSP:OPAQUE
DATA,
        ];
        yield [
            'data' => <<<DATA
TZID:Asia/Irkutsk
URL:https://calendar.yandex.ru/event?event_id=1701424530
RRULE:FREQ=MONTHLY;BYDAY=3TH;INTERVAL=1
TRANSP:OPAQUE
DATA,
        ];
        yield [
            'data' => <<<DATA
URL:https://calendar.yandex.ru/event?event_id=1701424530
TZID:Asia/Irkutsk
RRULE:FREQ=MONTHLY;BYMONTHDAY=18;UNTIL=20220630;INTERVAL=1
TRANSP:OPAQUE
DATA,
        ];
        yield [
            'data' => <<<DATA
URL:https://calendar.yandex.ru/event?event_id=1701424530
TZID:Asia/Irkutsk
RRULE:FREQ=MONTHLY;BYMONTHDAY=18;INTERVAL=1
TRANSP:OPAQUE
DATA,
        ];
    }

    public function testNull(): void
    {
        $data = <<<DATA
URL:https://calendar.yandex.ru/event?event_id=1701424530
TZID:Asia/Irkutsk
RRULE:FREQ=WEEKLY;BYDAY=TU,WE,TH,FR,SA,SU;UNTIL=20220530T170000Z;INTERVAL=1
TRANSP:OPAQUE
DATA;

        $event = $this->createEvent($data);
        $service = $this->grabService();
        /* @phpstan-ignore-next-line */
        $rule = $service->getMonthlyRule($event, new \DateTimeZone('UTC'));
        Assert::assertNull($rule);
    }
}
