<?php

declare(strict_types=1);

namespace App\Tests\Services\CalDav\CalDAVEventHandleService;

use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GetDescriptionTest extends KernelTestCase
{
    use ToolTrait;

    public function testSuccess(): void
    {
        $data = <<<DATA
CREATED:20220510T144100Z
TZID:Asia/Irkutsk
DESCRIPTION:Расписание на каждые день
URL:https://calendar.yandex.ru/event?event_id=1701424530
DATA;
        $event = $this->createEvent($data);
        $service = $this->grabService();
        $result = $service->getDescription($event);
        Assert::assertEquals('Расписание на каждые день', $result);
    }

    public function testNull(): void
    {
        $data = <<<DATA
TZNAME:+08
TZID:Asia/Irkutsk
DTSTART:19910331T020000
RDATE:19910331T020000
DATA;
        $event = $this->createEvent($data);
        $service = $this->grabService();
        $result = $service->getDescription($event);
        Assert::assertNull($result);
    }
}
