<?php

declare(strict_types=1);

namespace App\Tests\Services\CalDav\CalDAVEventHandleService;

use App\Services\CalDAV\CalDAVRuntimeException;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GetDateTimeStartTest extends KernelTestCase
{
    use ToolTrait;

    /**
     * @dataProvider getDataProvider
     */
    public function testSuccess(string $result, string $data): void
    {
        $event = $this->createEvent($data);
        $service = $this->grabService();
        $start = $service->getDateTimeStart($event);
        Assert::assertEquals($result, $start->format('Y-m-d H:i:s'));
    }

    /**
     * @return array<integer, array<string, string>>
     */
    protected function getDataProvider(): iterable
    {
        yield [
            'result' => '2022-05-18 00:00:00',
            'data' => <<<DATA
BEGIN:VEVENT
TZID:Asia/Irkutsk
DTSTART;VALUE=DATE:20220518
DTEND;VALUE=DATE:20220519
DATA,
        ];

        yield [
            'result' => '2022-05-17 01:20:01',
            'data' => <<<DATA
BEGIN:VEVENT
TZID:Asia/Irkutsk
DTSTART;TZID=Asia/Irkutsk:20220517T012001
DTEND;TZID=Asia/Irkutsk:20220517T023000
DATA,
        ];
    }

    public function testException(): void
    {
        $data = <<<DATA
TZNAME:+08
DTSTART:19910331T020000
RDATE:19910331T020000
END:DAYLIGHT
TZID:Asia/Irkutsk
BEGIN:STANDARD
TZOFFSETFROM:+0800
TZOFFSETTO:+0700
TZNAME:+07
DTSTART:19910929T030000
RDATE:19910929T030000
DATA;
        $event = $this->createEvent($data);
        $service = $this->grabService();
        $this->expectException(CalDAVRuntimeException::class);
        $service->getDateTimeStart($event);
    }
}
