<?php

declare(strict_types=1);

namespace App\Tests\Services\CalDav\CalDAVEventHandleService;

use App\Services\CalDAV\CalDAVEvent;
use App\Services\CalDAV\CalDAVEventHandleService;
use App\Tests\Helpers\AccessPropertyInterface;
use App\Tests\Helpers\Mock;
use Carbon\CarbonImmutable;
use it\thecsea\simple_caldav_client\CalDAVObject;

trait ToolTrait
{
    protected function createEvent(string $data): CalDAVEvent
    {
        $object = new CalDAVObject(
            'https://caldav.yandex.ru/calendars/test@yandex.ru/events-1000/INU7ufOSyandex.ru.ics',
            $data,
            '1652367409382',
        );

        return new CalDAVEvent($object, CarbonImmutable::now(), CarbonImmutable::now());
    }

    /**
     * @return CalDAVEventHandleService|Mock
     */
    protected function grabService()
    {
        $service = parent::getContainer()->get(CalDAVEventHandleService::class);
        if ($this instanceof AccessPropertyInterface) {
            /* @phpstan-ignore-next-line */
            return $this->createService($service);
        }

        return $service;
    }
}
