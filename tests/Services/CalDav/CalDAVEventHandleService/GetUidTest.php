<?php

declare(strict_types=1);

namespace App\Tests\Services\CalDav\CalDAVEventHandleService;

use App\Services\CalDAV\CalDAVRuntimeException;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GetUidTest extends KernelTestCase
{
    use ToolTrait;

    public function testSuccess(): void
    {
        $data = <<<DATA
SUMMARY:Каждый день
UID:LB7OneU6yandex.ru
SEQUENCE:2
TZID:Asia/Irkutsk
DATA;
        $event = $this->createEvent($data);
        $service = $this->grabService();
        $result = $service->getUid($event);
        Assert::assertEquals('LB7OneU6yandex.ru', $result);
    }

    public function testException(): void
    {
        $data = <<<DATA
TZNAME:+08
DTSTART:19910331T020000
RDATE:19910331T020000
TZID:Asia/Irkutsk
DATA;
        $event = $this->createEvent($data);
        $service = $this->grabService();
        $this->expectException(CalDAVRuntimeException::class);
        $service->getUid($event);
    }
}
