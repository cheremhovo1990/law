<?php

declare(strict_types=1);

namespace App\Tests\Services\CalDav\CalDAVEventHandleService;

use App\Services\CalDAV\CalDAVRuntimeException;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GetSummaryTest extends KernelTestCase
{
    use ToolTrait;

    public function testSuccess(): void
    {
        $data = <<<DATA
DTEND;TZID=Asia/Irkutsk:20220517T023000
SUMMARY:Каждый день
TZID:Asia/Irkutsk
UID:LB7OneU6yandex.ru
DATA;
        $event = $this->createEvent($data);
        $service = $this->grabService();
        $result = $service->getSummary($event);
        Assert::assertEquals('Каждый день', $result);
    }

    public function testException(): void
    {
        $data = <<<DATA
TZNAME:+08
DTSTART:19910331T020000
RDATE:19910331T020000
TZID:Asia/Irkutsk
DATA;
        $event = $this->createEvent($data);
        $service = $this->grabService();
        $this->expectException(CalDAVRuntimeException::class);
        $service->getSummary($event);
    }
}
