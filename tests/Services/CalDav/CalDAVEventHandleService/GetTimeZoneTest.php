<?php

declare(strict_types=1);

namespace App\Tests\Services\CalDav\CalDAVEventHandleService;

use App\Services\CalDAV\CalDAVRuntimeException;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GetTimeZoneTest extends KernelTestCase
{
    use ToolTrait;

    public function testSuccess(): void
    {
        $data = <<<DATA
BEGIN:VTIMEZONE
TZID:Asia/Irkutsk
TZURL:http://tzurl.org/zoneinfo/Asia/Irkutsk
DATA;
        $event = $this->createEvent($data);
        $service = $this->grabService();
        $timeZone = $service->getTimeZone($event);
        Assert::assertInstanceOf(\DateTimeZone::class, $timeZone);
    }

    public function testException(): void
    {
        $data = <<<DATA
TZNAME:+08
DTSTART:19910331T020000
RDATE:19910331T020000
DATA;
        $this->expectException(CalDAVRuntimeException::class);
        $this->createEvent($data);
    }
}
