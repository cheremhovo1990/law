<?php

declare(strict_types=1);

namespace App\Tests\Services;

use App\Services\PhoneFilterService;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PhoneFilterServiceTest extends KernelTestCase
{
    /**
     * @dataProvider getDataProvide
     */
    public function testSuccess(string $phone, string $expected): void
    {
        $service = $this->grabService();
        $result = $service->normalize($phone);
        Assert::assertEquals($expected, $result);
    }

    /**
     * @return array<integer, mixed>
     */
    public function getDataProvide(): array
    {
        return [
            ['+7(915)142-07-63', '+79151420763'],
            ['7(915)142-07-63', '79151420763'],
        ];
    }

    protected function grabService(): PhoneFilterService
    {
        return parent::getContainer()->get(PhoneFilterService::class);
    }
}
