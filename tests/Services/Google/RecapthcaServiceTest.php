<?php

declare(strict_types=1);

namespace App\Tests\Services\Google;

use App\Exceptions\FailedVerificationRuntimeException;
use App\Services\Google\RecapthcaService;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class RecapthcaServiceTest extends KernelTestCase
{
    public function testNull(): void
    {
        $service = $this->grabService();
        $this->expectException(FailedVerificationRuntimeException::class);
        $service->verify(null);
    }

    public function testNotSuccess(): void
    {
        $response = new MockResponse(json_encode(['success' => false]));
        $client = new MockHttpClient([$response]);
        $service = $this->grabService();
        $service->setClient($client);
        $this->expectException(FailedVerificationRuntimeException::class);
        $service->verify('test');
    }

    public function testSuccess(): void
    {
        $response = new MockResponse(json_encode(['success' => true]));
        $client = new MockHttpClient([$response]);
        $service = $this->grabService();
        $service->setClient($client);
        Assert::assertTrue($service->verify('test'));
    }

    protected function grabService(): RecapthcaService
    {
        return parent::getContainer()->get(RecapthcaService::class);
    }
}
