<?php

declare(strict_types=1);

namespace App\Tests\Services;

use App\Services\Publication\ManagerPublicationService;
use App\Services\Publication\PublicationInterface;
use App\Services\Publication\PublicationTrait;
use App\Services\Publication\PublishCommandInterface;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ManagerPublicationServiceTest extends KernelTestCase
{
    public function testPublished(): void
    {
        $service = $this->grabService();
        $command = new Command(true);
        $entity = new Entity();
        Assert::assertFalse($entity->isPublish());
        $service->toPublish($command, $entity);
        Assert::assertTrue($entity->isPublish());
    }

    public function testNotPublished(): void
    {
        $service = $this->grabService();
        $command = new Command(false);
        $entity = new Entity();
        $entity->setPublishedAt(new \DateTimeImmutable());
        Assert::assertTrue($entity->isPublish());
        $service->toPublish($command, $entity);
        Assert::assertFalse($entity->isPublish());
    }

    public function testSame(): void
    {
        $service = $this->grabService();
        $command = new Command(true);
        $entity = new Entity();
        $dateTime = new \DateTimeImmutable();
        $entity->setPublishedAt($dateTime);
        Assert::assertSame($dateTime, $entity->getPublishedAt());
        $service->toPublish($command, $entity);
        Assert::assertSame($dateTime, $entity->getPublishedAt());
    }

    protected function grabService(): ManagerPublicationService
    {
        return parent::getContainer()->get(ManagerPublicationService::class);
    }
}

class Command implements PublishCommandInterface
{
    public function __construct(
        protected bool $publish = true,
    ) {
    }

    public function isPublish(): bool
    {
        return $this->publish;
    }
}

class Entity implements PublicationInterface
{
    use PublicationTrait;
}
