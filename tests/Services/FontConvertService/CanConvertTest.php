<?php

declare(strict_types=1);

namespace App\Tests\Services\FontConvertService;

use App\Entity\Font;
use App\Exceptions\NotFoundCharacterException;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CanConvertTest extends KernelTestCase
{
    use ToolTrait;

    public function testSuccess(): void
    {
        $service = $this->grabService();
        Assert::assertTrue($service->canConvert('М1jръ', Font::CODE_HIRMOS_IE_UCS, Font::CODE_PONOMAR_UNICODE));
    }

    public function testNotFound(): void
    {
        $service = $this->grabService();
        $this->expectException(NotFoundCharacterException::class);
        $service->canConvert('а Ӎ', Font::CODE_HIRMOS_IE_UCS, Font::CODE_PONOMAR_UNICODE);
    }
}
