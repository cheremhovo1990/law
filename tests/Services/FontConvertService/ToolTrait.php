<?php

declare(strict_types=1);

namespace App\Tests\Services\FontConvertService;

use App\Services\FontConvertService;

trait ToolTrait
{
    protected function grabService(): FontConvertService
    {
        return parent::getContainer()->get(FontConvertService::class);
    }
}
