<?php

declare(strict_types=1);

namespace App\Tests\Services\FontConvertService;

use App\Entity\Font;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ConvertTest extends KernelTestCase
{
    use ToolTrait;

    public function testSuccess(): void
    {
        $service = $this->grabService();
        $result = $service->convert('Язhкъ', Font::CODE_HIRMOS_IE_UCS, Font::CODE_PONOMAR_UNICODE);
        Assert::assertEquals('Ꙗзы́къ', $result);
    }
}
