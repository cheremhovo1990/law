<?php

declare(strict_types=1);

namespace App\Tests\Doctrine\Collections;

use App\Doctrine\Collections\Collection;
use App\Doctrine\Collections\CollectionTrait;
use App\Doctrine\Collections\ToArrayTrait;
use App\Services\ToArrayInterface;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CollectionTest extends KernelTestCase
{
    public function testToArray(): void
    {
        $collection = new TextCollection([['text' => 'Title']]);
        Assert::assertIsArray($collection->toArray());
        Assert::assertEquals([['text' => 'Title']], $collection->toArray());

        $class = new class() extends Collection {};
        Assert::assertIsArray($class->toArray());
    }

    public function testNotChange(): void
    {
        $collection = new TextCollection([['text' => 'Title']]);
        Assert::assertEquals($collection, $collection->createNewOrReturnThis([['text' => 'Title']]));
    }

    public function testLast(): void
    {
        $collection = new TextCollection([['text' => 'Title 1'], ['text' => 'Title 2']]);
        Assert::assertEquals(new Item('Title 2'), $collection->last());
    }

    public function testRemove(): void
    {
        $collection = new TextCollection([['text' => 'Title 1'], ['text' => 'Title 2']]);
        $remove = $collection->remove(0);
        Assert::assertEquals(new Item('Title 1'), $remove);
        Assert::assertCount(1, $collection);
        Assert::assertNull($collection->remove(100));
        Assert::assertFalse($collection->removeElement(50));
    }

    public function testContainerKey(): void
    {
        $collection = new TextCollection([['text' => 'Title 1'], ['text' => 'Title 2']]);
        Assert::assertTrue($collection->containsKey(0));
        Assert::assertFalse($collection->containsKey(50));
    }

    public function testAdd(): void
    {
        $collection = new TextCollection([['text' => 'Title 1'], ['text' => 'Title 2']]);
        Assert::assertTrue($collection->add(new Item('Title 3')));
        Assert::assertCount(3, $collection);
    }

    public function testEmpty(): void
    {
        $collection = new TextCollection([['text' => 'Title 1'], ['text' => 'Title 2']]);
        Assert::assertFalse($collection->isEmpty());
        Assert::assertTrue((new TextCollection())->isEmpty());
    }

    public function testClear(): void
    {
        $collection = new TextCollection([['text' => 'Title 1'], ['text' => 'Title 2']]);
        $collection->clear();
        Assert::assertTrue($collection->isEmpty());
    }

    public function testRemoveElement(): void
    {
        $collection = new TextCollection([['text' => 'Title 1'], ['text' => 'Title 2']]);
        $item = $collection->get(0);
        $collection->removeElement($item);
        Assert::assertCount(1, $collection);
    }

    public function testKeys(): void
    {
        $collection = new TextCollection(['first' => ['text' => 'Title 1'], 'second' => ['text' => 'Title 2']]);
        Assert::assertSame(['first', 'second'], $collection->getKeys());
    }

    public function testValues(): void
    {
        $collection = new TextCollection(['first' => ['text' => 'Title 1'], 'second' => ['text' => 'Title 2']]);
        Assert::assertSame([$collection->get('first'), $collection->get('second')], $collection->getValues());
    }

    public function testSet(): void
    {
        $collection = new TextCollection(['first' => ['text' => 'Title 1'], 'second' => ['text' => 'Title 2']]);
        $collection->set('three', $item = new Item('Title 3'));
        Assert::assertSame($item, $collection->get('three'));
    }

    public function testSlice(): void
    {
        $collection = new TextCollection(
            [
                'first' => ['text' => 'Title 1'],
                'second' => ['text' => 'Title 2'],
                'three' => ['text' => 'Title 3'],
            ],
        );
        Assert::assertSame(['three' => $collection->get('three')], $collection->slice(2));
    }

    public function testIndexOf(): void
    {
        $collection = new TextCollection(
            [
                'first' => ['text' => 'Title 1'],
                'second' => ['text' => 'Title 2'],
                'three' => ['text' => 'Title 3'],
            ],
        );
        $first = $collection->get('first');
        Assert::assertEquals('first', $collection->indexOf($first));
        Assert::assertFalse($collection->indexOf('four'));
    }

    public function testContains(): void
    {
        $collection = new TextCollection(
            [
                'first' => ['text' => 'Title 1'],
                'second' => ['text' => 'Title 2'],
                'three' => ['text' => 'Title 3'],
            ],
        );
        $first = $collection->get('first');
        Assert::assertTrue($collection->contains($first));
        Assert::assertFalse($collection->contains(new Item('Title 1')));
    }

    public function testMap(): void
    {
        $collection = new TextCollection(
            [
                'first' => ['text' => 'Title 1'],
            ],
        );
        $newCollection = $collection->map(function (Item $item) {
            $item = $item->toArray();
            $item['text'] = 'Title 2';

            return $item;
        });
        Assert::assertSame(['text' => 'Title 2'], $newCollection->get('first')->toArray());
    }

    public function testCreateNewOrReturnThisReplace(): void
    {
        $collection = new TextCollection([['text' => 'title']]);
        Assert::assertNotEquals($collection, $collection->createNewOrReturnThis([['text' => 'Title']]));
    }

    public function testCreateNewOrReturnThisAdd(): void
    {
        $collection = new TextCollection([['text' => 'title']]);
        Assert::assertNotEquals($collection, $collection->createNewOrReturnThis([['text' => 'title'], ['text' => 'Title']]));
    }

    public function testCreateNewOrReturnThisRemove(): void
    {
        $collection = new TextCollection([['text' => 'title'], ['text' => 'Title']]);
        Assert::assertNotEquals($collection, $collection->createNewOrReturnThis([['text' => 'title']]));
    }
}
class TextCollection extends Collection implements ToArrayInterface
{
    use CollectionTrait;
    use ToArrayTrait;

    public function __construct(array $elements = [])
    {
        $elements = array_map(function (array $element) {
            return new Item(
                text: $element['text'],
            );
        }, $elements);
        parent::__construct($elements);
    }
}

class Item implements ToArrayInterface
{
    public function __construct(
        protected string $text,
    ) {
    }

    /**
     * @return array<string, string>
     */
    public function toArray(): array
    {
        return [
            'text' => $this->text,
        ];
    }
}
