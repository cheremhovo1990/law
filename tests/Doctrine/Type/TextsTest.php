<?php

declare(strict_types=1);

namespace App\Tests\Doctrine\Type;

use App\Doctrine\Collections\TextCollection;
use App\Doctrine\Types\Texts;
use App\Helpers\JsonHelper;
use App\Tests\Helpers\AssertJson;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TextsTest extends KernelTestCase
{
    public function testSuccessToConvertToPHPValue(): void
    {
        $type = new Texts();
        $value = JsonHelper::encode([['text' => 'Title']]);
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToPHPValue($value, $platform);
        Assert::assertInstanceOf(TextCollection::class, $result);

        $result = $type->convertToPHPValue(null, $platform);
        Assert::assertInstanceOf(TextCollection::class, $result);
    }

    public function testConvertToDatabaseValueToNull(): void
    {
        $type = new Texts();
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToDatabaseValue(null, $platform);
        Assert::assertNull($result);
    }

    public function testConvertToDatabaseValue(): void
    {
        $type = new Texts();
        $value = new TextCollection([['text' => 'Title']]);
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToDatabaseValue($value, $platform);
        AssertJson::assertMatchesJsonType(['text' => 'string'], $result, '$[0]');
    }
}
