<?php

declare(strict_types=1);

namespace App\Tests\Doctrine\Type;

use App\Doctrine\Collections\BookLinkCollection;
use App\Doctrine\Models\BookLinkTypeEnum;
use App\Doctrine\Types\BookLinks;
use App\Helpers\JsonHelper;
use App\Tests\Helpers\AssertJson;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BookLinksTest extends KernelTestCase
{
    public function testSuccessToConvertToPHPValue(): void
    {
        $type = new BookLinks();
        $value = JsonHelper::encode([['link' => 'https://www.youtube.com/', 'type' => BookLinkTypeEnum::BUY->value]]);
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToPHPValue($value, $platform);
        Assert::assertInstanceOf(BookLinkCollection::class, $result);

        $result = $type->convertToPHPValue(null, $platform);
        Assert::assertInstanceOf(BookLinkCollection::class, $result);
    }

    public function testConvertToDatabaseValueToNull(): void
    {
        $type = new BookLinks();
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToDatabaseValue(null, $platform);
        Assert::assertNull($result);
    }

    public function testConvertToDatabaseValue(): void
    {
        $type = new BookLinks();
        $value = new BookLinkCollection([['link' => 'https://www.youtube.com/', 'type' => BookLinkTypeEnum::BUY->value]]);
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToDatabaseValue($value, $platform);
        AssertJson::assertMatchesJsonType(['link' => 'string', 'type' => 'string'], $result, '$[0]');
    }
}
