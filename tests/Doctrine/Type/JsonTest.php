<?php

declare(strict_types=1);

namespace App\Tests\Doctrine\Type;

use App\Doctrine\Types\Json;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class JsonTest extends KernelTestCase
{
    public static function testConvertToDatabaseValue(): void
    {
        $type = new Json();
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToDatabaseValue(['test' => 'Test'], $platform);
        Assert::assertIsString($result);
    }

    public function testConvertToDatabaseValueToNull(): void
    {
        $type = new Json();
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToDatabaseValue(null, $platform);
        Assert::assertNull($result);
    }
}
