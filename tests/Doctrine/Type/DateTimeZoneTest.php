<?php

declare(strict_types=1);

namespace App\Tests\Doctrine\Type;

use App\Doctrine\Types\DateTimeZone;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DateTimeZoneTest extends KernelTestCase
{
    public function testSuccessToConvertToPHPValue(): void
    {
        $type = new DateTimeZone();
        $value = 'UTC';
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToPHPValue($value, $platform);
        Assert::assertInstanceOf(\DateTimeZone::class, $result);
    }

    public function testConvertToPHPValueToNull(): void
    {
        $type = new DateTimeZone();
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToPHPValue(null, $platform);
        Assert::assertNull($result);
    }

    public function testConvertToDatabaseValue(): void
    {
        $type = new DateTimeZone();
        $value = new \DateTimeZone('UTC');
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToDatabaseValue($value, $platform);
        Assert::assertEquals('UTC', $result);
    }

    public function testConvertToDatabaseValueToNull(): void
    {
        $type = new DateTimeZone();
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToDatabaseValue(null, $platform);
        Assert::assertNull($result);
    }
}
