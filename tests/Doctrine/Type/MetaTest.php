<?php

declare(strict_types=1);

namespace App\Tests\Doctrine\Type;

use App\Doctrine\Exceptions\CannotConvertToDatabaseValueException;
use App\Doctrine\Models\Meta as MetaModel;
use App\Doctrine\Types\Meta;
use App\Helpers\JsonHelper;
use App\Tests\Helpers\AssertJson;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MetaTest extends KernelTestCase
{
    public function testSuccessToConvertToPHPValue(): void
    {
        $type = new Meta();
        $value = JsonHelper::encode(['title' => 'title', 'description' => 'Name']);
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToPHPValue($value, $platform);
        Assert::assertInstanceOf(MetaModel::class, $result);
    }

    public function testConvertToDatabaseValueException(): void
    {
        $type = new Meta();
        $platform = new PostgreSQLPlatform();
        $this->expectException(CannotConvertToDatabaseValueException::class);
        $type->convertToDatabaseValue(null, $platform);
    }

    public function testConvertToDatabaseValue(): void
    {
        $type = new Meta();
        $value = new MetaModel('title', 'Name');
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToDatabaseValue($value, $platform);
        AssertJson::assertContainsJson(['title' => 'title', 'description' => 'Name'], $result);
    }
}
