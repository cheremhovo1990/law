<?php

declare(strict_types=1);

namespace App\Tests\Doctrine\Type;

use App\Doctrine\Collections\ImagePivotCollection;
use App\Doctrine\Types\ImagePivots;
use App\Helpers\JsonHelper;
use App\Tests\Helpers\AssertJson;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ImagePivotsTest extends KernelTestCase
{
    public function testSuccessToConvertToPHPValue(): void
    {
        $type = new ImagePivots();
        $value = JsonHelper::encode([['id' => '1', 'type' => 'Type']]);
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToPHPValue($value, $platform);
        Assert::assertInstanceOf(ImagePivotCollection::class, $result);

        $result = $type->convertToPHPValue(null, $platform);
        Assert::assertInstanceOf(ImagePivotCollection::class, $result);
    }

    public function testConvertToDatabaseValue(): void
    {
        $type = new ImagePivots();
        $value = new ImagePivotCollection([['id' => 1, 'type' => 'Type']]);
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToDatabaseValue($value, $platform);
        AssertJson::assertMatchesJsonType(['id' => 'integer', 'type' => 'string'], $result, '$[0]');
    }
}
