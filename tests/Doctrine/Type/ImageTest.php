<?php

declare(strict_types=1);

namespace App\Tests\Doctrine\Type;

use App\Doctrine\Models\Image as ImageModel;
use App\Doctrine\Types\Image;
use App\Helpers\JsonHelper;
use App\Tests\Helpers\AssertJson;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ImageTest extends KernelTestCase
{
    public function testSuccessToConvertToPHPValue(): void
    {
        $type = new Image();
        $value = JsonHelper::encode(['id' => '883ccffe-2b58-4c2a-89c2-f01c16dae6fd', 'name' => 'Name']);
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToPHPValue($value, $platform);
        Assert::assertInstanceOf(ImageModel::class, $result);

        $result = $type->convertToPHPValue(null, $platform);
        Assert::assertNull($result);
    }

    public function testConvertToDatabaseValue(): void
    {
        $type = new Image();
        $value = new ImageModel('883ccffe-2b58-4c2a-89c2-f01c16dae6fd', 'Name');
        $platform = new PostgreSQLPlatform();
        $result = $type->convertToDatabaseValue($value, $platform);
        AssertJson::assertContainsJson(['id' => '883ccffe-2b58-4c2a-89c2-f01c16dae6fd', 'name' => 'Name'], $result);

        $result = $type->convertToDatabaseValue(null, $platform);
        Assert::assertNull($result);
    }
}
