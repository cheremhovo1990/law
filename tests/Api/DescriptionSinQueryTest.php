<?php

declare(strict_types=1);

namespace App\Tests\Api;

use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DescriptionSinQueryTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $descriptionSin = $this->grabDescriptionSinByName(PreceptFixtures::DESCRIPTION_SIN_NAME_FIRST);
        $client->jsonRequest(
            Request::METHOD_GET,
            sprintf('api/precept/%s/description/sin/%s', (string) $descriptionSin->getPrecept()->getId(), (string) $descriptionSin->getId()),
        );
        $this->assertResponseIsSuccessful();
    }
}
