<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class PreceptsQueryTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();
        $client->jsonRequest(Request::METHOD_GET, '/api/cp/precepts');
        $this->assertResponseIsSuccessful();
    }
}
