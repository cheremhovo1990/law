<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\MusicFixture;
use App\Tests\Helpers\AssertJson;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class MusicEditMutationTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();

        $entity = $this->grabMusicById(MusicFixture::ID_FIRST);

        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/music/%s/edit/mutation', $entity->getId()),
            [
                'title' => 'ЛЕСТВИЦА. 10 СТУПЕНЬ. О ЗЛОСЛОВИИ И КЛЕВЕТЕ',
                'link' => 'https://www.youtube.com/watch?v=ybntR38YbSQ',
            ],
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testFail(): void
    {
        $client = parent::createClient();

        $entity = $this->grabMusicById(MusicFixture::ID_FIRST);

        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf(
                '/api/cp/music/%s/edit/mutation',
                $entity->getId(),
            ),
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => false], $client->getResponse()->getContent());
    }
}
