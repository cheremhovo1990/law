<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\Entity\MetaKeyEnum;
use App\Tests\Helpers\AssertJson;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class MetaEditMutationTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $meta = $this->grabMetaByKey(MetaKeyEnum::MAIN->value);
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/meta/%s/edit/mutation', $meta->getKey()),
            [
                'key' => MetaKeyEnum::BOOKS->value,
                'description' => 'Description Test',
            ],
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testFail(): void
    {
        $client = parent::createClient();
        $meta = $this->grabMetaByKey(MetaKeyEnum::MAIN->value);
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/meta/%s/edit/mutation', $meta->getKey()),
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => false], $client->getResponse()->getContent());
    }
}
