<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SymbolsQueryTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();
        $client->jsonRequest(Request::METHOD_GET, '/api/cp/symbols');
        $this->assertResponseIsSuccessful();
    }
}
