<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\Tests\Helpers\AssertJson;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class PageCreateMutationTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();

        $client->jsonRequest(
            Request::METHOD_POST,
            '/api/cp/page/create/mutation',
            [
                'content' => 'Text',
            ],
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testFail(): void
    {
        $client = parent::createClient();

        $client->jsonRequest(Request::METHOD_POST, '/api/cp/page/create/mutation');
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => false], $client->getResponse()->getContent());
    }
}
