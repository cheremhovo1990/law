<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SinQueryTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();

        $sin = $this->grabSinById(PreceptFixtures::SIN_ID_FIRST);
        $client->jsonRequest(Request::METHOD_GET, sprintf('/api/cp/sin/%s', $sin->getId()));
        $this->assertResponseIsSuccessful();
    }
}
