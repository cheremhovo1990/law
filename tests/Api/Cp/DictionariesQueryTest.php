<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\Tests\Helpers\AssertJson;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DictionariesQueryTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();
        $params = [
            'page' => 2,
            'limit' => 20,
            'search' => [
                'have_titlo' => 1,
                'description' => 'test',
            ],
        ];
        $client->jsonRequest(Request::METHOD_GET, '/api/cp/dictionaries?' . http_build_query($params));
        $this->assertResponseIsSuccessful();
        AssertJson::assertMatchesJsonType(
            [
                'data' => 'array',
                'paginator' => 'array',
            ],
            $client->getResponse()->getContent(),
        );
        AssertJson::assertMatchesJsonType(
            [
                'page' => 'integer',
                'limit' => 'integer',
                'total' => 'integer',
                'maxPage' => 'integer',
            ],
            $client->getResponse()->getContent(),
            '$.paginator',
        );
        AssertJson::assertContainsJson(
            [
                'paginator' => [
                    'page' => 2,
                    'limit' => 20,
                ],
            ],
            $client->getResponse()->getContent(),
        );
    }

    public function testDefaultQuery(): void
    {
        $client = parent::createClient();
        $client->jsonRequest(Request::METHOD_GET, '/api/cp/dictionaries');
        $this->assertResponseIsSuccessful();

        AssertJson::assertMatchesJsonType(
            [
                'data' => 'array',
                'paginator' => 'array',
            ],
            $client->getResponse()->getContent(),
        );
        AssertJson::assertMatchesJsonType(
            [
                'page' => 'integer',
                'limit' => 'integer',
                'total' => 'integer',
                'maxPage' => 'integer',
            ],
            $client->getResponse()->getContent(),
            '$.paginator',
        );
        AssertJson::assertContainsJson(
            [
                'paginator' => [
                    'page' => 1,
                    'limit' => 100,
                ],
            ],
            $client->getResponse()->getContent(),
        );
    }
}
