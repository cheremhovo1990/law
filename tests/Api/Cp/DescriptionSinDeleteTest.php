<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\AssertJson;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DescriptionSinDeleteTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/description/sin/%s/delete/mutation', PreceptFixtures::DESCRIPTION_SIN_ID_FIRST),
            [
                'name' => 'Name test',
                'text' => 'Text test',
            ],
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testNotFound(): void
    {
        $client = parent::createClient();

        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/description/sin/%s/delete/mutation', '01G6NMRM9ZBMM1FAHMBCEN5B6Z'),
        );
        $this->assertResponseStatusCodeSame(404);
    }
}
