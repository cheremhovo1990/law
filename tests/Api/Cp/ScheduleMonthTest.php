<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use Carbon\CarbonImmutable;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class ScheduleMonthTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();
        $start = CarbonImmutable::now()->startOfMonth()->startOfWeek();
        $end = CarbonImmutable::now()->endOfMonth()->endOfWeek();
        $client->jsonRequest(Request::METHOD_GET, '/api/cp/schedule/month?' . http_build_query([
                'startDate' => $start->format('Y-m-d'),
                'endDate' => $end->format('Y-m-d'),
            ]));
        $this->assertResponseIsSuccessful();
    }
}
