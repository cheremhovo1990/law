<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\PageFixture;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class PageQueryTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $entity = $this->grabPageById(PageFixture::ID_FIRST);
        $client->jsonRequest(
            Request::METHOD_GET,
            sprintf('/api/cp/page/%s', $entity->getId()),
        );
        $this->assertResponseIsSuccessful();
    }
}
