<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\Entity\MetaKeyEnum;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class MetaOneQueryTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $meta = $this->grabMetaByKey(MetaKeyEnum::MAIN->value);
        $client->jsonRequest(Request::METHOD_GET, sprintf('/api/cp/meta/one/%s', $meta->getKey()));
        $this->assertResponseIsSuccessful();
    }
}
