<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\PageFixture;
use App\Tests\Helpers\AssertJson;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class PageEditMutationTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();

        $entity = $this->grabPageById(PageFixture::ID_FIRST);

        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/page/%s/edit/mutation', $entity->getId()),
            [
                'content' => 'Text',
            ],
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testFail(): void
    {
        $client = parent::createClient();

        $entity = $this->grabPageById(PageFixture::ID_FIRST);

        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf(
                '/api/cp/page/%s/edit/mutation',
                $entity->getId(),
            ),
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => false], $client->getResponse()->getContent());
    }
}
