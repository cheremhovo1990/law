<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use Carbon\CarbonImmutable;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class ScheduleWeekTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();
        $start = CarbonImmutable::now()->startOfWeek();
        $end = CarbonImmutable::now()->endOfWeek();
        $client->jsonRequest(Request::METHOD_GET, '/api/cp/schedule/week?' . http_build_query([
                'startDate' => $start->format('Y-m-d'),
                'endDate' => $end->format('Y-m-d'),
            ]));
        $this->assertResponseIsSuccessful();
    }
}
