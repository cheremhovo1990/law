<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class PreceptQueryTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();

        $precept = $this->grabPreceptById(PreceptFixtures::ID_SIX);
        $client->jsonRequest(Request::METHOD_GET, sprintf('/api/cp/precept/%s', $precept->getId()));
        $this->assertResponseIsSuccessful();
    }
}
