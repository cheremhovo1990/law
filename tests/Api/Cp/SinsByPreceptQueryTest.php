<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\AssertJson;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SinsByPreceptQueryTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $precept = $this->grabPreceptById(PreceptFixtures::ID_SIX);
        $client->jsonRequest(Request::METHOD_GET, sprintf('/api/cp/precept/%s/sins', (string) $precept->getId()));
        $this->assertResponseIsSuccessful();
        AssertJson::assertMatchesJsonType(
            ['data' => 'array'],
            $client->getResponse()->getContent(),
        );
    }
}
