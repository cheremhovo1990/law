<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\AssertJson;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DescriptionSinEditMutationTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $descriptionSin = $this->grabDescriptionSinByName(PreceptFixtures::DESCRIPTION_SIN_NAME_FIRST);
        $precept = $this->grabPreceptById(PreceptFixtures::ID_SIX);
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/description/sin/%s/edit/mutation', $descriptionSin->getId()),
            [
                'name' => 'Name test',
                'text' => 'Text test',
                'precept' => $precept->getId(),
            ],
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testFail(): void
    {
        $client = parent::createClient();
        $descriptionSin = $this->grabDescriptionSinByName(PreceptFixtures::DESCRIPTION_SIN_NAME_FIRST);
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/description/sin/%s/edit/mutation', $descriptionSin->getId()),
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => false], $client->getResponse()->getContent());
    }
}
