<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DescriptionSinQueryTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $descriptionSin = $this->grabDescriptionSinByName(PreceptFixtures::DESCRIPTION_SIN_NAME_FIRST);
        $client->jsonRequest(
            Request::METHOD_GET,
            sprintf('/api/cp/description/sin/%s', $descriptionSin->getId()),
        );
        $this->assertResponseIsSuccessful();
    }
}
