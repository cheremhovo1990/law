<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\AssertJson;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SinCreateMutationTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();

        $precept = $this->grabPreceptById(PreceptFixtures::ID_SIX);
        $client->jsonRequest(
            Request::METHOD_POST,
            '/api/cp/sin/create/mutation',
            [
                'name' => 'Test name',
                'precept' => $precept->getId(),
            ],
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testFail(): void
    {
        $client = parent::createClient();
        $client->jsonRequest(Request::METHOD_POST, '/api/cp/sin/create/mutation');
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => false], $client->getResponse()->getContent());
    }
}
