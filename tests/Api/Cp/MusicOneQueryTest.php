<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\MusicFixture;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class MusicOneQueryTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $entity = $this->grabMusicById(MusicFixture::ID_FIRST);
        $client->jsonRequest(
            Request::METHOD_GET,
            sprintf('/api/cp/music/one/%s', $entity->getId()),
        );
        $this->assertResponseIsSuccessful();
    }
}
