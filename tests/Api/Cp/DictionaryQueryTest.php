<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\DictionaryFixture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DictionaryQueryTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();
        $client->jsonRequest(Request::METHOD_GET, sprintf('/api/cp/dictionary/%s', DictionaryFixture::ID_FIRST));
        $this->assertResponseIsSuccessful();
    }
}
