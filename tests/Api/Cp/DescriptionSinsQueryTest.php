<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\Tests\Helpers\AssertJson;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DescriptionSinsQueryTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();
        $client->jsonRequest(Request::METHOD_GET, '/api/cp/description/sins');
        $this->assertResponseIsSuccessful();
        AssertJson::assertMatchesJsonType(
            ['data' => 'array'],
            $client->getResponse()->getContent(),
        );
    }
}
