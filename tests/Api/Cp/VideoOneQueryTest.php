<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\VideoFixture;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class VideoOneQueryTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $video = $this->grabVideoByID(VideoFixture::ID_FIRST);
        $client->jsonRequest(
            Request::METHOD_GET,
            sprintf('/api/cp/video/one/%s', $video->getId()),
        );
        $this->assertResponseIsSuccessful();
    }
}
