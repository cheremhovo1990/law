<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\DictionaryFixture;
use App\Tests\Helpers\AssertJson;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DictionaryEditMutationTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();

        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/dictionary/%s/edit/mutation', DictionaryFixture::ID_FIRST),
            [
                'name' => 'Ꙗзыцы',
                'normalizeName' => 'Ꙗзыцы',
                'description' => 'Ꙗзыцы',
                'titlo' => '',
            ],
        );

        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testFail(): void
    {
        $client = parent::createClient();
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/dictionary/%s/edit/mutation', DictionaryFixture::ID_FIRST),
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => false], $client->getResponse()->getContent());
    }
}
