<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\AssertJson;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class PreceptEditMutationTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = static::createClient();
        $precept = $this->grabPreceptById(PreceptFixtures::ID_SIX);
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/precept/%s/edit/mutation', $precept->getId()),
            [
                'link' => 'https://azbyka.ru/biblia/test',
                'verse' => 'Исх.test',
            ],
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testFail(): void
    {
        $client = static::createClient();
        $precept = $this->grabPreceptById(PreceptFixtures::ID_SIX);
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/precept/%s/edit/mutation', $precept->getId()),
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => false], $client->getResponse()->getContent());
    }
}
