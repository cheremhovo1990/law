<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\BookFixture;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class BookQueryTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $book = $this->grabBookById(BookFixture::ID_FIRST);
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/book/%s', $book->getId()),
        );
        $this->assertResponseIsSuccessful();
    }
}
