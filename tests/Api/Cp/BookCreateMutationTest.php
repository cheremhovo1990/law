<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\ImageFixture;
use App\Doctrine\Models\BookLinkTypeEnum;
use App\Entity\LanguageOfThePublicationEnum;
use App\Tests\Helpers\AssertJson;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class BookCreateMutationTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $image = $this->grabImageByName(ImageFixture::NAMES_FIRST);
        $client->jsonRequest(
            Request::METHOD_POST,
            '/api/cp/book/create/mutation',
            [
                'name' => 'Name test',
                'links' => [
                    ['link' => 'https://www.litres.ru/igumen-mitrofan-gudkov/izbavi-gospodi-dushu-mou-ot-gneva/', 'type' => BookLinkTypeEnum::BUY->value],
                ],
                'isbnCollection' => [
                    ['text' => '978-5-91362-213-6'],
                ],
                'meta' => [
                    'title' => 'Title',
                    'description' => 'Description',
                ],
                'dateOfWriting' => '2022-04-02',
                'description' => 'Description test',
                'pageSize' => 100,
                'languageOfThePublication' => LanguageOfThePublicationEnum::RUSSIAN->value,
                'coverImage' => [
                    'id' => $image->getId(),
                    'name' => $image->getName(),
                ],
            ],
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testFail(): void
    {
        $client = parent::createClient();
        $client->jsonRequest(
            Request::METHOD_POST,
            '/api/cp/book/create/mutation',
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => false], $client->getResponse()->getContent());
    }
}
