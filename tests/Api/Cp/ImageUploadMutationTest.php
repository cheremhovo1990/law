<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\Tests\Helpers\AssertJson;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ImageUploadMutationTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();

        $files = [
            'image' => [
                'name' => 'book-1.jpg',
                'full_path' => 'book-1.jpg',
                'type' => 'image/jpeg',
                'tmp_name' => '/app/tests/data/book-1.jpg',
                'error' => 0,
                'size' => 104792,
            ],
        ];

        $client->setServerParameter('HTTP_ACCEPT', 'application/json');
        $client->request(
            method: 'POST',
            uri: '/api/cp/image/upload/mutation',
            files: $files,
        );
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }
}
