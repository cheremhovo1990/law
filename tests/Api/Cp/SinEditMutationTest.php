<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\PreceptFixtures;
use App\Tests\Helpers\AssertJson;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SinEditMutationTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();

        $sin = $this->grabSinById(PreceptFixtures::SIN_ID_FIRST);
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/sin/%s/edit/mutation', $sin->getId()),
            [
                'name' => 'Test name',
                'precept' => $sin->getPrecept()->getId(),
            ],
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testFail(): void
    {
        $client = parent::createClient();
        $sin = $this->grabSinById(PreceptFixtures::SIN_ID_FIRST);
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/sin/%s/edit/mutation', $sin->getId()),
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => false], $client->getResponse()->getContent());
    }
}
