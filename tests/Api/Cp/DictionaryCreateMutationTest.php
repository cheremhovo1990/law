<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\Tests\Helpers\AssertJson;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DictionaryCreateMutationTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();

        $client->jsonRequest(
            Request::METHOD_POST,
            '/api/cp/dictionary/create/mutation',
            [
                'name' => 'Ꙗзыцы',
                'normalizeName' => 'Ꙗзыцы',
                'description' => 'Ꙗзыцы',
                'titlo' => '',
            ],
        );

        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testFail(): void
    {
        $client = parent::createClient();
        $client->jsonRequest(
            Request::METHOD_POST,
            '/api/cp/dictionary/create/mutation',
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => false], $client->getResponse()->getContent());
    }
}
