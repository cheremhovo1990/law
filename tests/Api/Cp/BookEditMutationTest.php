<?php

declare(strict_types=1);

namespace App\Tests\Api\Cp;

use App\DataFixtures\BookFixture;
use App\Doctrine\Models\BookLinkTypeEnum;
use App\Entity\LanguageOfThePublicationEnum;
use App\Tests\Helpers\AssertJson;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class BookEditMutationTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $book = $this->grabBookById(BookFixture::ID_FIRST);
        $coverImage = $book->getCoverImage();
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/book/%s/edit/mutation', $book->getId()),
            [
                'name' => 'Name test',
                'links' => [
                    ['link' => 'https://www.litres.ru/igumen-mitrofan-gudkov/izbavi-gospodi-dushu-mou-ot-gneva/', 'type' => BookLinkTypeEnum::BUY->value],
                ],
                'isbnCollection' => [
                    ['text' => '978-5-91362-213-7'],
                ],
                'dateOfWriting' => '2022-04-02',
                'description' => 'Description test',
                'pageSize' => 100,
                'languageOfThePublication' => LanguageOfThePublicationEnum::RUSSIAN->value,
                'coverImage' => [
                    'id' => $coverImage->getId(),
                    'name' => $coverImage->getName(),
                ],
            ],
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => true], $client->getResponse()->getContent());
    }

    public function testFail(): void
    {
        $client = parent::createClient();
        $book = $this->grabBookById(BookFixture::ID_FIRST);
        $client->jsonRequest(
            Request::METHOD_POST,
            sprintf('/api/cp/book/%s/edit/mutation', $book->getId()),
        );
        $this->assertResponseIsSuccessful();
        AssertJson::assertContainsJson(['success' => false], $client->getResponse()->getContent());
    }
}
