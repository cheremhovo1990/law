<?php

declare(strict_types=1);

namespace App\Tests\Api;

use App\Tests\Helpers\AssertJson;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class FeedbackCreateMutationTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $faker = Factory::create();
        $client = static::createClient();
        $client->jsonRequest(Request::METHOD_POST, '/api/feedback/create/mutation', [
            'email' => $faker->email(),
            'firstName' => $faker->firstName(),
            'theme' => $faker->text(),
            'message' => $faker->text(),
        ]);
        $this->assertResponseIsSuccessful();
        $response = $client->getResponse();
        AssertJson::assertContainsJson(['success' => true], $response->getContent());
    }

    public function testInvalid(): void
    {
        $client = static::createClient();
        $client->jsonRequest('POST', '/api/feedback/create/mutation');
        $this->assertResponseIsSuccessful();
        $response = $client->getResponse();
        AssertJson::assertContainsJson(['success' => false], $response->getContent());
    }
}
