<?php

declare(strict_types=1);

namespace App\Tests\Api\Book;

use App\DataFixtures\BookFixture;
use App\Tests\Helpers\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class BookQueryTest extends WebTestCase
{
    use DoctrineTrait;

    public function testSuccess(): void
    {
        $client = parent::createClient();
        $client->jsonRequest(Request::METHOD_GET, sprintf('/api/book/%s', BookFixture::ID_FIRST));
        $this->assertResponseIsSuccessful();
    }
}
