<?php

declare(strict_types=1);

namespace App\Tests\Api\Book;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class BooksQueryTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $client = parent::createClient();
        $client->jsonRequest(Request::METHOD_GET, '/api/books');
        $this->assertResponseIsSuccessful();
    }
}
