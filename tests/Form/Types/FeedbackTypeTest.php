<?php

declare(strict_types=1);

namespace App\Tests\Form\Types;

use App\Form\Types\FeedbackType;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class FeedbackTypeTest extends KernelTestCase
{
    use FormTrait;

    // tests
    public function testNotBlank(): void
    {
        $form = $this->submit(FeedbackType::class, []);
        $names = $this->findNamesByCode($form, NotBlank::IS_BLANK_ERROR);
        Assert::assertFalse($form->isValid());
        Assert::assertTrue(in_array('email', $names));
        Assert::assertTrue(in_array('firstName', $names));
        Assert::assertTrue(in_array('theme', $names));
        Assert::assertTrue(in_array('message', $names));
        Assert::assertCount(4, $names);
    }

    public function testEmail(): void
    {
        $form = $this->submit(FeedbackType::class, ['email' => 'test']);
        $names = $this->findNamesByCode($form, Email::INVALID_FORMAT_ERROR);
        Assert::assertFalse($form->isValid());
        Assert::assertTrue(in_array('email', $names));
        Assert::assertCount(1, $names);
    }

    public function testSuccess(): void
    {
        $form = $this->submit(FeedbackType::class, [
            'email' => 'email@test.com',
            'firstName' => 'First Name',
            'theme' => 'Theme',
            'message' => 'message',
        ]);
        Assert::assertTrue($form->isValid());
    }
}
