<?php

declare(strict_types=1);

namespace App\Tests\Form\Types\Model;

use App\Form\Types\Model\ImageType;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\NotBlank;

class ImageTypeTest extends KernelTestCase
{
    use FormTrait;

    public function testNotBlank(): void
    {
        $form = $this->submit(ImageType::class, []);
        $names = $this->findNamesByCode($form, NotBlank::IS_BLANK_ERROR);
        Assert::assertFalse($form->isValid());
        Assert::assertTrue(in_array('id', $names));
        Assert::assertTrue(in_array('name', $names));
        Assert::assertCount(2, $names);
    }

    public function testSuccess(): void
    {
        $form = $this->submit(ImageType::class, [
            'id' => 1,
            'name' => 'name',
        ]);

        Assert::assertTrue($form->isValid());
    }
}
