<?php

declare(strict_types=1);

namespace App\Tests\Form\Types\Model;

use App\Doctrine\Models\BookLinkTypeEnum;
use App\Form\Types\Model\BookLinkType;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\NotBlank;

class BookLinkTypeTest extends KernelTestCase
{
    use FormTrait;

    public function testNotBlank(): void
    {
        $form = $this->submit(BookLinkType::class, []);
        $names = $this->findNamesByCode($form, NotBlank::IS_BLANK_ERROR);
        Assert::assertFalse($form->isValid());
        Assert::assertTrue(in_array('link', $names));
        Assert::assertTrue(in_array('type', $names));
        Assert::assertCount(2, $names);
    }

    public function testSuccess(): void
    {
        $form = $this->submit(BookLinkType::class, [
            'link' => 'https://www.youtube.com/watch?v=2YNkY3-Hc3M&list=RD2YNkY3-Hc3M&start_radio=1',
            'type' => BookLinkTypeEnum::BUY->value,
        ]);

        Assert::assertTrue($form->isValid());
    }

    public function testChoice(): void
    {
        $form = $this->submit(BookLinkType::class, [
            'link' => 'https://www.youtube.com/watch?v=2YNkY3-Hc3M&list=RD2YNkY3-Hc3M&start_radio=1',
            'type' => 'test',
        ]);
        Assert::assertFalse($form->isValid());
    }
}
