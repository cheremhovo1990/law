<?php

declare(strict_types=1);

namespace App\Tests\Form\Types\Model;

use App\Form\Types\Model\MetaType;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MetaTypeTest extends KernelTestCase
{
    use FormTrait;

    // tests
    public function testSuccess(): void
    {
        $form = $this->submit(MetaType::class, [
            'title' => 'Title',
            'description' => 'Description',
        ]);

        Assert::assertTrue($form->isValid());
    }
}
