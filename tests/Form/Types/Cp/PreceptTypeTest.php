<?php

declare(strict_types=1);

namespace App\Tests\Form\Types\Cp;

use App\Form\Types\Cp\PreceptType;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\NotBlank;

class PreceptTypeTest extends KernelTestCase
{
    use FormTrait;

    // tests
    public function testNotBlank(): void
    {
        $form = $this->submit(PreceptType::class, []);
        $names = $this->findNamesByCode($form, NotBlank::IS_BLANK_ERROR);
        Assert::assertFalse($form->isValid());
        Assert::assertTrue(in_array('link', $names));
        Assert::assertTrue(in_array('verse', $names));
    }

    public function testSuccess(): void
    {
        $form = $this->submit(PreceptType::class, [
            'link' => 'Link',
            'verse' => 'Verse',
        ]);
        Assert::assertTrue($form->isValid());
    }
}
