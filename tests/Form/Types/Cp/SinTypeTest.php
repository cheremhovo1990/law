<?php

declare(strict_types=1);

namespace App\Tests\Form\Types\Cp;

use App\DataFixtures\PreceptFixtures;
use App\Form\Types\Cp\SinType;
use App\Tests\Helpers\DoctrineTrait;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\NotBlank;

class SinTypeTest extends KernelTestCase
{
    use DoctrineTrait;
    use FormTrait;

    // tests
    public function testNotBlank(): void
    {
        $form = $this->submit(SinType::class, []);
        $names = $this->findNamesByCode($form, NotBlank::IS_BLANK_ERROR);
        Assert::assertFalse($form->isValid());
        Assert::assertTrue(in_array('name', $names));
        Assert::assertTrue(in_array('precept', $names));
        Assert::assertCount(2, $names);
    }

    public function testSuccess(): void
    {
        $precept = $this->grabPreceptById(PreceptFixtures::ID_SIX);

        $form = $this->submit(SinType::class, [
            'name' => 'Name',
            'precept' => (string) $precept->getId(),
            'popoverContent' => 'Popover Content',
            'position' => 1,
            'isPublish' => false,
        ]);
        Assert::assertTrue($form->isValid());
    }
}
