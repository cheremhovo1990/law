<?php

declare(strict_types=1);

namespace App\Tests\Form\Types\Cp;

use App\Form\Types\Cp\MusicType;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\NotBlank;

class MusicTypeTest extends KernelTestCase
{
    use FormTrait;

    // tests
    public function testNotBlank(): void
    {
        $form = $this->submit(MusicType::class, []);
        $names = $this->findNamesByCode($form, NotBlank::IS_BLANK_ERROR);
        Assert::assertFalse($form->isValid());
        Assert::assertTrue(in_array('title', $names));
        Assert::assertTrue(in_array('link', $names));
        Assert::assertCount(2, $names);
    }

    public function testSuccess(): void
    {
        $form = $this->submit(MusicType::class, [
            'title' => 'Title',
            'link' => 'Link',
            'description' => 'Description',
            'publish' => true,
        ]);

        Assert::assertTrue($form->isValid());
    }
}
