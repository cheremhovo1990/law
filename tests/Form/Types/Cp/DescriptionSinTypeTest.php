<?php

declare(strict_types=1);

namespace App\Tests\Form\Types\Cp;

use App\DataFixtures\PreceptFixtures;
use App\Form\Types\Cp\DescriptionSinType;
use App\Tests\Helpers\DoctrineTrait;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\NotBlank;

class DescriptionSinTypeTest extends KernelTestCase
{
    use DoctrineTrait;
    use FormTrait;

    public function testNotBlank(): void
    {
        $form = $this->submit(DescriptionSinType::class, []);
        $names = $this->findNamesByCode($form, NotBlank::IS_BLANK_ERROR);
        Assert::assertFalse($form->isValid());
        Assert::assertTrue(in_array('name', $names));
        Assert::assertTrue(in_array('text', $names));
        Assert::assertTrue(in_array('precept', $names));
        Assert::assertCount(3, $names);
    }

    // tests
    public function testSuccess(): void
    {
        $precept = $this->grabPreceptById(PreceptFixtures::ID_SIX);

        $sins = $this->grabSinByNames([PreceptFixtures::SIN_NAME_FIRST, PreceptFixtures::SIN_NAME_SECOND]);
        $ids = [];
        foreach ($sins as $sin) {
            $ids[] = $sin->getId();
        }

        $form = $this->submit(DescriptionSinType::class, [
            'name' => 'Name',
            'text' => 'Text',
            'precept' => (string) $precept->getId(),
            'sins' => $ids,
        ]);
        Assert::assertTrue($form->isValid());
    }
}
