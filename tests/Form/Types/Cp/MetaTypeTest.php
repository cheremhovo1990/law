<?php

declare(strict_types=1);

namespace App\Tests\Form\Types\Cp;

use App\Entity\MetaKeyEnum;
use App\Form\Types\Cp\MetaType;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\NotBlank;

class MetaTypeTest extends KernelTestCase
{
    use FormTrait;

    public function testNotBlank(): void
    {
        $form = $this->submit(MetaType::class, []);
        $names = $this->findNamesByCode($form, NotBlank::IS_BLANK_ERROR);
        Assert::assertFalse($form->isValid());
        Assert::assertTrue(in_array('key', $names));
        Assert::assertTrue(in_array('description', $names));
        Assert::assertCount(2, $names);
    }

    public function testSuccess(): void
    {
        $form = $this->submit(MetaType::class, [
            'key' => MetaKeyEnum::MAIN->value,
            'title' => 'Title',
            'description' => 'Description',
        ]);
        Assert::assertTrue($form->isValid());
    }
}
