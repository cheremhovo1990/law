<?php

declare(strict_types=1);

namespace App\Tests\Form\Types\Cp;

use App\Form\Types\Cp\PageType;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\NotBlank;

class PageTypeTest extends KernelTestCase
{
    use FormTrait;

    // tests
    public function testNotBlank(): void
    {
        $form = $this->submit(PageType::class, []);
        $names = $this->findNamesByCode($form, NotBlank::IS_BLANK_ERROR);
        Assert::assertFalse($form->isValid());
        Assert::assertTrue(in_array('content', $names));
        Assert::assertCount(1, $names);
    }

    public function testSuccess(): void
    {
        $form = $this->submit(PageType::class, [
            'content' => 'Text 1',
        ]);

        Assert::assertTrue($form->isValid());
    }
}
