<?php

declare(strict_types=1);

namespace App\Tests\Form\Types\Cp;

use App\Doctrine\Models\BookLinkTypeEnum;
use App\Entity\LanguageOfThePublicationEnum;
use App\Form\Types\Cp\BookType;
use App\Form\Validator\BookLinkConstrain;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\NotBlank;

class BookTypeTest extends KernelTestCase
{
    use FormTrait;

    // tests
    public function testNotBlank(): void
    {
        $form = $this->submit(BookType::class, []);
        $names = $this->findNamesByCode($form, NotBlank::IS_BLANK_ERROR);
        $bookLink = $this->findNamesByCode($form, BookLinkConstrain::ERROR);

        Assert::assertFalse($form->isValid());
        Assert::assertTrue(in_array('name', $names));
        Assert::assertTrue(in_array('description', $names));
        Assert::assertTrue(in_array('links', $bookLink));
        Assert::assertTrue(in_array('pageSize', $names));
        Assert::assertTrue(in_array('isbnCollection', $names));
        Assert::assertTrue(in_array('languageOfThePublication', $names));
        Assert::assertCount(7, $names);
    }

    public function testSuccess(): void
    {
        $form = $this->submit(BookType::class, [
            'name' => 'Name',
            'links' => [
                ['link' => 'https://www.youtube.com/', 'type' => BookLinkTypeEnum::BUY->value],
            ],
            'isbnCollection' => [
                ['text' => '978-5-91362-213-6'],
            ],
            'description' => 'Description',
            'dateOfWriting' => '2022-03-19',
            'pageSize' => 1,
            'coverImage' => [
                'id' => 1,
                'name' => 'test.png',
            ],
            'publish' => false,
            'languageOfThePublication' => LanguageOfThePublicationEnum::CHURCH_SLAVIC->value,
        ]);
        Assert::assertTrue($form->isValid());
    }
}
