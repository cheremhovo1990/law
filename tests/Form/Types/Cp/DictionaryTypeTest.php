<?php

declare(strict_types=1);

namespace App\Tests\Form\Types\Cp;

use App\Form\Types\Cp\DictionaryType;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\NotBlank;

class DictionaryTypeTest extends KernelTestCase
{
    use FormTrait;

    // tests
    public function testNotBlank(): void
    {
        $form = $this->submit(DictionaryType::class, []);
        $names = $this->findNamesByCode($form, NotBlank::IS_BLANK_ERROR);
        Assert::assertFalse($form->isValid());
        Assert::assertTrue(in_array('name', $names));
        Assert::assertTrue(in_array('description', $names));
        Assert::assertCount(2, $names);
    }

    public function testSuccess(): void
    {
        $form = $this->submit(DictionaryType::class, [
            'name' => 'Name',
            'description' => 'Description',
            'titlo' => '0',
        ]);
        Assert::assertTrue($form->isValid());
    }
}
