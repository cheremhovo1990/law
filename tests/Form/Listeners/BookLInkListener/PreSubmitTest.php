<?php

declare(strict_types=1);

namespace App\Tests\Form\Listeners\BookLInkListener;

use App\Doctrine\Models\BookLinkTypeEnum;
use App\Form\Listeners\BookLinkListener;
use App\Helpers\JsonHelper;
use App\Tests\Helpers\AssertJson;
use App\Tests\Helpers\FormTrait;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormEvent;

class PreSubmitTest extends KernelTestCase
{
    use FormTrait;

    public function testSuccess(): void
    {
        $form = $this->create(FormType::class);
        $data = [
            'links' => [
                ['link' => 'https://www.youtube.com/watch?v=2YNkY3-Hc3M&list=RD2YNkY3-Hc3M&start_radio=1', 'type' => BookLinkTypeEnum::BUY->value],
                ['link' => 'https://www.youtube.com/watch?v=2YNkY3-Hc3M&list=RD2YNkY3-Hc3M&start_radio=1', 'type' => BookLinkTypeEnum::BUY->value],
                ['link' => '', 'type' => BookLinkTypeEnum::BUY->value],
                ['link' => 'https://www.youtube.com/watch?v=2YNkY3-Hc3M&list=RD2YNkY3-Hc3M&start_radio=2', 'type' => BookLinkTypeEnum::BUY->value],
            ],
        ];
        $event = new FormEvent($form, $data);
        $listener = $this->grabListener();
        $listener->preSubmit($event);
        $links = $event->getData()['links'];
        AssertJson::assertContainsJson([
            ['link' => 'https://www.youtube.com/watch?v=2YNkY3-Hc3M&list=RD2YNkY3-Hc3M&start_radio=1', 'type' => BookLinkTypeEnum::BUY->value],
            ['link' => 'https://www.youtube.com/watch?v=2YNkY3-Hc3M&list=RD2YNkY3-Hc3M&start_radio=2', 'type' => BookLinkTypeEnum::BUY->value],
        ], JsonHelper::encode($links));

        Assert::assertCount(2, $links);
    }

    protected function grabListener(): BookLinkListener
    {
        return parent::getContainer()->get(BookLinkListener::class);
    }
}
