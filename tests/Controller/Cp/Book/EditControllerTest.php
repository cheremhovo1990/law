<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp\Book;

use App\DataFixtures\BookFixture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EditControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();
        $client->request('GET', sprintf('/cp/book/%s/edit', BookFixture::ID_FIRST));

        $this->assertResponseIsSuccessful();
    }
}
