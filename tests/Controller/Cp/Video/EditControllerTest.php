<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp\Video;

use App\DataFixtures\VideoFixture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EditControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();
        $client->request('GET', sprintf('/cp/video/%s/edit', VideoFixture::ID_FIRST));

        $this->assertResponseIsSuccessful();
    }
}
