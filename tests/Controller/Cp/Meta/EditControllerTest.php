<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp\Meta;

use App\Entity\MetaKeyEnum;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EditControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();
        $client->request('GET', sprintf('/cp/meta/%s/edit', MetaKeyEnum::MAIN->value));

        $this->assertResponseIsSuccessful();
    }
}
