<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp\Sin;

use App\DataFixtures\PreceptFixtures;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EditControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();
        $client->request('GET', sprintf('/cp/precept/%s/sin/%s/edit', PreceptFixtures::ID_SIX, PreceptFixtures::SIN_ID_FIRST));

        $this->assertResponseIsSuccessful();
    }
}
