<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp\Schedule;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MonthControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();

        $client->request('GET', '/cp/schedule/month');

        $this->assertResponseIsSuccessful();
    }
}
