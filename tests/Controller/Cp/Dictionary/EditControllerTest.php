<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp\Dictionary;

use App\DataFixtures\DictionaryFixture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EditControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();
        $client->request('GET', sprintf('/cp/dictionary/%s/edit', DictionaryFixture::ID_FIRST));

        $this->assertResponseIsSuccessful();
    }
}
