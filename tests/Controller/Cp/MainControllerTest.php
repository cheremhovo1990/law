<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MainControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();

        $client->request('GET', '/cp');

        $this->assertResponseIsSuccessful();
    }
}
