<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp\DescriptionSin;

use App\DataFixtures\PreceptFixtures;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EditControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();
        $client->request('GET', sprintf('/cp/description/sin/%s/edit', PreceptFixtures::DESCRIPTION_SIN_ID_FIRST));

        $this->assertResponseIsSuccessful();
    }
}
