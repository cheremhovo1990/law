<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp\DescriptionSin;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CreateControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();

        $client->request('GET', '/cp/precept/description/sin/create');

        $this->assertResponseIsSuccessful();
    }
}
