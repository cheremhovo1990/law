<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp\Precept;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ListControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();

        $client->request('GET', '/cp/precepts');

        $this->assertResponseIsSuccessful();
    }
}
