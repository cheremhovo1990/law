<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FeedbackControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();

        $client->request('GET', '/cp/feedbacks');

        $this->assertResponseIsSuccessful();
    }
}
