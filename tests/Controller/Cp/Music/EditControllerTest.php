<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp\Music;

use App\DataFixtures\MusicFixture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EditControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();
        $client->request('GET', sprintf('/cp/music/%s/edit', MusicFixture::ID_FIRST));

        $this->assertResponseIsSuccessful();
    }
}
