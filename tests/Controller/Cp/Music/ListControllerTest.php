<?php

declare(strict_types=1);

namespace App\Tests\Controller\Cp\Music;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ListControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();

        $client->request('GET', '/cp/music');

        $this->assertResponseIsSuccessful();
    }
}
