<?php

declare(strict_types=1);

namespace App\Tests\Controller\Book;

use App\DataFixtures\BookFixture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();

        $client->request('GET', sprintf('/book/%s', BookFixture::ID_FIRST));

        $this->assertResponseIsSuccessful();
    }
}
