<?php

declare(strict_types=1);

namespace App\Tests\Controller\Book;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ListControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();

        $client->request('GET', '/books');

        $this->assertResponseIsSuccessful();
    }
}
