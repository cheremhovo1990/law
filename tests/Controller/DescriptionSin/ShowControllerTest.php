<?php

declare(strict_types=1);

namespace App\Tests\Controller\DescriptionSin;

use App\DataFixtures\PreceptFixtures;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShowControllerTest extends WebTestCase
{
    public function testOpenPage(): void
    {
        $client = static::createClient();

        $client->request('GET', sprintf('/precept/%s/description/sin/%s', PreceptFixtures::ID_SIX, PreceptFixtures::DESCRIPTION_SIN_ID_FIRST));

        $this->assertResponseIsSuccessful();
    }
}
