module.exports = {
  'env': {
    'browser': true,
    'es2021': true,
    'node': true
  },
  'extends': ['eslint:recommended', 'plugin:vue/essential', 'plugin:storybook/recommended'],
  'parserOptions': {
    'ecmaVersion': 'latest',
    'sourceType': 'module'
  },
  'plugins': ['vue'],
  'globals': {
    'require': true,
    'process': true,
    'grecaptcha': true
  },
  'rules': {
    'quotes': ['error', 'single'],
    'vue/multi-word-component-names': 0
  }
};
